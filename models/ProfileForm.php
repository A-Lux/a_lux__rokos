<?php

namespace app\models;

use yii\base\Model;

class ProfileForm extends Model
{
    public $email;
    public $username;
    public $password;
    public $company_name;
    public $bin;
    public $country;
    public $address;
    public $activity_type;
    public $name;
    public $position;
    public $phone;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password'          => 'Пароль',
            'email'             => 'Email',
            'company_name'      => 'Наименование компании',
            'bin'               => 'БИН',
            'country'           => 'Страна',
            'address'           => 'Адрес',
            'activity_type'     => 'Вид деятельности',
            'name'              => 'ФИО Представителя (полное)',
            'position'          => 'Должность',
            'phone'             => 'Телефон',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bin'], 'required'],
            [['email'], 'email'],
            ['password', 'string', 'min' => 6],

            [
                [
                    'username', 'company_name', 'country', 'address',
                    'activity_type', 'name', 'position', 'phone'
                ],
                'string', 'max' => 255
            ],

            ['bin', 'integer'],

            [['email'], 'unique', 'targetClass' => User::className(), 'message' => 'Этот email-адрес уже используется'],
            ];
    }

    /**
     * Update user
     * @return mixed
     */
    public function update()
    {
        $user                     = User::findOne(['id' => \Yii::$app->user->identity->id]);
        if(!$user){
            $user   = new User();
        }

        if ( $user !== null && $this->validate()) {
//            $user->email            = $this->email;
//            $user->username         = $this->email;
            $user->company_name     = $this->company_name;
            $user->bin              = $this->bin;
            $user->country          = $this->country;
            $user->address          = $this->address;
            $user->activity_type    = $this->activity_type;
            $user->name             = $this->name;
            $user->position         = $this->position;
            $user->phone            = $this->phone;

            if($user->save(false)){
                return $user;
            }

            return true;
        }

        return false;
    }
}