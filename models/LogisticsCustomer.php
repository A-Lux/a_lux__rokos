<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_customer".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int $statusActing
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 */
class LogisticsCustomer extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/logistics-customer/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'statusActing'], 'required'],
            [['statusActing'], 'integer'],
            [['name', 'title', 'title_en', 'title_kz', 'subtitle', 'subtitle_en', 'subtitle_kz',
                'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'image'         => 'Изображение',
            'statusActing'  => 'Статус фона',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок En',
            'title_kz'      => 'Заголовок Kz',
            'subtitle'      => 'Подзаголовок',
            'subtitle_en'   => 'Подзаголовок En',
            'subtitle_kz'   => 'Подзаголовок Kz',
        ];
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(LogisticsCustomer::find()->all(),'id','name');
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/logistics-customer/' . $this->image : '';
    }

    public function getTitle()
    {
        $name = "title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getSubtitle()
    {
        $name = "subtitle".Yii::$app->session["lang"];

        return $this->$name;
    }
}
