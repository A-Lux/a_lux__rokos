<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_customer_base".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class LogisticsCustomerBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_customer_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['customer_id'], 'integer'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'customer_id'   => 'Клиенты',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок En',
            'title_kz'      => 'Заголовок Kz',
            'content'       => 'Описание',
            'content_en'    => 'Описание En',
            'content_kz'    => 'Описание Kz',
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(LogisticsCustomer::className(), ['id' => 'customer_id']);
    }

    public function getTitle()
    {
        $name = "title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getContent()
    {
        $name = "content".Yii::$app->session["lang"];

        return $this->$name;
    }
}
