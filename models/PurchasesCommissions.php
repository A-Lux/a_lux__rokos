<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchases_commissions".
 *
 * @property int $id
 * @property int $purchase_id
 * @property int $user_id
 */
class PurchasesCommissions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchases_commissions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_id' => 'Purchase ID',
            'user_id' => 'User ID',
        ];
    }
}
