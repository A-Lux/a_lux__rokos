<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $image
 * @property string $copyright
 * @property string $copyright_en
 * @property string $copyright_kz
 */
class Logo extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/logo/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright'], 'required'],
            [['image', 'copyright', 'copyright_en', 'copyright_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'image'         => 'Логотип',
            'copyright'     => 'Копирайт',
            'copyright_en'  => 'Копирайт на английском',
            'copyright_kz'  => 'Копирайт на казахском',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/logo/' . $this->image : '/no-image.png';
    }

    public function getCopyright()
    {
        $copyright = "copyright".Yii::$app->session["lang"];
        return $this->$copyright;
    }
}
