<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_images".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $image
 */
class LogisticsImages extends \yii\db\ActiveRecord
{
    public $path = "uploads/images/logistics-images/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'customer_id'       => 'Клиенты',
            'image'             => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/logistics-images/' . $this->image : '/no-image.png';
    }

    public function getCustomer()
    {
        return $this->hasOne(LogisticsCustomer::className(), ['id' => 'customer_id']);
    }
}
