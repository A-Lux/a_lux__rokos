<?php
namespace app\models;

use app\models\User;
use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $email;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],

            ['email', 'exist',
                'targetClass' => '\app\models\User',
                'message' => 'Пользователь с такой эл.почтой нет'
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @return
     */
    public function resetPassword()
    {
        if (!$this->validate()) {
            return null;
        }

        $user    = User::findByEmail($this->email);
        if($user){
           $user->generatePasswordResetToken();

           if($user->save(false) && $this->sendLinkResetPassword($user)){
               return 1;
           }
        }

        return null;
    }

    protected function sendLinkResetPassword($user)
    {
        $host = \Yii::$app->request->hostInfo;

        $link   = $host . '/reset-password/update-password?token=' . $user->password_reset_token;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ROKOS'])
            ->setTo($user->email)
            ->setSubject('Восстановление доступа')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на ваш e-mail был полуен запрос на восстановление пароля.</p> </br>
                                 </br>
                                 <p>Пройдите пожалуйста по ссылке:</p> 
                                 <p><a href='$link'>$link</a></p>
                                 </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«ROKOS»</a></p>");

        return $emailSend->send();

    }

}
