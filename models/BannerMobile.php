<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_mobile".
 *
 * @property int $id
 * @property string $text
 * @property string $image
 * @property int $sort
 * @property string $text_en
 * @property string $text_kz
 */
class BannerMobile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/banner-mobile/';

    public static function tableName()
    {
        return 'banner_mobile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['text', 'text_en', 'text_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg, svg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'text'              => 'Текст',
            'image'             => 'Картинка',
            'sort'              => 'Sort',
            'text_en'           => 'Текст (EN)',
            'text_kz'           => 'Текст (KZ)',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banner-mobile/' . $this->image : '/no-image.png';
    }


    public function getText()
    {
        $name = "text".Yii::$app->session["lang"];
        return $this->$name;
    }
}
