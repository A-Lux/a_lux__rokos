<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_header".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class LogisticsHeader extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_header';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок En',
            'title_kz'      => 'Заголовок Kz',
            'content'       => 'Описание',
            'content_en'    => 'Описание En',
            'content_kz'    => 'Описание Kz',
        ];
    }

    public function getTitle()
    {
        $name = "title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getContent()
    {
        $name = "content".Yii::$app->session["lang"];

        return $this->$name;
    }
}
