<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property integer $role
 * @property string $username
 * @property string $phone
 * @property string $company_name
 * @property string $name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_GUEST = 0;
    const ROLE_CONTACT = 2;
    const ROLE_MPK = 3;
    const ROLE_LOGISTICS = 4;
    const ROLE_NEWS = 5;
    const ROLE_PORTFOLIO = 6;
    const ROLE_CAREER = 7;
    const ROLE_MAIN = 8;
    const ROLE_OPERATOR = 10;
    const ROLE_MANAGER = 12;
    const ROLE_ADMIN = 16; // super admin
    const ROLE_DEVELOPER = 32;
    const ROLE_COMMISSION = 64;
    CONST ROLE_DEFAULT = 128; // prostroi user
    const ROLE_ADMIN_FAKE = 256; //ne super admin
    public $password;
    public $password_verify;



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return array
     */
    public static function roles()
    {
        return [
            self::ROLE_GUEST => 'guest',
            self::ROLE_CONTACT => 'contact',
            self::ROLE_MPK => 'mpk',
            self::ROLE_LOGISTICS => 'logistics',
            self::ROLE_NEWS => 'news',
            self::ROLE_PORTFOLIO => 'portfolio',
            self::ROLE_CAREER => 'career',
            self::ROLE_MAIN => 'main',
            self::ROLE_OPERATOR => 'operator',
            self::ROLE_MANAGER => 'manager',
            self::ROLE_ADMIN => 'admin',
            self::ROLE_DEVELOPER => 'developer',
            self::ROLE_COMMISSION => 'commission',
            self::ROLE_DEFAULT => 'user',
            self::ROLE_ADMIN_FAKE => 'admin new',
        ];
    }

    /**
     * @return array
     */
    public static function roleDescription()
    {
        return [
            self::ROLE_GUEST => 'Гость',
            self::ROLE_CONTACT => 'Контакты',
            self::ROLE_MPK => 'МПК',
            self::ROLE_LOGISTICS => 'Логистика',
            self::ROLE_NEWS => 'Новости',
            self::ROLE_PORTFOLIO => 'Портфель',
            self::ROLE_CAREER => 'Карьера',
            self::ROLE_MAIN => 'Главная страница',
            self::ROLE_OPERATOR => 'Оператор',
            self::ROLE_MANAGER => 'Менеджер',
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_DEVELOPER => 'Разработчик',
            self::ROLE_COMMISSION => 'Коммиссия',
            self::ROLE_DEFAULT => 'Пользователь',
            self::ROLE_ADMIN_FAKE => 'Fake admin',
        ];
    }

    public static function getCommissionsList()
    {
        return ArrayHelper::map(self::find()->where(['role' => self::ROLE_COMMISSION])->asArray()->all(),
            'id', 'username');
        return self::find()->where(['role' => self::ROLE_COMMISSION])->all();
    }

    public static function getFakeAdminsList()
    {
        return ArrayHelper::map(self::find()->where(['role' => self::ROLE_ADMIN_FAKE])->asArray()->all(),
            'id', 'username');
        return self::find()->where(['role' => self::ROLE_ADMIN_FAKE])->all();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'string', 'max' => 255],
            [['email', 'username'], 'required'],
            [['email'], 'email'],
            [['role'], 'integer'],
            [['email', 'password'], 'required'],
            ['password', 'string', 'min' => 6],
            [['email'], 'unique', 'targetClass' => User::className(), 'message' => 'Этот email-адрес уже используется'],

//            [['company_name', 'country',
//                'address', 'activity_type', 'name', 'position', 'phone'], 'string'],
//
//            [['bin'], 'integer'],

//            [['password_verify'], 'required'],
//            [['password','password_verify'], 'string', 'min' => 6],
//            [['password_verify'], 'compare', 'compareAttribute' => 'password'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_verify' => 'Подтверждение пароля',
            'role' => 'Роль',
            'phone' => 'Телефон',
            'company_name' => 'Компания',
            'name' => 'Имя',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//            if ($insert) {
//                if (isset($this->password)) {
//                    $this->setPassword($this->password);
//                } elseif (empty($this->password_hash)) {
//                    $this->setPassword(\Yii::$app->security->generateRandomString());
//                }
//                $this->generateAuthKey();
//            }
//            return true;
//        }
//
//        return false;
//    }


    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->generateAuthKey();
        return $user->save(false) ? $user : null;
    }


    public function getRandomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $password .= $alphabet[$n];
        }
        return $password;
    }

    public static function getListCompanyName()
    {
        return ArrayHelper::map(static::find()->all(),'id','company_name');
    }


}
