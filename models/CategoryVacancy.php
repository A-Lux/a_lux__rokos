<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category_vacancy".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class CategoryVacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_vacancy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['metaDesc', 'metaDesc_en', 'metaDesc_kz', 'metaKey', 'metaKey_en', 'metaKey_kz'], 'string'],
            [['name', 'name_en', 'name_kz', 'metaName', 'metaName_en', 'metaName_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Наименование',
            'name_en'       => 'Наименование En',
            'name_kz'       => 'Наименование Kz',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия En',
            'metaName_kz'   => 'Мета Названия Kz',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание En',
            'metaDesc_kz'   => 'Мета Описание Kz',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова En',
            'metaKey_kz'    => 'Ключевые слова Kz',
        ];
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getMetaName()
    {
        $name = "metaName".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getMetaDesc()
    {
        $name = "metaDesc".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getMetaKey()
    {
        $name = "metaKey".Yii::$app->session["lang"];
        return $this->$name;
    }
}
