<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class PurchaseRequestForm extends Model
{
    public $purchase_id;
    public $user_id;
    public $name_product;
    public $unit;
    public $quantity;
    public $currency;
    public $price;
    public $vat;
    public $delivery_days;
    public $delivery_condition;
    public $payment_condition;
    public $description;
    public $delivery_time;
    public $procurement_method;
    public $purchase_stage;

    public $files;


    public $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'purchase_id', 'delivery_days',
                'purchase_stage', 'procurement_method'], 'integer'],
            [['quantity'], 'number'],
            [['description', 'files'], 'string'],
            [['unit', 'currency', 'price', 'delivery_condition',
                'payment_condition', 'delivery_time', 'name_product', 'vat'],
                'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'user_id'            => 'Полоьзователь',
            'purchase_id'        => 'Закупка',
            'name_product'       => 'Наименование товара',
            'vat'                => 'НДС',
            'unit'               => 'Ед. измерения',
            'quantity'           => 'Количество',
            'currency'           => 'Валюта',
            'price'              => 'Стоимость с НДС',
            'delivery_days'      => 'День поставки',
            'delivery_condition' => 'Условия поставки',
            'payment_condition'  => 'Условия оплаты',
            'description'        => 'Описание',
            'delivery_time'      => 'Срок поставки',
            'status_sent'        => 'Отправлена',
            'status_approved'    => 'Одобрена',
            'procurement_method' => 'Метод закупки',
            'purchase_stage'     => 'Этап закупки',
            'files'              => 'Файлы',
        ];
    }

    public function sentPurchase()
    {
        if ($this->validate()) {
            // if ($request = PurchaseRequests::findOne(
            //     [
            //         'user_id'     => Yii::$app->user->identity->id,
            //         'purchase_id' => $this->purchase_id
            //     ]
            // )) {

            // }
            // else {
                $request = new PurchaseRequests();
            // }

            $request->user_id            = Yii::$app->user->identity->id;
            $request->purchase_id        = $this->purchase_id;
            $request->procurement_method = $this->procurement_method;
            $request->purchase_stage     = $this->purchase_stage;
            $request->name_product       = $this->name_product;
            $request->unit               = $this->unit;
            $request->quantity           = $this->quantity;
            $request->currency           = $this->currency;
            $request->price              = $this->price;
            $request->vat                = $this->vat;
            $request->delivery_condition = $this->delivery_condition;
            $request->description        = $this->description;
            $request->payment_condition  = $this->payment_condition;
            $request->delivery_time      = $this->delivery_time;
            $request->status_sent        = 1;
            $request->created_at         = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');


            if ($request->save(false)) {
                $uploadFiles = $this->requestFiles($request);

                return $request;
            }

            return false;

        }

        return false;
    }

    public function savePurchase()
    {
        if ($this->validate()) {
            $request = PurchaseRequests::findOne(
                [
                    'user_id'     => Yii::$app->user->identity->id,
                    'purchase_id' => $this->purchase_id,
                ]
            );

            if (is_null($request)) {
                $request              = new PurchaseRequests();
                $request->purchase_id = $this->purchase_id;
                $request->user_id     = Yii::$app->user->identity->id ? Yii::$app->user->identity->id : '';
            }

            $request->procurement_method = $this->procurement_method;
            $request->purchase_stage     = $this->purchase_stage;
            $request->name_product       = $this->name_product;
            $request->unit               = $this->unit;
            $request->quantity           = $this->quantity;
            $request->currency           = $this->currency;
            $request->price              = $this->price;
            $request->vat                = $this->vat;
            $request->description        = $this->description;
            $request->delivery_condition = $this->delivery_condition;
            $request->payment_condition  = $this->payment_condition;
            $request->delivery_time      = $this->delivery_time;


            if ($request->save(false)) {
                $uploadFiles = $this->requestFiles($request);

                return $request;
            }

            return false;

        }

        return false;
    }

    /**
     * Finds user by [[id]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }

    protected function requestFiles($purchaseRequest)
    {
        $requestFile = PurchaseRequestFiles::find()
                                           ->where(
                                               [
                                                   'user_id'             => Yii::$app->user->identity->id,
                                                   'purchase_request_id' => $purchaseRequest->id,
                                               ]
                                           )
                                           ->one()
        ;

        if (is_null($requestFile)) {
            $requestFile                      = new PurchaseRequestFiles();
            $requestFile->user_id             = Yii::$app->user->identity->id;
            $requestFile->purchase_request_id = $purchaseRequest->id;
        }

        $files    = UploadedFile::getInstancesByName('files');
        $oldFiles = '';

        if (empty($files)) {
            $requestFile->content = $oldFiles;
        } else {
            $path      = $requestFile->pathFile();
            $jsonFiles = [];
            $time      = time();
            foreach ($files as $key => $file) {
                $hash = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[] = [
                    'name'      => $file->baseName,
                    'file'      => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles = json_decode($oldFiles, true);

            if (!empty($oldFiles)) {
                foreach ($oldFiles as $old) {
                    if (!empty($old['file'])) {
                        if (file_exists($path . $old['file'])) {
                            unlink($path . $old['file']);
                        }
                    }
                }
            }
            $requestFile->content = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }

        if ($requestFile->save()) {
            return $requestFile;
        }

        return 'error';
    }

}
