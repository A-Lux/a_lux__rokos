<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $city_id
 * @property string $topic
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property string $content
 * @property int $isRead
 * @property int $status
 * @property string $reply
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $reCaptcha;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'email', 'content', 'isRead', 'topic', 'city_id'], 'required'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
                'secret' => '6LfNkcgUAAAAAOh76kzxhlPHG6aTlI4XmTOVsCk-'],
            [['content', 'reply'], 'string'],
            [['isRead', 'city_id', 'status'], 'integer'],
            [['fio', 'telephone', 'topic'], 'string', 'max' => 255],
            [['email'],'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'city_id'       => 'Город',
            'topic'         => 'Тема обращения',
            'fio'           => 'Фио',
            'telephone'     => 'Телефон',
            'email'         => 'E-mail',
            'content'       => 'Сообщение',
            'isRead'        => 'Прочитано',
            'created'       => 'Дата создание',
            'status'        => 'Статус заявки',
            'reply'         => 'Комментарий менеджера',
        ];
    }

    public function saveRequest($name,$phone, $email, $content)
    {
        $this->fio = $name;
        $this->telephone = $phone;
        $this->email = $email;
        $this->content = $content;
        $this->isRead = 0;
        return $this->save(false);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
