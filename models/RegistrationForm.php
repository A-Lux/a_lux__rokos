<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Registration form
 */
class RegistrationForm extends Model
{
    public $email;
    public $username;
    public $password;
    public $password_repeat;
    public $company_name;
    public $bin;
    public $country;
    public $address;
    public $activity_type;
    public $name;
    public $position;
    public $phone;


    const ROLE = User::ROLE_DEFAULT;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'company_name', 'bin', 'country',
                'address', 'activity_type', 'name', 'position', 'phone'], 'required'],
            [['email'], 'email'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
            ['password', 'string', 'min' => 6],

            [
                [
                    'username', 'company_name', 'country', 'address',
                    'activity_type', 'name', 'position', 'phone'
                ],
                'string', 'max' => 255
            ],

            ['bin', 'integer'],

            [['email'], 'unique', 'targetClass' => User::className(), 'message' => 'Этот email-адрес уже используется'],
            [['bin'], 'unique', 'targetClass' => User::className(), 'message' => 'Этот номер телефона уже используется'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'email' => 'Email',
            'company_name' => 'Наименование компании',
            'bin' => 'БИН',
            'country' => 'Страна',
            'address' => 'Адрес',
            'activity_type' => 'Вид деятельности',
            'name' => 'ФИО Представителя (полное)',
            'position' => 'Должность',
            'phone' => 'Телефон',
        ];
    }

    public function signUp()
    {
        if ($this->validate()) {
            $user                   =  new User();
            $user->email            = $this->email;
            $user->username         = $this->email;
            $user->company_name     = $this->company_name;
            $user->bin              = $this->bin;
            $user->country          = $this->country;
            $user->address          = $this->address;
            $user->activity_type    = $this->activity_type;
            $user->name             = $this->name;
            $user->position         = $this->position;
            $user->phone            = $this->phone;
            $user->role             = User::ROLE_DEFAULT;

            $user->setPassword($this->password);
            $user->generateAuthKey();

            if($user->save(false)){
                return $user;
            }

            return true;

        }
        return  false;
    }


}
