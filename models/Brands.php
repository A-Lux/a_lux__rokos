<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 */
class Brands extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/brands/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'image'     => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/brands/' . $this->image : '';
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }
}
