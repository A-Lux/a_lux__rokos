<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "career_feedback".
 *
 * @property int $id
 * @property string $contact_person
 * @property int $city_id
 * @property int $position_id
 * @property string $email
 * @property string $telephone
 * @property string $file
 * @property string $comment
 * @property string $href
 * @property string $created_at
 * @property string $reply
 * @property int $status
 */
class CareerFeedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/files/career-feedback/';

    public static function tableName()
    {
        return 'career_feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_person', 'city_id', 'position_id', 'email', 'telephone', 'file'], 'required'],
            [['city_id', 'position_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['reply', 'comment'], 'string'],
            [['contact_person', 'email', 'telephone', 'href'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'doc,pdf,csv,txt,docx,pptx'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'contact_person'    => 'Контактное лицо',
            'city_id'           => 'Город',
            'position_id'       => 'Должность',
            'email'             => 'E-mail',
            'telephone'         => 'Телефон',
            'file'              => 'Файл',
            'created_at'        => 'Дата создание',
            'comment'           => 'Комментарий',
            'href'              => 'Ссылка на резюме',
            'status'            => 'Статус заявки',
            'reply'             => 'Комментарий менеджера',
        ];
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/files/career-feedback/' . $this->file : '';
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }


    public function getPosition()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'position_id']);
    }
}
