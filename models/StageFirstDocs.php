<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "stage_first_docs".
 *
 * @property int $id
 * @property int $stage_id
 * @property int $type
 * @property string $title
 * @property int $is_required
 */
class StageFirstDocs extends \yii\db\ActiveRecord
{
    const FILE = 0;
    const TEXT_AREA = 1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stage_first_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stage_id', 'type', 'title'], 'required'],
            [['stage_id', 'type', 'is_required'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stage_id' => 'Stage ID',
            'type' => 'Type',
            'title' => 'Title',
            'is_required' => 'Is Required',
        ];
    }

    public static function getTypesList()
    {
        return [self::FILE => 'Загрузка файла', self::TEXT_AREA => 'Тестовое поле'];
    }
}
