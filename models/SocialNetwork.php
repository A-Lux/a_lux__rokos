<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "social_network".
 *
 * @property int $id
 * @property string $instagram
 * @property string $youtube
 */
class SocialNetwork extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_network';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instagram', 'youtube'], 'required'],
            [['instagram', 'youtube'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'instagram' => 'Instagram',
            'youtube'   => 'Youtube',
        ];
    }
}
