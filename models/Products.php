<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $catalog_id
 * @property int $code
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $fullname
 * @property string $fullname_en
 * @property string $fullname_kz
 * @property string $weight
 * @property string $manufacturer
 * @property string $manufacturer_en
 * @property string $manufacturer_kz
 * @property int $price
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $image
 * @property string $created_at
 */
class Products extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/products/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalog_id', 'name', 'fullname', 'weight', 'content'], 'required'],
            [['catalog_id', 'code', 'price'], 'integer'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'name_en', 'name_kz', 'fullname', 'fullname_en', 'fullname_kz', 'weight', 'manufacturer', 'manufacturer_en', 'manufacturer_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'catalog_id'            => 'Родитель',
            'code'                  => 'Код',
            'name'                  => 'Наименование',
            'name_en'               => 'Наименование En',
            'name_kz'               => 'Наименование Kz',
            'fullname'              => 'Наименование полное',
            'fullname_en'           => 'Наименование полное En',
            'fullname_kz'           => 'Наименование полное Kz',
            'weight'                => 'Вес',
            'manufacturer'          => 'Производитель',
            'manufacturer_en'       => 'Производитель En',
            'manufacturer_kz'       => 'Производитель Kz',
            'price'                 => 'Цена',
            'image'                 => 'Изображение',
            'content'               => 'Описание',
            'content_en'            => 'Описание En',
            'content_kz'            => 'Описание Kz',
            'created_at'            => 'Created At',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/products/' . $this->image : '/no-image.png';
    }

    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }

    public function getCatalogName()
    {
        return (isset($this->catalog)) ? $this->catalog->getName() : 'не задано';
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getFullname()
    {
        $name = "fullname".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getManufacturer()
    {
        $name = "manufacturer".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getContent()
    {
        $name = "content".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function lastCatalog($catalog)
    {
        if(!empty($catalog->parent)) {
            return $this->lastCatalog($catalog->parent);
        } else {
            return $catalog->name;
        }
    }
}
