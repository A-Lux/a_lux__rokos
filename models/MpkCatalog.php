<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpk_catalog".
 *
 * @property int $id
 * @property string $file
 * @property string $link
 * @property int $status
 */
class MpkCatalog extends \yii\db\ActiveRecord
{
    public $pathFile = 'uploads/files/mpk-catalog/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mpk_catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'status'], 'required'],
            [['status'], 'integer'],
            [['file', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'file'      => 'Файл',
            'link'      => 'Ссылка',
            'status'    => 'Статус',
        ];
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/files/mpk-catalog/' . $this->file : '';
    }
}
