<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $link
 * @property int $status
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/partner/';

    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['link','image', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'image'     => 'Партнеры',
            'link'      => 'Ссылка',
            'status'    => 'Статус',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/partner/' . $this->image : '';
    }
}
