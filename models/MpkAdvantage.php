<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpk_advantage".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $image
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 */
class MpkAdvantage extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/mpk-advantage/';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mpk_advantage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text', 'text_en', 'text_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'title_en'  => 'Заголовок En',
            'title_kz'  => 'Заголовок Kz',
            'image'     => 'Изображение',
            'text'      => 'Описание',
            'text_en'   => 'Описание En',
            'text_kz'   => 'Описание Kz',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/mpk-advantage/' . $this->image : '/no-image.png';
    }

    public function getTitle()
    {
        $name = "title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getText()
    {
        $name = "text".Yii::$app->session["lang"];

        return $this->$name;
    }
}
