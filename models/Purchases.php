<?php

namespace app\models;


use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use app\models\PurchaseRequests;
use PHPExcel;

/**
 * This is the model class for table "purchases".
 *
 * @property int $id
 * @property int $customer
 * @property string $name
 * @property int $procurement_method
 * @property int $procurement_type
 * @property int $purchase_stage
 * @property string $purchase stage
 * @property int $currency
 * @property int $unit
 * @property int $quantity
 * @property string $delivery_time
 * @property string $delivery_condition
 * @property string $payment_condition
 * @property string $description
 * @property string $docs
 * @property string $tech_task
 * @property string $protocol
 * @property string $protocol2
 * @property int $is_active
 * @property int $stage_first
 * @property int $stage_second
 */
class Purchases extends \yii\db\ActiveRecord
{
    const IS_DISABLED       = 0;
    const IS_ACTIVE         = 1;

    const STAGE_FIRST_DISABLED          = 0;
    const STAGE_FIRST_ACTIVE            = 1;

    const STAGE_SECOND_DISABLED          = 0;
    const STAGE_SECOND_ACTIVE            = 1;

    const PURCHASE_STAGE_FIRST_ACCEPTANCE           = 1;
    const PURCHASE_STAGE_FIRST_CONSIDERATION        = 2;
    const PURCHASE_STAGE_FIRST_COMPLETION           = 3;

    const PURCHASE_STAGE_SECOND_ACCEPTANCE          = 4;
    const PURCHASE_STAGE_SECOND_CONSIDERATION       = 5;
    const PURCHASE_STAGE_SECOND_COMPLETION          = 6;

    const PURCHASE_STAGE_PROLONGATION               = 7;
    const PURCHASE_STAGE_ANNULMENT                  = 8;


    /**
     * {@inheritdoc}
     */
    public $admin;
    public $commission;

    public $docsFile;
    public $techTaskFile;
    public $file;

    public static function tableName()
    {
        return 'purchases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer', 'procurement_method', 'procurement_type', 'currency', 'unit', 'quantity', 'is_active',
                'delivery_time', 'description', 'name', 'purchase_stage', 'delivery_condition', 'payment_condition',
                'admin', 'commission'
            ], 'required'],
            [['customer', 'procurement_method', 'purchase_stage', 'procurement_type',
                'currency', 'unit', 'quantity', 'is_active',
                'admin', 'commission', 'stage_first', 'stage_second'], 'integer'],
            [['description'], 'string'],
            [['name', 'delivery_condition', 'payment_condition', 'docs', 'tech_task', 'protocol', 'protocol2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                        => 'ID',
            'customer'                  => 'Заказчик',
            'name'                      => 'Наименование закупки',
            'procurement_method'        => 'Метод закупки',
            'procurement_type'          => 'Вид закупки',
            'purchase_stage'            => 'Этап закупки',
            'currency'                  => 'Валюта',
            'unit'                      => 'Единица измерения',
            'quantity'                  => 'Количество',
            'delivery_time'             => 'Срок поставки',
            'delivery_condition'        => 'Условия поставки',
            'payment_condition'         => 'Условия оплаты',
            'description'               => 'Описание',
            'docs'                      => 'Документация',
            'tech_task'                 => 'Техническое задание',
            'protocol'                  => 'Выписка',
            'protocol2'                 => 'Выписка2',
            'is_active'                 => 'Видно?',
            'admin'                     => 'Администратор',
            'commission'                => 'Комиссия',
            'stage_first'               => 'Этап №1',
            'stage_second'              => 'Этап №2',
        ];
    }

    public static function getListName()
    {
        return ArrayHelper::map(static::find()->all(),'id','name');
    }

    public function getStageFirst()
    {
        return $this->hasOne(StageFirst::className(), ['purchase_id' => 'id']);
    }

    public function getStageFirstDocs()
    {
        return $this->hasMany(StageFirstDocs::className(), ['stage_id' => 'id'])
                    ->viaTable(StageFirst::tableName(), ['purchase_id' => 'id']);
    }

    public function getStageSecond()
    {
        return $this->hasOne(StageSecond::className(), ['purchase_id' => 'id']);
    }

    public function getStageSecondDocs()
    {
        return $this->hasMany(StageSecondDocs::className(), ['stage_id' => 'id'])
                    ->viaTable(StageSecond::tableName(), ['purchase_id' => 'id']);
    }

    public function getAdminUser()
    {
        return $this->hasOne(PurchasesAdmins::className(), ['purchase_id' => 'id']);
    }

    public function getCommissionUser()
    {
        return $this->hasOne(PurchasesCommissions::className(), ['purchase_id' => 'id']);
    }

    public function getCustomerName()
    {
        foreach (PurchasingGuide::getCustomersDropDown() as $key => $type) {
            if ($this->customer === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getProcurementMethodName()
    {
        foreach (PurchasingGuide::getProcurementMethodsDropDown() as $key => $type) {
            if ($this->procurement_method === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getProcurementStageName()
    {
        foreach (PurchasingGuide::getProcurementStagesDropDown() as $key => $type) {
            if ($this->purchase_stage === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getCurrencyName()
    {
        foreach (PurchasingGuide::getCurrencyDropDown() as $key => $type) {
            if ($this->currency === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getUnitName()
    {
        foreach (PurchasingGuide::getUnitDropDown() as $key => $type) {
            if ($this->unit === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getProcurementTypeName()
    {
        foreach (PurchasingGuide::getProcurementTypesDropDown() as $key => $type) {
            if ($this->procurement_type === $key) {
                return $type;
            }
        }
        return null;
    }


    public function uploadFile()
    {
        $filename = Yii::$app->security->generateRandomString(20);
        $filename .= '.' . $this->file->getExtension();
        $folder = Yii::getAlias('uploads/files/' . $this->name);
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }
        if (!is_writable($folder)) {
            chmod($folder, 0777);
        }
        while (true) {
            if (file_exists($folder . DIRECTORY_SEPARATOR . $filename)) {
                $filename = Yii::$app->security->generateRandomString(20);
                $filename .= '.' . $this->file->getExtension();
            } else {
                $this->file->saveAs($folder . DIRECTORY_SEPARATOR . $filename);
                break;
            }
        }
        return $filename ? 'uploads/files/' . $this->name . '/' . $filename : ' ';
    }

    public function deleteFile($path)
    {
        if (is_file($path)) {
            FileHelper::unlink($path);
        }
    }

    /**
     * @return array
     */
    public static function active()
    {
        return [
            self::IS_ACTIVE     => 'active',
            self::IS_DISABLED   => 'disabled',
        ];
    }

    /**
     * @return array
     */
    public static function activeDescription()
    {
        return [
            self::IS_ACTIVE     => 'активный',
            self::IS_DISABLED   => 'закрытый ',
        ];
    }

    /**
     * @return array
     */
    public static function stageFirst()
    {
        return [
            self::STAGE_FIRST_ACTIVE     => 'active',
            self::STAGE_FIRST_DISABLED   => 'disabled',
        ];
    }

    /**
     * @return array
     */
    public static function stageFirstDescription()
    {
        return [
            self::STAGE_FIRST_ACTIVE     => 'активный',
            self::STAGE_FIRST_DISABLED   => 'закрытый ',
        ];
    }

    /**
     * @return array
     */
    public static function stageSecond()
    {
        return [
            self::STAGE_SECOND_ACTIVE     => 'active',
            self::STAGE_SECOND_DISABLED   => 'disabled',
        ];
    }

    /**
     * @return array
     */
    public static function stageSecondDescription()
    {
        return [
            self::STAGE_SECOND_ACTIVE     => 'активный',
            self::STAGE_SECOND_DISABLED   => 'закрытый ',
        ];
    }

    /**
     * @return array
     */
    public static function purchaseStage()
    {
        return [
            self::PURCHASE_STAGE_FIRST_ACCEPTANCE       => 'stage first acceptance',
            self::PURCHASE_STAGE_FIRST_CONSIDERATION    => 'stage first consideration',
            self::PURCHASE_STAGE_FIRST_COMPLETION       => 'stage first completion',
            self::PURCHASE_STAGE_SECOND_ACCEPTANCE      => 'stage second acceptance',
            self::PURCHASE_STAGE_SECOND_CONSIDERATION   => 'stage second consideration',
            self::PURCHASE_STAGE_SECOND_COMPLETION      => 'stage second completion',
            self::PURCHASE_STAGE_PROLONGATION           => 'stage prolongation',
            self::PURCHASE_STAGE_ANNULMENT              => 'stage annulment',
        ];
    }

    /**
     * @return array
     */
    public static function purchaseStageDescription()
    {
        return [
            self::PURCHASE_STAGE_FIRST_ACCEPTANCE       => 'Прием заявок 1 этап',
            self::PURCHASE_STAGE_FIRST_CONSIDERATION    => 'Рассмотрение заявок 1 этап',
            self::PURCHASE_STAGE_FIRST_COMPLETION       => 'Завершение 1 этапа',
            self::PURCHASE_STAGE_SECOND_ACCEPTANCE      => 'Прием заявок 2 этап',
            self::PURCHASE_STAGE_SECOND_CONSIDERATION   => 'Рассмотрение заявок 2 этап',
            self::PURCHASE_STAGE_SECOND_COMPLETION      => 'Завершение 2 этапа',
            self::PURCHASE_STAGE_ANNULMENT              => 'Аннулирование',
        ];
    }

    public function getDocs()
    {
        return ($this->docs) ? '/' . $this->docs : ' ';
    }

    public function getTechTask()
    {
        return ($this->tech_task) ? '/' . $this->tech_task : ' ';
    }

    public function getProtocol()
    {
        return ($this->protocol) ? '/' . $this->protocol : ' ';
    }





    public function pathFile()
    {
        return 'uploads/files/purchases/';
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/files/purchases/' . $this->file : '';
    }

    public static function getJsonToArray($request)
    {
        $documents  = Json::decode($request, true);

        if(empty($documents)){
            return null;
        }

        return $documents;
    }

    public function getFileLink($file)
    {
        return  '/uploads/files/purchases/' . $file;
    }

    public function getStagesPurchaseDate()
    {
        if($this->purchase_stage == 1 || $this->purchase_stage == 2 || $this->purchase_stage == 3){
            if(!empty($this->stageFirst->start_date) && !empty($this->stageFirst->end_date)){
                return $this->stageFirst->start_date . ' - ' . $this->stageFirst->end_date;
            }elseif(empty($this->stageFirst->start_date)){
                return $this->stageFirst->end_date;
            }elseif(empty($this->stageFirst->end_date)){
                return '- ' . $this->stageFirst->start_date;
            }else{
                return '';
            }
        }elseif($this->purchase_stage == 4 || $this->purchase_stage == 5 || $this->purchase_stage == 6){
            if(!empty($this->stageSecond->start_date) && !empty($this->stageSecond->end_date)){
                return $this->stageSecond->start_date . ' - ' . $this->stageSecond->end_date;
            }elseif(empty($this->stageSecond->start_date)){
                return $this->stageSecond->end_date;
            }elseif(empty($this->stageSecond->end_date)){
                return '- ' . $this->stageSecond->start_date;
            }else{
                return '';
            }
        }else{
            return  '';
        }
    }

    public function getPurchaseStageName()
    {
        return ArrayHelper::getValue(Purchases::purchaseStageDescription(), $this->purchase_stage);
    }

    public function getRequestPurchase()
    {
        if($this->purchase_stage == 1 || $this->purchase_stage == 4){
            return true;
        }

        return false;
    }

    public function getRequests()
    {
        return $this->hasMany(PurchaseRequests::className(), ['purchase_id' => 'id']);
    }

}

