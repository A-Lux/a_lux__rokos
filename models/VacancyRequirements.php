<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy_requirements".
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string $requirement
 * @property string $requirement_en
 * @property string $requirement_kz
 */
class VacancyRequirements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy_requirements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vacancy_id', 'requirement'], 'required'],
            [['vacancy_id'], 'integer'],
            [['requirement', 'requirement_en', 'requirement_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Vacancy ID',
            'requirement' => 'Requirement',
        ];
    }

    public function getRequirement()
    {
        $name = "requirement".Yii::$app->session["lang"];

        return $this->$name;
    }
}
