<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpk".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $mission_title
 * @property string $mission_title_en
 * @property string $mission_title_kz
 * @property string $mission_content
 * @property string $mission_content_en
 * @property string $mission_content_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class Mpk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mpk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'mission_title', 'mission_content', 'content'], 'required'],
            [['description', 'description_en', 'description_kz', 'mission_title', 'mission_title_en', 'mission_title_kz', 'mission_content', 'mission_content_en', 'mission_content_kz', 'content', 'content_en', 'content_kz'], 'string'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'title'                 => 'Заголовок',
            'title_en'              => 'Заголовок En',
            'title_kz'              => 'Заголовок Kz',
            'description'           => 'Описание',
            'description_en'        => 'Описание En',
            'description_kz'        => 'Описание Kz',
            'mission_title'         => 'Заголовок миссии',
            'mission_title_en'      => 'Заголовок миссии En',
            'mission_title_kz'      => 'Заголовок миссии Kz',
            'mission_content'       => 'Контент миссии',
            'mission_content_en'    => 'Контент миссии En',
            'mission_content_kz'    => 'Контент миссии Kz',
            'content'               => 'Контент',
            'content_en'            => 'Контент En',
            'content_kz'            => 'Контент Kz',
        ];
    }

    public function getTitle()
    {
        $title = "title".Yii::$app->session["lang"];

        return $this->$title;
    }

    public function getDescription()
    {
        $name = "description".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getMissionTitle()
    {
        $name = "mission_title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getMissionContent()
    {
        $name = "mission_content".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getContent()
    {
        $name = "content".Yii::$app->session["lang"];

        return $this->$name;
    }
}
