<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PurchaseRequests;

/**
 * PurchaseRequestsSearch represents the model behind the search form of `app\models\PurchaseRequests`.
 */
class PurchaseRequestsSearch extends PurchaseRequests
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'purchase_id', 'delivery_days', 'status_sent', 'status_approved', 'quantity',
                'procurement_method', 'purchase_stage', 'purchase_customer'],
                'integer'],
            [['description', 'unit', 'currency', 'price', 'delivery_condition',
                'payment_condition', 'delivery_time', 'name_product', 'vat', 'created_at'],
                'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchaseRequests::find()->orderBy('id DESC')->where(['status_sent' => self::STATUS_SENT_YES]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                => $this->id,
            'user_id'           => $this->user_id,
            'purchase_id'       => $this->purchase_id,
            'delivery_days'     => $this->delivery_days,
            'status_sent'       => $this->status_sent,
            'status_approved'   => $this->status_approved,
            'quantity'          => $this->quantity,
            'procurement_method'    => $this->procurement_method,
            'purchase_stage'        => $this->purchase_stage,
            'purchase_customer'     => $this->purchase_customer,
            'created_at'        => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'unit stage', $this->unit])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'delivery_condition', $this->delivery_condition])
            ->andFilterWhere(['like', 'payment_condition', $this->payment_condition])
            ->andFilterWhere(['like', 'delivery_time', $this->delivery_time])
            ->andFilterWhere(['like', 'name_product', $this->name_product])
            ->andFilterWhere(['like', 'vat', $this->vat]);

        return $dataProvider;
    }

}
