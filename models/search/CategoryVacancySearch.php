<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CategoryVacancy;

/**
 * CategoryVacancySearch represents the model behind the search form of `app\models\CategoryVacancy`.
 */
class CategoryVacancySearch extends CategoryVacancy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'name_en', 'name_kz', 'metaName', 'metaName_en', 'metaName_kz', 'metaDesc', 'metaDesc_en', 'metaDesc_kz', 'metaKey', 'metaKey_en', 'metaKey_kz'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryVacancy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_kz', $this->name_kz])
            ->andFilterWhere(['like', 'metaName', $this->metaName])
            ->andFilterWhere(['like', 'metaName_en', $this->metaName_en])
            ->andFilterWhere(['like', 'metaName_kz', $this->metaName_kz])
            ->andFilterWhere(['like', 'metaDesc', $this->metaDesc])
            ->andFilterWhere(['like', 'metaDesc_en', $this->metaDesc_en])
            ->andFilterWhere(['like', 'metaDesc_kz', $this->metaDesc_kz])
            ->andFilterWhere(['like', 'metaKey', $this->metaKey])
            ->andFilterWhere(['like', 'metaKey_en', $this->metaKey_en])
            ->andFilterWhere(['like', 'metaKey_kz', $this->metaKey_kz]);

        return $dataProvider;
    }
}
