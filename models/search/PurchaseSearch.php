<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Purchases;

/**
 * PurchaseSearch represents the model behind the search form of `app\models\Purchases`.
 */
class PurchaseSearch extends Purchases
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customer', 'purchase_stage', 'procurement_method', 'procurement_type', 'currency', 'unit', 'quantity', 'is_active'], 'integer'],
            [['name', 'delivery_time', 'delivery_condition', 'payment_condition', 'description', 'docs', 'tech_task', 'protocol'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Purchases::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id'                 => $this->id,
                'customer'           => $this->customer,
                'procurement_method' => $this->procurement_method,
                'procurement_type'   => $this->procurement_type,
                'currency'           => $this->currency,
                'unit'               => $this->unit,
                'quantity'           => $this->quantity,
                'delivery_time'      => $this->delivery_time,
                'is_active'          => $this->is_active,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'purchase stage', $this->purchase_stage])
              ->andFilterWhere(['like', 'delivery_condition', $this->delivery_condition])
              ->andFilterWhere(['like', 'payment_condition', $this->payment_condition])
              ->andFilterWhere(['like', 'description', $this->description])
              ->andFilterWhere(['like', 'docs', $this->docs])
              ->andFilterWhere(['like', 'tech_task', $this->tech_task])
              ->andFilterWhere(['like', 'protocol', $this->protocol])
              ->andFilterWhere(['like', 'protocol2', $this->protocol2])

        ;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchFront($params)
    {
        $query = Purchases::find()
                          ->where(['is_active' => 1])
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id'                 => $this->id,
                'customer'           => $this->customer,
                'procurement_method' => $this->procurement_method,
                'procurement_type'   => $this->procurement_type,
                'purchase_stage'     => $this->purchase_stage,
                'currency'           => $this->currency,
                'unit'               => $this->unit,
                'quantity'           => $this->quantity,
                'delivery_time'      => $this->delivery_time,
                'is_active'          => $this->is_active,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'delivery_condition', $this->delivery_condition])
              ->andFilterWhere(['like', 'payment_condition', $this->payment_condition])
              ->andFilterWhere(['like', 'description', $this->description])
              ->andFilterWhere(['like', 'docs', $this->docs])
              ->andFilterWhere(['like', 'tech_task', $this->tech_task])
              ->andFilterWhere(['like', 'protocol', $this->protocol])
              ->andFilterWhere(['like', 'protocol2', $this->protocol2])
                ->orderBy(['id'=>SORT_DESC])
        ;

        return $dataProvider;
    }
}
