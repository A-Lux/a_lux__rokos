<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VacancyResponsibilities;

/**
 * VacancyResponsibilitiesSearch represents the model behind the search form of `app\models\VacancyResponsibilities`.
 */
class VacancyResponsibilitiesSearch extends VacancyResponsibilities
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vacancy_id'], 'integer'],
            [['responsibility'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VacancyResponsibilities::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vacancy_id' => $this->vacancy_id,
        ]);

        $query->andFilterWhere(['like', 'responsibility', $this->responsibility]);

        return $dataProvider;
    }
}
