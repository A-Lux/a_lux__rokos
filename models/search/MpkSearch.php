<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mpk;

/**
 * MpkSearch represents the model behind the search form of `app\models\Mpk`.
 */
class MpkSearch extends Mpk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'title_en', 'title_kz', 'description', 'description_en', 'description_kz', 'mission_title', 'mission_title_en', 'mission_title_kz', 'mission_content', 'mission_content_en', 'mission_content_kz', 'content', 'content_en', 'content_kz'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mpk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'title_kz', $this->title_kz])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'description_en', $this->description_en])
            ->andFilterWhere(['like', 'description_kz', $this->description_kz])
            ->andFilterWhere(['like', 'mission_title', $this->mission_title])
            ->andFilterWhere(['like', 'mission_title_en', $this->mission_title_en])
            ->andFilterWhere(['like', 'mission_title_kz', $this->mission_title_kz])
            ->andFilterWhere(['like', 'mission_content', $this->mission_content])
            ->andFilterWhere(['like', 'mission_content_en', $this->mission_content_en])
            ->andFilterWhere(['like', 'mission_content_kz', $this->mission_content_kz])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'content_en', $this->content_en])
            ->andFilterWhere(['like', 'content_kz', $this->content_kz]);

        return $dataProvider;
    }
}
