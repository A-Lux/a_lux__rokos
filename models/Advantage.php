<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advantage".
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $title_en
 * @property string $subtitle_en
 * @property string $image_en
 * @property string $title_kz
 * @property string $subtitle_kz
 * @property string $image_kz
 */
class Advantage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/advantage/';
    public static function tableName()
    {
        return 'advantage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'subtitle'], 'required'],
            [['title', 'subtitle', 'image', 'title_en', 'subtitle_en', 'image_en', 'title_kz', 'subtitle_kz', 'image_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Текст',
            'subtitle'      => 'Контент',
            'image'         => 'Картинка',
            'title_en'      => 'Текст (EN)',
            'subtitle_en'   => 'Контент (EN)',
            'image_en'      => 'Картинка (EN)',
            'title_kz'      => 'Текст (KZ)',
            'subtitle_kz'   => 'Контент (KZ)',
            'image_kz'      => 'Картинка (KZ)',
        ];
    }

    public function getTitle()
    {
        $title = "title".Yii::$app->session["lang"];
        return $this->$title;
    }


    public function getSubtitle()
    {
        $name = "subtitle".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getImage()
    {
        $image = "image".Yii::$app->session["lang"];
        return ($this->$image) ? '/uploads/images/advantage/' . $this->$image : '/no-image.png';
    }
}
