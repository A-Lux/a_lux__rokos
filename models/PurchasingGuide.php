<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "purchasing_guide".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 */
class PurchasingGuide extends \yii\db\ActiveRecord
{
//    public $type_name;
    const CUSTOMER = 0; // Заказчик
    const PROCUREMENT_METHOD = 1; // Метод закупки
    const PROCUREMENT_TYPE = 2; // Вид закупки
    const CURRENCY = 3; // Валюта
    const UNIT = 4; // Ед. измерения
    const STAGE_FIRST = 5; //Статус закупки 1 этап
    const STAGE_SECOND = 6; //Статус закупки 1 этап

    const PROCUREMENT_STAGE = 7; // Этап закупки



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchasing_guide';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'typeName' => 'Тип'
        ];
    }

    public static function getAllTypes()
    {
        return [
            self::CUSTOMER => 'Заказчик',
            self::PROCUREMENT_METHOD => 'Метод закупки',
            self::PROCUREMENT_STAGE => 'Этап закупки',
            self::PROCUREMENT_TYPE => 'Вид закупки',
            self::CURRENCY => 'Валюта',
            self::UNIT => 'Единица измерения',
            self::STAGE_FIRST => 'Статус закупки 1 этап',
            self::STAGE_SECOND => 'Статус закупки 2 этап',
        ];
    }

    public static function getFirstStageDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::STAGE_FIRST])->asArray()->all(), 'id', 'name');
    }

    public static function getSecondStageDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::STAGE_SECOND])->asArray()->all(), 'id', 'name');
    }

    public static function getCustomersDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::CUSTOMER])->asArray()->all(), 'id', 'name');
    }

    public static function getProcurementMethodsDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::PROCUREMENT_METHOD])->asArray()->all(), 'id', 'name');
    }

    public static function getProcurementStagesDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::PROCUREMENT_STAGE])->asArray()->all(), 'id', 'name');
    }

    public static function getProcurementTypesDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::PROCUREMENT_TYPE])->asArray()->all(), 'id', 'name');
    }

    public static function getCurrencyDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::CURRENCY])->asArray()->all(), 'id', 'name');
    }

    public static function getUnitDropDown()
    {
        return ArrayHelper::map(self::find()->where(['type' => self::UNIT])->asArray()->all(), 'id', 'name');
    }

    public function getTypeName()
    {
        foreach (static::getAllTypes() as $key => $type) {
            if ($this->type === $key) {
                return $type;
            }
        }
        return null;
    }
}
