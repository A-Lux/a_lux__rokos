<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stage_second_docs".
 *
 * @property int $id
 * @property int $stage_id
 * @property int $type
 * @property string $title
 * @property int $is_required
 */
class StageSecondDocs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stage_second_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stage_id', 'type', 'title'], 'required'],
            [['stage_id', 'type', 'is_required'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stage_id' => 'Stage ID',
            'type' => 'Type',
            'title' => 'Title',
            'is_required' => 'Is Required',
        ];
    }
}
