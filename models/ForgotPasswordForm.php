<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class ForgotPasswordForm extends Model
{
    public $email;
    public $password;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            // username and password are both required
            [['email'], 'required'],
            ['email', 'email'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'password'      => 'Пароль',
            'email'         => 'Email',
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $user           = $this->getUser();
            $newPassword    = Yii::$app->security->generateRandomString(8);
            $user->setPassword($newPassword);



            if($user->save(false)){
                return $user;
            }

            return false;

        }
        return  false;
    }

//    protected function sendPassword($email, $password)
//    {
//        $emailSend = Yii::$app->mailer->compose()
//            ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
//            ->setTo('kkokoneor@gmial.com')
//            ->setSubject('')
//            ->setHtmlBody(
//                "<p>E-mail: $email</p></br>
//                 <p>Новый пароль: $password</p></br>
//                 ");
//
//        $emailSend->send();
//    }

    /**
     * Finds user by [[username]]
     *
     * @return \app\models\User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
