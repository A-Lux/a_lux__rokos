<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_geography".
 *
 * @property int $id
 * @property string $country
 */
class LogisticsGeography extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_geography';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country'], 'required'],
            [['country'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'country'       => 'Страна',
        ];
    }
}
