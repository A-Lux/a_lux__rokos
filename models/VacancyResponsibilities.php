<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy_responsibilities".
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string $responsibility
 * @property string $responsibility_kz
 * @property string $responsibility_en
 */
class VacancyResponsibilities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy_responsibilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vacancy_id', 'responsibility'], 'required'],
            [['vacancy_id'], 'integer'],
            [['responsibility', 'responsibility_kz', 'responsibility_en'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'vacancy_id'            => 'Vacancy ID',
            'responsibility'        => 'Responsibility',
            'responsibility_en'     => 'Responsibility en',
            'responsibility_kz'     => 'Responsibility kz',
        ];
    }

    public function getResponsibility()
    {
        $name = "responsibility".Yii::$app->session["lang"];

        return $this->$name;
    }
}
