<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy_description".
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 */
class VacancyDescription extends \yii\db\ActiveRecord
{

    public $content;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vacancy_id', 'description'], 'required'],
            [['vacancy_id'], 'integer'],
            [['description', 'description_en', 'description_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Vacancy ID',
            'description' => 'Description',
        ];
    }

    public function getDescription()
    {
        $name = "description".Yii::$app->session["lang"];

        return $this->$name;
    }
}
