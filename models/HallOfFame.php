<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hall_of_fame".
 *
 * @property int $id
 * @property string $rank
 * @property string $rank_en
 * @property string $rank_kz
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $position
 * @property string $position_en
 * @property string $position_kz
 * @property string $image
 */
class HallOfFame extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/hall-of-fame/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hall_of_fame';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rank', 'name', 'position'], 'required'],
            [['rank', 'rank_en', 'rank_kz', 'name', 'name_en', 'name_kz', 'position', 'position_en', 'position_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'rank'          => 'Достижение',
            'rank_en'       => 'Достижение En',
            'rank_kz'       => 'Достижение Kz',
            'name'          => 'ФИО',
            'name_en'       => 'ФИО En',
            'name_kz'       => 'ФИО Kz',
            'position'      => 'Должность',
            'position_en'   => 'Должность En',
            'position_kz'   => 'Должность Kz',
            'image'         => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/hall-of-fame/' . $this->image : '/no-image.png';
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getRank()
    {
        $name = "rank".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getPosition()
    {
        $name = "position".Yii::$app->session["lang"];

        return $this->$name;
    }
}
