<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partner_feedback".
 *
 * @property int $id
 * @property string $file
 * @property string $created_at
 * @property string $reply
 * @property int $status
 */
class PartnerFeedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/files/partner-feedback/';
    public static function tableName()
    {
        return 'partner_feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['created_at'], 'safe'],
            [['reply'], 'string'],
            [['status'], 'integer'],
            [['file'], 'file', 'extensions' => 'doc, pdf, csv, txt, docx, pptx'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'file'          => 'Файл',
            'created_at'    => 'Дата создание',
            'status'        => 'Статус заявки',
            'reply'         => 'Комментарий менеджера',
        ];
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/files/partner-feedback/' . $this->file : '';
    }

    public function getFilePath()
    {
        return ($this->file) ? '/uploads/files/partner-feedback/' . $this->file : '';
    }




}
