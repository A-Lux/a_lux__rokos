<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy".
 *
 * @property int $id
 * @property int $category_id
 * @property int $city_id
 * @property string $name
 * @property string $content
 * @property string $price
 * @property string $name_en
 * @property string $content_en
 * @property string $name_kz
 * @property string $content_kz
 * @property string $schedule
 * @property string $schedule_en
 * @property string $schedule_kz
 * @property string $address
 * @property string $address_en
 * @property string $address_kz
 * @property string $email
 * @property string $by_phone
 * @property string $by_phone_en
 * @property string $by_phone_kz
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'city_id', 'name', 'email'], 'required'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['category_id', 'city_id'], 'integer'],
            [['name', 'name_en', 'name_kz',
                'price', 'email',
                'by_phone', 'by_phone_en', 'by_phone_kz',
                'address', 'address_en', 'address_kz',
                'schedule', 'schedule_en', 'schedule_kz'
                ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'category_id'   => 'Категория',
            'city_id'       => 'Город',
            'name'          => 'Название (RU)',
            'content'       => 'Содержание (RU)',
            'name_en'       => 'Название (EN)',
            'content_en'    => 'Содержание (EN)',
            'name_kz'       => 'Название (KZ)',
            'content_kz'    => 'Содержание (KZ)',
            'price'         => 'Заплата',
            'email'         => 'Эл. адрес',
            'by_phone'      => 'Справки по телефону',
            'by_phone_en'   => 'Справки по телефону (EN)',
            'by_phone_kz'   => 'Справки по телефону (KZ)',
            'address'       => 'Адрес',
            'address_en'    => 'Адрес (EN)',
            'address_kz'    => 'Адрес (KZ)',
            'schedule'      => 'График работы',
            'schedule_en'   => 'График работы (EN)',
            'schedule_kz'   => 'График работы (KZ)',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(CategoryVacancy::className(), ['id' => 'category_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }


    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getContent()
    {
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getByPhone()
    {
        $byPhone = "by_phone".Yii::$app->session["lang"];
        return $this->$byPhone;
    }

    public function getAddress()
    {
        $address = "address".Yii::$app->session["lang"];
        return $this->$address;
    }

    public function getSchedule()
    {
        $schedule = "schedule".Yii::$app->session["lang"];
        return $this->$schedule;
    }

    public static function getList()
    {
        $name = "name".Yii::$app->session["lang"];

        return \yii\helpers\ArrayHelper::map(Vacancy::find()->all(),'id',$name);
    }

    public function getResponses()
    {
        return $this->hasMany(VacancyResponsibilities::className(), ['vacancy_id' => 'id']);
    }

    public function getRequirements()
    {
        return $this->hasMany(VacancyRequirements::className(), ['vacancy_id' => 'id']);
    }

    public function getConditions()
    {
        return $this->hasMany(VacancyConditions::className(), ['vacancy_id' => 'id']);
    }

    public function getDescription()
    {
        return $this->hasMany(VacancyDescription::className(), ['vacancy_id' => 'id']);
    }
}
