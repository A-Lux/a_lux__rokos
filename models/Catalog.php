<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 * @property int $level
 * @property int $sort
 * @property int $status
 * @property string $created_at
 */

class Catalog extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/catalog/';

    public static function tableName()
    {
        return 'catalog';
    }

    public function rules()
    {
        return [
            [['name', 'sort', 'status', 'level', 'parent_id'], 'required'],

            [['parent_id', 'sort', 'status', 'level'], 'integer'],

            [['created_at'], 'safe'],

            [[
                'name', 'name_en', 'name_kz', 'url'
            ], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'parent_id'     => 'Родительский каталог',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'image'         => 'Изображение',
            'sort'          => 'Сортировка',
            'status'        => 'Статус',
            'created_at'    => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = Catalog::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/catalog/' . $this->image : '';
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getParent()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'parent_id']);
    }

    public function getParentName(){
        return (isset($this->parent))? $this->parent->name:'Не задано';
    }

    public function getChilds()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
