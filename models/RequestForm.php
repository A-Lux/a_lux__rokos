<?php

namespace frontend\models;

use app\models\PurchaseRequests;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Request form
 */
class RequestForm extends Model
{
    public $purchase;

    public $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'purchase'      => 'purchase',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase'], 'required'],

            [['purchase'], 'integer'],
        ];
    }

    /**
     * Create new request
     * @return bool|PurchaseRequests
     */
    public function request()
    {
        if ($this->validate()) {
            $request                        = new PurchaseRequests();
            $request->user_id               = $this->getUser()->id;
            $request->purchase_id           = $this->purchase;
            $request->save();

            return $request;
        }

        return false;
    }

    /**
     * Finds user
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['user_id' => Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }

}
