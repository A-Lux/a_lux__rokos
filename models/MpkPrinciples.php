<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpk_principles".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 */
class MpkPrinciples extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/mpk-principles/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mpk_principles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'name_en', 'name_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Описание',
            'name_en'   => 'Описание En',
            'name_kz'   => 'Описание Kz',
            'image'     => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/mpk-principles/' . $this->image : '/no-image.png';
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];

        return $this->$name;
    }
}
