<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'text_en', 'text_kz'], 'required'],
            [['text', 'text_en', 'text_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'text_en' => 'Текст (EN)',
            'text_kz' => 'Текст (KZ)',
        ];
    }

    public function getText()
    {
        $name = "text".Yii::$app->session["lang"];

        return $this->$name;
    }
}
