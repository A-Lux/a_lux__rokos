<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_feedback".
 *
 * @property int $id
 * @property int $city_id
 * @property string $address
 * @property string $name
 * @property string $contact_person
 * @property string $email
 * @property string $telephone
 * @property string $created_at
 * @property string $reply
 * @property int $status
 */
class ClientFeedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'address', 'name', 'contact_person', 'email', 'telephone'], 'required'],
            [['created_at'], 'safe'],
            [['reply'], 'string'],
            [['city_id','status'], 'integer'],
            [[ 'address', 'name', 'contact_person', 'email', 'telephone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'city_id'           => 'Город',
            'address'           => 'Адрес',
            'name'              => 'Наименование торговой точки',
            'contact_person'    => 'Контактное лицо',
            'email'             => 'E-mail',
            'telephone'         => 'Телефон',
            'created_at'        => 'Дата создание',
            'status'            => 'Статус заявки',
            'reply'             => 'Комментарий менеджера',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
