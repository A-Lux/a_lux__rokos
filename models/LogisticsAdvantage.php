<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logistics_advantage".
 *
 * @property int $id
 * @property string $image
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 */
class LogisticsAdvantage extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/logistics-advantage/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistics_advantage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'subtitle'], 'required'],
            [['image', 'title', 'title_en', 'title_kz', 'subtitle', 'subtitle_en', 'subtitle_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'image'         => 'Изображение',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок En',
            'title_kz'      => 'Заголовок Kz',
            'subtitle'      => 'Подзаголовок',
            'subtitle_en'   => 'Подзаголовок En',
            'subtitle_kz'   => 'Подзаголовок Kz',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/logistics-advantage/' . $this->image : '/no-image.png';
    }

    public function getTitle()
    {
        $name = "title".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getSubtitle()
    {
        $name = "subtitle".Yii::$app->session["lang"];

        return $this->$name;
    }
}
