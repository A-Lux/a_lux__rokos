<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_request_files".
 *
 * @property int $id
 * @property int $user_id
 * @property int $purchase_request_id
 * @property int $stage_id
 * @property string $content
 * @property string $model
 */
class PurchaseRequestFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_request_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'purchase_request_id', 'stage_id'], 'integer'],
            [['content'], 'string'],
            [['model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'user_id'               => 'User ID',
            'purchase_request_id'   => 'Purchase Request ID',
            'stage_id'              => 'Stage ID',
            'content'               => 'Content',
            'model'                 => 'Model',
        ];
    }

    public function pathFile()
    {
        return 'uploads/files/request-files/';
    }

    public function getFile()
    {
        return ($this->content) ? '/uploads/files/request-files/' . $this->content : '';
    }
}
