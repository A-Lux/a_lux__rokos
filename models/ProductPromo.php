<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_promo".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $category
 * @property string $category_en
 * @property string $category_kz
 * @property string $image
 */
class ProductPromo extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/product-promo/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'category'], 'required'],
            [['name', 'name_en', 'name_kz', 'category', 'category_en', 'category_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'name_en'       => 'Название En',
            'name_kz'       => 'Название Kz',
            'category'      => 'Категория',
            'category_en'   => 'Категория En',
            'category_kz'   => 'Категория Kz',
            'image'         => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/product-promo/' . $this->image : '/no-image.png';
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getCategory()
    {
        $name = "category".Yii::$app->session["lang"];
        return $this->$name;
    }
}
