<?php
namespace app\models;

use app\models\User;
use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * ResetPasswordUpdate
 */
class ResetPasswordUpdateForm extends Model
{
    public  $new_password;
    public  $new_password_repeat;

    public  $token;
    private $_user;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'new_password'              => \Yii::t('main-label', 'Новый пароль'),
            'new_password_repeat'       => \Yii::t('main-label', 'Подтверждение нового пароля'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_password', 'new_password_repeat'], 'required'],
            ['new_password', 'string', 'min' => 8],

            ['token', 'string'],

            ['new_password_repeat', 'compare',
                'compareAttribute' => 'new_password',
                'message' => \Yii::t('main-error', 'Пароли не совпадают')],
        ];
    }

    public function updatePassword()
    {
        if ($this->validate()) {

            $user   = User::findOne(['password_reset_token' => $this->token]);
            $user->setPassword($this->new_password);
            $user->password_reset_token = null;

            if($user->save(false)){
                return $user;
            }

            return $user;
        }
        return null;

    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['password_reset_token' => $this->token]);
        }

        return $this->_user;
    }

}