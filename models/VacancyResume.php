<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy_resume".
 *
 * @property int $id
 * @property string $fio
 * @property int $city_id
 * @property string $email
 * @property string $phone
 * @property string $comment
 * @property string $href
 * @property string $file
 */
class VacancyResume extends \yii\db\ActiveRecord
{
    public $path = 'uploads/files/vacancy-resume/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy_resume';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'city_id', 'email', 'phone'], 'required'],
            [['city_id'], 'integer'],
            [['comment'], 'string'],
            [['fio', 'email', 'phone', 'href'], 'string', 'max' => 255],
            [['file'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'fio'       => 'Fio',
            'city_id'   => 'City ID',
            'email'     => 'Email',
            'phone'     => 'Phone',
            'comment'   => 'Comment',
            'href'      => 'Href',
            'file'      => 'File',
        ];
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/files/vacancy-resume/' . $this->file : '';
    }
}
