<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_requests".
 *
 * @property int $id
 * @property int $user_id
 * @property int $purchase_id
 * @property string $unit
 * @property string $name_product
 * @property double $quantity
 * @property string $currency
 * @property string $price
 * @property string $vat
 * @property int $delivery_days
 * @property string $delivery_condition
 * @property string $payment_condition
 * @property string $description
 * @property string $delivery_time
 * @property int $status_sent
 * @property int $status_approved
 * @property int $purchase_customer
 * @property int $procurement_method
 * @property int $purchase_stage
 * @property string|null $created_at
 *
 * @property Purchases $purchase
 * @property User $user
 */
class PurchaseRequests extends \yii\db\ActiveRecord
{
    public $purchase_customer;

    const STATUS_SENT_NOT           = 0;
    const STATUS_SENT_YES           = 1;

    const STATUS_APPROVED_ENTER         = 0;
    const STATUS_APPROVED_NOT           = 1;
    const STATUS_APPROVED_YES           = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'purchase_id', 'delivery_days', 'status_sent',
                'status_approved', 'procurement_method', 'purchase_stage'],
                'integer'],
            [['quantity'], 'number'],
            [['created_at'], 'safe'],
            [['description'], 'string'],
            [['unit', 'currency', 'price', 'delivery_condition',
                'payment_condition', 'delivery_time', 'name_product', 'vat'],
                'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                        => 'ID',
            'user_id'                   => 'Участник закупки',
            'purchase_id'               => 'Наименование закупки',
            'name_product'              => 'Наименование товара',
            'vat'                       => 'НДС',
            'unit'                      => 'Ед. измерения',
            'quantity'                  => 'Количество',
            'currency'                  => 'Валюта',
            'price'                     => 'Стоимость с НДС',
            'delivery_days'             => 'День поставки',
            'delivery_condition'        => 'Условия поставки',
            'payment_condition'         => 'Условия оплаты',
            'description'               => 'Описание',
            'delivery_time'             => 'Срок поставки',
            'status_sent'               => 'Отправлена',
            'status_approved'           => 'Статус заявки ',
            'purchase_customer'         => 'Заказчик',
            'procurement_method'        => 'Метод закупки',
            'purchase_stage'            => 'Этап закупки',
            'created_at'                => 'Дата отправки',
        ];
    }

    /**
     * Gets query for [[Purchase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPurchase()
    {
        return $this->hasOne(Purchases::className(), ['id' => 'purchase_id']);
    }

    public function getByUserId($id)
    {
        return	$this->where('user_id = ' . (int) $id);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public static function sentStatuses()
    {
        return [
            self::STATUS_SENT_NOT       => 'not',
            self::STATUS_SENT_YES       => 'yes',
        ];
    }

    /**
     * @return array
     */
    public static function sentDescription()
    {
        return [
            self::STATUS_SENT_NOT       => 'не отправлено',
            self::STATUS_SENT_YES       => 'отправлено',
        ];
    }

    /**
     * @return array
     */
    public static function approvedStatuses()
    {
        return [
            self::STATUS_APPROVED_ENTER     => 'enter',
            self::STATUS_APPROVED_NOT       => 'not',
            self::STATUS_APPROVED_YES       => 'yes',
        ];
    }

    /**
     * @return array
     */
    public static function approvedDescription()
    {
        return [
            self::STATUS_APPROVED_ENTER     => 'поступила',
            self::STATUS_APPROVED_NOT       => 'не принято',
            self::STATUS_APPROVED_YES       => 'принято',
        ];
    }

    public function getProcurementMethodName()
    {
        foreach (PurchasingGuide::getProcurementMethodsDropDown() as $key => $type) {
            if ($this->procurement_method === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getCurrencyName()
    {
        foreach (PurchasingGuide::getCurrencyDropDown() as $key => $type) {
            if ($this->currency === $key) {
                return $type;
            }
        }
        return null;
    }

    public function getUnitName()
    {
        foreach (PurchasingGuide::getUnitDropDown() as $key => $type) {
            if ($this->unit === $key) {
                return $type;
            }
        }
        return null;
    }
}
