<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stage_second".
 *
 * @property int $id
 * @property int $purchase_id
 * @property string $start_date
 * @property string $end_date
 * @property int $remaining
 * @property string $file
 */
class StageSecond extends \yii\db\ActiveRecord
{
    public $files = [];

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->remaining = strtotime($this->end_date) - strtotime($this->start_date);
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stage_second';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_id'], 'required'],
            [['purchase_id', 'purchase_status', 'remaining'], 'integer'],
            [['start_date', 'end_date', 'files', 'file'], 'safe'],
            ['end_date', 'compare', 'compareAttribute' => 'start_date', 'operator' => '>=', 'type' => 'datetime', 'message' => 'Дата начала не может быть позже даты окончания'],
            [['file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_id' => 'Purchase ID',
            'purchase_status' => 'Purchase Status',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'remaining' => 'Remaining',
            'file' => 'File',
        ];
    }

    public function getAllFiles()
    {
        return $this->hasMany(StageSecondDocs::className(), ['stage_id' => 'id']);
    }
}
