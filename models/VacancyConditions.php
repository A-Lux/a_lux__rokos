<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy_conditions".
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string $condition
 * @property string $condition_en
 * @property string $condition_kz
 */
class VacancyConditions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy_conditions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vacancy_id', 'condition'], 'required'],
            [['vacancy_id'], 'integer'],
            [['condition', 'condition_en', 'condition_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Vacancy ID',
            'condition' => 'Condition',
        ];
    }

    public function getCondition()
    {
        $name = "condition".Yii::$app->session["lang"];

        return $this->$name;
    }
}
