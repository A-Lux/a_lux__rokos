<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $telephone
 * @property string $email
 * @property string $name_en
 * @property string $address_en
 * @property string $name_kz
 * @property string $address_kz
 * @property int $status
 * @property int $statusCity
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'telephone', 'email', 'status',], 'required'],
            [['address', 'address_en', 'address_kz'], 'string'],
            [['status', 'statusCity'], 'integer'],
            [['name', 'telephone', 'email', 'name_en', 'name_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название (RU)',
            'address'       => 'Адрес (RU)',
            'name_en'       => 'Название (EN)',
            'address_en'    => 'Адрес (EN)',
            'name_kz'       => 'Название (KZ)',
            'address_kz'    => 'Адрес (KZ)',
            'telephone'     => 'Телефон',
            'email'         => 'E-mail',
            'status'        => 'Как основные контакты',
            'statusCity'    => 'Статус города',
        ];
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getAddress()
    {
        $address = "address".Yii::$app->session["lang"];
        return $this->$address;
    }
}
