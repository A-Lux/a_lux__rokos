<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stage_second}}`.
 */
class m201106_135252_create_stage_second_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stage_second}}', [
            'id' => $this->primaryKey(),
            'purchase_id' => $this->integer()->notNull(),
            'purchase_status' => $this->integer()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'remaining' => $this->integer()->notNull(),
            'file' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stage_second}}');
    }
}
