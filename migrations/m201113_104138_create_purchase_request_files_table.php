<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchase_request_files}}`.
 */
class m201113_104138_create_purchase_request_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchase_request_files}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'purchase_request_id' => $this->integer(),
            'stage_id' => $this->integer(),
            'content' => $this->text(),
            'model' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchase_request_files}}');
    }
}
