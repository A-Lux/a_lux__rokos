<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchases_commissions}}`.
 */
class m201106_041525_create_purchases_commissions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchases_commissions}}', [
            'id' => $this->primaryKey(),
            'purchase_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchases_commissions}}');
    }
}
