<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stage_first_docs}}`.
 */
class m201106_135459_create_stage_first_docs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stage_first_docs}}', [
            'id' => $this->primaryKey(),
            'stage_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'is_required' => $this->tinyInteger()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stage_first_docs}}');
    }
}
