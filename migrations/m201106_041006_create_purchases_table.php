<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchases}}`.
 */
class m201106_041006_create_purchases_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchases}}', [
            'id' => $this->primaryKey(),
            'customer' => $this->integer(),
            'name' => $this->string(),
            'procurement_method' => $this->integer(),
            'procurement_type' => $this->integer(),
            'purchase_stage' => $this->string(),
            'currency' => $this->integer(),
            'unit' => $this->integer(),
            'quantity' => $this->integer(),
            'delivery_time' => $this->date(),
            'delivery_condition' => $this->string(),
            'payment_condition' => $this->string(),
            'description' => $this->text(),
            'docs' => $this->string(),
            'tech_task' => $this->string(),
            'is_active' => $this->tinyInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchases}}');
    }
}
