<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchase_requests}}`.
 */
class m201113_091417_create_purchase_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchase_requests}}',
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'purchase_id' => $this->integer(),
                'unit' => $this->string(),
                'quantity' => $this->float(),
                'currency' => $this->string(),
                'price' => $this->string(),
                'delivery_days' => $this->integer(),
                'delivery_condition' => $this->string(),
                'payment_condition' => $this->string(),
                'description' => $this->text(),
                'delivery_time' => $this->string(),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchase_requests}}');
    }
}
