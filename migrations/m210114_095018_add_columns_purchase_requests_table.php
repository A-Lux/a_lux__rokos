<?php

use yii\db\Migration;

/**
 * Class m210114_095018_add_columns_purchase_requests_table
 */
class m210114_095018_add_columns_purchase_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('purchase_requests', 'name_product', $this->string(255)->null());

        $this->addColumn('purchase_requests', 'vat', $this->string(255)->null());

        $this->addColumn('purchase_requests', 'status_sent', $this->integer()
            ->defaultValue(0)->null());

        $this->addColumn('purchase_requests', 'status_approved', $this->integer()
            ->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('purchase_requests', 'name_product');
        $this->dropColumn('purchase_requests', 'vat');
        $this->dropColumn('purchase_requests', 'status_sent');
        $this->dropColumn('purchase_requests', 'status_approved');
    }
}
