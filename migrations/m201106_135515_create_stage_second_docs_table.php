<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stage_second_docs}}`.
 */
class m201106_135515_create_stage_second_docs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stage_second_docs}}', [
            'id' => $this->primaryKey(),
            'stage_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'is_required' => $this->tinyInteger()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stage_second_docs}}');
    }
}
