<?php

use yii\db\Migration;

/**
 * Class m210201_094940_add_columns_purchases_table
 */
class m210201_094940_add_columns_purchases_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('purchases', 'stage_first', $this->integer()
            ->defaultValue(0)->null());

        $this->addColumn('purchases', 'stage_second', $this->integer()
            ->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('purchases', 'stage_first');
        $this->dropColumn('purchases', 'stage_second');
    }
}
