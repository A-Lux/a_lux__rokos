<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m201104_113959_add_bin_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'company_name', $this->string());
        $this->addColumn('{{%user}}', 'bin', $this->string());
        $this->addColumn('{{%user}}', 'country', $this->string());
        $this->addColumn('{{%user}}', 'address', $this->string());
        $this->addColumn('{{%user}}', 'activity_type', $this->string());
        $this->addColumn('{{%user}}', 'name', $this->string());
        $this->addColumn('{{%user}}', 'position', $this->string());
        $this->addColumn('{{%user}}', 'phone', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'company_name');
        $this->dropColumn('{{%user}}', 'bin');
        $this->dropColumn('{{%user}}', 'country');
        $this->dropColumn('{{%user}}', 'address');
        $this->dropColumn('{{%user}}', 'activity_type');
        $this->dropColumn('{{%user}}', 'name');
        $this->dropColumn('{{%user}}', 'position');
        $this->dropColumn('{{%user}}', 'phone');

    }
}
