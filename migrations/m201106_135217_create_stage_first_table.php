<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stage_first}}`.
 */
class m201106_135217_create_stage_first_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stage_first}}', [
            'id' => $this->primaryKey(),
            'purchase_id' => $this->integer()->notNull(),
            'purchase_status' => $this->integer()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'remaining' => $this->integer()->notNull(),
            'file' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stage_first}}');
    }
}
