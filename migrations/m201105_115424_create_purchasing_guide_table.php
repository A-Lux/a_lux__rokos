<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchasing_guide}}`.
 */
class m201105_115424_create_purchasing_guide_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchasing_guide}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchasing_guide}}');
    }
}
