<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%purchases_admins}}`.
 */
class m201106_041508_create_purchases_admins_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%purchases_admins}}', [
            'id' => $this->primaryKey(),
            'purchase_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%purchases_admins}}');
    }
}
