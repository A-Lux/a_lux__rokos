<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\ClientFeedback */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="collapse" id="collapse1">
    <section class="feedback">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                        <? $form = ActiveForm::begin([
                            'id'      => 'client-form',
                            'action'  => ['/site/client-feedback'],
                        ]) ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'city_id')->dropDownList(City::getList(), [
                                    'prompt' => Yii::t('form', 'Город'),
                                    'class' => 'feedback_input form-control',
                                    'id' => 'cities',
                                    'required' => 'required'])->label(false) ?>

                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'address', [
                                    'options' => ['class' => 'input-case'],
                                ])->textInput([
                                    'maxlength'   => true,
                                    'class'       => 'feedback_input',
                                    'placeholder' => Yii::t('form', 'Адрес'),
                                    'required' => 'required'
                                ])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'name', [
                                    'options' => ['class' => 'input-case'],
                                ])->textInput([
                                    'maxlength'   => true,
                                    'class'       => 'feedback_input',
                                    'placeholder' => Yii::t('form', 'Наименование торговой точки'),
                                    'required' => 'required'
                                ])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'contact_person', [
                                    'options' => ['class' => 'input-case'],
                                ])->textInput([
                                    'maxlength'   => true,
                                    'class'       => 'feedback_input',
                                    'placeholder' => Yii::t('form', 'Контактное лицо'),
                                    'required' => 'required'
                                ])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'email', [
                                    'options' => ['class' => 'input-case'],
                                ])->textInput([
                                    'maxlength'   => true,
                                    'class'       => 'feedback_input',
                                    'placeholder' => 'E-mail',
                                    'required' => 'required'
                                ])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($clientFeedback, 'telephone', [
                                    'options' => ['class' => 'input-case'],
                                ])->textInput([
                                    'maxlength'   => true,
                                    'class'       => 'feedback_input',
                                    'placeholder' => Yii::t('form', 'Телефон'),
                                    'required' => 'required'
                                ])->label(false) ?>
                                <script>
                                    $('input[name="ClientFeedback[telephone]"]').inputmask("8(999) 999-9999");
                                </script>
                            </div>
                            <div class="col-sm-12">
                                <? $text =  Yii::t('main', 'Отправить');?>
                                <?= Html::submitButton($text) ?>
                            </div>
                        </div>
                        <? ActiveForm::end() ?>
                </div>
                <div class="col-sm-6">
                    <p>
                        <?= Yii::t('form', 'Наши Клиенты: магазины, супермаркеты, рынки, рестораны, кейтеринговые компании,прочие
                        точки общепита.');?>
                        <?= Yii::t('form', 'Рады будем видеть Вас в числе наших клиентов. Для этого заполните форму на сайте,
                        или обратитесь к менеджеру по качеству сервиса: 
                        <a href="tel:+7 708 623-93-30"><span>+7 708 623-93-30</span></a>');?>
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
