<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\RegistrationForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

//$model  = new RegistrationForm();

?>

<form name="registrationForm" id="registration-form">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
           value="<?= Yii::$app->request->getCsrfToken() ?>" />

    <label for="company-name" class="col-form-label">Наименование компании:</label>
    <input type="text" class="form-control" id="company-name"
           name="company_name" required
    >

    <label for="bin" class="col-form-label">БИН</label>
    <input type="text" class="form-control" id="bin"
           name="bin" required
    >

    <label for="country" class="col-form-label">Страна</label>
    <input type="text" class="form-control" id="country"
           name="country"
    >

    <label for="address" class="col-form-label">Адрес</label>
    <input type="text" class="form-control" id="address"
           name="address"
    >

    <label for="email" class="col-form-label">Email</label>
    <input type="email" class="form-control" id="email"
           name="email"
    >

    <label for="activity_type" class="col-form-label">Вид деятельности</label>
    <input type="text" class="form-control" id="activity_type"
           name="activity_type"
    >

    <label for="name" class="col-form-label">ФИО Представителя (полное)</label>
    <input type="text" class="form-control" id="name"
           name="name" required
    >

    <label for="position" class="col-form-label">Должность</label>
    <input type="text" class="form-control" id="position"
           name="position"
    >

    <label for="phone" class="col-form-label">Телефон</label>
    <input type="text" class="form-control" id="phone"
           name="phone"
    >

    <label for="password" class="col-form-label">Пароль</label>
    <input type="password" class="form-control" id="password"
           name="password" required
    >

    <label for="password_repeat" class="col-form-label">Повторите пароль</label>
    <input type="password" class="form-control" id="password_repeat"
           name="password_repeat" required
    >

    <div class="login-btn">
        <button type="submit">Регистрация</button>
    </div>

</form>

<script>
    $('#registration-form').on('submit', function (e) {
        e.preventDefault();

        let form    = new FormData(document.forms.registrationForm);

        axios.post("/sign-up/index", form)
            .then(function (response) {
                console.log(response)
                if(response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        text: response.data.message
                    });
                    window.location.href = "/profile";
                } else if(response.data.status === 0){
                    Swal.fire({
                        icon: 'warning',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>


<div class="registration-btn">
    <a href="" id="login-btn" data-toggle="modal"
       data-target="#login-modal">Вход</a>
</div>
