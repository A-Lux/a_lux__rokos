<?php


use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Feedback;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <?php if(Yii::$app->session->getFlash('errorFeedback')):?>
        <div id="error" class="message">
            <a id="close" title="Закрыть"  href="/"
               onClick="document.getElementById('error').setAttribute('style','display: none;');">&times;</a>
            <?= Yii::$app->session->getFlash('errorFeedback'); ?>
        </div>
    <?php endif;?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('main', 'Обратный звонок');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <? $form = ActiveForm::begin([
                'id'      => 'feedback-form',
                'action'  => ['/site/footer-feedback'],
            ]) ?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="cities"><?= Yii::t('form', 'Город');?></label>
                    <?= $form->field($model, 'city_id')->dropDownList(City::getList(), [
                        'prompt' => Yii::t('form', 'Город'),
                        'class' => 'cities form-control',
                        'id' => 'cities',
                        'required' => 'required'])->label(false) ?>


                    <label for="recipient-name" class="col-form-label"><?= Yii::t('form', 'Тема обращения');?>:</label>
                    <?= $form->field($model, 'topic')->textInput([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'recipient-name',
                        'placeholder' => Yii::t('form', 'Тема обращения'),
                        'required'    => 'required'
                    ])->label(false) ?>

                    <label for="recipient-name" class="col-form-label"><?= Yii::t('form', 'Имя');?>:</label>
                    <?= $form->field($model, 'fio')->textInput([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'recipient-name',
                        'placeholder' => Yii::t('form', 'Имя'),
                        'required'    => 'required'
                    ])->label(false) ?>

                    <label for="recipient-phone" class="col-form-label"><?= Yii::t('form', 'Телефон');?>:</label>
                    <?= $form->field($model, 'telephone')->textInput([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'recipient-phone',
                        'placeholder' => Yii::t('form', 'Телефон'),
                        'required'    => 'required'
                    ])->label(false) ?>
                    <script>
                        $('input[name="Feedback[telephone]"]').inputmask("8(999) 999-9999");
                    </script>

                    <label for="recipient-email" class="col-form-label">E-mail:</label>
                    <?= $form->field($model, 'email')->textInput([
                        'maxlength'   => true,
                        'type'        => 'email',
                        'class'       => 'form-control',
                        'id'          => 'recipient-email',
                        'placeholder' => 'E-mail',
                        'required'    => 'required'
                    ])->label(false) ?>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label"></label>
                    <?= $form->field($model, 'content')->textarea([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'message-text',
                        'placeholder' => '',
                        'required'    => 'required'
                    ])->label(false) ?>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-end">
                        <div class="col-sm-8 col-md-8">
                            <?= $form->field($model, 'reCaptcha')->widget(
                                \Himiklab\yii2\recaptcha\ReCaptcha::className())->label(false) ?>
                        </div>
                    </div>
            </div>
<!--                --><?//= \Himiklab\yii2\recaptcha\ReCaptcha::widget(['name' => 'Feedback[reCaptcha]']) ?>
            <div class="modal-footer">
                <? $text =  Yii::t('main', 'Отправить');?>
                <?= Html::submitButton($text, ['class' => 'btn order_btn ml-0']) ?>
            </div>
            <? ActiveForm::end() ?>
        </div>
    </div>
</div>
