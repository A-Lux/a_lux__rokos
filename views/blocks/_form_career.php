<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\City;
use app\models\Vacancy;

/* @var $this yii\web\View */
/* @var $model app\models\CareerFeedback */
/* @var $form yii\widgets\ActiveForm */

?>

<? $form = ActiveForm::begin([
    'id'      => 'career-form',
    'action'  => ['/site/career-feedback'],
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>
    <p><?= Yii::t('form', 'Если вы хотите сотрудничать с нами, заполните следущие поля');?>:</p>
    <?= $form->field($careerFeedback, 'contact_person', [
        'options' => ['class' => 'input-case'],
    ])->textInput([
        'maxlength'   => true,
        'class'       => 'mb-3',
        'placeholder' => Yii::t('form', 'Контактное лицо'),
        'required' => 'required'
    ])->label(false) ?>

    <?= $form->field($careerFeedback, 'city_id')->dropDownList(City::getList(), [
            'prompt' => Yii::t('form', 'Город'),
            'class'  => 'cities mb-3',
            'required' => 'required'])->label(false) ?>

    <?= $form->field($careerFeedback, 'position_id')->dropDownList(Vacancy::getList(), [
        'prompt' => Yii::t('form', 'Вакансия'),
        'class'  => 'cities mb-3',
        'required' => 'required'])->label(false) ?>

    <?= $form->field($careerFeedback, 'email', [
        'options' => ['class' => 'input-case'],
    ])->textInput([
        'maxlength'   => true,
        'type'        => 'email',
        'class'       => 'cities mb-3',
        'placeholder' => 'E-mail',
        'required' => 'required'
    ])->label(false) ?>

    <?= $form->field($careerFeedback, 'telephone', [
        'options' => ['class' => 'input-case'],
    ])->textInput([
        'maxlength'   => true,
        'class'       => 'mb-3',
        'placeholder' => Yii::t('form', 'Телефон'),
        'required' => 'required'
    ])->label(false) ?>
    <script>
        $('input[name="CareerFeedback[telephone]"]').inputmask("8(999) 999-9999");
    </script>

    <div class="container mb-3">
        <div class="row">
            <div class="col-12 flex pr-0 pl-0">

                <label class="custom-file-upload pr-5 pt-1 pl-4 pb-1">
                    <?php
                    echo $form->field($careerFeedback, 'file')->fileInput(
                        [
                            'maxlength'   => true,
                            'required'    => 'required'
                        ]
                    )->label(false);
                    ?>
                    <?= Yii::t('form', 'Загрузка резюме');?></label>
            </div>
        </div>
    </div>
    <? $text =  Yii::t('main', 'Отправить');?>
    <?= Html::submitButton($text, ['class' => 'pb-2 pt-2 mb-5']) ?>

<? ActiveForm::end() ?>
