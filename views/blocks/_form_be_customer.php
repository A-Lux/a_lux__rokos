<?php


use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */


$model = new Feedback();
?>

<div class="modal fade" id="beCustomer" tabindex="-1" role="dialog" aria-labelledby="beCustomerLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="beCustomerLabel"><?= Yii::t('main', 'Стать клиентом');?></h5>

            </div>
            <? $form = ActiveForm::begin([
                'id'      => 'feedback-form',
                'action'  => ['/site/footer-feedback'],
            ]) ?>
            <div class="modal-body">
                <div class="form-group">

                    <label for="recipient-name" class="col-form-label"><?= Yii::t('form', 'Имя');?>:</label>
                    <?= $form->field($model, 'fio')->textInput([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'recipient-name',
                        'placeholder' => Yii::t('form', 'Имя'),
                        'required' => 'required'
                    ])->label(false) ?>

                    <label for="recipient-phone" class="col-form-label"><?= Yii::t('form', 'Телефон');?>:</label>
                    <?= $form->field($model, 'telephone')->textInput([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'recipient-phone',
                        'placeholder' => Yii::t('form', 'Телефон'),
                        'required' => 'required'
                    ])->label(false) ?>
                    <script>
                        $('input[name="Feedback[telephone]"]').inputmask("8(999) 999-9999");
                    </script>

                    <label for="recipient-email" class="col-form-label">E-mail:</label>
                    <?= $form->field($model, 'email')->textInput([
                        'maxlength'   => true,
                        'type'        => 'email',
                        'class'       => 'form-control',
                        'id'          => 'recipient-email',
                        'placeholder' => 'E-mail',
                        'required' => 'required'
                    ])->label(false) ?>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label"></label>
                    <?= $form->field($model, 'content')->textarea([
                        'maxlength'   => true,
                        'class'       => 'form-control',
                        'id'          => 'message-text',
                        'placeholder' => '',
                        'required' => 'required'
                    ])->label(false) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn order_btn ml-0" data-dismiss="modal"><?= Yii::t('main', 'Закрыть');?></button>
                <? $text =  Yii::t('main', 'Отправить');?>
                <?= Html::submitButton($text, ['class' => 'btn order_btn ml-0']) ?>
            </div>


            <? ActiveForm::end() ?>
        </div>
    </div>
</div>
