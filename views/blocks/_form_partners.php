<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\PartnerFeedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="collapse" id="collapse2">
    <section class="feedback">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="feedback__form">
                        <div class="row">
                            <p><?= Yii::t('form', 'Если Вы производитель, отправьте нам коммерческое предложение или обратитесь
                                по контактам:');?>
                                </p>

                            <p>
                                <?= Yii::t('form', 'Руководитель отдела Закупа - Махмутова Малика Шервалиевна:
                                    <a href="tel:+7-701-871-1202">+7-701-871-1202</a>');?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <? $partnerForm = ActiveForm::begin([
                        'id'      => 'partner-form',
                        'action'  => ['/site/partner-feedback'],
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]) ?>
                    
                    <div class="row">
                        <label for="" class="label_upload">
                            <p><?= Yii::t('form', 'Загрузить коммерческое предложение');?></p>
                            <?php
                            echo $partnerForm->field($partnerFeedback, 'file')->fileInput(
                                [
                                    'maxlength'   => true,
                                    'class'       => 'feedback_upload',
                                    'required'    => 'required'
                                ]
                            )->label(false);
                            ?>

                        </label>
                    </div>
                    <? $text =  Yii::t('main', 'Отправить');?>
                    <?= Html::submitButton($text, ['class' => 'submit_btn']) ?>
                    <? ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </section>

</div>