<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\RegistrationForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


?>

<form name="loginForm" id="login-form">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
           value="<?= Yii::$app->request->getCsrfToken() ?>" />

    <label for="email" class="col-form-label">Email</label>
    <input type="email" class="form-control" id="email"
           name="email" required
    >
    <p id="email-field__error" class="error text-danger"></p>

    <div class="password-p">
       <a href="/reset-password/index">Забыли пароль?</a>
    </div>

    <label for="password" class="col-form-label">Пароль</label>
    <input type="password" class="form-control" id="password"
           name="password" required
    >
    <p id="password-field__error" class="error text-danger"></p>

    <div class="login-btn">
        <button type="submit">Вход</button>
    </div>

</form>

<script>
    $('#login-form').on('submit', function (e) {
        e.preventDefault();

       //  alert(132131);
        let form    = new FormData(document.forms.loginForm);

        axios.post("/login/index", form)
            .then(function (response) {
                console.log(response)
                if(response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        text: response.data.message
                    });
                    window.location.href = "/profile";
                } else if(response.data.status === 0){
                    Swal.fire({
                        icon: 'warning',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>


<div class="registration-btn">
       <a href="" id="registration-btn" data-toggle="modal"
              data-target="#registration-modal">
              Регистрация
       </a>
</div>
