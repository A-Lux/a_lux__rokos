<?php


use yii\helpers\Html;
use yii\web\View;
use app\models\CareerFeedback;
use app\models\City;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$model = new CareerFeedback();

?>

<? $form = ActiveForm::begin([
    'id' => 'feedback-form',
    'action' => ['/content/vacancy-resume'],
    'options' => ['class' => 'apply-form', 'enctype' => 'multipart/form-data'],
]) ?>
    <div class="flexible">
        <?= $form->field($model, 'position_id')->textInput([
            'maxlength' => true,
            'value' => $position,
            'hidden' => true,
        ])->label(false) ?>
    </div>
    <div class="flexible">
        <label for="initials"><?= Yii::t('form', 'ФИО');?></label>
        <?= $form->field($model, 'contact_person')->textInput([
            'maxlength' => true,
            'id' => 'initials',
            'placeholder' => Yii::t('form', 'Авдеев Сергей Юрьевич'),
            'required' => 'required'
        ])->label(false) ?>
    </div>
    <div class="flexible">
        <label for="cities"><?= Yii::t('form', 'Город');?></label>
        <?= $form->field($model, 'city_id')->dropDownList(City::getList(), [
            'prompt' => Yii::t('form', 'Город'),
            'class' => 'cities',
            'id' => 'cities',
            'required' => 'required'])->label(false) ?>
    </div>
    <div class="flexible">
        <label for="email"><?= Yii::t('form', 'Электронная почта');?></label>
        <?= $form->field($model, 'email')->textInput([
            'maxlength' => true,
            'type'      => 'email',
            'id' => 'email',
            'placeholder' => 'info@mail.ru',
            'required' => 'required'
        ])->label(false) ?>
    </div>
    <div class="flexible">
        <label for="phone-number"><?= Yii::t('form', 'Телефон');?></label>
        <?= $form->field($model, 'telephone')->textInput([
            'maxlength' => true,
            'id' => 'phone-number',
            'placeholder' => Yii::t('form', 'Телефон'),
            'required' => 'required'
        ])->label(false) ?>
        <script>
            $('input[name="CareerFeedback[telephone]"]').inputmask("8(999) 999-9999");
        </script>
    </div>
    <div class="flexible">
        <label for="comments"><?= Yii::t('form', 'Комментарий');?></label>
        <?= $form->field($model, 'comment')->textarea([
            'maxlength' => true,
            'id' => 'comments',
            'placeholder' => Yii::t('form', 'Оставить комментарий'),
            'required' => 'required'
        ])->label(false) ?>
    </div>
    <div class="separator"></div>
    <h2 class="resume-header"><?= Yii::t('form', 'Резюме');?></h2>
    <div class="flexible">
        <label for="url"><?= Yii::t('form', 'Ссылка на резюме со сторонних сайтов');?></label>
        <?= $form->field($model, 'href')->textInput([
            'maxlength' => true,
            'id' => 'url',
            'placeholder' => 'url',
            'required' => 'required'
        ])->label(false) ?>
    </div>
    <div class="flexible">
        <label for="file"><?= Yii::t('form', 'Файл резюме');?></label>
        <label class="custom-file-upload pr-5 pt-1 pl-4 pb-1">
        <?php
        echo $form->field($model, 'file')->fileInput(
            [
                'maxlength'   => true,
                'required'    => 'required'
            ]
        )->label(false);
        ?>
        <?= Yii::t('form', 'Загрузка резюме');?></label>
    </div>
    <div class="separator"></div>
    <? $text =  Yii::t('main', 'Отправить');?>
    <?= Html::submitButton($text, ['class' => 'pb-2 pt-2 mb-5']) ?>

<? ActiveForm::end() ?>
