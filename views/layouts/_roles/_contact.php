<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            [
                'label' => 'Контакты',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/'],'active' => $this->context->id == 'contact'],
                    ['label' => 'Социальные сети', 'icon' => 'fa fa-user', 'url' => ['/admin/social-network/'],'active' => $this->context->id == 'social-network'],
                    ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/'],'active' => $this->context->id == 'emailforrequest'],
                    ['label' => 'Город', 'icon' => 'fa fa-user', 'url' => ['/admin/city/'],'active' => $this->context->id == 'city'],
                ],
            ],
        ],
    ]
) ?>

