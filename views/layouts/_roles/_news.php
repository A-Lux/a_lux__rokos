<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/admin/news/'],'active' => $this->context->id == 'news'],
        ],
    ]
) ?>

