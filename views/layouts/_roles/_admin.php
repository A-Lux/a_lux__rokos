<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
        'items' => [
            ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/admin/source-message/'], 'active' => $this->context->id == 'source-message'],
            ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/'], 'active' => $this->context->id == 'menu'],
            ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/admin/logo/'], 'active' => $this->context->id == 'logo'],
            [
                'label' => 'Страницы',
                'icon' => 'book',
                'url' => '#',
                'items' => [
                    ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/admin/banner/'], 'active' => $this->context->id == 'banner'],
                    ['label' => 'Баннер для мобильной', 'icon' => 'fa fa-user', 'url' => ['/admin/banner-mobile/'], 'active' => $this->context->id == 'banner-mobile'],

                    [
                        'label' => 'О компании',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Наши преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/advantage/'], 'active' => $this->context->id == 'advantage'],
                            ['label' => 'О компании', 'icon' => 'fa fa-user', 'url' => ['/admin/about/'], 'active' => $this->context->id == 'about'],
                        ],
                    ],
                    [
                        'label' => 'МПК',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'МПК', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk/'], 'active' => $this->context->id == 'mpk'],
                            ['label' => 'МПК преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-advantage/'], 'active' => $this->context->id == 'mpk-advantage'],
                            ['label' => 'Наши принципы', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-principles/'], 'active' => $this->context->id == 'mpk-principles'],
                            ['label' => 'Продвигаемые продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/product-promo/'], 'active' => $this->context->id == 'product-promo'],
                            ['label' => 'МПК Каталог', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-catalog/'], 'active' => $this->context->id == 'mpk-catalog'],
                        ],
                    ],
                    ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/admin/news/'], 'active' => $this->context->id == 'news'],
                    [
                        'label' => 'Логистика',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Первый блок', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-header/'], 'active' => $this->context->id == 'logistics-header'],
                            ['label' => 'Преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-advantage/'], 'active' => $this->context->id == 'logistics-advantage'],
                            ['label' => 'География перевозок', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-geography/'], 'active' => $this->context->id == 'logistics-geography'],
                            ['label' => 'Клиенты', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-customer/'], 'active' => $this->context->id == 'logistics-customer'],
                            ['label' => 'Клиентская база', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-customer-base/'], 'active' => $this->context->id == 'logistics-customer-base'],
                            ['label' => 'Слайдер изображении', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-images/'], 'active' => $this->context->id == 'logistics-images/'],
                        ],
                    ],
                    ['label' => 'Партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partner/'], 'active' => $this->context->id == 'partner'],
                    ['label' => 'Изображении', 'icon' => 'fa fa-user', 'url' => ['/admin/images/'], 'active' => $this->context->id == 'images'],

                ],
            ],
            [
                'label' => 'Портфолио',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Каталог', 'icon' => 'fa fa-user', 'url' => ['/admin/catalog/'], 'active' => $this->context->id == 'catalog'],
                    ['label' => 'Продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/products/'], 'active' => $this->context->id == 'products'],
                ],
            ],

            [
                'label' => 'Карьера',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Категория вакансии', 'icon' => 'fa fa-user', 'url' => ['/admin/category-vacancy/'], 'active' => $this->context->id == 'category-vacancy'],
                    ['label' => 'Вакансия', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy/'], 'active' => $this->context->id == 'vacancy'],
                    ['label' => 'Доска почета', 'icon' => 'fa fa-user', 'url' => ['/admin/hall-of-fame/'], 'active' => $this->context->id == 'hall-of-fame'],
//                                ['label' => 'Условия', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy-conditions/'],'active' => $this->context->id == 'vacancy-conditions'],
//                                ['label' => 'Описание', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy-description/'],'active' => $this->context->id == 'vacancy-description'],
//                                ['label' => 'Требование', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy-requirements/'],'active' => $this->context->id == 'vacancy-requirements'],
//                                ['label' => 'Обязанность', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy-responsibilities/'],'active' => $this->context->id == 'vacancy-responsibilities'],
                ],
            ],

            [
                'label' => 'Заявки',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Клиенты', 'icon' => 'fa fa-user', 'url' => ['/admin/client-feedback/'], 'active' => $this->context->id == 'client-feedback'],
                    ['label' => 'Партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partner-feedback/'], 'active' => $this->context->id == 'partner-feedback'],
                    ['label' => 'Заявки на вакансию', 'icon' => 'fa fa-user', 'url' => ['/admin/career-feedback/'], 'active' => $this->context->id == 'career-feedback'],
                ],
            ],

            [
                'label' => 'Контакты',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/'], 'active' => $this->context->id == 'contact'],
                    ['label' => 'Социальные сети', 'icon' => 'fa fa-user', 'url' => ['/admin/social-network/'], 'active' => $this->context->id == 'social-network'],
                    ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/'], 'active' => $this->context->id == 'emailforrequest'],
                    ['label' => 'Город', 'icon' => 'fa fa-user', 'url' => ['/admin/city/'], 'active' => $this->context->id == 'city'],
                ],
            ],


            ['label' => 'Обратная связь ', 'icon' => 'fa fa-user', 'url' => ['/admin/feedback/'], 'active' => $this->context->id == 'feedback'],
            [
                'label' => 'Пользователи',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    [
                        'label' => 'Сотрудники',
                        'icon' => 'fa fa-user',
                        'url' => ['/admin/user'],
                        'active' => $this->context->id == 'user'
                    ],
                    [
                        'label' => 'Участники закупки',
                        'icon' => 'fa fa-user',
                        'url' => ['/admin/user-purchase'],
                        'active' => $this->context->id == 'user-purchase'
                    ],
                ],
            ],
            [
                'label' => 'Закупки',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    [
                        'label' => 'Закупки',
                        'icon' => 'fa fa-user',
                        'url' => ['/admin/purchase-v2'],
                        'active' => $this->context->id == 'purchase-v2'
                    ],
                    [
                        'label' => 'Справочники',
                        'icon' => 'fa fa-user',
                        'url' => ['/admin/purchasing-guide'],
                        'active' => $this->context->id == 'purchasing-guide'
                    ],

                    [
                        'label' => 'Заявки на участие',
                        'icon' => 'fa fa-user',
                        'url' => ['/admin/purchase-requests'],
                        'active' => $this->context->id == 'purchase-requests'
                    ],
                ],
            ],
        ],
    ]
) ?>