<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/admin/source-message/'],'active' => $this->context->id == 'source-message'],
            ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/'],'active' => $this->context->id == 'menu'],
            ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/admin/logo/'],'active' => $this->context->id == 'logo'],
            ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/admin/banner/'],'active' => $this->context->id == 'banner'],
            ['label' => 'Баннер для мобильной', 'icon' => 'fa fa-user', 'url' => ['/admin/banner-mobile/'],'active' => $this->context->id == 'banner-mobile'],

            ['label' => 'Партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partner/'],'active' => $this->context->id == 'partner'],
            ['label' => 'Изображении', 'icon' => 'fa fa-user', 'url' => ['/admin/images/'],'active' => $this->context->id == 'images'],
            [
                'label' => 'О компании',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Наши преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/advantage/'],'active' => $this->context->id == 'advantage'],
                    ['label' => 'О компании', 'icon' => 'fa fa-user', 'url' => ['/admin/about/'],'active' => $this->context->id == 'about'],
                ],
            ],
            [
                'label' => 'Контакты',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/'],'active' => $this->context->id == 'contact'],
                    ['label' => 'Социальные сети', 'icon' => 'fa fa-user', 'url' => ['/admin/social-network/'],'active' => $this->context->id == 'social-network'],
                    ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/'],'active' => $this->context->id == 'emailforrequest'],
                    ['label' => 'Город', 'icon' => 'fa fa-user', 'url' => ['/admin/city/'],'active' => $this->context->id == 'city'],
                ],
            ],
        ],
    ]
) ?>

