<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            [
                'label' => 'Логистика',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Первый блок', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-header/'],'active' => $this->context->id == 'logistics-header'],
                    ['label' => 'Преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-advantage/'],'active' => $this->context->id == 'logistics-advantage'],
                    ['label' => 'География перевозок', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-geography/'],'active' => $this->context->id == 'logistics-geography'],
                    ['label' => 'Клиенты', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-customer/'],'active' => $this->context->id == 'logistics-customer'],
                    ['label' => 'Клиентская база', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-customer-base/'],'active' => $this->context->id == 'logistics-customer-base'],
                    ['label' => 'Слайдер изображении', 'icon' => 'fa fa-user', 'url' => ['/admin/logistics-images/'],'active' => $this->context->id == 'logistics-images/'],
                ],
            ],
        ],
    ]
) ?>

