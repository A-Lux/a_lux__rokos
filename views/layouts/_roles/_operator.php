<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            [
                'label' => 'Карьера',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Категория вакансии', 'icon' => 'fa fa-user', 'url' => ['/admin/category-vacancy/'],'active' => $this->context->id == 'category-vacancy'],
                    ['label' => 'Вакансия', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy/'],'active' => $this->context->id == 'vacancy'],
                    ['label' => 'Доска почета', 'icon' => 'fa fa-user', 'url' => ['/admin/hall-of-fame/'],'active' => $this->context->id == 'hall-of-fame'],
                ],
            ],

            [
                'label' => 'Заявки',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Клиенты', 'icon' => 'fa fa-user', 'url' => ['/admin/client-feedback/'],'active' => $this->context->id == 'client-feedback'],
                    ['label' => 'Партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partner-feedback/'],'active' => $this->context->id == 'partner-feedback'],
                    ['label' => 'Заявки на вакансию', 'icon' => 'fa fa-user', 'url' => ['/admin/career-feedback/'],'active' => $this->context->id == 'career-feedback'],
                ],
            ],

            ['label' => 'Обратная связь ', 'icon' => 'fa fa-user', 'url' => ['/admin/feedback/'],'active' => $this->context->id == 'feedback'],

        ],
    ]
) ?>

