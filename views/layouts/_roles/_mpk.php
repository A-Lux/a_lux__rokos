<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            [
                'label' => 'МПК',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'МПК', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk/'],'active' => $this->context->id == 'mpk'],
                    ['label' => 'МПК преимущества', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-advantage/'],'active' => $this->context->id == 'mpk-advantage'],
                    ['label' => 'Наши принципы', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-principles/'],'active' => $this->context->id == 'mpk-principles'],
                    ['label' => 'Продвигаемые продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/product-promo/'],'active' => $this->context->id == 'product-promo'],
                    ['label' => 'МПК Каталог', 'icon' => 'fa fa-user', 'url' => ['/admin/mpk-catalog/'],'active' => $this->context->id == 'mpk-catalog'],
                ],
            ],
        ],
    ]
) ?>

