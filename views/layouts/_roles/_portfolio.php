<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        'items' => [
            [
                'label' => 'Портфолио',
                'icon' => 'fa fa-user',
                'url' => '#',
                'items' => [
                    ['label' => 'Каталог', 'icon' => 'fa fa-user', 'url' => ['/admin/catalog/'],'active' => $this->context->id == 'catalog'],
                    ['label' => 'Продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/products/'],'active' => $this->context->id == 'products'],
                ],
            ],
        ],
    ]
) ?>

