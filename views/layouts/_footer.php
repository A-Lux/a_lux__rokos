<?php

use yii\bootstrap\ActiveForm;
use app\models\LoginFormFront;
use app\models\RegistrationForm;

$loginForm = new LoginFormFront();
$registrationForm = new RegistrationForm();

$fieldOptions1 = [
    'options' => ['class' => 'email-info has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>",
];

$fieldOptions2 = [
    'options' => ['class' => 'password-input has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$fieldOptions3 = [
    'options' => ['class' => 'email-info has-feedback'],
];

$this->registerCSS(<<<CSS
.help-block-error{
    color:red !important;
}
CSS
);

$this->registerJS(<<<JS

// $('#login-form').on('submit',function(e) {
//     let emailFieldError = $('#email-field__error');
//     let passwordFieldError = $('#password-field__error');
//     emailFieldError.text('');
//     passwordFieldError.text('');
//     $.ajax({
//     url:'/login/index',
//     type:'POST',
//     data:$(this).serialize(),
//     success:function(response){
//         response = JSON.parse(response);
//         console.log(response);
//         console.log(response.password);
//         if(response.hasOwnProperty('password')){
//             passwordFieldError.text(response.password)
//         }
//          if(response.hasOwnProperty('username')){
//             emailFieldError.text(response.username)
//         }
//     }
//     })
//   return false;
// });


function printErrors(field_name,errors=null){
    let errorBlock = $('.field-registrationform-'+field_name).find('.help-block-error');
    errorBlock.removeClass('help-block')
    errorBlock.text(errors);
}

function clearErrorBlocks(){
    $('.help-block-error').each(function() {
      $(this).addClass('help-block')
    })
}


JS
)
?>
<div class="modal fade modal-login" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog profile-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вход</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> 
            <div class="modal-body">
            <?= $this->render('/blocks/_form_login'); ?>
                <!-- <?php //$form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false,
                    //'action' => \yii\helpers\Url::to('/login/index')]);
                ?>
                <?//= $form->field($loginForm, 'email')->textInput(['autofocus' => true])
                    //->label('Email') ?>
                <p id="email-field__error" class="error text-danger"></p>
                <div class="password-p">
                    <p>Пароль</p>
                    <a href="/reset-password/index">Забыли пароль?</a>
                </div>
                <?//= $form->field($loginForm, 'password')->passwordInput()->label(false) ?>
                <p id="password-field__error" class="error text-danger"></p>

                <div class="login-btn">
                    <button>Вход</button>
                </div>
                <?php //ActiveForm::end(); ?>

                <div class="registration-btn">
                    <a href="" id="registration-btn" data-toggle="modal"
                       data-target="#registration-modal">Регистрация</a>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-registration" id="registration-modal" tabindex="-1" role="dialog"
     aria-labelledby="registration-modalTitle" aria-hidden="true">
    <div class="modal-dialog profile-modal modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Регистрация</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('/blocks/_form_registration'); ?>

<!--                <form name="registrationForm" id="registration-form">-->
<!--                    <div class="form-group field-registrationform-company_name required">-->
<!--                        <label class="control-label" for="registrationform-company_name">-->
<!--                            Наименование компании-->
<!--                        </label>-->
<!--                        <input type="text" id="registrationform-company_name" class="form-control"-->
<!--                               name="RegistrationForm[company_name]" aria-required="true"-->
<!--                        >-->
<!--                        <p class="help-block help-block-error"></p>-->
<!--                    </div>-->
<!---->
<!--                </form>-->

<!--                --><?php //$form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false,
//                    'action' => \yii\helpers\Url::to('/sign-up/index')]);
//                ?>
<!--                --><?//= $form->field($registrationForm, 'company_name')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'bin')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'country')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'address')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'email')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'activity_type')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'name')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'position')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'phone')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'password')->passwordInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($registrationForm, 'password_repeat')->passwordInput(['autofocus' => true]) ?>
<!--                <div class="login-btn">-->
<!--                    <button>Регистрация</button>-->
<!--                </div>-->
<!--                --><?php //ActiveForm::end(); ?>
<!--                <div class="registration-btn">-->
<!--                    <a href="">Вход</a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3 px-0">
                <div class="row">
                    <img class="footerLogoPic" src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                    <div class="mobile-ver">
                        <a href="<?= Yii::$app->view->params['social']->instagram ?>" target="_blank">
                            <img src="/public/dist/images/instagram.png">
                        </a>
                        <a href="<?= Yii::$app->view->params['social']->youtube ?>" target="_blank">
                            <img src="/public/dist/images/youtube.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <? foreach (Yii::$app->view->params['headerMenu'] as $item): ?>
                    <a href="<?= $item->url; ?>" class="nav_item__footer"><?= $item->getText(); ?></a>
                <? endforeach; ?>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 mobile-flex">
                <p class="footer_address__p">
                    <? if (!empty(Yii::$app->view->params['contacts'])): ?>
                        <?= Yii::$app->view->params['contacts']->getName(); ?>
                        <?= Yii::$app->view->params['contacts']->getAddress(); ?><br>
                        <?= Yii::$app->view->params['contacts']->telephone; ?>
                        <?= Yii::$app->view->params['contacts']->email; ?>
                    <? endif; ?>
                </p>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3  mobile-flex">
                <div class="row">
                    <p class="order_p"><?= Yii::t('main', 'Обратный звонок'); ?></p>
                </div>
                <div class="row">
                    <a href="" class="order_btn" href="#" data-toggle="modal" data-target="#exampleModal">
                        <p>
                            <?= Yii::t('main', 'Заказать'); ?>
                        </p>

                    </a>
                    <?= $this->render('/blocks/_form_feedback_footer', [
                        'model' => Yii::$app->view->params['feedback'],
                    ]); ?>
                </div>
            </div>
            <div class="offset-sm-1">

            </div>
        </div>
    </div>
    <div class="row footer_flex--border">
        <div class="container">
            <div class="footer_flex">
                <div class="col-sm-12 col-md-5">
                    <p>
                        <?= Yii::$app->view->params['logo']->getCopyright(); ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-7 px-5">
                    <p>
                        <?= Yii::t('main', 'Разработано в'); ?>:
                        <a href="">
                            <img src="/public/dist/images/a-lux.png" alt="">
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.min.js"></script>
</footer>