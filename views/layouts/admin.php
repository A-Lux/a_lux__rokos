<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/public/dist/images/favpng.png" type="image/png">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index']) ?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
        <!--        --><? //= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Заявки">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-success"><?= count(Yii::$app->view->params['feedback']); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">У вас <?= count(Yii::$app->view->params['feedback']); ?> непрочитанных
                                заявки
                            </li>
                            <li>
                                <ul class="menu">

                                    <? if (Yii::$app->view->params['feedback'] != null): ?>
                                        <? foreach (Yii::$app->view->params['feedback'] as $v): ?>
                                            <li>
                                                <a href="/admin/feedback/view?id=<?= $v->id; ?>">
                                                    <i class="fa fa-user text-green"></i> <?= $v->fio; ?>
                                                </a>
                                            </li>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                </ul>
                            </li>
                            <li class="footer"><a href="/admin/feedback/">Посмотреть все заявки</a></li>
                        </ul>
                    </li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">

                                <p>
                                    <?= Yii::$app->user->identity->username ?> - Administrator
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?= Yii::$app->user->identity->username ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Поиск ...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <? if (Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN): ?>
                <?= $this->render('@app/views/layouts/_roles/_admin'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_MANAGER): ?>
                <?= $this->render('@app/views/layouts/_roles/_manager'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_OPERATOR): ?>
                <?= $this->render('@app/views/layouts/_roles/_operator'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_MAIN): ?>
                <?= $this->render('@app/views/layouts/_roles/_main'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_CONTACT): ?>
                <?= $this->render('@app/views/layouts/_roles/_contact'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_CAREER): ?>
                <?= $this->render('@app/views/layouts/_roles/_career'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_LOGISTICS): ?>
                <?= $this->render('@app/views/layouts/_roles/_logistics'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_MPK): ?>
                <?= $this->render('@app/views/layouts/_roles/_mpk'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_PORTFOLIO): ?>
                <?= $this->render('@app/views/layouts/_roles/_portfolio'); ?>
            <? elseif (Yii::$app->user->identity->role == \app\models\User::ROLE_NEWS): ?>
                <?= $this->render('@app/views/layouts/_roles/_news'); ?>
            <? else: ?>
            <? endif; ?>
        </section>
    </aside>


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= $content ?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2007-2019 <a href="https://a-lux.kz">A-Lux</a>.</strong>
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/private/dist/js/vacancy.js'); ?>
<script>
    $(document).ready(function () {
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor(editor);
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
