<?php
$directoryURI = $_SERVER['REQUEST_URI'];
$header = false;
if ($directoryURI != "/" && $directoryURI != "/site/index") {
    $header = true;
}
?>
<header class="header">
    <!--gega -->
    <nav id="main-nav">

        <ul>
            <input type="text">
            <li><a>
                    <img src="/public/dist/images/logo.png" alt="" class="logoPicSpec"
                         style="filter: brightness(1.14) grayscale(0.39);">

                </a></li>
            <li><a>
                    <!-- <form action="" method="POST">
                                    <input type="text" class="specialInput">
                                </form> -->
                </a></li>
            <? foreach (Yii::$app->view->params['headerMenu'] as $item): ?>
                <li><a href="<?= $item->url; ?>"><?= $item->getText(); ?></a></li>
            <? endforeach; ?>
        </ul>
    </nav>
    <!-- Конец мобильной версии меню -->
    <?php
    if ($header) {
        echo '<div class="absolute_backround-slider" style="display: none">';
    } else {
        echo '<div class="absolute_backround-slider"  style="">';
    }
    ?>
    <div class="glide_header">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <? foreach (Yii::$app->view->params['banner'] as $item): ?>
                    <li class="glide__slide"
                        style="background: url(<?= $item->getImage(); ?>);background-position: center;">
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
        <div class="glide__bullets" data-glide-el="controls[nav]">
            <? foreach (Yii::$app->view->params['banner'] as $item): ?>
                <button class="glide__bullet"></button>
            <? endforeach; ?>
            <!-- <button class="glide__bullet" data-glide-dir="=0"></button>
            <button class="glide__bullet" data-glide-dir="=1"></button>
            <button class="glide__bullet" data-glide-dir="=2"></button>
            <button class="glide__bullet" data-glide-dir="=3"></button> -->
        </div>
    </div>
    </div>

    <div class="container  indexStyleMobile">
        <div class="row header-nav">
            <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="logo">
                    <a href="/">
                        <img src="/public/dist/images/logo.png" alt="" class="logoPic"
                             style="filter: brightness(1.14) grayscale(0.39);">
                    </a>
                    <div class="lang-version-mob">
                        <!--                        <a href="/lang/?url=kz">Kaz</a>-->
                        <!--                        <a href="/lang/?url=ru">Ru</a>-->
                        <!--                        <a href="/lang/?url=en">Eng</a>-->
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-md-12 col-lg-9">
                <div class="menu">
                    <div class="menu__upper__layer">
                        <? foreach (Yii::$app->view->params['headerMenu'] as $item): ?>
                            <a class="" href="<?= $item->url; ?>"><?= $item->getText(); ?></a>
                        <? endforeach; ?>
                        <a href="/purchases">Закупки</a>
                        <div class="dropdown profile-dropdown">
                            <? if(Yii::$app->user->isGuest): ?>
                            <button class="btn dropdown-toggle profile-icon" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Профиль <img src="/public/dist/images/Union.svg" alt="">
                            </button>
                            <div class="dropdown-menu profile-menu" aria-labelledby="dropdownMenuButton">
                                <p>Войдите или зарегистрируйтесь</p>
                                <button class="dropdown-item login-btn"
                                        data-toggle="modal" data-target="#loginModal">Войти
                                </button>
                                <button class="dropdown-item login-btn"
                                        data-toggle="modal" data-target="#loginModal">Регистрация</button>
                            </div>

                            <? else: ?>
                                <a href="/profile" class="btn dropdown-toggle profile-icon" type="button">
                                    Профиль <img src="/public/dist/images/Union.svg" alt="">
                                </a>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="menu__sub__layer">
                        <form action="/search/index" method="get">
                            <input type="text" name="text">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 d-flex align-items-end">
                <div class="lang-version">
                    <!--                    <a href="/lang/?url=kz">Kaz</a>-->
                    <!--                    <a href="/lang/?url=ru">Ru</a>-->
                    <!--                    <a href="/lang/?url=en">Eng</a>-->
                </div>
            </div>
        </div>
    </div>

    <?php
    if ($header) {
        echo '<a href="#about_company"  class="mouse_nav--absolute" style="display: none">';
    } else {
        echo '<a href="#about_company"  class="mouse_nav--absolute">';
    }
    ?>


    </a>
</header>