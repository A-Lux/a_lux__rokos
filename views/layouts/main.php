<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
PublicAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/public/dist/images/favpng.png" type="image/png">
    <link rel="stylesheet" href="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" /> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-KA6wR/X5RY4zFAHpv/CnoG2UW1uogYfdnP67Uv7eULvTveboZJg0qUpmJZb5VqzN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@5/dist/backdrop.css" />
    <script src="https://cdn.jsdelivr.net/npm/mobile-detect@1.4.4/mobile-detect.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"
            integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



</head>
<body>
<?php $this->beginBody() ?>

<!-- HEADER -->
<?=$this->render('_header')?>
<!-- END HEADER -->
<?php if(Yii::$app->session->getFlash('successFeedback')):?>
    <div id="success" class="message">
        <a id="close" title="Закрыть"  href="/"
           onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        <?= Yii::$app->session->getFlash('successFeedback'); ?>
    </div>
<?php endif;?>

<?php if(Yii::$app->session->getFlash('error')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="/"
           onClick="document.getElementById('error').setAttribute('style','display: none;');">&times;</a>
        <?= Yii::$app->session->getFlash('error'); ?>
    </div>
<?php endif;?>

<?php if(Yii::$app->session->getFlash('success')):?>
    <div id="success" class="message">
        <a id="close" title="Закрыть"  href="/"
           onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif;?>

<?//= \yii2mod\alert\Alert::widget() ?>

<?=$content?>

<!-- FOOTER -->
<?=$this->render('_footer')?>


<script src="https://unpkg.com/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> -->

<script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
<script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>

<script src="https://unpkg.com/tippy.js@5.1.1/dist/tippy-bundle.iife.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script>
        var headerInner = window.location.pathname ;
        setTimeout(() => {
            if (headerInner != "/site/index" && headerInner != "/") {
                document.querySelector(".menu__upper__layer").classList.add("innerHeader");
                document.querySelector(".lang-version").classList.add("innerHeader");
                document.querySelector(".lang-version-mob").classList.add("innerHeader");
                document.querySelector(".logoPic").src = "/public/dist/images/internal-logo.png";
                document.querySelector(".header").classList.add("innerHeaderMaxHeight");
                // document.querySelector(".absolute_backround-slider").style.display = "none";
                // document.querySelector(".mouse_nav--absolute").style.display = "none";
                document.querySelector(".hc-nav-trigger").classList.add("innerHeader");

                document.querySelector(".hc-nav-trigger").addEventListener("click", () => {
                    document.querySelector(".menu__sub__layer").children[0].children[0].classList.toggle("InputBackground");
                });
            };

        }, 500);
        if (headerInner == "/site/index" || headerInner == "/") {

           window.glide =  new Glide(".glide_header", {
                autoplay: 3000,
                type: 'carousel',
                hoverpause: false,
            }).mount();
            window.glide1 =  new Glide('.glide', {
                autoplay: 3000,
                hoverpause: false,
                type: 'carousel',
                startAt: 0,
                perView: 5,
                breakpoints: {
                    600: {
                        perView: 3
                    }
                }
            }).mount();
            if (window.innerWidth < 580) {
                let mobileImagesSlider = document.querySelectorAll("header .glide__slide");
                $.ajax({
                    type: 'GET',
                    url: 'http://rokos.kz/api/banner',
                    success: function(response) {
                        let images = JSON.parse(response);
                        for (let image of images) {
                            mobileImagesSlider[image.id].style.background = `url("/uploads/images/banner-mobile/`+image.image+`")`;
                            console.log(image.id);
                        }
                    }
                });
                window.onload = function() {
                    window.glide.destroy();
                    window.glide1.destroy();
                    window.glide =  new Glide(".glide_header", {
                        autoplay: 3000,
                        type: 'carousel',
                        hoverpause: false,
                    }).mount();
                    window.glide1 =  new Glide('.glide', {
                        autoplay: 3000,
                        hoverpause: false,
                        type: 'carousel',
                        startAt: 0,
                        perView: 5,
                        breakpoints: {
                            600: {
                                perView: 3
                            }
                        }
                    }).mount();
                }
            }
        }
    </script>
   
    <!-- <script>
        $(document).ready(function() {
            var $disabledResults = $("#cities");
            $disabledResults.select2();
            $('#cities').select2({
                minimumResultsForSearch: Infinity
            });
        });
    </script> -->
    
    
    <script>
            function showPage(shown, hidden) {
                document.getElementById(shown).style.position='static';

                document.getElementById(hidden).style.position='absolute';
                document.getElementById(hidden).style.left='-9999px';
                return false;
            }
            function showProducts(shown, hidden1, hidden2, hidden3, hidden4) {
                document.getElementById(shown).style.display='block';
                document.getElementById(hidden1).style.display='none';
                document.getElementById(hidden2).style.display='none';
                document.getElementById(hidden3).style.display='none';
                document.getElementById(hidden4).style.display='none';
                return false;
            }
    </script>
    <script>
        var $slider = $(".autoplay1");
        $slider.slick({
            slidesToShow: 3.9999999,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 1082,
                    settings: {
                        arrows: false
                    }
                }
            ]
        })
    .on("beforeChange", function(e, slick, current, next) {
        if( next >= (slick.slideCount - slick.options.slidesToShow)) {
            $slider.addClass("atEnd");
        } else {
            $slider.removeClass("atEnd");   
        }
    });
    var $slider2 = $(".autoplay2");
        $slider2.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 1082,
                    settings: {
                        arrows: false
                    }
                }
            ]
        })
        .on("beforeChange", function(e, slick, current, next) {
            if( next >= (slick.slideCount - slick.options.slidesToShow)) {
                $slider2.addClass("atEnd");
            } else {
                $slider2.removeClass("atEnd");   
            }
        });
        if (document.getElementsByClassName("clientImages")[0].childElementCount > 3) {
            $('.clientImages').slick({
                slidesToShow: 2.9999,
                slidesToScroll: 1,
                autoplay: 4000,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 460,
                        settings: {
                            slidesToShow: 1,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 778,
                        settings: {
                            slidesToShow: 2,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 1032,
                        settings: {
                            slidesToShow: 3,
                        }
                    }
                ]
            });
        }
        
    </script>
    <script>
        var navLink = $('.product-buttons .pr-production').on('click', function(e) {
            e.preventDefault();
            navLink.not(this).removeClass('acting');
            $(this).addClass('acting');
        });
        var navLink2 = $('.slider .entry').on('click', function(e) {
            e.preventDefault();
            navLink2.not(this).removeClass('active');
            $(this).addClass('active');
        });
    </script>
    <script>

    var $slider2 = $(".ourPartnersAutoplay");
    $slider2.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 2000,
        variableWidth: false,
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    arrows: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    arrows: false
                }
            },
            {
                breakpoint: 1082,
                settings: {
                    arrows: false
                }
            }
        ]
    })
    .on("beforeChange", function(e, slick, current, next) {
        if( next >= (slick.slideCount - slick.options.slidesToShow)) {
            $slider2.addClass("atEnd");
        } else {
            $slider2.removeClass("atEnd");   
        }
    });

    </script>
    <script>
        function showClients(shown, hidden) {
                document.getElementById(shown).style.display='block';
                document.getElementById(hidden).style.display='none';
                return false;
        }
    </script>
<!-- END FOOTER -->
<?php $this->endBody() ?>

</html>

<?php $this->endPage() ?>
