<?php


use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

?>
<div class="modal fade modal-login" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog profile-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вход</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
                <div class="email-info">
                    <p>E-mail</p>
                    <input type="text">
                </div>
                <div class="password-p">
                    <p>Пароль</p>
                    <a href="">Забыли пароль?</a>
                </div>
                <div class="password-input">
                    <input type="password">
                </div>
                <div class="login-btn">
                    <button>Вход</button>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="registration-btn">
                    <a href="" id="registration-btn" data-toggle="modal" data-target="#registration-modal">Регистрация</a>
                </div>
            </div>
        </div>
    </div>
</div>
