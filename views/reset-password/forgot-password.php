<div class="reset-password">
    <div class="container">
        <a href="/profile"> <img src="/public/dist/images/arrow_back_24px.svg" alt="">
            Восстановление пароля
        </a>
        <div class="password-inner">
            <form id="forgot-password" name="forgot-password">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->getCsrfToken() ?>" />

                <div class="form-group">
                    <label class="col-md-4 control-label">Введите свой E-mail</label>
                    <div class="col-md-4">
                        <input type="text" name="email" class='form-control form-inform' placeholder="mail@gmail.com">
                        <span class="push-info"><img src="/public/dist/images/error_24px_sharp.svg" alt="">
                            После отправки вам придет на почту письмо
                        </span>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="profile">
                        <div class="save-changes">
                            <button type="submit">Отправить</button>
    <!--                        <button data-toggle="modal" data-target="#send-email">Отправить</button>-->
                        </div>
                        <div class="modal fade" id="send-email" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog profile-modal" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="email-info">
                                            <p>
                                                На вашу почту было отправлено сообщение с ссылкой на изменение вашего
                                                пароля
                                            </p>
                                        </div>
                                        <div class="login-btn">
                                            <a href="/">Продолжить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $('#update-profile').on('submit', function (e) {
        e.preventDefault();
        // alert(123123);

        let form    = new FormData(document.forms.updateProfile);

        axios.post("/profile/update", form)
            .then(function (response) {
                console.log(response)
                if(response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        text: response.data.message
                    });
                    // window.location.href = "/profile";
                } else if(response.data.status === 0){
                    Swal.fire({
                        icon: 'warning',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>