<div class="reset-password">
    <div class="container">
        <a href="/"> <img src="/public/dist/images/arrow_back_24px.svg" alt="">
            Изменение пароля
        </a>
        <form id="reset-password-update" name="resetPasswordUpdate">

            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->getCsrfToken() ?>" />

            <input type="hidden" name="token" value="<?= $token; ?>">

            <div class="password-inner">
                <div class="form-group">
                    <label class="col-md-4 control-label">Введите новый пароль</label>
                    <div class="col-md-4">
                        <input  type="password"
                                class="form-control "
                                name="new_password" placeholder="********" required
                        >
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Повторите новый пароль</label>
                    <div class="col-md-4">
                        <input id="password-field-2" type="password"
                               class="form-control"
                               name="new_password_repeat"
                               placeholder="********">
                        <span toggle="#password-field-2" class="fa fa-fw fa-eye field-icon toggle-password-2"></span>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="profile">
                        <div class="save-changes">
                            <button type="submit">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#reset-password-update').on('submit', function (e) {
        e.preventDefault();

        // alert(123123);

        let form    = new FormData(document.forms.resetPasswordUpdate);

        axios.post("/reset-password/secure", form)
            .then(function (response) {
                // console.log(response.data)
                if(response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        text: response.data.message
                    });
                    window.location.href = "/";
                } else if(response.data.status === 0){
                    Swal.fire({
                        icon: 'warning',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>