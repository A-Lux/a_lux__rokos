<?php

use yii\helpers\Url;

?>
<div class="purchase-page">
    <div class="container">
        <h1><?= $purchase->name ?></h1>
        <div class="purchases-inner-row">
            <div class="row">
                <div class="col-xl-6">
                    <div class="purchses-wrapper">
                        <div class="purchase-content">
                            <h2>Метод закупки</h2>
                            <p><?= $purchase->procurementMethodName ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Этап закупки</h2>
                            <p><?= $purchase->purchaseStageName; ?></p>
                            <p><?= $purchase->stagesPurchaseDate ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Заказчик</h2>
                            <p><?= $purchase->customerName ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Валюта</h2>
                            <p><?= $purchase->currencyName ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Срок поставки</h2>
                            <p><?= $purchase->delivery_time ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="purchses-wrapper">
                        <div class="purchase-content">
                            <h2>Наименование закупки</h2>
                            <p> <?= $purchase->name ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Ед. измерения</h2>
                            <p> <?= $purchase->unitName ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Условия поставки</h2>
                            <p><?= $purchase->delivery_condition ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Количество</h2>
                            <p><?= $purchase->quantity ?></p>
                        </div>
                        <div class="purchase-content">
                            <h2>Условие оплаты</h2>
                            <p><?= $purchase->payment_condition ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Описание</h2>
            <?= strip_tags($purchase->description) ?>
        </div>
        <div class="purchase-inner-bottom">
            <? if(Yii::$app->user->isGuest): ?>
                <a href="#" data-toggle="modal" data-target="#loginModal">
                    <div class="tehnical-task download-doc">
                        <p>Техническое задание</p>
                    </div>
                </a>
                <a href="#" data-toggle="modal" data-target="#loginModal">
                    <div class="documents download-doc">
                        <p>Документация</p>
                    </div>
                </a>
            <div class="request-btn" data-toggle="modal" data-target="#loginModal">
                <a href="#">
                    <? if(!is_null($userPurchase) && ($userPurchase->status_sent == 1)): ?>
                        <button>Посмотреть заявку</button>
                    <? elseif(!$userPurchase->status_approved == 1): ?>

                    <? else:?>
                        <button>Подать заявку</button>
                    <? endif; ?>
                </a>

            </div>

            <? else: ?>
                <a href="<?= !is_null(\app\models\Purchases::getJsonToArray($purchase->docs))
                    ? Url::to(['/purchases/documentations', 'id' => $purchase->id])
                    : '#' ?>">
                    <div class="tehnical-task download-doc">
                        <p>Техническое задание</p>
                    </div>
                </a>
                <a href="<?= !is_null(\app\models\Purchases::getJsonToArray($purchase->tech_task))
                    ? Url::to(['/purchases/technical-tasks', 'id' => $purchase->id])
                    : '#' ?>" >
                    <div class="documents download-doc">
                        <p>Документация</p>
                    </div>
                </a>
                <div class="request-btn">
                    <a href="<?= Url::to(['purchases/admission','id'=>$purchase->id])?>">
                        <? if(!is_null($userPurchase) && ($userPurchase->status_sent == 1)): ?>
                            <button>Посмотреть заявку</button>
                        <? else: ?>
                            <button>Подать заявку</button>
                        <? endif; ?>
                    </a>

                </div>
            <? endif; ?>

        </div>
    </div>
</div>