<?php

use yii\helpers\ArrayHelper;
use app\models\PurchaseRequests;
use app\models\Purchases;

?>
<div class="admission profile">
    <div class="container">
        <div class="profile-inner">
            <div class="page-title">
                <h1>Заявка на участие</h1>
                <p>Статус заявки:
                    <span>
                        <? if(!is_null($userPurchase)): ?>
                            <? if(!($userPurchase->status_approved == 0)): ?>
                                <?= $userPurchase->status_approved == 2
                                    ? 'Принято'
                                    : 'не принято' ?>
                            <? else: ?>
                                <?= $userPurchase->status_sent == 1 ? 'Отправлено' : 'Не отправлено' ?>
                            <? endif; ?>
                       <? else: ?>
                            Не отправлено
                        <? endif; ?>
                    </span>
                </p>
            </div>
        </div>
        <div class="page-title">
            <h1>Основное</h1>
        </div>
        <form name="purchaseRequest" id="purchase-request" enctype="multipart/form-data">

            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->getCsrfToken() ?>"/>

            <input type="hidden" name="purchase_id" value="<?= $purchase->id ?>">
            <input type="hidden" name="purchase_stage" value="<?= $purchase->purchase_stage ?>">
            <input type="hidden" name="procurement_method" value="<?= $purchase->procurement_method ?>">

            <div class="row">
                <div class="col-xl-3">
                    <div class="profile-inner">
                        <div class="page-title">
                            <p><b>Заказчик</b></p>
                            <p><?= $purchase->customerName ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="profile-inner">
                        <div class="page-title">
                            <p><b>Наименование закупки</b></p>
                            <p><?= $purchase->name ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="profile-inner">
                        <div class="page-title">
                            <p><b>Пользователь</b></p>
                            <p><?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : ' ' ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="profile-inner request-inform">
                        <div class="page-title">
                            <p><b>Этап закупки</b>(<?= ArrayHelper::getValue(
                                Purchases::purchaseStageDescription(),
                                $purchase->purchase_stage); ?>)
                            </p>
                            <p>
                                <?= $purchase->stagesPurchaseDate ?>
                            </p>
                        </div>
                        <!-- <div class="request-stage">
                            <img src="/public/dist/images/sync_24px_sharp.svg" alt="">
                            <p class="request-p">Ваша заявка в обработке и может быть еще <br> изменена</p>
                        </div> -->
                    </div>
                </div>
                <div class="col-xl-4 col-12 col-md-6">
                    <div class="profile-inner">
                        <div class="profile-lists">
                            <p><b>Наименование товара</b></p>
                            <input type="text" name="name_product"
                                value="<?=
                                (!is_null($userPurchase) && !empty($userPurchase->name_product)) ?
                                    $userPurchase->name_product
                                    : ' '
                                ?>"
                            >
                        </div>
                    </div>
                    <div class="profile-inner">
                        <div class="profile-lists">
                            <p><b>Стоимость с НДС </b></p>
                            <input type="text" name="price"
                                value="0">
<!--                                --><?//=
//                                (!is_null($userPurchase) && !empty($userPurchase->price)) ?
//                                    $userPurchase->price
//                                    : '0'
//                                ?><!--"-->
<!--                            >-->
                        </div>
                        <div class="profile-lists">
                            <p><b>в том числе НДС</b></p>
                            <input type="text" name="vat"
                                   value="0">
<!--                                   --><?//=
//                                   (!is_null($userPurchase) && !empty($userPurchase->vat)) ?
//                                       $userPurchase->vat
//                                       : '0'
//                                   ?><!--" >-->
                        </div>
                        <div class="profile-lists">
                            <p><b>Условия поставки</b></p>
                            <input type="text" name="delivery_condition"
                                   value="<?= //                                   (!is_null($userPurchase) && !empty($userPurchase->delivery_condition)) ?
//                                       $userPurchase->delivery_condition :
                                       $purchase->delivery_condition
                                   ?>" >
                        </div>
                        <div class="profile-lists">
                            <p><b>Условия оплаты</b></p>
                            <input type="text" name="payment_condition"
                                   value="<?=
//                                   (!is_null($userPurchase) && !empty($userPurchase->payment_condition)) ?
//                                       $userPurchase->payment_condition
//                                       :
                                       $purchase->payment_condition
                                   ?>" >
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12 col-md-6">
                    <div class="profile-inner">
                        <div class="profile-lists">
                            <p><b>Участник закупки</b></p>
                            <input type="text"
                                   value="<?= Yii::$app->user->identity->company_name ?>" disabled>
                        </div>
                    </div>
                    <div class="profile-inner">
                        <div class="profile-lists">
                            <p><b>Валюта</b></p>
                            <input type="text" name="currency" required>
<!--                                   --><?//=
//                                   (!is_null($userPurchase) && !empty($userPurchase->currency)) ?
//                                       $userPurchase->currency
//                                       : $purchase->currencyName
//                                   ?><!--"-->
<!--                                   required>-->
                        </div>
                        <div class="profile-lists">
                            <p><b>Ед. измерения</b></p>
                            <input type="text" name="unit"
                                   value="0">
<!--                                   --><?//=
//                                   (!is_null($userPurchase) && !empty($userPurchase->unit)) ?
//                                       $userPurchase->unit
//                                       : $purchase->unitName
//                                   ?><!--" required>-->
                        </div>
                        <div class="profile-lists">
                            <p><b>Количество </b></p>
                            <input type="text" name="quantity"
                                   value="0"
<!--                                   --><?//=
//                                   (!is_null($userPurchase) && !empty($userPurchase->quantity)) ?
//                                       $userPurchase->quantity
//                                       : $purchase->quantity
//                                   ?><!--" required>-->
                        </div>
                        <div class="profile-lists">
                            <p><b>Срок поставки </b></p>
                            <input type="text" name="delivery_time"
                                   value="">
<!--                                   --><?//=
//                                   (!is_null($userPurchase) && !empty($userPurchase->delivery_time)) ?
//                                       $userPurchase->delivery_time
//                                       : $purchase->delivery_time
//                                   ?><!--" required>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-12 col-md-6">
                    <div class="profile-inner">
                        <div class="profile-lists">
                            <p><b>Описание Заказчика</b></p>
                            <div class="about-request">
                                <textarea style="width: 100%; height: 100%;" type="text" class="textarea_descr" name="description"><?=$purchase->description;?></textarea>
<?//=(!is_null($userPurchase) && !empty($userPurchase->description)) ?
//                                        $userPurchase->description :
//                                        $purchase->description;?>
<!--                                </textarea>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <input type="hidden" name="purchase" value="<?= $purchase->id; ?>">
            <? if($purchase->stage_first == 1): ?>
                <div class="page-title">
                    <h1>Формы</h1>
                    <h3>Этап №1 </h3>
                    <p>
                        <?= $purchase->stageFirst->start_date; ?>
                        - <?= $purchase->stageFirst->end_date; ?>
                    </p>
                </div>
                <div class="profile-inner">
                    <div class="row">
                        <? if ($purchase->stageFirst): ?>
                            <?php foreach ($purchase->stageFirst->allFiles as $file): ?>
                                <div class="col-xl-5">
                                    <div class="profile-lists profile-docs">
                                        <p><b>
                                                <?= $file->title ?>
                                                <?= $file->is_required == 1 ? ' *' : ''; ?>
                                            </b></p>
                                        <?php if ($file->type == \app\models\StageFirstDocs::FILE): ?>
                                            <label for="myfile-<?= $file->id ?>" class="file-label">Прикрепите файл</label>
                                            <input type="file" class="my" id="myfile-<?= $file->id ?>"
                                                   name="files[]" placeholder="Прикрепите файл"
                                                   multiple="multiple" data-id="<?= $file->id ?>"
                                                   data-title="<?= $file->title ?>"
                                                <?= $file->is_required == 1 ? 'required' : ''; ?>>

                                        <?php else: ?>
                                            <input type="text">
                                        <?php endif; ?>

                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <div class="col-xl-5">
                                <div class="profile-lists profile-docs">
                                    <p>
                                        Если Вы планируете загрузить дополнительные документы, то
                                        нужно загрузить весь список документов заново.
                                    </p>
                                </div>
                            </div>
                            <? if(!is_null($files)): ?>
                                <?php foreach ($files as $file): ?>
                                    <div class="col-xl-5">
                                        <div class="profile-lists profile-docs">
                                            <p>
                                                <a href="<?= '/uploads/files/request-files/' . $file['file'] ?>" target="_blank">
                                                    <?= $file['name']; ?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <? endif; ?>
                        <? endif; ?>

                    </div>
                </div>

            <? endif; ?>
            <? if($purchase->stage_second == 1): ?>
                <div class="page-title">
                    <h3>Этап №2 </h3>
                        <p>
                            <?= $purchase->stageSecond->start_date; ?>
                            - <?= $purchase->stageSecond->end_date; ?>
                        </p>
                </div>
                <div class="profile-inner">
                    <div class="row">
                        <? if ($purchase->stageSecond): ?>
                            <?php foreach ($purchase->stageSecond->allFiles as $file): ?>
                                <div class="col-xl-5">
                                    <div class="profile-lists profile-docs">
                                        <p><b>
                                                <?= $file->title ?>
                                                <?= $file->is_required == 1 ? ' *' : ''; ?>
                                            </b></p>
                                        <?php if ($file->type == \app\models\StageFirstDocs::FILE): ?>
                                            <label for="myfile2-<?= $file->id ?>" class="file-label">Прикрепите файл</label>
                                            <input type="file" class="my" id="myfile2-<?= $file->id ?>"
                                                   name="files[]" placeholder="Прикрепите файл"
                                                   multiple="multiple" data-id="<?= $file->id ?>"
                                                   data-title="<?= $file->title ?>"
                                                <?= $file->is_required == 1 ? 'required' : ''; ?>>
                                        <?php else: ?>
                                            <input type="text" name="de">
                                        <?php endif; ?>

                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="col-xl-5">
                                <div class="profile-lists profile-docs">
                                    <p>
                                        Если Вы планируете загрузить дополнительные документы, то
                                        нужно загрузить весь список документов заново.
                                    </p>
                                </div>
                            </div>
                            <? if(!is_null($files)): ?>
                                <?php foreach ($files as $file): ?>
                                    <div class="col-xl-5">
                                        <div class="profile-lists profile-docs">
                                            <p>
                                                <a href="<?= '/uploads/files/request-files/' . $file['file'] ?>" target="_blank">
                                                    <?= $file['name']; ?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <? endif; ?>
                        <? endif; ?>
                    </div>
                </div>

            <? endif; ?>

            <? if($purchase->requestPurchase): ?>
                <? if(!is_null($userPurchase) && ($userPurchase->status_approved == 1)): ?>


                <? else: ?>
                    <div class="save-changes">
                        <button class="sent-purchase" type="submit">Отправить заявку</button>
                    </div>
                    <div class="task-inform">
                        <p>Перед тем как отправить заявку ознакомьтесь с
                            <a href="<?= !is_null(\app\models\Purchases::getJsonToArray($purchase->docs))
                                ? \yii\helpers\Url::to(['/purchases/documentations',
                                    'id' => $purchase->id])
                                : '#' ?>">
                                “Техническим заданием”
                            </a>
                        </p>
                    </div>

                    <div class="save-request-changes">
                        <button class="saved-purchase"  type="submit">Сохранить</button>
                    </div>
                <? endif; ?>

            <? endif; ?>
        </form>

        <div class="modal fade modal-login" id="sendRequest" tabindex="-1" role="dialog"
            aria-labelledby="sendRequestLabel" aria-hidden="true">
            <div class="modal-dialog profile-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="email-info">
                            <p>Ваша заявка была отправлена. Для внесения изменений и отправки измененной
                                заявки перейдите в личный кабинет в категорию</p>
                            <a href="/profile">Мои заявки</a>
                        </div>
                        <div class="login-btn">
                            <button>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $("body").on('click', '.sent-purchase', function(e){
        e.preventDefault();
        let filesInputs = document.querySelectorAll('.my');
        let arr = []
        let checked = []
        let notchecked = []
        filesInputs.forEach(item => {
            if (item.attributes.required) {
                checked.push(item)
            }

            if (item.files[0] == undefined) {
                return false;
            } else {
                arr.push(item.files[0])
            }
        })

        let form = new FormData(document.forms.purchaseRequest);
        form.append('files', arr)

        checked.forEach(checkedItem => {
            if (checkedItem.files[0] == undefined) {
                notchecked.push(checkedItem)
            }
        })
        if (notchecked.length != 0) {
            Swal.fire({
                icon: 'warning',
                customClass: 'swalModalProfile',
                html: `
                    <p>Загрузите документ в поле ${checked.map(item => {
                        return item.getAttribute('data-title')
                    })}</p>
                `,
            });
        } else {
            axios.post('/purchase-request/sent-purchase', form)
                .then(function (response) {
                    if (response.data.status === 1) {
                        Swal.fire({
                            icon: 'success',
                            customClass: 'swalModalProfile',
                            html: `
                                <p>${response.data.message}</p>
                                <a href='http://rokos.kz/profile'>Мои заявки</a>
                            `,
                        });
                    } else if (response.data.status === 0) {
                        Swal.fire({
                            icon: 'warning',
                            customClass: 'swalModalProfile',
                            html: `
                                <p>${response.data.message}</p>
                                <a href='http://rokos.kz/profile'>Мои заявки</a>
                            `,
                        });
                    }
                })
                .catch(function (error) {
                    Swal.fire({
                        icon: 'error',
                        customClass: 'swalModalProfile',
                        text: error.response.data.message
                    });
                });
        }
    });

    $("body").on('click', '.saved-purchase', function(e){
        e.preventDefault();
        let filesInputs = document.querySelectorAll('.my');
        let arr = []
        let fileId
        filesInputs.forEach(item => {
            if (item.files[0] == undefined) {
                return false;
            } else {
                arr.push(item.files[0])
                fileId = item.getAttribute('data-id')
                item.files[0].file_id = fileId
                console.log(item.files[0])
            }
        })
        let form = new FormData(document.forms.purchaseRequest);
        form.append('files', arr)
        form.append('file_id', fileId)
        // if (arr.length == 0) {
        //     Swal.fire({
        //         icon: 'warning',
        //         customClass: 'swalModalProfile',
        //         html: `
        //             <p>Нужно загрузить файл</p>
        //         `,
        //     });
        // } else {
            axios.post('/purchase-request/save-purchase', form)
            .then(function (response) {

                console.log(response.data);
                if (response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        customClass: 'swalModalProfile',
                        html: `
                            <p>${response.data.message}</p>
                            <a href='http://rokos.kz/profile'>Мои заявки</a>
                        `,
                    });
                } else if (response.data.status === 0) {
                    Swal.fire({
                        icon: 'warning',
                        customClass: 'swalModalProfile',
                        html: `
                            <p>${response.data.message}</p>
                            <a href='http://rokos.kz/profile'>Мои заявки</a>
                        `,
                    });
                }
            })
            .catch(function (error) {
                Swal.fire({
                    icon: 'error',
                    customClass: 'swalModalProfile',
                    text: error.response.data.message
                });
            });
        // }
    });

</script>
