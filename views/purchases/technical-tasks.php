<?php

use app\models\Purchases;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $purchase Purchases */

$files      = Purchases::getJsonToArray($purchase->tech_task);

?>

<div class="purchases">
    <div class="container">
        <div class="purchases-top">
            <div class="row">
                <div class="col-xl-12 col-14 col-md-12">
                    <div class="purchases-title">
                        <h1>Документация закупки <?= $purchase->name; ?></h1>
                    </div>
                </div>
                <div class="col-xl-8 col-8 col-md-6">

                </div>
                <div class="col-xl-2 col-6 col-md-2">
                </div>
                <div class="col-xl-1 pl-0 col-6 col-md-2">
                </div>
            </div>
        </div>
        <div class="purchases-select-inner">
            <div class="row">
                <div class="col-xl-3 pr-0 col-md-6">
                </div>
                <div class="col-xl-3 col-md-6">
                </div>
            </div>
        </div>
        <div class="purchases-tables-inner">

            <? foreach ($files as $file): ?>
                <a href="<?= $purchase->getFileLink($file['file']) ?>" target="_blank">
                    <div class="tehnical-task download-doc">
                        <p><?= $file['name'] ?></p>
                    </div>
                </a>

            <? endforeach; ?>

        </div>
        <div class="purchases-bottom">
        </div>
    </div>
</div>
