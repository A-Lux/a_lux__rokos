<?php

use app\models\PurchasingGuide;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager; ?>
<div class="purchases">
    <div class="container">
        <div class="purchases-top mg-btn">
            <div class="row">
                <div class="col-xl-1 col-4 col-md-2">
                    <div class="purchases-title">
                        <h1>Закупки</h1>
                    </div>
                </div>
                <div class="col-xl-8 col-8 col-md-6">
                    <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::current()]); ?>
                    <div class="purchases-search">
                        <?= Html::activeTextInput($searchModel, 'name',
                            [
                                'autocomplete' => 'off',
                                'placeholder' => 'Поиск по закупкам'
                            ]) ?>
                    </div>
                    <!--                    <div class="purchases-search">-->
                    <!--                        <input type="text" placeholder="Поиск по закупкам">-->
                    <!--                    </div>-->
                    <?php ActiveForm::end() ?>
                </div>
                <div class="col-xl-2 col-6 col-md-2">
                    <div class="purchases-inner purchases-date">
                        <div class="purchases-icon">
                            <img src="/public/dist/images/rokos-date.png" alt="">
                        </div>
                        <div class="purchases-info">
                            <p>
                                <?= date("d-m-Y", time()); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 pl-0 col-6 col-md-2">
                    <div class="purchases-inner purchases-time">
                        <div class="purchases-icon">
                            <img src="/public/dist/images/clock.png" alt="">
                        </div>
                        <div class="purchases-info">
                            <p>
                                <?= date("H:i", time()); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="purchases-select-inner">
            <div class="row">
                <div class="col-xl-3 pr-0 col-md-6">
                    <div class="purchases-select purchases-select-left">
                        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::current()]); ?>
                        <?= Html::activeDropDownList($searchModel, 'customer',
                            PurchasingGuide::getCustomersDropDown(),
                            [
                                'prompt' => 'Заказчики',
                                'onchange' => 'this.form.submit()'
//                                'autocomplete' => 'off',
//                                'placeholder' => 'Поиск по закупкам'
                            ]) ?>

                        <?php ActiveForm::end() ?>
                        <!--                        <select name="" id="">-->
                        <!--                            <option value="">Все методы</option>-->
                        <!--                            <option value="">opt2</option>-->
                        <!--                        </select>-->
                    </div>
                </div>
                <div class="col-xl-3 pr-0 col-md-6">
                    <div class="purchases-select purchases-select-left">
                        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::current()]); ?>
                        <?= Html::activeDropDownList($searchModel, 'procurement_type',
                            PurchasingGuide::getProcurementTypesDropDown(),
                            [
                                'prompt' => 'Виды закупки',
                                'onchange' => 'this.form.submit()'
//                                'autocomplete' => 'off',
//                                'placeholder' => 'Поиск по закупкам'
                            ]) ?>

                        <?php ActiveForm::end() ?>
                        <!--                        <select name="" id="">-->
                        <!--                            <option value="">Виды закупки</option>-->
                        <!--                            <option value="">opt2</option>-->
                        <!--                        </select>-->
                    </div>
                </div>
                <div class="col-xl-3 pr-0 col-md-6">
                    <div class="purchases-select purchases-select-left">
                        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::current()]); ?>
                        <?= Html::activeDropDownList($searchModel, 'procurement_method',
                            PurchasingGuide::getProcurementMethodsDropDown(),
                            [
                                'prompt' => 'Метод закупки',
                                'onchange' => 'this.form.submit()'
//                                'autocomplete' => 'off',
//                                'placeholder' => 'Поиск по закупкам'
                            ]) ?>

                        <?php ActiveForm::end() ?>
                        <!--                        <select name="" id="">-->
                        <!--                            <option value="">Этапы закупки</option>-->
                        <!--                            <option value="">opt2</option>-->
                        <!--                        </select>-->
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="purchases-select">
                        <select name="" id="">
                            <option value="">Этапы закупки</option>
                            <option value="">opt2</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="purchases-tables-inner">
            <div class="purchases-top-row">
                <div class="row">
                    <div class="col-auto">
                        <div class="purchases-table-title">
                            <p>№</p>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="purchases-table-title">
                            <p>Заказчик</p>
                        </div>
                    </div>
                    <div class="col-xl-3 p-0 col-md-3">
                        <div class="purchases-table-title">
                            <p>Наименование закупки</p>
                        </div>
                    </div>
                    <div class="col-xl-1 col-md-1">
                        <div class="purchases-table-title">
                            <p>Метод</p>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2 mg-left">
                        <div class="purchases-table-title">
                            <p>Вид закупки</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3">
                        <div class="purchases-table-title">
                            <p>Этап закупки</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach ($dataProvider->getModels() as $index => $purchase): ?>
                <a href="<?= Url::to(['purchases/view', 'id' => $purchase->id]) ?>" class="purchases-column-link">
                    <div class="purchases-medium-row">
                        <div class="row">
                            <div class="col-auto">
                                <div class="purchases-table-info dop-size">
                                    <p><?= $purchase->id ?></p>
                                </div>
                            </div>
                            <div class="col-xl-2 col-md-2">
                                <div class="purchases-table-info">
                                    <p><?= $purchase->customerName ?></p>
                                </div>
                            </div>
                            <div class="col-xl-3 p-0 col-md-3">
                                <div class="purchases-table-info">
                                    <p><?= $purchase->name ?></p>
                                </div>
                            </div>
                            <div class="col-xl-1 col-md-1">
                                <div class="purchases-table-info">
                                    <p><?= $purchase->procurementMethodName ?></p>
                                </div>
                            </div>
                            <div class="col-xl-2 col-md-2 mg-left">
                                <div class="purchases-table-info">
                                    <p><?= $purchase->procurementTypeName ?></p>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-3">
                                <div class="purchases-table-info">
                                    <p>
                                        <?= $purchase->stagesPurchaseDate ?>
                                    </p>
                                    <span><?= $purchase->purchaseStageName; ?></span>
                                    <span>
<!--                                        --><?//= $purchase->purchaseStageName; ?>
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="purchases-bottom">
            <div class="row">
                <div class="col-xl-4 col-12 col-md-12">
                    <div class="purchases-link">
                        <a href="">Пользовательское руководство для поставщиков</a>
                    </div>
                </div>
                <div class="col-xl-5 col-12 col-md-12">
                    <?= LinkPager::widget([
                        'pagination' => $dataProvider->getPagination(),
                    ]); ?>
<!--                    <div class="purchases-pagination">-->
<!--                        <a href="" class="pagination-active">1</a>-->
<!--                        <a href="">2</a>-->
<!--                        <a href="">3</a>-->
<!--                        <a href="">4</a>-->
<!--                        <p>....</p>-->
<!--                        <a href="">16</a>-->
<!--                    </div>-->
                </div>
                <div class="col-xl-3 col-12 col-md-12">
                    <div class="purchases-link">
                        <button type="button" class="btn " data-toggle="modal" data-target="#exampleModal">
                            Подписаться на рассылку
                        </button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Подписаться на рассылку</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>E-mail</p>
                                        <input type="text">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn">Подписаться</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>