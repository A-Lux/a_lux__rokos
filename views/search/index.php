<?php

use yii\helpers\Url;
use app\controllers\SearchController;

?>

<div class="logicstics mt-4">
    <div class="container">
        <div class="inner-title col-md-12">
            <?php
            if ($count):?>
                <? $text = '<span style="color: #bf9e4d; font-weight: bold;">'.$search .'</span>' ?>
                <div class="" style="color: #77cced; font-size: 22px;margin-bottom: 30px;margin-right: 500px;"><?= Yii::t('main', 'Результаты поиска по запросу {text} .', [
                        'text' => $text,
                    ]);?> </div>
            <? else: ?>
                <? $text = '<span
                            style="color: #bf9e4d; font-weight: bold;">'.$search .'</span>' ?>
                <div class="" style="color: #77cced; font-size: 22px;margin-bottom: 300px;"><?= Yii::t('main', 'По запросу ничего {text} не найдено',[
                        'text' => $text,
                    ]);?></div>
            <? endif; ?>
        </div>
    </div>


    <!--About-->
    <? if ($about != null || $advantage != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main-index', 'О компании');?></h2>
                    </div>
                </div>
                <div class="row">
                    <!-- Item units "about company" -->
                    <? foreach ($advantage as $item): ?>
                        <div class="col-sm-4 col-md-3 about_company__item mb-4">
                            <div class="row">
                                <a href="/site/index/#about_company">
                                    <?= $item->getTitle(); ?>
                                </a>
                            </div>
                            <div class="row">
                                <a href="/site/index/#about_company">
                                    <p>
                                        <?= $item->getSubtitle(); ?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <? endforeach; ?>

                </div>
                <div class="row">
                    <div class="col-sm-6 ">
                        <div class="row">
                            <? foreach ($about as $item): ?>
                                <div class="col-sm-12">
                                    <a href="/site/index/#about_company">
                                        <p class="about__text"><?= SearchController::cutStr($item->getText(), 50); ?></p>
                                    </a>
                                </div>
                            <? endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    <? endif; ?>
    <!--End About-->

    <!--PORTFOLIO-->
    <? if($products != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main', 'Портфель');?></h2>
                    </div>
                </div>
                <div class="portfolio mb-5">
                    <div class="container mb-5 mt-5">
                        <div class="row">
                            <? foreach ($products as $item): ?>
                                <div class="col-sm-6 col-md-4 col-lg-3" data-toggle="modal"
                                     data-target="#product-<?= $item->id; ?>">
                                    <div class="portfolio-product-item" data-toggle="modal" data-target="#first">
                                        <img src="<?= $item->getImage(); ?>" alt="">
                                        <p><?= $item->getName(); ?></p>
                                        <h4><?= $item->price ? $item->price . ' тг': ''; ?></h4>
                                        <a href="#" class="more" data-toggle="modal"
                                           data-target="#product-<?= $item->id; ?>"><?= Yii::t('main', 'Подробнее');?></a>
                                    </div>
                                </div>

                                <!-- Модальные окна -->
                                <div class="modal fade" id="product-<?=$item->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-6 text-center">
                                                            <img src="<?= $item->getImage();?>">
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 d-flex flex-column">
                                                            <h2><?= $item->getFullname();?></h2>
                                                            <p><?= $item->weight ? Yii::t('main-portfolio', 'Вес') . ':  ' . $item->weight : ''; ?></p>
                                                            <?= $item->getContent(); ?>
                                                            <p class="production-company"><?= !empty($item->manufacturer) ? 'Производство: '. $item->getManufacturer(): '';?></p>
                                                            <p class="production-category"><?= Yii::t('main-portfolio', 'Категория');?>: <a href="#"><?= $item->lastCatalog($item->catalog); ?></a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Модальные окна -->
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </>
        </section>
    <? endif;?>
    <!--End PORTFOLIO-->

    <!--CAREER-->
    <? if($career != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main', 'Портфель');?></h2>
                    </div>
                </div>
                <div class="carier">
                    <div class="container mb-5 pt-5 pb-5">
                        <div class="row">
                            <div class="container pb-5 pt-5 carier-bg">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                            <div class="container pl-0 pr-0">

                                                <? foreach($career as $item): ?>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <a style="text-decoration: none" href="<?= \yii\helpers\Url::to(['/content/salesman', 'id' => $item->id])?>">
                                                                <h2><?= $item->getName();?></h2></a>
                                                            <p><?= \app\controllers\ContentController::cutStr($item->getContent(), 200)?></p>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <? endif;?>
    <!--End CAREER-->


    <!--News-->
    <? if ($news != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main', 'Новости');?></h2>
                    </div>
                </div>
            </div>
            <div class="container mt-4 mb-4">
                <div class="row">
                    <? for ($i = 0; $i < count($news); $i++): ?>
                        <? if (!isset($news[$i])) continue; ?>
                        <div class="col-sm-12 col-md-4 flex">
                            <a href="<?= Url::to(['content/news-card', 'id' => $news[$i]->id]) ?>">
                                <p><?= $news[$i]->getName(); ?></p>
                            </a>
                        </div>
                    <? endfor; ?>
                </div>
            </div>
        </section>
    <? endif; ?>
    <!--End News-->


    <!--CONTACT-->
    <? if($contact != null): ?>
    <section class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="about_company--h2"><?= Yii::t('main', 'Контакты');?></h2>
                </div>
            </div>
            <div class="contact mb-5">
                <div class="container contact-bg">
                    <div class="row mb-5">
                        <? for($k = 0; $k < count($contact); $k++): ?>
                            <? if(!isset($contact[$k])) continue; ?>
                            <div class="col-sm-12 col-md-4">
                                <h3><?= $contact[$k]->getName()?></h3>
                                <p><?= $contact[$k]->getAddress();?></p>
                                <p><?= $contact[$k]->telephone;?></p>
                                <a href="mailto:<?= $contact[$k]->email;?>"><?= $contact[$k]->email;?></a>
                            </div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <? endif;?>
    <!--End CONTACT-->


    <!--MPK-->
    <? if($mpk != null || $mission != null  || $principles != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main', 'МПК');?></h2>
                    </div>
                </div>
            </div>
        </section>
                <div class="mpk">
                    <div class="mpk__container-logo pt-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <a href="/content/mpk">
                                        <h3><?= $mpk->title; ?></h3>
                                        <p><?= SearchController::cutStr($mpk->description, 150); ?></p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? if($mission != null): ?>
                        <div class="mpk__container-logo pt-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <a href="/content/mpk">
                                            <h3><?= SearchController::cutStr($mission->getMissionTitle(), 150); ?>
                                            </h3>
                                        </a>
                                        <a href="/content/mpk">
                                            <p>
                                                <?= SearchController::cutStr($mission->getMissionContent(), 150); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>

                    <? if($principles != null): ?>
                        <div class="container mb-5">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h1 class="ourPrinciples"><?= Yii::t('main-mpk', 'Наши принципы');?></h1>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <? for ($i = 0; $i < count($principles); $i++): ?>
                                    <? if (!isset($principles[$i])) continue; ?>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="mpk__priniciples">
                                                <img src="<?= $principles[$i]->getImage(); ?>" alt="">
                                            <a href="/content/mpk">
                                                <p><?= $principles[$i]->getName() ?></p>
                                            </a>
                                        </div>
                                    </div>
                                <? endfor; ?>

                            </div>
                        </div>
                    <? endif; ?>
                </div>

    <? endif;?>
    <!--End MPK-->


    <!--LOGISTICS-->
    <? if($logistics_header != null || $logistics_advantage != null || $logistics_customer != null): ?>
        <section class="about_company">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="about_company--h2"><?= Yii::t('main', 'Логистика');?></h2>
                    </div>
                </div>
                <div class="logicstics mt-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <a href="/content/logistics">
                                    <h1><?= $logistics_header->title;?></h1>
                                    <?= SearchController::cutStr($logistics_header->content, 150);?>
                                </a>
                            </div>
                        </div>
                        <? if($logistics_advantage != null): ?>
                            <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h1 class="ourTransportBase"><?= Yii::t('main-logistics', 'Наша транспортная база');?></h1>
                                <div class="container pl-0 pr-0 mt-5">
                                    <div class="row">
                                        <? foreach($logistics_advantage as $item): ?>
                                            <div class="col-sm-12 col-md-3 flexible">
                                                <img src="<?=$item->getImage();?>" alt="">
                                                <div>
                                                    <a href="/content/logistics">
                                                        <h2><?=$item->getTitle();?></h2>
                                                        <p><?=$item->getSubtitle();?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        <? endforeach;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    </div>

                    <? if($logistics_customer != null): ?>
                    <div class="container mt-5">
                        <? foreach ($logistics_customer as $item): ?>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <a href="/content/logistics">
                                        <h3 class="clientPage__HEADER"><?= $item->getTitle();?></h3>
                                        <p>
                                            <?= $item->getContent();?>
                                        </p>
                                    </a>
                                </div>
                            </div>
                        <? endforeach;?>
                    </div>
                    <? endif;?>

                </div>

            </div>
        </section>
    <? endif;?>
    <!--End LOGISTICS-->

    <div class="border mt-4"></div>


</div>