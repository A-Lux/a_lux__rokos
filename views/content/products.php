<div class="row">
    <? foreach ($products as $item): ?>
        <div class="col-sm-6 col-md-4 col-lg-3" data-toggle="modal" data-target="#product-<?=$item->id;?>">
            <div class="portfolio-product-item">
                <img src="<?= $item->getImage();?>" alt="">
                <p><?= $item->getName(); ?></p>
                <h4><?= $item->price ? $item->price . ' тг': ''; ?></h4>
                <a href="#" class="more" data-toggle="modal" data-target="#product-<?=$item->id;?>"><?= Yii::t('main', 'Подробнее');?></a>
            </div>
        </div>

        <!-- Модальные окна -->
        <div class="modal fade" id="product-<?=$item->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 text-center">
                                    <img src="<?= $item->getImage();?>">
                                </div>
                                <div class="col-sm-12 col-md-6 d-flex flex-column">
                                    <h2><?= $item->getFullname();?></h2>
                                    <p><?= $item->weight ? Yii::t('main-portfolio', 'Вес') . ':  ' . $item->weight : ''; ?></p>
                                    <?= $item->getContent(); ?>
                                    <p class="production-company"><?= !empty($item->manufacturer) ? 'Производство: '. $item->getManufacturer(): '';?></p>
                                    <p class="production-category"><?= Yii::t('main-portfolio', 'Категория');?>: <a href="#"><?= $item->lastCatalog($item->catalog); ?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Модальные окна -->
    <? endforeach; ?>
</div>
<a href="#filters" id="mouseNavigation" class="mouse_nav--top">
</a>