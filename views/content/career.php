<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="carier">
    <div class="container mb-5 pt-5 pb-5">
        <? if(!empty($hallOfFame)): ?>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h1><?= Yii::t('main', 'Доска почета')?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="container">
                        <div class="col-sm-12 col-md-12">
                            <div class="slider autoplay1">
                                <? foreach($hallOfFame as $item): ?>
                                <div>
                                    <img src="<?= $item->getImage();?>" alt="">
                                    <div>
                                        <h2 class="fame__nickname"><?= $item->getRank();?></h2>
                                        <h2 class="fame__name"><?= $item->getName();?></h2>
                                        <p><?= $item->getPosition();?></p>
                                    </div>
                                </div>
                                <? endforeach;?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="row">
            <div class="container pb-5 pt-5 carier-bg">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1><?= Yii::t('main-career', 'Развивайтесь вместе с нами!');?></h1>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                            <p>
                                <?= Yii::t('main-career', 'Группа компаний RGG уделяет большое значение развитию своих сотрудников и считает
                                своим долгом поддерживать амбициозных людей в их стремлении добиваться успехов
                                вместе с нашей командой.');?>
                                <?= Yii::t('main-career', 'Мы понимаем, что в своем стремлении к успеху наша главная
                                ценность – это наши сотрудники. Именно поэтому приглашаем к сотрудничеству
                                талантливых людей готовых стать частью нашей команды.');?>
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <select class="vacancy__select select-category">
                                        <option selected="" hidden=""><?= Yii::t('main-career', 'Отрасль');?></option>
                                        <? foreach ($category as $item): ?>
                                            <option value="<?= $item->id;?>" class="career-category" data-id="<?= $item->id;?>">
                                                <?= $item->name?>
                                            </option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <select class="vacancy__select select-city">
                                        <option selected="" hidden=""><?= Yii::t('main-career', 'Город');?></option>
                                        <? foreach ($city as $item): ?>
                                            <option value="<?= $item->id;?>" class="career-city" data-id="<?= $item->id;?>">
                                                <?= $item->name?>
                                            </option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="career-filter mt-4" id="career-filter">
                            <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                <div class="container pl-0 pr-0">
                                    <? foreach ($vacancy as $item): ?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a style="text-decoration: none"
                                                   href="<?= \yii\helpers\Url::to(['/content/salesman', 'id' => $item->id]) ?>">
                                                    <h2><?= $item->getName(); ?></h2></a>
                                                <p><?= \app\controllers\ContentController::cutStr($item->getContent(), 200) ?></p>
                                            </div>
                                        </div>
                                    <? endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-sm-12 col-md-5">
                        <?= $this->render('/blocks/_form_career', [
                            'careerFeedback' => $careerFeedback
                        ]); ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
