
<div class="container">
    <div class="inner-title">
        <?php
        if($count){?>
            <? $text = '<span style="color: #bf9e4d; font-weight: bold;">'.$search .'</span>' ?>
            <div class="" style="color: #77cced; font-size: 22px;margin-bottom: 30px;margin-right: 500px;"><?= Yii::t('main', 'Результаты поиска по запросу {text} .', [
                    'text' => $text,
                ]);?> </div>
        <?}else{?>
            <? $text = '<span style="color: #bf9e4d; font-weight: bold;">'.$search .'</span>' ?>
            <div class="" style="color: #77cced; font-size: 22px;margin-bottom: 300px;"><?= Yii::t('main', 'По запросу ничего {text} не найдено',[
                    'text' => $text,
                ]);?></div>
        <?}?>
    </div>
</div>

<? if(count($products) != 0):?>

    <div class="row">
        <? foreach ($products as $item): ?>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
                    <img src="<?= $item->getImage();?>" alt="">
                    <p><?= $item->getName(); ?></p>
                    <h4><?= $item->price ? $item->price . ' тг': ''; ?></h4>
                    <a href="#" class="more" data-toggle="modal" data-target="#product-<?=$item->id;?>"><?= Yii::t('main', 'Подробнее');?></a>
                </div>
            </div>

            <!-- Модальные окна -->
            <div class="modal fade" id="product-<?=$item->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <img src="<?= $item->getImage();?>">
                                    </div>
                                    <div class="col-sm-12 col-md-6 d-flex flex-column">
                                        <h2><?= $item->getFullname();?></h2>
                                        <p><?= $item->weight ? Yii::t('main-portfolio', 'Вес') . ':  ' . $item->weight : ''; ?></p>
                                        <?= $item->getContent(); ?>
                                        <p class="production-company"><?= !empty($item->manufacturer) ? 'Производство: '. $item->getManufacturer(): '';?></p>
                                        <p class="production-category"><?= Yii::t('main-portfolio', 'Категория');?>: <a href="#"><?= $item->lastCatalog($item->catalog); ?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Модальные окна -->
        <? endforeach; ?>
    </div>

<? endif;?>