<div class="logicstics mt-4">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1><?= $header->getTitle();?></h1>
                <?= $header->getContent();?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourTransportBase"><?= Yii::t('main-logistics', 'Наша транспортная база');?></h1>
                <div class="container pl-0 pr-0 mt-5">
                    <div class="row">
                        <? foreach($advantage as $item): ?>
                            <div class="col-sm-12 col-md-3 flexible">
                            <img src="<?=$item->getImage();?>" alt="">
                            <div>
                                <h2><?=$item->getTitle();?></h2>
                                <p><?=$item->getSubtitle();?></p>
                            </div>
                        </div>
                        <? endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="logicstics-geography">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-5 mt-5">
                    <div class="geography-form__block">
                        <h1><?= Yii::t('main-logistics', 'География наших перевозок');?></h1>
                        <ul>
                            <? foreach($geography as $item): ?>
                            <li>
                                <p><?=$item->country;?></p>
                            </li>
                            <? endforeach;?>

                        </ul>
                        <a href="#" class="becomeClient" data-toggle="modal" data-target="#beCustomer"><?= Yii::t('main', 'Стать клиентом');?></a>


                    </div>
                </div>
                <div class="col-sm-12 col-md-7"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourClientBase"><?= Yii::t('main-logistics', 'Наша клиентская база');?></h1>
            </div>
        </div>
        <div class="row">
            <? for($i = 0; $i < count($customer); $i++): ?>
            <?php if(!isset($customer[$i])) continue;?>
                <div class="col-sm-12 col-md-6">
                    <div class="clientsBase <?= $customer[$i]->statusActing == 1 ? 'acting leftClient' : 'rightClient'; ?>" style="background-image: url('<?= $customer[$i]->getImage();?>')">
                        <div>
                            <h1><?= $customer[$i]->getTitle();?></h1>
                            <p><?= $customer[$i]->getSubtitle();?></p>
                            <a href="#" class="becomeClient" data-toggle="modal" data-target="#beCustomer"><?= Yii::t('main', 'Стать клиентом');?></a>
                        </div>
                    </div>
                </div>
            <? endfor;?>

        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3 class="clientPage__HEADER"><?= $customerBase->getTitle();?></h3>
                <p>
                   <?= $customerBase->getContent();?>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 pl-0 pr-0">
                <div class="clientImages">
                    <? foreach($images as $item): ?>
                            <a href="<?=$item->getImage();?>">
                                <img src="<?=$item->getImage();?>">
                            </a>
                    <? endforeach;?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="line mt-4 mb-5">
    </div>
    <? if(!empty($partners)): ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourClientBase"><?= Yii::t('main-logistics', 'С нами сотрудничают');?></h1>
            </div>
        </div>
    </div>
    <div class="container mb-5" style="max-width: 1000px;">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="slider ourPartnersAutoplay">
                    <? foreach($partners as $item): ?>
                        <div>
                        <img src="<?= $item->getImage();?>" alt="Бриг">
                        <h4><?=$item->name;?></h4>
                    </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <? endif; ?>
</div>

<?= $this->render('/blocks/_form_be_customer');?>


