<div class="portfolio mb-5">
    <div class="container mb-5">
        <div class="row mt-5 product-buttons">
            <? $i = 0; ?>
            <? foreach ($catalog as $item): ?>
                <? $i++; ?>
                <div id="catalog-level-1" class="col-sm-12 col-md-6 <?= $i % 2 == 1 ? 'partners-production acting' : 'own-production' ?> pr-production">
                    <a href="#" class="category_filter categories_button" data-id="<?= $item->id; ?>"
                       next="#firstCategory">
                        <div class="container">
                            <div class="row">
                                <div class="col-4 <?= $i % 2 == 1 ? 'handshakke' : 'baloons' ?> ">
                                    <p style="opacity: 0">1</p>
                                </div>
                                <div class="col-8">
                                    <p><?= $item->getName(); ?></p>
                                </div>
                            </div>
                        </div>
                    </a>

                </div>
            <? endforeach; ?>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3 product-search">
                <input type="text" placeholder="<?= Yii::t('main-portfolio', 'Поиск по названию');?>" class="search-products">
            </div>
            <div class="col-sm-12 col-md-9 selected-category" id="filters">
                <div class="container pl-0 pr-0 h-100">
                    <div class="row h-100">
                        <div class="col-sm-12 col-md-4">
                            <select id="firstCategory" class="product-types" next="#secondCategory">
                                <option selected hidden class="category-p"><?= Yii::t('main-portfolio', 'Тип товара');?></option>
                                <? foreach ($category as $item): ?>
                                    <? if ($item->level == 2): ?>
                                        <option value="<?= $item->id ?>" class="categories_button"
                                                data-id="<?= $item->id; ?>"><?= $item->getName(); ?></option>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <select id="secondCategory" class="product-types" next="#thirdCategory" disabled>
                                <option selected hidden class="category-p"><?= Yii::t('main-portfolio', 'Бренды');?></option>
                                <? foreach ($catalog as $item): ?>
                                    <? if ($item->level == 3): ?>
                                        <option value="<?= $item->id ?>" class="categories_button"
                                                data-id="<?= $item->id; ?>"><?= $item->getName(); ?></option>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <select id="thirdCategory" class="product-types" disabled>
                                <option selected hidden class="category-p"><?= Yii::t('main-portfolio', 'Категории');?></option>
                                <? foreach ($catalog as $item): ?>
                                    <? if ($item->level == 4): ?>
                                        <option value="<?= $item->id ?>" class="categories_button"
                                                data-id="<?= $item->id; ?>"><?= $item->getName(); ?></option>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="logo__wrapper">
                    <img src="">
                </div>
            </div>
            <div class="col-sm-9 col-md-9"></div>
        </div>
    </div>
    <div id="products-filter" style="position: relative" class="container mb-5 mt-5">
        <div class="row">
            <? foreach ($products as $item): ?>
                <div class="col-sm-6 col-md-4 col-lg-3" data-toggle="modal" data-target="#product-<?=$item->id;?>">
                    <div class="portfolio-product-item">
                        <img src="<?= $item->getImage();?>" alt="">
                        <p><?= $item->getName(); ?></p>
                        <h4><?= $item->price ? $item->price . ' тг': ''; ?></h4>
                        <a href="#" class="more" data-toggle="modal" data-target="#product-<?=$item->id;?>"><?= Yii::t('main', 'Подробнее');?></a>
                    </div>
                </div>

                <!-- Модальные окна -->
                <div class="modal fade" id="product-<?=$item->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 text-center">
                                            <img src="<?= $item->getImage();?>">
                                        </div>
                                        <div class="col-sm-12 col-md-6 d-flex flex-column">
                                            <h2><?= $item->getFullname();?></h2>
                                            <p><?= $item->weight ? Yii::t('main-portfolio', 'Вес') . ':  ' . $item->weight : ''; ?></p>
                                            <?= $item->getContent(); ?>
                                            <p class="production-company"><?= !empty($item->manufacturer) ? 'Производство: '. $item->getManufacturer(): '';?></p>
                                            <p class="production-category"><?= Yii::t('main-portfolio', 'Категория');?>: <a href="#"><?= $item->lastCatalog($item->catalog); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Модальные окна -->
            <? endforeach; ?>
        </div>

        <div class="evenMoreContent">

        </div>
<!--        <div class="row mt-4">-->
<!--            <div class="cols-sm-12 col-md-12">-->
<!--                <span class="evenMore">Подробнее</span>-->
<!--            </div>-->
<!--        </div>-->
        <a href="#filters" id="mouseNavigation" class="mouse_nav--top">
        </a>
    </div>
    <!-- Начало -->
    <!-- Конец -->
</div>
