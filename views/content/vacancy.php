<div class="col-sm-12 col-md-12 pl-0 pr-0">
    <div class="container pl-0 pr-0">
        <? foreach ($vacancy as $item): ?>
            <div class="row">
                <div class="col-sm-12">
                    <a style="text-decoration: none"
                       href="<?= \yii\helpers\Url::to(['/content/salesman', 'id' => $item->id]) ?>">
                        <h2><?= $item->getName(); ?></h2></a>
                    <p><?= \app\controllers\ContentController::cutStr($item->getContent(), 200) ?></p>
                </div>
            </div>
        <? endforeach; ?>

    </div>
</div>