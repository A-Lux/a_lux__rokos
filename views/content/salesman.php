<div class="vacancy">
    <a href="/content/career" class="backward"><i class="fas fa-arrow-alt-circle-left"></i></a>
    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div>
                    <h1 class="vacancy-header"><?= $vacancy->getName();?></h1>
                    <p class="vacancy-address"><?= !empty($vacancy->address) ? $vacancy->getAddress() : ''; ?></p>
                    <?= !empty($vacancy->schedule) ? '<p class="vacancy_employment-type">' . $vacancy->getSchedule() . '</p>' : ''; ?>

                    <h3><?= $vacancy->getContent();?></h3>
                    <ul class="vacancy-categories">
                        <? if(!empty($vacancyDescriptions)): ?>
                            <? foreach($vacancyDescriptions as $item): ?>
                                <li>
                                    <p><?= $item->getDescription();?></p>
                                </li>
                            <? endforeach; ?>
                        <? endif;?>
                    </ul>

                    <? if(!empty($vacancyResponsibilities)): ?>
                        <h2 class="vacancy_duties-header"><?= Yii::t('main-salesman', 'Обязанности');?>:</h2>
                        <ul class="vacancy-duties">
                            <? foreach ($vacancyResponsibilities as $item): ?>
                                <li>
                                    <p><?= $item->getResponsibility(); ?></p>
                                </li>
                            <? endforeach;?>
                        </ul>
                    <? endif; ?>

                    <? if(!empty($vacancyRequirements)): ?>
                        <h2 class="vacancy_requires-header"><?= Yii::t('main-salesman', 'Требования');?>:</h2>
                        <ul class="vacancy-requires">
                            <? foreach($vacancyRequirements as $item): ?>
                                <li>
                                    <p><?= $item->getRequirement(); ?></p>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>

                    <? if(!empty($vacancyConditions)): ?>
                        <h2 class="vacancy_conditions-header"><?= Yii::t('main-salesman', 'Условия');?>:</h2>
                        <ul class="vacancy-conditions">
                            <? foreach($vacancyConditions as $item): ?>
                                <li>
                                    <p><?= $item->getCondition(); ?></p>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>

                    <? if(!empty($vacancy->email)): ?>
                        <h2 class="vacancy_summary-header"><?= Yii::t('main-salesman', 'Резюме принимаются на электронный адрес');?>:</h2>
                        <a href="mailto:<?= $vacancy->email;?>"><?= $vacancy->email;?></a>
                    <? endif; ?>

                    <? if(!empty($vacancy->by_phone)): ?>
                        <h2 class="vacancy_tinquiries-header"><?= Yii::t('main-salesman', 'Справки по телефону');?>:</h2>
                        <?= $vacancy->getByPhone(); ?>
                    <? endif; ?>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="resume_container">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="71"
                         height="70" viewBox="0 0 71 70">
                        <image id="big-new-email" width="71" height="70"
                               xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEcAAABGCAMAAACpMnmIAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABJlBMVEXE5fH////E5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fEAAABOUfaUAAAAYHRSTlMAAAFiwuAStzLf+AebH8tH7Xz9EbMv3S5e9gaWHMlC6ngOr64s2lr1WQWRGsVA6NugkD9w/PplZGxrDdwEtrAKJgu+hhmtiIy1ewLu8II7b27jXF0lYH68A0P55tmsPnZSU8KEAAAAAWJLR0RhsrBMhgAAAAlwSFlzAAALEgAACxIB0t1+/AAAAAd0SU1FB+MKCwwcKBttUO8AAAJlSURBVFjD7ddpV9pAFIDhSTAq7qK1LojYAtalYkG7iKJVXLq5tFatlXr//69oJkNCkpm5M5Nw+qnvx+TmOUkmJxBCFNmZPqcvY0v3WywV0z8AtIH+dM5gFljZwTROZgj8hjKJneERCDcynMwZHYNoY6NJnPEJiDcxbuzYkzngy03aZs7UNIibnjJxns2ArJnn+s7sHMibm9V05hcAb2Fex8kvgqrFvNopLCkZgKWCwrGLOQ3GfQCKNuYsv9BSaC+X5U6prM0AlEsyp7JiwACsvBI6q2tGCm1tlXfWN4wZgI31uPN609tR3Xqj11bVm9+sRZ36Nt2681b+Po9nv9uhh2zXw06JneZ7bYX2gR1UCjm7De8k94ycPe9WVHfD15Wn0L4RQ8i+e0wjH73PFGoaOs0uY5GDwwAydxp5tu6HH8kRHPtQ4LROHHknrcBxGc85hiPiAJx2oDN/4Bx9+M79sTP6GnKZUwCHOvCJbf/sD3xBna/+mDdvWd/Ad6AYvXD8fKLLahWh68SgVhO5P81WZNZjAid+RtoxpuskhDpMyEkE+UzYSQAFTMQJQReXdVmXFwIm6gTQFbruVzwTc3zoGnWueSbudKAs6mR5hnMY9B37Uc394BneYdBN5aesyo2AEThayx9nRI4GxDFCRwnxjNhRQAJG4qCQiJE5CCRkpI4UEjNyRwJJGMQRQjIGcwSQlEEdDpIzuBODEEbhRCCMUTkhCGWUDtyyf3r2LaRz4K5wT+5/3UFaB+Dh94NyRsfR6b/zr5zHnjiPpN0Tp01qPXH+EPLUA+aJfl/U2mUnTeW2+zH3F2m490oaP7TCAAAAAElFTkSuQmCC"/>
                    </svg>
                    <p><?= Yii::t('main', 'Если вы не нашли подходящую вакансию, вы можете прислать нам свое резюме.');?></p>
                    <form action="">
                        <a href="#resume"><?= Yii::t('main', 'Отправить резюме');?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="vacancy-bg">
        <div class="container mt-5 pt-5">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <div class="apply">
                        <h1 class="apply-header"><?= Yii::t('main', 'Подать заявку');?></h1>
                        <?= $this->render('/blocks/_form_vacancy_resume', ['position' => $vacancyPosition])?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">

                </div>
            </div>
        </div>
    </div>
</div>
