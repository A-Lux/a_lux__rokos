<div class="vegetables mb-5">
    <a href="/content/news" class="backward"><i class="fas fa-arrow-alt-circle-left"></i></a>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 mt-5">
                <h1><?= $news->getName();?></h1>
                <img src="<?= $news->getImage();?>">

                <?= $news->getContent();?>
            </div>

        </div>
    </div>
</div>
