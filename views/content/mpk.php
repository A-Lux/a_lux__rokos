<div class="mpk">
    <div class="mpk__container-logo pt-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h3><?= $mpk->getTitle(); ?></h3>
                    <p><?= $mpk->getDescription(); ?></p>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12 col-md-5 mpk__flexible">
                    <? foreach ($advantage as $item): ?>
                        <div class="mpk__advantages">
                            <h1><?= $item->getTitle();?></h1>
                            <img src="<?= $item->getImage();?>" alt="">
                            <?= $item->getText();?>
                        </div>
                    <? endforeach; ?>

                </div>
                <div class="col-sm-12 col-md-1"></div>
                <div class="col-sm-12 col-md-4">
                    <div class="mpk__production">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h1><?= Yii::t('main', 'Продукция компании');?></h1>
                            </div>
                            <div class="container mt-4">

                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="mpk__production-logo">
                                            <img src="/public/dist/images/mpk__img-03.png" alt="">
                                            <p>«Дәстүр Дәмі»</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="mpk__production-logo">
                                            <img src="/public/dist/images/mpk__img-04.png" alt="">
                                            <p>«МПК»</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-4">
                                <p>Колбасные изделия, деликатесы, фарши, пельмени.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2"></div>
            </div>
        </div>
    </div>
    <div class="mission mt-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 mt-4 mb-4">
                    <h1><?= $mpk->getMissionTitle(); ?>
                    </h1>
                    <p>
                        <?= $mpk->getMissionContent(); ?>
                    </p>
                    <a href="#" class="becomeClient" data-toggle="modal" data-target="#beCustomer"><?= Yii::t('main', 'Стать клиентом');?></a>
                </div>
                <div class="col-sm-12 col-md-4"></div>

                <?= $this->render('/blocks/_form_be_customer');?>
            </div>
        </div>
    </div>
    <div class="container mpk-production mb-5 mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="mpk-production__header"><?= Yii::t('main-mpk', 'Продукция МПК');?></h1>
            </div>
            <div class="col-sm-12 col-md-12">
                <p>
                    <?= $mpk->getContent(); ?>
                </p>
            </div>
        </div>
        <div class="row">
            <? if($promo != null): ?>
            <div class="col-sm-12 col-md-3">
                <div class="mpk-production__img-block">
                    <img src="<?= $promo[0]->getImage();?>" alt="">
                    <h4><?= $promo[0]->getName();?></h4>
                    <p><?= $promo[0]->getCategory();?></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="mpk-production__img-block">
                    <img src="<?= $promo[1]->getImage();?>" alt="">
                    <h4><?= $promo[1]->getName();?></h4>
                    <p><?= $promo[1]->getCategory();?></p>
                </div>
            </div>
            <? endif; ?>
            <div class="col-sm-12 col-md-6">
                <img src="<?= ($images[0]->getImage()) ? $images[0]->getImage() : '';?>" class="right__block-img" alt="">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6">
                <img src="<?= ($images[1]->getImage()) ? $images[1]->getImage() : '';?>" class="left__block-img" alt="">
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="container pl-0 pr-0">
                    <div class="row">
                        <? if($promo[2] != null):?>
                        <div class="col-sm-12 col-md-6">
                            <div class="mpk-production__img-block">
                                <img src="<?= $promo[2]->getImage();?>" alt="">
                                <h4><?= $promo[2]->getName();?></h4>
                                <p><?= $promo[2]->getCategory();?></p>
                            </div>
                        </div>
                        <? endif;?>
                        <div class="col-sm-12 col-md-6">
                            <div class="mpk-production__look-forward">
                                <h1><?= Yii::t('main', '<span>Посмотреть</span> полный каталог');?></h1>
                                <? if($catalog->status == 0): ?>
                                    <a href="<?= $catalog->link?>" target="_blank" class="lookForward"><?= Yii::t('main', 'Перейти');?></a>
                                <? else: ?>
                                    <a href="<?= $catalog->link?>" target="_blank" class="lookForward"><?= Yii::t('main', 'Перейти');?></a>
                                <? endif;?>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 3.4rem">
                        <div class="col-sm-12 col-md-12">
                            <img src="<?= ($images[2]->getImage()) ? $images[2]->getImage() : '';?>" class="right__block-img" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourPrinciples"><?= Yii::t('main-mpk', 'Наши принципы');?></h1>
            </div>
        </div>
        <div class="row mt-3">
            <? for ($i = 0; $i < 3; $i++): ?>
                <? if (!isset($principles[$i])) continue; ?>
                <div class="col-sm-12 col-md-4">
                    <div class="mpk__priniciples">
                        <img src="<?= $principles[$i]->getImage(); ?>" alt="">
                        <p><?= $principles[$i]->getName() ?></p>
                    </div>
                </div>
            <? endfor; ?>

        </div>
        <div class="row mt-5">
            <? for ($i = 3; $i < count($principles); $i++): ?>
                <? if (!isset($principles[$i])) continue; ?>
                <div class="col-sm-12 col-md-4">
                    <div class="mpk__priniciples">
                        <img src="<?= $principles[$i]->getImage(); ?>" alt="">
                        <p><?= $principles[$i]->getName(); ?></p>
                    </div>
                </div>
            <? endfor; ?>
        </div>
    </div>
</div>
