<?php

use yii\helpers\Url;
?>

<div class="news">
	<div class="container mt-4 mb-4">
		<div class="row">
            <? for($i = 0; $i < 3; $i++): ?>
            <? if(!isset($news[$i])) continue; ?>
			<div class="col-sm-12 col-md-4 flex">
				<a href="<?= Url::to(['content/news-card', 'id' => $news[$i]->id])?>">
					<img src="<?=$news[$i]->getImage();?>">
					<p class="date"><?=$news[$i]->getDate();?></p>
					<p><?= $news[$i]->getName();?></p>
				</a>
			</div>
            <? endfor; ?>

		</div>
	</div>
	<div class="container mb-4">
		<div class="row">
            <? for($i = 3; $i < 6; $i++): ?>
                <? if(!isset($news[$i])) continue; ?>
                <div class="col-sm-12 col-md-4 flex">
                    <a href="<?= Url::to(['content/news-card', 'id' => $news[$i]->id])?>">
                        <img src="<?=$news[$i]->getImage();?>">
                        <p class="date"><?=$news[$i]->getDate();?></p>
                        <p><?= $news[$i]->getName();?></p>
                    </a>
                </div>
            <? endfor; ?>
		</div>
	</div>
	<div class="container mb-5">
		<div class="row">
            <? for($i = 6; $i < 9; $i++): ?>
                <? if(!isset($news[$i])) continue; ?>
                <div class="col-sm-12 col-md-4 flex">
                    <a href="<?= Url::to(['content/news-card', 'id' => $news[$i]->id])?>">
                        <img src="<?=$news[$i]->getImage();?>">
                        <p class="date"><?=$news[$i]->getDate();?></p>
                        <p><?= $news[$i]->getName();?></p>
                    </a>
                </div>
            <? endfor; ?>
		</div>
	</div>
	<!-- Начало -->
	<!-- Модальные окна -->
	<!-- <div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 flex">
									<img src="/public/dist/images/gallery-image_01.png">
									<p class="date">6 ноября 2017</p>
									<p>Участие в выставке WorldFood Kazakhstan</p>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- Конец -->
</div>

