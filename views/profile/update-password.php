<div class="reset-password">
    <div class="container">
        <a href="/profile"> <img src="/public/dist/images/arrow_back_24px.svg" alt="">
            Изменение пароля
        </a>
        <form id="update-password" name="updatePassword">

            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->getCsrfToken() ?>" />

            <div class="password-inner">
                <div class="form-group">
                    <label class="col-md-4 control-label">Введите старый пароль</label>
                    <div class="col-md-4">
                        <input  type="password"
                                class="form-control "
                                name="password" placeholder="********"
                        >
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Введите новый пароль</label>
                    <div class="col-md-4">
                        <input id="password-field-2" type="password"
                               class="form-control"
                               name="new_password"
                               placeholder="********">
                        <span toggle="#password-field-2" class="fa fa-fw fa-eye field-icon toggle-password-2"></span>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="profile">
                        <div class="save-changes">
                            <button type="submit">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#update-password').on('submit', function (e) {
        e.preventDefault();

        // alert(123123);

        let form    = new FormData(document.forms.updatePassword);

        axios.post("/profile/secure", form)
            .then(function (response) {
                console.log(response)
                if(response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        text: response.data.message
                    });
                    // window.location.href = "/profile";
                } else if(response.data.status === 0){
                    Swal.fire({
                        icon: 'warning',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>