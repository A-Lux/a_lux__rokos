<?php

use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\PurchaseRequests;

$user = Yii::$app->user->identity;

?>

<div class="profile content-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-personal-tab" data-toggle="pill" href="#v-pills-personal"
                       role="tab" aria-controls="v-pills-personal" aria-selected="true"> <i class="far fa-user"></i>
                        Персональные данные</a>
                    <a class="nav-link" id="v-pills-requests-tab" data-toggle="pill" href="#v-pills-requests" role="tab"
                       aria-controls="v-pills-requests" aria-selected="false"> <i class="far fa-clipboard"></i> Мои
                        заявки</a>
                    <a href="/login/logout" class="logout-link"><img src="/public/dist/images/input_24px.svg" alt="">Выйти</a>
                </div>
            </div>
            <? if ($user): ?>
                <div class="col-xl-10">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-personal" role="tabpanel"
                             aria-labelledby="v-pills-personal-tab">
                            <form name="updateProfile" id="update-profile">

                                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                                       value="<?= Yii::$app->request->getCsrfToken() ?>"/>

                                <div class="profile-inner">
                                    <div class="profile-lists">
                                        <p><b>Наименование компании</b></p>
                                        <input type="text" placeholder="Имя"
                                               name="company_name" value="<?= $user->company_name; ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>БИН</p>
                                                <input type="text" placeholder="3214242332352"
                                                       name="bin" value="<?= $user->bin; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>Адрес</p>
                                                <input type="text" placeholder="Курмангазы 25"
                                                       name="address" value="<?= $user->address; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>Страна</p>
                                                <input type="text" placeholder="Казахстан"
                                                       name="country" value="<?= $user->country; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>ФИО Представителя</p>
                                                <input type="text" placeholder="Неснов Николай Николаевич"
                                                       name="name" value="<?= $user->name; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>Вид деятельности</p>
                                                <input type="text" name="activity_type"
                                                       value="<?= $user->activity_type; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>Должность</p>
                                                <input type="text" name="position"
                                                       value="<?= $user->position; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>Телефон</p>
                                                <input type="text" placeholder="8 777 454 25 25"
                                                       name="phone" value="<?= $user->phone; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <p>E-mail</p>
                                                <input type="text" placeholder="hardmetall1@gmai.com"
                                                       name="email" value="<?= $user->email; ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="profile-inner">
                                            <div class="profile-lists">
                                                <div class="password-form">
                                                    <div class="password-form-top">
                                                        <p>Пароль</p>
                                                        <a href="/reset-password/index">Забыли пароль?</a>
                                                    </div>
                                                    <div class="password-form-input">
                                                        <input type="text" placeholder="********" disabled>
                                                        <a href="/profile/update-password">
                                                            <img src="/public/dist/images/create.png" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-12">
                                        <div class="save-changes">
                                            <button type="submit">Сохранить изменения</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="v-pills-requests" role="tabpanel"
                             aria-labelledby="v-pills-requests-tab">
                            <div class="purchases">
                                <div class="purchases-top mg-btn">
                                    <div class="purchases-search purchases-search-top">
                                        <input type="text" placeholder="Поиск по закупкам">
                                    </div>
                                </div>
                                <div class="purchases-tables-inner">
                                    <div class="purchases-top-row">
                                        <div class="row">
                                            <div class="purchases-num">
                                                <div class="purchases-table-title">
                                                    <p>№</p>
                                                </div>
                                            </div>
                                            <div class="col-xl-2">
                                                <div class="purchases-table-title">
                                                    <p>Заказчик</p>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 p-0">
                                                <div class="purchases-table-title">
                                                    <p>Наименование закупки</p>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="purchases-table-title">
                                                    <p>Этап закупки</p>
                                                </div>
                                            </div>
                                            <div class="col-xl-2">
                                                <div class="purchases-table-title">
                                                    <p>Стоим-сть с НДС</p>
                                                </div>
                                            </div>
                                            <div class="col-xl-1">
                                                <div class="purchases-table-title">
                                                    <p>Статус заявки</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $count = 1; ?>
                                    <? if (!is_null($userPurchases)): ?>
                                        <? foreach ($userPurchases as $userPurchase): ?>
<!--                                                                                        <a href="/uploads/files/purchases/--><?//= json_decode($userPurchase->purchase->protocol)[0]->file ?><!--">-->
<!--                                                                                            Протокол: --><?//= json_decode($userPurchase->purchase->protocol)[0]->name ?>
<!--                                                                                        </a>-->
                                            <a href="<?= \yii\helpers\Url::to(['/profile/purchase',
                                                'purchase_id' => $userPurchase->purchase->id,
                                                'request_id' => $userPurchase->id
                                            ]) ?>"
                                               class="purchases-column-link">
                                                <div class="purchases-medium-row">
                                                    <div class="row">
                                                        <div class="purchases-num">
                                                            <div class="purchases-table-info">
                                                                <p><?= $count; ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2">
                                                            <div class="purchases-table-info">
                                                                <p><?= $userPurchase->purchase->customerName; ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 p-0">
                                                            <div class="purchases-table-info">
                                                                <p><?= $userPurchase->purchase->name; ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-6">
                                                            <div class="purchases-table-info">
                                                                <p>
                                                                    <?= $userPurchase->purchase->stagesPurchaseDate ?>
                                                                </p>
                                                                <span>
                                                                    <? if ($userPurchase->status_approved == 1 && $userPurchase->purchase_stage == 1): ?>
                                                                        <?= 'Прием заявок 1 этапа' ?>

                                                                    <? elseif ($userPurchase->status_approved == 1 && $userPurchase->purchase_stage == 3): ?>
                                                                        <?= 'Завершение 1 этапа' ?>

                                                                    <? elseif ($userPurchase->status_approved == 1 && $userPurchase->purchase_stage == 4): ?>
                                                                        <?= 'Прием заявок 2 этапа' ?>

                                                                    <? elseif ($userPurchase->status_approved == 1 && $userPurchase->purchase_stage == 6): ?>
                                                                        <?= 'Завершение 2 этапа' ?>

                                                                    <? else: ?>
                                                                        <?= $userPurchase->purchase->purchaseStageName; ?>
                                                                    <? endif; ?>
                                                                </span>
<!--                                                                <span>2</span>-->
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-6">
                                                            <div class="purchases-table-info">
                                                                <p><?= $userPurchase->price . ' ' . $userPurchase->currency; ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-1 col-6">
                                                            <div class="purchases-table-info">
                                                                <p class="purchases-active">
                                                                    <? if (!($userPurchase->status_approved == 0)): ?>
                                                                        <?= $userPurchase->status_approved == 2
                                                                            ? 'Принято'
                                                                            : 'не принято' ?>
                                                                    <? else: ?>
                                                                        <?= $userPurchase->status_sent == 1
                                                                            ? 'Отправлено'
                                                                            : 'Сохранено' ?>
                                                                    <? endif; ?>
<!--                                                                    --><?// print_r('<pre>');print_r($userPurchases);die; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <?php $count++; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>


<script>
    $('#update-profile').on('submit', function (e) {
        e.preventDefault();
        // alert(123123);

        let form = new FormData(document.forms.updateProfile);

        axios.post("/profile/update", form)
            .then(function (response) {
                console.log(response)
                if (response.data.status === 1) {
                    Swal.fire({
                        icon: 'success',
                        customClass: 'swalModalProfile',
                        text: response.data.message
                    });
                    // window.location.href = "/profile";
                } else if (response.data.status === 0) {
                    Swal.fire({
                        icon: 'warning',
                        customClass: 'swalModalProfile',
                        text: response.data.message
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    text: error.response.data.message
                });
            });
    });
</script>