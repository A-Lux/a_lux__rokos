<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';

$config = [
    'id' => 'basic',
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '8FTHIDwCElvGbo2tZ4zipwV41uDJZLDE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'main*' => [
                    'class'             => yii\i18n\DbMessageSource::className(),
                    'sourceLanguage'    => 'ru',
                    'sourceMessageTable'=>'{{%source_message}}',
                    'messageTable'      =>'{{%message}}',
                    'forceTranslation'  => true,
                ],
                'form*' => [
                    'class'             => yii\i18n\DbMessageSource::className(),
                    'sourceLanguage'    => 'ru',
                    'sourceMessageTable'=>'{{%source_message}}',
                    'messageTable'      =>'{{%message}}',
                    'forceTranslation'  => true,
                ],
            ],
        ],
        'db' => $db,

        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfNkcgUAAAAAJ4U-gI7MY6DVqHB-FMrNRwH5zt2',
            'secret' => '6LfNkcgUAAAAAOh76kzxhlPHG6aTlI4XmTOVsCk-',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
//            'language' => 'ru-RU',
            'class' => 'yii\i18n\Formatter',
        ],

    ],

    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],


    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
