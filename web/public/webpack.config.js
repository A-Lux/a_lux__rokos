const path = require("path");


module.exports = {
   entry:["./src/js/main.js"],
   output: {
       path: path.resolve(__dirname, "dist"),
       filename: "js/bundle.js"
   },
   devServer:{
       contentBase: "./dist"
   },
   module:{
       rules:[
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader'
          }
        }
      ]
   }
};