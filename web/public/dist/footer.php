<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-3 px-0">
                    <div class="row">
                        <img class="footerLogoPic" src="images/internal-footer_logo.svg" alt="">
                    </div>
                    <div class="row mobile-ver">
                        <div class="col-sm-3 col-md-3 col-lg-2">
                            <object class="svgClass" type="image/svg+xml" data="instagram.svg">

                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                    <defs>
                                        <style>
                                            .cls-1,
                                            .cls-2 {
                                                fill: #262626;
                                            }

                                            .cls-2 {
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>footer_instagram</title>
                                    <path class="cls-1"
                                        d="M211.09,42.89a49.51,49.51,0,1,1-35,14.5,49.31,49.31,0,0,1,35-14.5Zm32.3,17.24a45.71,45.71,0,1,0,13.35,32.28,45.65,45.65,0,0,0-13.35-32.28Z"
                                        transform="translate(-161.57 -42.89)" />
                                    <path class="cls-2"
                                        d="M198.42,92.41a12.68,12.68,0,1,1,12.67,12.69,12.69,12.69,0,0,1-12.67-12.69Zm25.77-24.82H198a11.77,11.77,0,0,0-11.74,11.74v26.18A11.79,11.79,0,0,0,198,117.25h26.18a11.78,11.78,0,0,0,11.74-11.74V79.33a11.76,11.76,0,0,0-11.74-11.74ZM198,70.53h26.18a8.8,8.8,0,0,1,8.8,8.8v26.18a8.81,8.81,0,0,1-8.8,8.8H198a8.81,8.81,0,0,1-8.8-8.8V79.33a8.8,8.8,0,0,1,8.8-8.8Zm28.79,3.7a2.58,2.58,0,1,0,2.58,2.57,2.57,2.57,0,0,0-2.58-2.57Zm.3,18.18a16,16,0,1,0-16,16,16,16,0,0,0,16-16Z"
                                        transform="translate(-161.57 -42.89)" />
                                </svg>
                            </object>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                            <object class="svgClass" type="image/svg+xml" data="whatsup.svg">

                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 99.04 99.04">
                                    <defs>
                                        <style>
                                            .cls-1,
                                            .cls-2 {
                                                fill: #262626;
                                            }

                                            .cls-2 {
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>footer_whatsup</title>
                                    <path class="cls-1"
                                        d="M463,42.89a49.52,49.52,0,1,1-35,14.5,49.39,49.39,0,0,1,35-14.5Zm32.27,17.24a45.64,45.64,0,1,0,13.38,32.28,45.51,45.51,0,0,0-13.38-32.28Z"
                                        transform="translate(-413.46 -42.89)" />
                                    <path class="cls-2"
                                        d="M488.23,91.14a24.81,24.81,0,0,0-49.57-.46,7.46,7.46,0,0,0-.05,1.07,24.59,24.59,0,0,0,3.56,12.72l-4.5,13.22,13.77-4.36a25,25,0,0,0,12,3,24.72,24.72,0,0,0,24.84-24.62c0-.22,0-.39,0-.61Zm-13.43,5.6c-.63-.3-3.59-1.78-4.14-2s-1-.31-1.4.33a27.49,27.49,0,0,1-1.92,2.33c-.36.44-.71.46-1.29.19a15.37,15.37,0,0,1-4.91-3,18.11,18.11,0,0,1-3.4-4.14c-.32-.66-.05-.93.25-1.29a7,7,0,0,0,.93-1,1,1,0,0,0,.22-.27,5.5,5.5,0,0,0,.39-.72,1,1,0,0,0-.06-1c-.11-.3-1.34-3.32-1.86-4.5s-1-1-1.35-1-.76,0-1.18,0a2.06,2.06,0,0,0-1.61.76,6.5,6.5,0,0,0-2.17,5,6.33,6.33,0,0,0,.36,2,13.19,13.19,0,0,0,2.14,4.19c.3.41,4.19,6.69,10.39,9.11s6.22,1.59,7.32,1.5,3.59-1.42,4.11-2.88A5,5,0,0,0,476,97.4a3.31,3.31,0,0,0-1.15-.66Zm-11.38,15.74A20.85,20.85,0,0,1,452,109.05l-8,2.55,2.58-7.71a20.54,20.54,0,0,1-4-12.14,16.64,16.64,0,0,1,.11-2,20.88,20.88,0,0,1,41.57.38c0,.55,0,1.1,0,1.62a20.83,20.83,0,0,1-20.86,20.73Z"
                                        transform="translate(-413.46 -42.89)" />
                                </svg></object>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                            <object class="svgClass" type="image/svg+xml" data="facebook.svg">

                                <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 99.04 99.04">
                                    <defs>
                                        <style>
                                            .cls-1,
                                            .cls-2 {
                                                fill: #262626;
                                            }

                                            .cls-2 {
                                                fill-rule: evenodd;
                                            }
                                        </style>
                                    </defs>
                                    <title>footer_facebook</title>
                                    <path class="cls-1"
                                        d="M85.15,42.89a49.51,49.51,0,1,1-35,14.5,49.36,49.36,0,0,1,35-14.5Zm32.3,17.24a45.69,45.69,0,1,0,13.36,32.28,45.62,45.62,0,0,0-13.36-32.28Z"
                                        transform="translate(-35.64 -42.89)" />
                                    <path class="cls-2"
                                        d="M88.94,82.29V78.23a2.12,2.12,0,0,1,2.22-2.41h5.67V67.15H89c-8.69,0-10.64,6.42-10.64,10.59v4.55h-5V92.41h5.1v25.28H88.55V92.41H96l.33-4,.6-6.14Z"
                                        transform="translate(-35.64 -42.89)" />
                                </svg></object>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                            <object class="svgClass" type="image/svg+xml" data="telegram.svg">

                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="26px" height="26px">
                                    <image x="0px" y="0px" width="26px" height="26px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAQAAAADQ4RFAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCQUPBCFFk8OoAAACUElEQVQ4y42Uz2tTQRDHP0ljEENMgz0EawVBMNJOJAhKaEEwkkPUS8il9lJbKdY/od6Egge9CUKpvRQpaC1GSw/FxIORoCktZq3JSRDS8iiBWiI9FJN6yMvLvhp/zLu8nZnvzndmv7sObCYOIiQYIIgP2KFElgVyal/PctogcVZ5iZv7XMCPnwgP8ZBiVeK0M/HKnGzJLTn0W8Qt41KROfE2PQ4zEGCZDW6obdpv2cUs3cSUYYHEy3s+MaxqYCPf3BOQDmYI06+qzZ6m2GwH0deqxggbTFntS0W6GintPhBfk6RsSRwQh6zJnT9B5LAMyhvZtUiOy5o4XEToYbpd86EwIwzhB1Ys5xPuEXGR4LnaO5DeyRCjhIEifiBvdbYn8yScDJDR0p2haOgpBo8I840JGt3ktQGlGXAR5LMJ6GGYm5wC4CcPWOYFfgA+ajTWCbrwYZjLDKfNv3fc5jiLHAHgB6XWeWHg07V3hiiz7DLJJc6yZEJgrVCzn5+LHQJsA6g6mf1MKIub6zzTpPzBhgiw46JEH0XN+Yop9mzqX9HlRC8lJ1ku61orGHi4SJ0l6qYzb6sUJetkgaS4be5lPMQKV7nCJlApfG2FxE2SBSc5yozqtXhMXyENhbec4zV5G7lRyuRsgm2ZpT2HHNXqHGsItnGf5ujkmqrxV5MOUlTVYPM+jdHNjHT8AzLNScbABKkqMcIsHiSpEyPFeWKqaoFAGfTznS8yfmCSNB4WilTpV4Y1LC0cZ5ITzJNmHQMI0EuUJGXuqiXbhG2w/3osfwEq/sy/t8WxjAAAAABJRU5ErkJggg==" />
                                </svg>
                            </object>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 ">
                    
                        <a href="" class="nav_item__footer">О RGG</a>
                        <a href="" class="nav_item__footer">Портфель</a>
                        <a href="" class="nav_item__footer">Карьера</a>
                        <a href="" class="nav_item__footer">Новости</a>
                        <a href="" class="nav_item__footer">Контакты</a>
                        <a href="" class="nav_item__footer">МПК</a>
                        <a href="" class="nav_item__footer">Логистика</a>
          
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mobile-flex">
                    <p class="footer_address__p">
                        РК, г. Астана
                        ул. Пушкина, 75<br>
                        +7 7172 677 285
                        info@rokos.kz
                    </p>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3  mobile-flex">
                    <div class="row">
                        <p class="order_p">Обратный звонок</p>
                    </div>
                    <div class="row">
                        <a href="" class="order_btn" href="#"  data-toggle="modal" data-target="#exampleModal" >
                            <p>
                                Заказать
                            </p>

                        </a>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Обратный звонок</h5>
                                            
                                            </div>
                                            <div class="modal-body">
                                              <form>
                                                <div class="form-group">
                                                  <label for="recipient-name" class="col-form-label">Имя:</label>
                                                  <input type="text" class="form-control" id="recipient-name">
                                                  <label for="recipient-name" class="col-form-label">Телефон:</label>
                                                  <input type="text" class="form-control" id="recipient-name">
                                                  <label for="recipient-name" class="col-form-label">Должность:</label>
                                                  <input type="text" class="form-control" id="recipient-name">
                                                </div>
                                                <div class="form-group">
                                                  <label for="message-text" class="col-form-label"></label>
                                                  <textarea class="form-control" id="message-text"></textarea>
                                                </div>
                                              </form>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn order_btn ml-0" data-dismiss="modal">Закрыть</button>
                                              <button type="button" class="btn order_btn ml-0">Отправить</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                    </div>
                </div>
                <div class="offset-sm-1">

                </div>
            </div>
        </div>
        <div class="row footer_flex--border">
            <div class="container">
                <div class="footer_flex">
                    <div class="col-sm-12 col-md-5">
                        <p>
                            2006 - 2019 © Rokos Group Global
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-7 px-5">
                        <p>
                            Разработано в:
                            <a href="">
                                <img src="images/a-lux.png" alt="">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="js/hc-offcanvas-nav.js"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
    <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="js/bundle.js"></script>
    <script>
        var headerInner = window.location.pathname ;
setTimeout(() => {
    if (headerInner != "/index.php" && headerInner != "/") {
        document.querySelector(".menu__upper__layer").classList.add("innerHeader");
        document.querySelector(".lang-version").classList.add("innerHeader");
        document.querySelector(".lang-version-mob").classList.add("innerHeader");
        document.querySelector(".logoPic").src = "images/internal-logo.png";
        document.querySelector(".header").classList.add("innerHeaderMaxHeight");
        // document.querySelector(".absolute_backround-slider").style.display = "none";  
        // document.querySelector(".mouse_nav--absolute").style.display = "none";  
        document.querySelector(".hc-nav-trigger").classList.add("innerHeader");

        document.querySelector(".hc-nav-trigger").addEventListener("click", () => {
            document.querySelector(".menu__sub__layer").children[0].children[0].classList.toggle("InputBackground");
        })
        
    }; 
    
}, 500);


if (headerInner == "/index.php" || headerInner == "/") {
    
        // window.glide = new Glide(".glide_header", {
        //     autoplay: 3000,
        //     type: 'carousel',
        //     hoverpause: false,
        // }).mount()
        // window.glide1 = new Glide('.glide', {
        //     autoplay: 3000,
        //     hoverpause: false,
        //     type: 'carousel',
        //     startAt: 0,
        //     perView: 5,
        //     breakpoints: {
        //         600: {
        //             perView: 3
        //         }
        //     }
        // }).mount()

        // console.log( "window.glide")
    }
    
    var scroll = new SmoothScroll('a[href*="#"]');
    
    
    
    </script>
    <script>
        $(document).ready(function() {
            var $disabledResults = $("#cities");
            $disabledResults.select2();
            $('#cities').select2({
                minimumResultsForSearch: Infinity
            });

        });
    </script>
    <script>
        $(document).ready(function() {
            $('.select2-selection__arrow')[0].innerHTML="<span></span>"
        })
    </script>
    <script>
        $(document).ready(function() {
            $('html').on('click', function(e) {
                if (typeof $(e.target).data('original-title') == 'undefined' &&
                     !$(e.target).parents().is('.popover.in')) {
                    $('[data-original-title]').popover('hide');
                  }
            });
        })
    </script>
</body>

</html>