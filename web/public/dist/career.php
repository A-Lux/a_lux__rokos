  
  <?php require_once("header.php"); ?>

<div class="carier">
	<div class="container mb-5 carier-bg pt-5 pb-5">
		<div class="row">
		<div class="container mb-5">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<p>Группа компаний RGG уделяет большое значение развитию своих сотрудников и считает
					своим долгом поддерживать талантливых людей в их стремлении добиваться успехов
					вместе с нашей командой. Мы понимаем, что в своем стремлении к успеху наша главная
					ценность – это наши сотрудники. Именно поэтому приглашаем к сотрудничеству
					талантливых людей готовых стать частью нашей команды.</p>
				</div>
			</div>
		</div>	
			
			<div class="col-sm-12 col-md-6">
				<div class="container">
					
					<div class="row">
						<div class="col-sm-12">
							<a style="text-decoration: none" href="/salesman.php"><h2>Супервайзер торговых представителей "КОМО"</h2></a>
							<p>В фокусную команду международного бренда «Комо» требуется супервайзер торговых
представителей...</p>
							
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<a style="text-decoration: none" href="/salesman.php"><h2>Торговый представитель</h2></a>
							<p>В ТОО "RGG" в связи с расширением пакета требуются Торговые представители следующих
категорий...</p>
							
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<a style="text-decoration: none" href="/salesman.php"><h2>Супервайзер торговых представителей "КОМО"</h2></a>
							<p>В фокусную команду международного бренда «Комо» требуется супервайзер торговых
представителей...</p>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 flex-ai-s">
				<form action="" method="">
					<p>Если вы хотите сотрудничать с нами, заполните следущие поля:</p>
					<input type="text" placeholder="Контактное лицо" class="mb-3">
					<select name="cities" class="cities mb-3">
						<option value="" hidden selected>Город</option>
						<option value="Алматы">Алматы</option>
						<option value="Астана">Астана</option>
						<option value="Шымкент">Шымкент</option>
						
					</select>
					<select name="cities" class="cities mb-3">
						<option value="" hidden selected>Должность</option>
						<option value="vac1">Вариант1</option>
						<option value="vac2">Вариант2</option>
						<option value="vac3">Вариант3</option>
						
					</select>
					<input type="text" placeholder="Email" class="mb-3">
					<input type="text" placeholder="Телефон" class="mb-3">
					<div class="container mb-3">
						<div class="row">
							<div class="col-12 flex pr-0 pl-0">
								
								<label class="custom-file-upload pr-5 pt-1 pl-4 pb-1"><input type="file">Выберите файл</label>
							</div>
						</div>
					</div>
					
					<input class="pb-2 pt-2 mb-5" type="submit" value="Отправить">
				</form>
			</div>
		</div>
	</div>
</div>

<?php require_once("footer.php"); ?>