<?php require_once("header.php"); ?>
<div class="logicstics mt-5">
    <div class="logicstics__container-logo">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1>Доверьте груз профессионалам!</h1>
                    <p>Мы полностью отвечаем за Ваш груз с момента поступления его к нам, до его отгрузки со склада.</p>
                    <p>Рады предложить Вам услуги по перемещению груза автомобильным и железнодорожным транспортом, услуги 
                    СВХ и таможенной очистки грузов, складирование, разгрузку и погрузку, документальное оформление и отчетность.
                    </p>
                    <h3>Сотрудничая с нами Вы получаете дополнительные преимущества:</h3>
                    <p>логистические комплексы, оборудованные парком весоизмерительных приборов и механизацией ручного труда, возможность аренды офисных помещений.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1 class="ourTransportBase">Наша транспортная база</h1>
                    <div class="container pl-0 pr-0 mt-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-3 flexible">
                                <img src="images/logicstics-01.png" alt="">
                                <div>
                                    <h2>44</h2>
                                    <p>единицы транспорта</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 flexible">
                                <img src="images/logicstics-02.png" alt="">
                                <div>
                                    <h2>20</h2>
                                    <p>парковочных мест для грузового автотранспорта</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 flexible">
                                <img src="images/logicstics-03.png" alt="">
                                <div>
                                    <h2>4,5 - 6 м</h2>
                                    <p>рабочая высота потолка</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 flexible">
                                <img src="images/logicstics-04.png" alt="">
                                <div>
                                    <h2>от -18 °С до +5 °С</h2>
                                    <p>температура хранения</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="logicstics-geography mt-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5 mt-5">
                    <div class="geography-form__block">
                        <h1>География наших перевозок</h1>
                        <ul>
                            <li>
                                <p>Казахстан</p>
                            </li>
                            <li>
                                <p>Россия</p>
                            </li>
                            <li>
                                <p>Беларусь</p>
                            </li>
                            <li>
                                <p>Европа</p>
                            </li>
                        </ul>
                        <a href="#" class="becomeClient">Стать клиентом</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourClientBase">Наша клиентская база</h1>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6">
                <div class="clientsBase acting leftClient" onclick="return showClients('clientPage__01','clientPage__02')">
                    <div>
                        <h1>24 тысяч</h1>
                        <p>Общее количество контрагентов</p>
                        <a href="#" class="becomeClient">Стать клиентом</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="clientsBase rightClient" onclick="return showClients('clientPage__02','clientPage__01')">
                    <div>
                        <h1>14 тысяч</h1>
                        <p>Оптово-розничных клиентов в дистрибьюции</p>
                        <a href="#" class="becomeClient">Стать клиентом</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5" id="clientPage__01">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3>Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических центров в Астане - 8000 м2, в Актобе - 6500 м2, а в Шымкенте - 2500 м2.</h3>
                <p>
                Технические параметры логистических центров «Рокос»: одноэтажное складское здание со встроенным АБК, бетонный пол с упрочняющим антипылевым покрытием с допустимой нагрузкой 6 т/м2, пандусы для работ с автотранспортом, перрон для приемки железнодорожных подвижных составов.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 pl-0 pr-0">
                <div class="clientImages">
                    <div>
                        <img src="images/logicstics-clientbase_img-01.png">
                    </div>
                    <div>
                        <img src="images/logicstics-clientbase_img-02.png">
                    </div>
                    <div>
                        <img src="images/logicstics-clientbase_img-03.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5" id="clientPage__02"  style="position: absolute; left: -9999px; top: 0;">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3 class="clientPage__HEADER">Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических центров в Астане - 8000 м2, в Актобе - 6500 м2, а в Шымкенте - 2500 м2.</h3>
                <p>
                Клиенты 2
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 pl-0 pr-0">
                <div class="clientImages">
                    <div>
                        <img src="images/logicstics-clientbase_img-01.png">
                    </div>
                    <div>
                        <img src="images/logicstics-clientbase_img-02.png">
                    </div>
                    <div>
                        <img src="images/logicstics-clientbase_img-03.png">
                    </div>
                    <div>
                        <img src="images/logicstics-clientbase_img-01.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line mt-1 mb-5">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1 class="ourCollabs">С нами сотрудничают</h1>
            </div>
        </div>
    </div>
    <div class="container mb-5" style="max-width: 900px;"> 
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="slider ourPartnersAutoplay">
                    <div>
                        <img src="images/logicstics-brig.png" alt="Бриг">
                        <h4>ТОО «БРИГ»</h4>
                    </div>
                    <div>
                        <img src="images/logicstics-applecity.png" alt="Бриг">
                        <h4>ТОО «Apple city distributors»</h4>
                    </div>
                    <div>
                        <img src="images/logicstics-commonmarket.png" alt="Бриг">
                        <h4>ТОО «Common market Co»</h4>
                    </div>
                    <div>
                        <img src="images/logicstics-milland.png" alt="Бриг">
                        <h4>ТОО «АП Милланд»</h4>
                    </div>
                    <div>
                        <img src="images/logicstics-brig.png" alt="Бриг">
                        <h4>ТОО «БРИГ»</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script>
	$(document).ready(function() {
	  var clientBase = $('.clientsBase').on('click', function(e) {
	    e.preventDefault();
	    clientBase.not(this).removeClass('acting');
	    $(this).addClass('acting');
	  });
	});
</script>
<script>
    var $slider = $(".clientAutoplay");
    $slider.slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
  		autoplaySpeed: 2000,
        variableWidth: false,
        arrows: false,
        responsive: [
        	{
        		breakpoint: 500,
        		settings: {
        			slidesToShow: 1,
        			arrows: false
        		}
        	},
        	{
        		breakpoint: 600,
        		settings: {
        			slidesToShow: 2,
        			arrows: false
        		}
        	},
        	{
        		breakpoint: 800,
        		settings: {
        			slidesToShow: 2,
        			arrows: false
        		}
        	},
        	{
        		breakpoint: 1082,
        		settings: {
        			arrows: false
        		}
        	}
        ]
    })
</script>
<script>

var $slider = $(".ourPartnersAutoplay");
$slider.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
      autoplaySpeed: 2000,
    variableWidth: false,
    responsive: [
        {
            breakpoint: 500,
            settings: {
                slidesToShow: 1,
                arrows: false
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows: false
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 3,
                arrows: false
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 3,
                arrows: false
            }
        },
        {
            breakpoint: 1082,
            settings: {
                arrows: false
            }
        }
    ]
})
.on("beforeChange", function(e, slick, current, next) {
    if( next >= (slick.slideCount - slick.options.slidesToShow)) {
        $slider.addClass("atEnd");
    } else {
        $slider.removeClass("atEnd");   
    }
});

</script>
<script>
    function showClients(shown, hidden) {
            document.getElementById(shown).style.position='static';
			document.getElementById(hidden).style.position='absolute';
			document.getElementById(hidden).style.left='-9999px';
  			return false;
    }
</script>