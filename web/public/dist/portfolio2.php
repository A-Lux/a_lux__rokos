<?php require_once("header.php"); ?>
<div class="portfolio mb-5">
	<div class="container mb-5">
		<div class="row mt-5 product-buttons">
			<div class="col-sm-12 col-md-6 own-production acting pr-production">
				<a href="#">
				<div class="container">
					<div class="row">
						<div class="col-4 baloons">
							<p style="opacity: 0">1</p>
						</div>
						<div class="col-8">
							<p>Продукция собственного производства</p>
						</div>
					</div>	
				</div>
				</a>
				
			</div>
			<div class="col-sm-12 col-md-6 partners-production pr-production">
				<a href="#">
				<div class="container">
					<div class="row">
						<div class="col-4 handshakke">
							<p style="opacity: 0">1</p>
						</div>
						<div class="col-8">
							<p>Продукция производства наших партнеров</p>
						</div>
					</div>	
				</div>
				</a>
			</div>
		</div>
	</div>
    <div class="container" style="max-width: 1248px;">
		<div class="row">
			<div class="col-sm-12 col-md-5">
				<form class="product-search" action="" method="post">
					<input type="search" placeholder="Поиск по названию">
				</form>
			</div>
			<div class="col-sm-12 col-md-7">
				<div class="container pl-0 pr-0">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Тип товара
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Первый</a>
								<a class="dropdown-item" href="#">Второй</a>
								<a class="dropdown-item" href="#">Третий</a>
							</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Тип товара
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Первый</a>
								<a class="dropdown-item" href="#">Второй</a>
								<a class="dropdown-item" href="#">Третий</a>
							</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Тип товара
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Первый</a>
								<a class="dropdown-item" href="#">Второй</a>
								<a class="dropdown-item" href="#">Третий</a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="container mt-5 mb-5" style="max-width: 1000px;">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="slider autoplay1">
					<div><a class="entry active" href="#"><img class="inner cls-1" src="/public/dist/images/alfoor-logo.png"></a></div>
					<div><a class="entry" href="#"><img class="inner cls-1" src="/public/dist/images/ricco-logo.png"></a></div>
					<div><a class="entry" href="#"><img class="inner cls-1" src="/public/dist/images/arla-logo.png"></a></div>
					<div><a class="entry" href="#"><img class="inner cls-1" src="/public/dist/images/piryatin-logo.png"></a></div>
					<div><a class="entry" href="#"><img class="inner cls-1" src="/public/dist/images/puck-logo.png"></a></div>
				</div>
			</div>
		</div>
	</div>
    <div id="alfoor-page" class="container mb-5 mt-5">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/first.jpg" alt="">
					<p>Масло Эталон Вкуса 170 гр.</p>
					<h4>320 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/second.jpg" alt="">
					<p>Масло Эталон Вкуса 450 гр.</p>
					<h4>610 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/third.jpg" alt="">
					<p>Масло Достойный Выбор 170 гр.</p>
					<h4>230 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/fourth.jpg" alt="">
					<p>Масло Достойный Выбор 450 гр.</p>
					<h4>470 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/portfolio-product-item_01.png" alt="">
					<p>Масло Достойный Выбор 170 гр.</p>
					<h4>230 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/portfolio-product-item_02.png" alt="">
					<p>Масло Достойный Выбор 450 гр.</p>
					<h4>470 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/portfolio-product-item_03.png" alt="">
					<p>Масло Эталон Вкуса 170 гр.</p>
					<h4>320 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
					<img src="/public/dist/images/portfolio-product-item_04.png" alt="">
					<p>Масло Эталон Вкуса 450 гр.</p>
					<h4>610 тг</h4>
					<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
				</div>
			</div>
		</div>
		<div class="evenMoreContent">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/first.jpg" alt="">
						<p>Масло Эталон Вкуса 170 гр.</p>
						<h4>320 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/second.jpg" alt="">
						<p>Масло Эталон Вкуса 450 гр.</p>
						<h4>610 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/third.jpg" alt="">
						<p>Масло Достойный Выбор 170 гр.</p>
						<h4>230 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/fourth.jpg" alt="">
						<p>Масло Достойный Выбор 450 гр.</p>
						<h4>470 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/portfolio-product-item_01.png" alt="">
						<p>Масло Достойный Выбор 170 гр.</p>
						<h4>230 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/portfolio-product-item_02.png" alt="">
						<p>Масло Достойный Выбор 450 гр.</p>
						<h4>470 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/portfolio-product-item_03.png" alt="">
						<p>Масло Эталон Вкуса 170 гр.</p>
						<h4>320 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="portfolio-product-item"  data-toggle="modal" data-target="#first">
						<img src="/public/dist/images/portfolio-product-item_04.png" alt="">
						<p>Масло Эталон Вкуса 450 гр.</p>
						<h4>610 тг</h4>
						<a href="#" class="more" data-toggle="modal" data-target="#first">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="cols-sm-12 col-md-12">
				<span class="evenMore">Подробнее</span>
			</div>
		</div>
	</div>
    <!-- Начало -->
  <!-- Модальные окна -->
  <div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
  				</button>
  			</div>
  			<div class="modal-body">
  				<div class="container">
  					<div class="row">
  						<div class="col-sm-12 col-md-6 text-center">
  							<img src="/public/dist/images/portfolio-product-item_01-large.png">
  						</div>
  						<div class="col-sm-12 col-md-6 d-flex flex-column align-items-center">
  							<h2>Масло достойный выбор</h2>
  							<p>170 гр.</p>
  							<ul>
  								<li>
  									<p>– 5 °C – 2 мес.</p>
  								</li>
  								<li>
  									<p>– 15 °C – 4 мес.</p>
  								</li>
  								<li>
  									<p>– 18 °C – 6 мес.</p>
  								</li>
  								<li>
  									<p>– 25 °C – 12 мес.</p>
  								</li>
  							</ul>
  							<p class="production-company">Производство: ТОО «АЯНА ПЛЮС», Казахстан, Завод Аяла, Алматы</p>
  							<p class="production-category">Категория: <a href="#">Собственные торговые марки</a></p>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Конец -->
</div>
<?php require_once("footer.php"); ?>