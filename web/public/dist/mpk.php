<?php require_once("header.php"); ?>
<div class="mpk">
    <div class="mpk__container-logo pt-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h3>МПК «Актобе» – один из лидеров мясоперерабатывающей промышленности Казахстана.</h3>
                    <p>Наше предприятие с уникальным, современным оборудованием и квалифицированным персоналом выпускает более 100 наименований колбасных изделий, деликатесов и полуфабрикатов под торговыми марками «Dástúr Dámі» и «МПК». Благодаря профессиональной и слаженой работе коллектива, неустанному подбору специалистов, предприятие успешно обеспечивает высокое качество продукции.</p>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12 col-md-5 col-lg-5 mpk__flexible">
                    <div class="mpk__advantages">
                        <h1>Мощность мясокомбината</h1>
                        <img src="images/mpk__img-01.png" alt="">
                        <h2>5000</h2>
                        <p>тонн в год</p>
                    </div>
                    <div class="mpk__advantages">
                        <h1>Численность работников</h1>
                        <img src="images/mpk__img-02.png" alt="">
                        <h2>400</h2>
                        <p>человек</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-1 col-lg-1"></div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="mpk__production">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h1>Продукция компании</h1>
                            </div>
                            <div class="container mt-4">
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="mpk__production-logo">
                                            <img src="images/mpk__img-03.png" alt="">
                                            <p>«Дәстүр Дәмі»</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="mpk__production-logo">
                                            <img src="images/mpk__img-04.png" alt="">
                                            <p>«МПК»</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1 col-lg-2"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-4">
                                <p>Колбасные изделия, деликатесы, фарши, пельмени.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2"></div>
            </div>
        </div>
    </div>
    <div class="mission mt-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <h1><span>Наша миссия</span> — достигать лидерства,
                    удовлетворяя ежедневные потребности
                    населения Казахстана в качественной 
                    и безопасной продукции
                    </h1>
                    <p>
                        Собственная система логистических решений и филиальная сеть складов позволяют гарантировать достаточный ассортимент во всех регионах Казахстана
                    </p>
                    <a href="#" class="becomeClient">Стать клиентом</a>
                </div>
                <div class="col-sm-12 col-md-4"></div>
            </div>
        </div>
    </div>
    <div class="container mpk-production mb-5 mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1>Продукция МПК</h1>
            </div>
            <div class="col-sm-12 col-md-12">
                <p>
                Колбасы МПК «Актобе» – это прекрасное сочетание цены и качества. Комбинат постоянно участвует в национальных, городских выставках и других мероприятиях Казахстана, являясь их лауреатом и дипломантом. В 2017 году наша продукция была удостоена золотой медали «За качество и вкус выпускаемой продукции» на Международной выставке «InterFood Astana» по пищевой промышленности.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="mpk-production__img-block">
                    <img src="images/mpk__production-img-01.png" alt="">
                    <h4>Мусульманский с сыром</h4>
                    <p>сервелат</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="mpk-production__img-block">
                    <img src="images/mpk__production-img-02.png" alt="">
                    <h4>Деликатесы Қазы</h4>
                    <p>конина варёно-копчёная</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <img src="images/mpk__production-img-03.png" alt="">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6">
                <img src="images/mpk__production-img-06.png" alt="">
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="container pl-0 pr-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="mpk-production__img-block">
                                <img src="images/mpk__production-img-04.png" alt="">
                                <h4>Туркестан</h4>
                                <p>сервелат</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="mpk-production__look-forward">
                                <h1><span>Посмотреть</span> полный каталог</h1>
                                <a href="#" class="lookForward">Перейти</a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 3.4rem">
                        <div class="col-sm-12 col-md-12">
                            <img src="images/mpk__production-img-05.png" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="container mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h1>Наши принципы</h1>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_01.png" alt="">
                    <p>Стабильное качество
                    продуктов, соответствие
                    международным стандартам</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_02.png" alt="">
                    <p>Работа с сертифицированными
поставщиками сырья и надежными
партнерами</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_03.png" alt="">
                    <p>Использование
современных технологий
и традиций народов Казахстана</p>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_04.png" alt="">
                    <p>Качественный торговый сервис
на стабильно высоком уровне</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_05.png" alt="">
                    <p>Контроль на всех этапах,
от закупки  до доставки</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="mpk__priniciples">
                    <img src="images/mpk__priciples-img_06.png" alt="">
                    <p>Грамотное построение
системы сбыта продукции</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once("footer.php"); ?>