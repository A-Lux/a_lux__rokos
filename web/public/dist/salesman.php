<?php require_once("header.php"); ?>
<div class="vacancy">
	<div class="container mt-5">
		<div class="row">
			<div class="col-sm-12 col-md-8">
				<div>
					<h1 class="vacancy-header">Торговый представитель</h1>
					<p class="vacancy-address">г. Нур-Султан, ул. Пушкина, 75</p>
					<p class="vacancy_employment-type">Полная занятость</p>

					<h3>В ТОО "RGG" в связи с расширением пакета требуются Торговые представители следующих категорий:</h3>
					<ul class="vacancy-categories">
						<li>
							<p>Традиционная розница «ВС»</p>
						</li>
						<li>
							<p>Ключевая розница «Категория А»</p>
						</li>
						<li>
							<p>HoReCa</p>
						</li>
						<li>
							<p>Рынки</p>
						</li>
					</ul>

					<h2 class="vacancy_duties-header">Обязанности:</h2>
					<ul class="vacancy-duties">
						<li>
							<p>Выполнение плана продаж продукции на закреплённой территории;</p>
						</li>
						<li>
							<p>Расширение ассортимента продукции Компании в торговой точке;</p>
						</li>
						<li>
							<p>Ежедневный, еженедельный, ежемесячный анализ продаж и постановка целей по выполнению планов;
</p>
						</li>
						<li>
							<p>Проверка товарных запасов Клиента;</p>
						</li>
						<li>
							<p>Формирование эффективных заявок по количеству и ассортименту продукции на основе анализа продаж Клиента; </p>
						</li>
						<li>
							<p>Презентация продукции Клиентам;</p>
						</li>
						<li>
							<p>Работа по осуществлению мерчендайзинга в торговых точках;</p>
						</li>
						<li>
							<p>
Работа по расширению, открытию и регистрации новых торговых точек с целью максимального охвата всех Клиентов на территории своего района;</p>
						</li>
						<li>
							<p>Осуществление контроля за своевременной доставкой продукции согласно заявки;</p>
						</li>
					</ul>

					<h2 class="vacancy_requires-header">Требования:</h2>
					<ul class="vacancy-requires">
						<li><p>Желателен опыт работы в дистрибуции не менее 1 года;</p></li>
						<li><p>Умение договариваться и вести результативные переговоры с Клиентами;</p></li>
						<li><p>Целеустремленность;</p></li>
					</ul>

					<h2 class="vacancy_conditions-header">Условия:</h2>
					<ul class="vacancy-conditions">
						<li>
							<p>Работа в успешной, стабильной Компании</p>
						</li>
						<li>
							<p>Оформление в соответствии с законодательством РК;</p>
						</li>
						<li>
							<p>Работа с собственными и международными брендами;</p>
						</li>
						<li>
							<p>Своевременная выплата заработной платы;</p>
						</li>
						<li>
							<p>Пятидневный график работы;</p>
						</li>
						<li>
							<p>Обучение.</p>
						</li>	
					</ul>

					<h2 class="vacancy_summary-header">Резюме принимаются на электронный адрес:</h2>
					<a href="mailto:a.kravchenko@rokos.kz">a.kravchenko@rokos.kz</a>

					<h2 class="vacancy_tinquiries-header">Справки по телефону:</h2>
					<p class="vacancy_tinquiries-initials">Александр Кравченко</p>
					<a href="tel:+77023350329">+7 702 335 03 29</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">	
				<div class="resume_container">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="71" height="70" viewBox="0 0 71 70">
					  <image id="big-new-email" width="71" height="70" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEcAAABGCAMAAACpMnmIAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABJlBMVEXE5fH////E5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fHE5fEAAABOUfaUAAAAYHRSTlMAAAFiwuAStzLf+AebH8tH7Xz9EbMv3S5e9gaWHMlC6ngOr64s2lr1WQWRGsVA6NugkD9w/PplZGxrDdwEtrAKJgu+hhmtiIy1ewLu8II7b27jXF0lYH68A0P55tmsPnZSU8KEAAAAAWJLR0RhsrBMhgAAAAlwSFlzAAALEgAACxIB0t1+/AAAAAd0SU1FB+MKCwwcKBttUO8AAAJlSURBVFjD7ddpV9pAFIDhSTAq7qK1LojYAtalYkG7iKJVXLq5tFatlXr//69oJkNCkpm5M5Nw+qnvx+TmOUkmJxBCFNmZPqcvY0v3WywV0z8AtIH+dM5gFljZwTROZgj8hjKJneERCDcynMwZHYNoY6NJnPEJiDcxbuzYkzngy03aZs7UNIibnjJxns2ArJnn+s7sHMibm9V05hcAb2Fex8kvgqrFvNopLCkZgKWCwrGLOQ3GfQCKNuYsv9BSaC+X5U6prM0AlEsyp7JiwACsvBI6q2tGCm1tlXfWN4wZgI31uPN609tR3Xqj11bVm9+sRZ36Nt2681b+Po9nv9uhh2zXw06JneZ7bYX2gR1UCjm7De8k94ycPe9WVHfD15Wn0L4RQ8i+e0wjH73PFGoaOs0uY5GDwwAydxp5tu6HH8kRHPtQ4LROHHknrcBxGc85hiPiAJx2oDN/4Bx9+M79sTP6GnKZUwCHOvCJbf/sD3xBna/+mDdvWd/Ad6AYvXD8fKLLahWh68SgVhO5P81WZNZjAid+RtoxpuskhDpMyEkE+UzYSQAFTMQJQReXdVmXFwIm6gTQFbruVzwTc3zoGnWueSbudKAs6mR5hnMY9B37Uc394BneYdBN5aesyo2AEThayx9nRI4GxDFCRwnxjNhRQAJG4qCQiJE5CCRkpI4UEjNyRwJJGMQRQjIGcwSQlEEdDpIzuBODEEbhRCCMUTkhCGWUDtyyf3r2LaRz4K5wT+5/3UFaB+Dh94NyRsfR6b/zr5zHnjiPpN0Tp01qPXH+EPLUA+aJfl/U2mUnTeW2+zH3F2m490oaP7TCAAAAAElFTkSuQmCC"/>
					</svg>
					<p>Если вы не нашли подходящую вакансию, вы можете прислать нам свое резюме.</p>
					<form action="">
						<input class="send-resume" type="submit" value="Отправить резюме">
					</form>
				</div>
			</div>
		</div>
	</div>
<div class="vacancy-bg">
	<div class="container mt-5 pt-5">
		<div class="row">
			<div class="col-sm-12 col-md-7">
			  <div class="apply">
				<h1 class="apply-header">Подать заявку</h1>
				<form action="" class="apply-form">
					<div>
						<label for="initials">ФИО</label>
						<input id="initials" type="text" placeholder="Авдеев Сергей Юрьевич">
					</div>
					<div>
						<label for="cities">Город</label>
						<select name="cities" id="cities" class="cities">
							<option value="nur-sultan">Нур-Султан</option>
							<option value="almaty">Алматы</option>
							<option value="shymkent">Шымкент</option>
							<option value="shymkent">Шымкент</option>						
						</select>
					</div>
					<div>
						<label for="email">Электронная почта</label>
						<input id="email" type="email" placeholder="info@mail.ru">
					</div>
					<div>
						<label for="phone-number">Телефон</label>
						<input id="phone-number" type="tel" pattern="[8]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}" placeholder="8 (">
					</div>
					<div>
						<label for="comments">Комментарий</label>
						<textarea name="comments" id="comments" placeholder="Оставить комментарий"></textarea>
					</div>
					<div class="separator"></div>
					<h2 class="resume-header">Резюме</h2>
					<div>
						<label for="url">Ссылка на резюме со сторонних сайтов</label>
						<input type="url" id="url" placeholder="www.hh.ru/89651">
					</div>
					<div>
						<label for="file">Файл резюме</label>
						<label class="custom-file-upload pr-5 pt-1 pl-4 pb-1"><input type="file">Выберите файл</label>
					</div>
					<div class="separator"></div>
					<input class="pb-2 pt-2 mb-5" type="submit" value="Отправить">
				</form>
			  </div>
			</div>
			<div class="col-sm-12 col-md-5">
				
			</div>
		</div>
	</div>
</div>
</div>
<?php require_once("footer.php"); ?>