<!DOCTYPE html>
<html lang="en">




<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="images/logo2.png" type="image/png">
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/glide.core.css">
    <link rel="stylesheet" href="css/glide.theme.css">

    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" href="css/hc-offcanvas-nav.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/mobile-detect@1.4.4/mobile-detect.min.js"></script>
    <title>Rokos</title>
</head>


<body>
    <?php 
    $directoryURI = $_SERVER['REQUEST_URI'];
    $header = false;
    if ($directoryURI != "/" && $directoryURI != "/index.php") {
        $header = true;
    }
?>

    <header class="header">
        <!-- Мобильная версия меню -->
        <nav id="main-nav">
            <ul>
                <input type="text">
                <li><a></a></li>
                <li><a></a></li>
                <li><a class="anchorHref" href="/">О RGG</a></li>
                <li><a href="/portfolio.php">Портфель</a></li>
                <li><a href="/career.php">Карьера</a></li>
                <li><a href="/news.php">Новости</a></li>
                <li><a href="/contact.php">Контакты</a></li>
                <li><a href="#">МПК</a></li>
                <li><a href="#">Логистика</a></li>
            </ul>
        </nav>
        <!-- Конец мобильной версии меню -->
        <?php 
            if($header){
                echo '<div class="absolute_backround-slider" style="display: none">';
            }
            else{
                echo '<div class="absolute_backround-slider"  style="">';
            }
        ?>
        <div class="glide_header">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide"
                        style="background: url(./images/header-bg3.png);background-position: center;"></li>
                    <li class="glide__slide"
                        style="background: url(./images/header-bg.png);background-position: center;">
                        <div class="container_absolute">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand1.png" alt="brand1" id="brand1">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand2.png" alt="brand2" id="brand2">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand3.png" alt="brand3" id="brand3">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand4.png" alt="brand4" id="brand4">
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand5.png" alt="brand5" id="brand5">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand6.png" alt="brand6" id="brand6">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand7.png" alt="brand7" id="brand7">
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <a href="">
                                            <img src="images/brand8.png" alt="brand8" id="brand8">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                    <li class="glide__slide"
                        style="background: url(./images/header-bg2.png);background-position: center;"></li>
                    <li class="glide__slide"
                        style="background: url(./images/header-bg1.png);background-position: center;"></li>
                </ul>
            </div>
            <div class="glide__bullets" data-glide-el="controls[nav]">
                <button class="glide__bullet" data-glide-dir="=0"></button>
                <button class="glide__bullet" data-glide-dir="=1"></button>
                <button class="glide__bullet" data-glide-dir="=2"></button>
                <button class="glide__bullet" data-glide-dir="=3"></button>
            </div>
        </div>
        </div>

        <div class="container  indexStyleMobile">
            <div class="row header-nav">
                <div class="col-sm-12 col-md-12 col-lg-3">
                    <div class="logo">
                        <a href="/index.php">
                            <img src="images/logo.png" alt="" class="logoPic" style="filter: brightness(1.15) grayscale(0.39);">
                        </a>
                        <div class="lang-version-mob">
                            <a href="#">Kaz</a>
                            <a href="#">Ru</a>
                            <a href="#">Eng</a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="menu">
                        <div class="menu__upper__layer">
                            <a class="anchorHref" href="/">О RGG </a>
                            <a href="/portfolio.php">Портфель</a>
                            <a href="/career.php">Карьера </a>
                            <a href="/news.php">Новости</a>
                            <a href="/contact.php">Контакты </a>
                            <a href="/">МПК</a>
                            <a href="/">Логистика</a>
                        </div>
                        <div class="menu__sub__layer">
                            <form action="" method="POST">
                                <input type="text">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 d-flex align-items-end">
                    <div class="lang-version">
                        <a>Kaz</a>
                        <a>Ru</a>
                        <a>Eng</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
    if($header){
        echo '<a href="#about_company"  class="mouse_nav--absolute" style="display: none">';
    }
    else{
        echo '<a href="#about_company"  class="mouse_nav--absolute">';
    }
    ?>


        </a>
    </header>