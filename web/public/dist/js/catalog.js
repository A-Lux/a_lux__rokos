$('body').on('click', '.category_filter', function (e) {
    $('.logo__wrapper img').attr("src", "");
    var id = $(this).attr('data-id');
    var selects = document.querySelectorAll("#firstCategory, #secondCategory, #thirdCategory");
    selects[0].innerHTML="<option>Тип товара</option>";
    selects[1].innerHTML="<option>Бренды</option>";
    selects[2].innerHTML="<option>Категории</option>";
    
    selects[1].disabled = true;
    selects[2].disabled = true;
    var disabledArr = [
        "firstCategory",
        "secondCategory",
        "thirdCategory",
    ];
    $('.product-types').change(function() {
        var id = $(this).attr('id');
        var index = $.inArray(id, disabledArr);
        $('#' + disabledArr[index+1]).removeAttr('disabled');
    });
    var next = this.getAttribute('next');
    $.ajax({
        type: 'GET',
        url: "/content/categories",
        data: {id:id},
        success: function(html){
            $(next).html(html);
        }
    });
});

$('body').on('change', '.product-types', function (e) {
    var id = $(this).val();
    var next = this.getAttribute('next');
    $("#firstCategory").change(function() {
        $('.logo__wrapper img').attr("src", "");
    });
    $("#secondCategory").change(function() {
        $('.logo__wrapper img').attr("src", $("#secondCategory option:selected").attr('data-image'));
    });
    $(next).prop("disabled", false);

    $.ajax({
        type: 'GET',
        url: "/content/categories",
        data: {id:id},
        success: function(html){
            $(next).html(html);
            if(html.length <  70){
                $(next).prop("disabled", true);
            }
            if($(next)[0].children.length == 1) {
                $(next).prop("disabled", true);
            }
            let ccc = document.querySelectorAll('.category-p');
            ccc[1].innerHTML = "<p>Бренды</p>";
            ccc[2].innerHTML = "<p>Категории</p>"
        }
    });
});
$('body').on('change', '.product-types', function(e){

    var id = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/content/products',
        data: {id:id},
        success: function(html){
            $('#products-filter').html(html);
        }
    });
});

$('body').on('click', '.category_filter', function(e){
    var id = $(this).attr('data-id');

    $.ajax({
        type: 'GET',
        url: '/content/products',
        data: {id:id},
        success: function(html){
            $('#products-filter').html(html);
        }
    });
});


$('body').on('keyup', '.search-products', function () {

   var search = $('.search-products').val();

    $.ajax({
        type: 'GET',
        url: "/content/search",
        data: {search: search},
        success: function(html){
            $('#products-filter').html(html);
            $('.pr-production').removeClass('acting');
        }
    });

});

$("#careerfeedback-city_id option:first-child").attr("hidden", "true");
$("#careerfeedback-position_id option:first-child").attr("hidden", "true");
$("#cities option:first-child").attr("hidden", "true");


$('.my').change(function() {
    if ($(this).val() != '') $(this).prev().text('Выбрано файлов: ' + $(this)[0].files.length);
    else $(this).prev().text('Выберите файлы');
});


$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

$(".toggle-password-2").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
});
