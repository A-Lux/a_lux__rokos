$('body').on('change', '.select-category', function(e){

    var category_id = $(this).val();

    var city_id     = $('.select-city').val();

    $.ajax({
        type: 'GET',
        url: '/content/vacancy',
        data: {city_id:city_id, category_id:category_id},
        success: function(html){
            $('#career-filter').html(html);
        }
    });

});

$('body').on('change', '.select-city', function(e){

    var category_id = $('.select-category').val();

    var city_id     = $(this).val();

    $.ajax({
        type: 'GET',
        url: '/content/vacancy',
        data: {city_id:city_id, category_id:category_id},
        success: function(html){
            $('#career-filter').html(html);
        }
    });

});
