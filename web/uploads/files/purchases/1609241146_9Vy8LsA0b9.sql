-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: rokos
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `text_en` text NOT NULL,
  `text_kz` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'<p>TOO &laquo;Rokos Group Global&raquo; - уникальная компания, не имеющая аналогов в Казахстане по инфраструктуре и холодной логистике, работающая с 1996 года. Сегодня это крупная, динамично развивающаяся компания, с широкой дистрибуционной сетью во всех каналах продаж. Собственные холодные склады дают возможность контролировать условия хранения и предлагать бережное хранение продуктов</p>\r\n','<p>TOO &laquo;Rokos Group Global &raquo; - работает с 1996 года. Сегодня это крупная, динамично развивающаяся компания, с широкой дистрибуционной сеткой во всех каналах продаж. Собственные холодные склады дают возможность контролировать условия хранения и предлагать бережное хранение продуктов.</p>\r\n','<p>TOO &laquo;Rokos Group Global &raquo; - работает с 1996 года. Сегодня это крупная, динамично развивающаяся компания, с широкой дистрибуционной сеткой во всех каналах продаж. Собственные холодные склады дают возможность контролировать условия хранения и предлагать бережное хранение продуктов.</p>\r\n'),(2,'<p>Более чем за 20 лет плодотворной работы, наша команда профессионалов наладила партнерские отношения с крупными заводами-производителями таких стран как: Украина, Дания, Россия, Норвегия, Беларусь, Парагвай и Аргентина</p>\r\n','<p>Более чем за 20 лет плодотворной работы, наша команда профессионалов наладила партнерские отношения с крупными заводами-производителями таких стран как: Украина, Дания, Россия, Норвегия, Беларусь, Парагвай и Аргентина.</p>\r\n','<p>Более чем за 20 лет плодотворной работы, наша команда профессионалов наладила партнерские отношения с крупными заводами-производителями таких стран как: Украина, Дания, Россия, Норвегия, Беларусь, Парагвай и Аргентина.</p>\r\n'),(3,'<p>Наш уникальный продуктовый портфель включает в себя такие ключевые группы питания как: колбасные изделия, деликатесы, сыры, масло, куриная разделка, мясо, рыба, майонез, холодная заморозка овощей и галет</p>\r\n','<p>Наш уникальный продуктовый портфель включает в себя такие ключевые группы питания как: колбасные изделия, деликатесы, сыры, масло, куриная разделка, мясо, рыба, майонез, холодная заморозка овощей и галет.</p>\r\n','<p>Наш уникальный продуктовый портфель включает в себя такие ключевые группы питания как: колбасные изделия, деликатесы, сыры, масло, куриная разделка, мясо, рыба, майонез, холодная заморозка овощей и галет.</p>\r\n');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advantage`
--

DROP TABLE IF EXISTS `advantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `image_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  `image_kz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advantage`
--

LOCK TABLES `advantage` WRITE;
/*!40000 ALTER TABLE `advantage` DISABLE KEYS */;
INSERT INTO `advantage` VALUES (1,'<span>50</span><span>тыс</span>','Продуктов повседневного спроса','1569585491_ru_about_us__item1.png','<span>50</span><span>тыс</span>','Собственное производство, склады и дистрибьюция','1569585634_en_about_us__item1.png','<span>50</span><span>тыс</span>','Собственное производство, склады и дистрибьюция','1569585634_kz_about_us__item1.png'),(2,'<span>7</span><span></span>',' Филиалов республиканского                         масштаба, 6 из которых                         собственные','1572325985_ru_about_us__item2.png','','','','','',''),(3,'<span>240</span><span></span>',' Собственных машин типа                         изотерма, 60 из которых                         «VOLVO» 20 тонн','1572326006_ru_about_us__item3.png','','','','','',''),(4,'<span>1</span>                     <span></span>','Собственный                         логистический                         центр в г. Актобе','1572326037_ru_about_us__item4.png','','','','','',''),(5,'<span>22</span><span>тыс</span>','более 22 тыс контрагентов','1572326057_ru_about_us__item5.png','','','','','',''),(6,'<span>30</span><span>тыс м&#178</span>','Собственная складская                         площадь от +5°С до -20°С','1572326077_ru_about_us__item6.png','','','','','',''),(7,'<span>1500</span><span></span>','Общее количество                         персонала','1572326107_ru_about_us__item7.png','','','','','',''),(8,'<span>2000</span><span>SKU</span>','Мы работаем в сектор FOOD/FROZEN FOOD                       и постоянно улучшаем                         качество портфеля','1572326138_ru_about_us__item8.png','','','','','','');
/*!40000 ALTER TABLE `advantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `text_en` varchar(255) NOT NULL,
  `text_kz` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'','1576724230_1.jpg',1,'',''),(2,'','1575454584_br222.jpg',2,'',''),(3,'','1569583672_header-bg2.png',3,'',''),(4,'','1569583683_header-bg1.png',4,'','');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner_mobile`
--

DROP TABLE IF EXISTS `banner_mobile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `text_en` varchar(255) DEFAULT NULL,
  `text_kz` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner_mobile`
--

LOCK TABLES `banner_mobile` WRITE;
/*!40000 ALTER TABLE `banner_mobile` DISABLE KEYS */;
INSERT INTO `banner_mobile` VALUES (1,'','','',1,'1577091899_баннер новый.jpg'),(2,'','','',2,'1575533841_460 3-min.jpg'),(3,'','','',3,'1577353925_46ввввввв0 — 2-min.jpg'),(4,'','','',4,'1575518543_460 — 3-min.jpg');
/*!40000 ALTER TABLE `banner_mobile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `career_feedback`
--

DROP TABLE IF EXISTS `career_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `career_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_person` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` text,
  `href` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `reply` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `career_feedback`
--

LOCK TABLES `career_feedback` WRITE;
/*!40000 ALTER TABLE `career_feedback` DISABLE KEYS */;
INSERT INTO `career_feedback` VALUES (1,'Test',1,1,'Test','Test','1569846839_Sommerville-Software-Engineering-10ed.pdf','0000-00-00 00:00:00',NULL,NULL,0,NULL),(2,'asdasd',1,1,'asdasd','8(123) 123-1231','','2019-10-30 09:25:26',NULL,NULL,0,NULL),(3,'454',2,1,'info@a-lux.kz','8(770) 545-0391','1573551302_Коммерческое предложение дизайн.docx','2019-11-12 09:35:02',NULL,NULL,0,NULL),(4,'Amir Amir',2,2,'info@sos.co','8(123) 123-3212','1577369124_Амирхон\'s Resume .pdf','2019-12-26 14:05:24','я хочу работать','https://hh.kz',0,NULL),(5,'Ahmad Al\'-Bahari',1,2,'ahmad.sila@kavkaz.com','8(123) 123-1231','1577369289_Амирхон\'s Resume .pdf','2019-12-26 14:08:09',NULL,NULL,0,NULL),(6,'Джамангарин Алдияр Болатович ',5,2,'oo-o-oo@list.ru','8(702) 969-6557','1577371020_резюме 4 разряд.docx','2019-12-26 14:37:00',NULL,NULL,0,NULL),(7,'Максутов Кайржан Мулдашович',2,1,'kairzhanmak@gmail.com','8(701) 250-0706','1577472681_CV_Maxutov.docx','2019-12-27 18:51:21',NULL,NULL,0,NULL),(8,'забосина',5,5,'too.ilteko@mail.ru','8(701) 292-3200','1578211207_1111111.docx','2020-01-05 08:00:07',NULL,NULL,0,NULL),(9,'Сакен Ахантаевич',3,5,'saken_66@bk.ru','8(747) 513-8641','1579512838_РЕЗЮМЕ .docx','2020-01-20 09:33:58','Рассмотрим Ваше объявление на вакантную должность','',0,NULL),(10,'Горбунов Владимир Вячеславович',1,2,'gvv_1989@mail.ru','8(771) 911-7889','1579711402_Резюме Горбунов В.В. г. Степногорск.docx','2020-01-22 16:43:22',NULL,NULL,0,NULL),(11,'Евгений Александрович Орехов',5,5,'domus-smolensk@mail.ru','+79107109000','1581064628_Орехов Евгений Александрович (2).pdf','2020-02-07 08:37:08','Добрый день. Прошу рассмотреть мою кандидатуру на вакансию Директор филиала г.Актобе','https://smolensk.hh.ru/resume/f8b7884bff03a8d96d0039ed1f5a46544c6236',0,NULL),(12,'Орехов Евгений Александрович',5,5,'domus-smolensk@mail.ru','8(910) 710-9000','1581867707_Орехов Евгений Александрович(1).pdf','2020-02-16 15:41:47','Прошу рассмотреть мою вакансию на должность Директор филиала г.Актобе','https://smolensk.hh.ru/resume/f8b7884bff03a8d96d0039ed1f5a46544c6236#key-skills',0,NULL),(13,'Омар Салтанат',1,2,'saltosha8686@mail.ru','8(701) 998-2809','1582028591_резюме Омар С.Ж.docx','2020-02-18 12:23:11',NULL,NULL,0,NULL),(14,'Ізбасаров Айбар',5,2,'aibar951@mail.ru','8(777) 591-0055','1582692906_резюме Менеджер.doc','2020-02-26 04:55:06',NULL,NULL,0,NULL),(15,'Ізбасаров Айбар',5,2,'aibar951@mail.ru','8(777) 591-0055','1582692907_резюме Менеджер.doc','2020-02-26 04:55:07',NULL,NULL,0,NULL),(16,'Евгений Александрович Орехов',5,5,'DOMUS-SMOLENSK@MAIL.RU','8(910) 710-9000','1582727142_Евгений Александрович Орехов.pdf','2020-02-26 14:25:42','Ссылка на моё резюме https://hh.ru/resume/f8b7884bff03a8d96d0039ed1f5a46544c6236\r\nОтправлено с помощью мобильного приложения hh https://hh.ru/mobile?from=share_android','HH.ru',0,NULL),(17,'Манасбаев Жанибек',4,5,'manasbaev96@mail.ru','8(707) 667-9855','1585057435_резюме Манасбаев Ж-1.doc','2020-03-24 13:43:55',NULL,NULL,0,NULL),(18,'Манасбаев Жанибек',4,5,'manasbaev96@mail.ru','8(707) 667-9855','1585057445_резюме Манасбаев Ж-1.doc','2020-03-24 13:44:05',NULL,NULL,0,NULL),(19,'Ергожин Бекберген Ерқанатұлы',6,2,'atyrauova2704@gmail.com','8(702) 722-7292','1585576969_Резюме Бекберген Ерғожин.docx','2020-03-30 14:02:49',NULL,NULL,0,NULL),(20,'Ергожин Бекберген Ерқанатұлы',6,2,'atyrauova2704@gmail.com','8(702) 722-7292','1585576976_Резюме Бекберген Ерғожин.docx','2020-03-30 14:02:56',NULL,NULL,0,NULL),(21,'Султаналиев Омирсерик Баймуханович',1,5,'010169sob@gmail.com','8(702) 000-0169','1585815664_15.03.2020 Г. Резюме Султангалиева О.Б..doc','2020-04-02 08:21:04',NULL,NULL,0,NULL),(22,'Иралимова Галия Молдакановна',1,5,'galiuw_89@mail.ru','8(707) 207-8720','1586278535_Иралимова Галия Молдакановна.pdf','2020-04-07 16:55:35',NULL,NULL,0,NULL),(23,'Айтжанов Олжас Хайдарович ',6,2,'olzhas_0829@mail.ru','8(702) 886-6390','1586582911_Олжас Хайдарович.pdf','2020-04-11 05:28:31',NULL,NULL,0,NULL),(24,'Дабылова Алия',5,5,'aliya.aitkalievna@gmail.com','8(775) 304-9526','1589465033_резюме Дабылова Алия .docx','2020-05-14 14:03:53',NULL,NULL,0,NULL),(25,'Дабылова Алия',5,5,'aliya.aitkalievna@gmail.com','8(775) 304-9526','1589465048_резюме Дабылова Алия .docx','2020-05-14 14:04:08',NULL,NULL,0,NULL),(26,'Ли Геннадий ',5,5,'lego_aktobe@mail.ru','8(701) 052-2525','1589813487_Геннадий Владимирович Ли.pdf','2020-05-18 14:51:27',NULL,NULL,0,NULL),(27,'Кунакбаев Дархан Булатович',5,2,'longfellow_dits@mail.ru','8(775) 828-3565','1593454582_Образец резюме НПП Атамекен.doc','2020-06-29 18:16:22',NULL,NULL,0,NULL),(28,'Кунакбаев Дархан Булатович',5,2,'longfellow_dits@mail.ru','8(775) 828-3565','1593454693_Образец резюме НПП Атамекен.doc','2020-06-29 18:18:14',NULL,NULL,0,NULL),(29,'Кунакбаев Дархан Булатович',5,2,'longfellow_dits@mail.ru','8(775) 828-3565','1593454833_резюме.doc','2020-06-29 18:20:33',NULL,NULL,0,NULL),(30,'Кунакбаев Дархан Булатович',5,2,'longfellow_dits@mail.ru','8(775) 828-3565','1593455484_Образец резюме НПП Атамекен.doc','2020-06-29 18:31:24',NULL,NULL,0,NULL),(31,'Утешова Сымбат Нуреденовна',5,2,'sym04@mail.ru','8(702) 472-8152','1593872518_РЕЗЮМЕ УТЕШОВА.С.Н.pdf','2020-07-04 14:21:58',NULL,NULL,0,NULL),(32,'Утешова Сымбат Нуреденовна',5,2,'sym04@mail.ru','8(702) 472-8152','1593872532_РЕЗЮМЕ УТЕШОВА.С.Н.pdf','2020-07-04 14:22:12',NULL,NULL,0,NULL),(33,'Утешова Сымбат Нуреденовна',5,2,'sym04@mail.ru','8(702) 472-8152','1593872589_РЕЗЮМЕ УТЕШОВА.С.Н.pdf','2020-07-04 14:23:09',NULL,NULL,0,NULL),(34,'Кунакбаев Дархан Булатович',5,2,'longfellow_dits@mail.ru','8(775) 828-3565','1594639671_Образец резюме НПП Атамекен.doc','2020-07-13 11:27:52',NULL,NULL,0,NULL),(35,'Демеуов Аскар Зарлыкович',5,5,'demask76@mail.ru','8(771) 507-2379','1595483402_Резюме Демеуова Аскара Зарлыковича.doc','2020-07-23 05:50:02','Здравствуйте!\r\nВысылаю на рассмотрение свое резюме\r\nЗаранее благодарен!','Резюме Демеуова Аскара',0,NULL),(36,'Myrzagul Bekmambetov',5,5,'myrza.bekmambetov@indox.ru','8(770) 167-8924','1596263677_РМЗ ОБЩ.docx','2020-08-01 06:34:37','отправил резюме','merza.bekmambetov@indox.ru',0,NULL),(37,'Аяшова Раушан Аманжолқызы',5,2,'a777rau@gmail.com','8(776) 916-9498','1596547344_резюме.docx','2020-08-04 13:22:24',NULL,NULL,0,NULL),(38,'Кубашева Камила Миргалиевна ',1,5,'kamilakubasheva@gmail.com','8(708) 199-0519','1599134886_Камила Миргалиевна Кубашева.pdf','2020-09-03 12:08:06',NULL,NULL,0,NULL),(39,'Кендюх Ольга Михайловна',1,2,'olga-kendjukh@mail.ru','8(705) 160-6565','1600683273_Кендюх Ольга Михайловна Резюме.pdf','2020-09-21 10:14:33',NULL,NULL,0,NULL),(40,'Курбанов Нуршидин Муратжанович',2,2,'kurbanov-nurshid@mail.ru','8(778) 395-0049','1600687765_Cv.pdf','2020-09-21 11:29:25',NULL,NULL,0,NULL),(41,'Хамидуллин Олжас Инаматович',1,5,'olzhas_atyrau@mail.ru','8(701) 639-0903','1602694598_CV of Khamidullin Olzhas, rus version.doc','2020-10-14 16:56:38','Готов к переездам.','hh.kz',0,NULL),(42,'Хамидуллин Олжас Инаматович',1,5,'olzhas_atyrau@mail.ru','8(701) 639-0903','1602694821_CV of Khamidullin Olzhas, rus version.doc','2020-10-14 17:00:21',NULL,NULL,0,NULL),(43,'Живетин Владлен Вячеславович',5,5,'zhivl@inbox.ru','8(778) 411-1337','1602773485_Резюме Zhivetin  RU.doc','2020-10-15 14:51:25','Прошу рассмотреть мою вакансию, Стаж работы более 30 лет, образование высшие, дата рождения 1969 год. Последнее место работы \"Тенгизшевроил\" 30 лет, на руководящей должности.  ','https://dropmefiles.com/T00Z4',0,NULL),(44,'Алимов Султан Жанатович',5,2,'sultanalimov098@gmail.com','8(708) 651-2881','1603266165_CV Султан Алимов (2).pdf','2020-10-21 07:42:45',NULL,NULL,0,NULL),(45,'Алимов Султан Жанатович',5,2,'sultanalimov098@gmail.com','8(708) 651-2881','1603266181_CV Султан Алимов (2).pdf','2020-10-21 07:43:01',NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `career_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,NULL,'Собственные <br> торговые марки','','',NULL,1,1,1,'2019-11-09 09:49:37'),(2,NULL,'Продукция производства <br> наших партнеров','','',NULL,1,2,1,'2019-11-09 09:49:49'),(3,1,'Сыры','','',NULL,2,3,1,'2019-11-09 09:58:14'),(4,3,'Достойный выбор','','','1573465271_достойный выбор.png',3,4,1,'2019-11-09 09:59:31'),(5,2,'Сыры ','','',NULL,2,5,1,'2019-11-09 10:00:04'),(6,3,'Белорусские сыры','','','1573465255_Белорусские сыры.png',3,6,1,'2019-11-09 10:00:57'),(7,5,'Шостка','','','1573465285_Шостка.png',3,7,1,'2019-11-09 10:01:23'),(8,1,'Продукция МПК','','',NULL,2,8,1,'2019-11-09 10:04:26'),(9,8,'Дәстүр Дәмі','','','1573465240_Дастур Дами.png',3,9,1,'2019-11-09 10:05:07'),(10,5,'Комо','','','1573296601_Комо.png',3,10,1,'2019-11-09 10:25:48'),(11,5,'Пирятин','','','1573465648_Пирятин.png',3,11,1,'2019-11-09 10:27:21'),(12,5,'Сваля','','',NULL,3,12,1,'2019-11-09 10:27:37'),(13,5,'Брест-Литовск','','','1573465849_Брест-литовск.png',3,13,1,'2019-11-09 10:27:51'),(14,5,'Арла','','','1573465863_арла.png',3,14,1,'2019-11-09 10:28:06'),(15,5,'Княжна','','',NULL,3,15,1,'2019-11-09 10:28:22'),(16,1,'Масло сливочное','','',NULL,2,16,1,'2019-11-09 10:29:15'),(17,2,'Масло сливочное','','',NULL,2,17,1,'2019-11-09 10:29:31'),(18,16,'Достойный выбор','','',NULL,3,18,1,'2019-11-09 10:29:52'),(19,16,'Эталон Вкуса','','','1573465918_Эталон вкуса.png',3,19,1,'2019-11-09 10:30:12'),(20,17,'Верхний Услон','','',NULL,3,20,1,'2019-11-09 10:30:29'),(21,17,'Украина','','',NULL,3,21,1,'2019-11-09 10:30:47'),(22,17,'Авангард','','',NULL,3,22,1,'2019-11-09 10:31:01'),(23,17,'Воронеж','','',NULL,3,23,1,'2019-11-09 10:31:16'),(24,17,'Русагро','','',NULL,3,24,1,'2019-11-09 10:31:34'),(25,8,'МПК','','','1573466231_МПК.png',3,25,1,'2019-11-09 10:33:23'),(26,9,'Деликатесы','','',NULL,4,26,1,'2019-11-09 10:33:44'),(27,9,'Колбасы полукопченые','','',NULL,4,27,1,'2019-11-09 10:33:57'),(28,9,'Колбасы вареные','','',NULL,4,28,1,'2019-11-09 10:34:17'),(29,9,'Сардельки сосиски','','',NULL,4,29,1,'2019-11-09 10:34:32'),(30,9,'Фарши замороженные','','',NULL,4,30,1,'2019-11-09 10:36:27'),(31,9,'Полуфабрикаты замороженные','','',NULL,4,31,1,'2019-11-09 10:37:08'),(33,25,'Колбасы полукопченые','','',NULL,4,32,1,'2019-11-09 10:38:44'),(34,25,'Колбасы вареные','','',NULL,4,33,1,'2019-11-09 10:39:18'),(35,25,'Сардельки сосиски','','',NULL,4,34,1,'2019-11-09 10:39:45'),(36,25,'Фарши замороженные','','',NULL,4,35,1,'2019-11-09 10:40:20'),(37,25,'Полуфабрикаты замороженные','','',NULL,4,36,1,'2019-11-09 10:40:52'),(38,2,'Куриная продукция','','',NULL,2,37,1,'2019-11-09 10:42:35'),(39,38,'Alfoor','','','1573466091_Альфур.png',3,38,1,'2019-11-09 10:43:38'),(40,38,'Агропродукт','','',NULL,3,39,1,'2019-11-09 10:44:34'),(41,38,'Витебск','','',NULL,3,40,1,'2019-11-09 10:45:02'),(42,38,'Алатау Кус','','',NULL,3,41,1,'2019-11-09 10:45:34'),(43,38,'Окорочка США','','',NULL,3,42,1,'2019-11-09 10:46:07'),(44,2,'Мясная продукция','','',NULL,2,43,1,'2019-11-09 10:46:56'),(45,44,'Говядина','','',NULL,3,44,1,'2019-11-09 10:47:31'),(46,44,'Конина','','',NULL,3,45,1,'2019-11-09 10:48:02'),(47,44,'Субпродукты','','',NULL,3,46,1,'2019-11-09 10:48:30'),(48,1,'Морепродукты','','',NULL,2,47,1,'2019-11-09 10:55:10'),(49,2,'Морепродукты','','',NULL,2,48,1,'2019-11-09 10:55:46'),(50,49,'Морская рыба','','',NULL,3,49,1,'2019-11-09 10:56:57'),(51,48,'Актобе балык','','',NULL,3,50,1,'2019-11-09 10:57:27'),(52,2,'Майонезы, кетчупы и соусы','','',NULL,2,51,1,'2019-11-09 10:57:59'),(53,52,'Mr Ricco','','','1573466259_Мр Рикко (2).png',3,52,1,'2019-11-09 10:58:39'),(54,66,'Майонезы','','',NULL,4,53,1,'2019-11-09 10:59:15'),(55,66,'Кетчупы','','',NULL,4,54,1,'2019-11-09 10:59:42'),(56,66,'Соусы белые','','',NULL,4,55,1,'2019-11-09 11:00:25'),(57,66,'Соусы красные','','',NULL,4,56,1,'2019-11-09 11:00:55'),(58,66,'Томатная паста','','',NULL,4,57,1,'2019-11-09 11:01:21'),(59,2,'Замороженные овощи и котлеты','','',NULL,2,58,1,'2019-11-09 11:01:51'),(60,59,'Бондюэль','','','1573466284_Бондюэль.png',3,59,1,'2019-11-09 11:02:21'),(61,2,'Прочее','','',NULL,2,60,1,'2019-11-09 11:02:48'),(62,61,'Картофель фри','','',NULL,3,61,1,'2019-11-09 11:03:16'),(63,61,'Сосиски','','',NULL,3,62,1,'2019-11-09 11:04:00'),(64,61,'Чай','','',NULL,3,63,1,'2019-11-09 11:04:28'),(65,61,'Сырье для B2B (кожа куриная, жир)','','',NULL,3,64,1,'2019-11-09 11:05:12'),(66,52,'Миладора','','','1573529844_Миладора.png',3,65,1,'2019-11-12 03:37:24'),(68,53,'Майонезы ','','',NULL,4,66,1,'2019-11-12 05:08:43'),(69,53,'Кетчупы','','',NULL,4,67,1,'2019-11-12 05:09:25'),(70,53,'Соусы белые','','',NULL,4,68,1,'2019-11-12 05:09:46'),(71,53,'Соусы красные','','',NULL,4,69,1,'2019-11-12 05:10:02'),(72,53,'Томатная паста','','',NULL,4,70,1,'2019-11-12 05:11:08');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_vacancy`
--

DROP TABLE IF EXISTS `category_vacancy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_vacancy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `metaName` varchar(255) DEFAULT NULL,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaDesc_en` text,
  `metaDesc_kz` text,
  `metaKey` text,
  `metaKey_en` text,
  `metaKey_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_vacancy`
--

LOCK TABLES `category_vacancy` WRITE;
/*!40000 ALTER TABLE `category_vacancy` DISABLE KEYS */;
INSERT INTO `category_vacancy` VALUES (1,'Маркетинг','','','','','','','','','','',''),(2,'Менеджмент','','','','','','','','','','',''),(4,'Склад','','','','','','','','','','',''),(5,'Административный персонал','','','','','','','','','','',''),(6,'Торговый отдел','','','','','','','','','','','');
/*!40000 ALTER TABLE `category_vacancy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Нур-Султан'),(2,'Алматы'),(3,'Караганда'),(4,'Костанай'),(5,'Актобе'),(6,'Атырау'),(7,'Шымкент');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_feedback`
--

DROP TABLE IF EXISTS `client_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `reply` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_feedback`
--

LOCK TABLES `client_feedback` WRITE;
/*!40000 ALTER TABLE `client_feedback` DISABLE KEYS */;
INSERT INTO `client_feedback` VALUES (1,0,'тараз','тест','тест','тест','тест','2019-09-30 11:22:42',0,NULL),(2,0,'улица 36','damn','neor','asdasdad','123123123213','2019-10-29 09:26:11',0,NULL),(3,0,'Розыбакиева 45/17','апап','апапап','апа@ma.rr','8(545) 454-5454','2019-12-12 02:41:56',0,NULL),(4,2,'asdsaasdasd','12323dasd','123123123','asdasdasd','8(123) 211-2312','2019-12-26 08:42:51',0,NULL),(5,2,'asdsaasdasd','12323dasd','123123123','asdasdasd','8(123) 211-2312','2019-12-26 08:43:04',0,NULL),(6,2,'asdsaasdasd','12323dasd','123123123','asdasdasd','8(123) 211-2312','2019-12-26 08:44:14',0,NULL),(7,1,'qewqasdd','123123123','qwasdasd','12312312312','8(231) 231-2312','2019-12-26 08:46:18',0,NULL),(8,1,'Сарайшык 5/1','пока нет ','Таглимова Айсулу Сериковна ','taglimovaaa@gmail.com','8(700) 111-6478','2020-06-04 09:30:22',0,NULL),(9,2,'пос. Бескайнар, 225','ТОО \"Oi-Qaragai Lesnaya Skazka\"','Жанна Баймагамбетова','Baimagambetova.Zh@skazka.kz','8(701) 747-2460','2020-06-30 09:13:04',0,NULL),(10,2,'мкр.Жетысу-1,23 А','Три угла','Кудайбергенова','mariyam-1965@mail.ru','8(727) 373-6349','2020-07-15 02:49:06',0,NULL),(11,5,'А.Молдагулова 52Б','Пинта Бар гриль','Турдакын Нурлан Кенесханович','pinta.mgr.aktobe@gmail.com','8(747) 665-6741','2020-07-31 16:45:48',0,NULL),(12,1,'XXX','XXX','DEL MONTE','aleguyader@freshdelmonte.com','8(066) 131-4607','2020-08-12 09:59:05',0,NULL),(13,1,'dSxLYctalDhy','upWdoXytfBJYEVHz','gotXdIVDxMKA','carolinejones10463@gmail.com','7001446439','2020-08-24 03:55:28',0,NULL),(14,1,'oyeqUFHifC','njcTNZMiBqmRPJ','miQdMBXwUZrNFo','carolinejones10463@gmail.com','2558905692','2020-08-24 03:56:16',0,NULL),(15,6,'северная промззона 51','карабатан','Жанар Енсегенова','ensegenova_2016@mail.ru','8(712) 276-3614','2020-09-08 06:24:30',0,NULL),(16,1,'wKrktQoO','VWjcmdOuZ','KqvIfhUpMsujd','westeremma2@gmail.com','3079842364','2020-09-10 07:50:31',0,NULL),(17,1,'IoGznfZtbkrQsUg','OXhToPlZQxYJGEmN','vWFYPkTcDizLU','westeremma2@gmail.com','7162378063','2020-09-10 07:50:33',0,NULL),(18,1,'ecyIDfCYzSQs','ZqoErjszUOGci','TPFpLyUtbwKmj','tatedelphia269@gmail.com','3929069748','2020-09-18 09:57:17',0,NULL),(19,1,'vpcSWIPy','biSEWMKQpXCqhLG','KEyVufGNWPzpAcm','tatedelphia269@gmail.com','6989174275','2020-09-18 09:57:19',0,NULL),(20,1,'EGDCVdjzNxkfgaA','nXxtdSrH','jDJzIQpSnBV','vularlyob@gmail.com','8633153556','2020-09-25 13:25:17',0,NULL),(21,1,'qZzucRgFP','FeiBIJrcE','ofzuPQOgYFpEK','vularlyob@gmail.com','7767576958','2020-09-25 13:25:19',0,NULL),(22,7,'ZPdfYDApwN','oHnBsLrfTAc','ucbZaiQfLrvC','ranririmono@gmail.com','2469843843','2020-10-04 21:39:07',0,NULL),(23,7,'XaxotuZqGycfT','gNfGmtKMwjPodz','YckGAViKprBz','ranririmono@gmail.com','9281980059','2020-10-04 21:39:09',0,NULL),(24,7,'GFvNbrwIf','upVvHswPFNLzlgC','EBPIwbqYpWVFT','forrakingnit2005@gmail.com','6352827478','2020-10-11 01:43:39',0,NULL),(25,7,'OJCminMhkuFl','JdULgsRvnkG','uRmBaQGLcYpf','forrakingnit2005@gmail.com','4969001364','2020-10-11 01:43:40',0,NULL),(26,7,'tVjohMXJpsEzygk','gGmciWFy','ROvDCewpzHjcoJQn','oughlesedrx@gmail.com','6826108391','2020-10-17 19:47:23',0,NULL),(27,7,'xJujNyYMSfabV','CaJlkzMdVbIsDgOP','KRSdNxfuehzsn','oughlesedrx@gmail.com','3540141039','2020-10-17 19:47:25',0,NULL),(28,7,'AkgVCGcU','aJMkvAQiBrfSg','SpoHPOqLMJvDwu','therisualhl@gmail.com','5481789518','2020-10-23 19:16:14',0,NULL),(29,7,'xyJkrmphnloME','mlPpEOoiAdjyRg','LsGWFINnHeyjMPod','therisualhl@gmail.com','2217808183','2020-10-23 19:16:15',0,NULL),(30,1,'ЖК \"Seven\"','Продуктовые магазины','Ескендиров Султан Нурланович','s.yeskendirov@mail.ru','8(702) 727-7411','2020-10-29 05:22:47',0,NULL),(31,7,'CNYncgwlKTfUAr','aohVAOlncjJywN','dwqRnUsPJDxmhFK','woshiniamatsu@gmail.com','6974745318','2020-10-30 13:20:42',0,NULL),(32,7,'APniczXQlERs','UzYwRVhjsKIrZDL','SdyAjkbMrKoJCQ','woshiniamatsu@gmail.com','9897201858','2020-10-30 13:20:43',0,NULL),(33,7,'OprQKxbDfzLMWRIa','XIFyUClveKZktpTP','ITcOjtLd','tiohypquaer@gmail.com','4285156681','2020-11-14 08:00:10',0,NULL),(34,7,'fnCteogsHB','ljDuZxhKSyCwoT','mWloUsGEbQByAuXt','tiohypquaer@gmail.com','3108504494','2020-11-14 08:00:11',0,NULL),(35,7,'bKdMOUeJS','AcJFYgoNSqw','iCtOfzXAro','dhochkufecxa73@gmail.com','4781777760','2020-11-23 14:43:15',0,NULL),(36,7,'yOPfkHQqbXZYpj','nTYMEpIs','eqOJcIsfXtpEumLi','dhochkufecxa73@gmail.com','3022517875','2020-11-23 14:43:16',0,NULL);
/*!40000 ALTER TABLE `client_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `address_en` text,
  `name_kz` varchar(255) DEFAULT NULL,
  `address_kz` text,
  `status` int(11) NOT NULL,
  `statusCity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'г. Алматы','Жетысуский район, мкр. Первомайский, ул. Механическая 26 В\r\n\r\n','+7 (727) 260 28 35','info@rokos.kz','г. Алматы','Жетысуский район, мкр. Первомайский, ул. Механическая 26 В','г. Алматы','Жетысуский район, мкр. Первомайский, ул. Механическая 26 В',0,0),(2,'г. Шымкент','ул. Койгельды батыра, здание 9/5\r\n\r\n','+7 (7252) 61 00 39','info@rokos.kz','г. Шымкент','ул. Койгельды батыра, здание 9/5','г. Шымкент','ул. Койгельды батыра, здание 9/5',0,0),(3,'г. Атырау','поселок «Мирный», ул. Ашхабадская 6\r\n\r\n','+7 (7122) 25 57 85','info@rokos.kz','г. Атырау','поселок «Мирный», ул. Ашхабадская 6','г. Атырау','поселок «Мирный», ул. Ашхабадская 6',0,0),(4,'г. Нур-Султан','ул. Пушкина, 75','+7 (7172) 67 72 83  <br>  +7 (7172) 67 72 84','info@rokos.kz','г. Нур-Султан','ул. Пушкина, 75','г. Нур-Султан','ул. Пушкина, 75',1,0),(6,'г. Караганда','ул. Пичугина, 4, строение 4/3\r\n\r\n','+7 (7212) 51 08 01 <br>  +7 (7212) 98 04 40','info@rokos.kz','г. Караганда','ул. Пичугина, 4, строение 4/3','г. Караганда','ул. Пичугина, 4, строение 4/3',0,0),(7,'г. Костанай','Костанайский район, п. Затобольск, ул. Семина, 30/2\r\n\r\n','+7 (71455) 2 59 21','info@rokos.kz','г. Костанай','Костанайский район, п. Затобольск, ул. Семина, 30/2','г. Костанай','Костанайский район, п. Затобольск, ул. Семина, 30/2',0,0),(8,'г. Актобе','41 разъезд, Курсантское шоссе 16 А\r\n\r\n','+7 (7132) 98 33 52','info@rokos.kz','г. Актобе','41 разъезд, Курсантское шоссе 16 А','г. Актобе','41 разъезд, Курсантское шоссе 16 А',0,0),(9,'г. Петропавловск','','+7 (7172) 67 72 83   +7 (7172) 67 72 84 ',' info@rokos.kz','г. Петропавловск','','г. Петропавловск','',0,1),(10,'г. Кокшетау','','+7 (7172) 67 72 83 <br>  +7 (7172) 67 72 84  ','info@rokos.kz','г. Кокшетау','','г. Кокшетау','\r\n\r\n',0,1),(11,'г. Павлодар ','','+7 (7172) 67 72 83 <br> +7 (7172) 67 72 84  ','info@rokos.kz','г. Павлодар ','','г. Павлодар ','',0,1),(12,'г. Сатпаев','',' +7 (7212) 51 08 01 <br>  +7 (7212) 98 04 40  ','info@rokos.kz','г. Сатпаев','\r\n','г. Сатпаев','',0,1),(13,'г. Жезказган','','+7 (7212) 51 08 01 <br> +7 (7212) 98 04 40  ','info@rokos.kz','г. Жезказган','','г. Жезказган','\r\n\r\n',0,1),(14,'г. Семей ','\r\n\r\n','+7 (7212) 51 08 01 <br> +7 (7212) 98 04 40 ',' info@rokos.kz','г. Семей ','','г. Семей ','',0,1),(15,'г. Усть-Каменогорск','','+7 (7212) 51 08 01 <br> +7 (7212) 98 04 40 ',' info@rokos.kz','г. Усть-Каменогорск','','г. Усть-Каменогорск','\r\n\r\n',0,1),(16,'г. Капчагай','\r\n\r\n','+7 (727) 260 28 35 ',' info@rokos.kz','г. Капчагай','\r\n','г. Капчагай','\r\n',0,1),(17,'г. Талдыкорган','\r\n\r\n','+7 (727) 260 28 35  ','info@rokos.kz','г. Талдыкорган','','г. Талдыкорган','',0,1),(18,'г. Кызылорда ','\r\n','+7 (7252) 61 00 39  ','info@rokos.kz','г. Кызылорда ','','г. Кызылорда ','',0,1),(19,'г. Тараз','','+7 (7252) 61 00 39 ',' info@rokos.kz','г. Тараз','','г. Тараз','',0,1),(20,'г. Уральск','','+7 (7132) 98 33 52 ',' info@rokos.kz','г. Уральск','','г. Уральск','',0,1),(21,'г. Актау ','','+7 (7132) 98 33 52  ','info@rokos.kz','г. Актау ','','г. Актау ','',0,1);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailforrequest`
--

DROP TABLE IF EXISTS `emailforrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailforrequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailforrequest`
--

LOCK TABLES `emailforrequest` WRITE;
/*!40000 ALTER TABLE `emailforrequest` DISABLE KEYS */;
INSERT INTO `emailforrequest` VALUES (1,'info@rokos.kz',0);
/*!40000 ALTER TABLE `emailforrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `isRead` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `city_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `reply` text,
  `topic` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (43,'Евгенич','8(771) 263-6218','evgeniya.malkina.90@mail.ru','Ищу торгового представителя вашей фирмы в городе Темиртау',1,'2019-12-13 10:09:17',0,0,NULL,''),(61,'Аутсорсинг','+74991234567','kononovmikhailjvq@mail.ru','Здравствуйте! \r\nМы удовлетворим все потребности вашей компании в контроле качества, аудитах поставщиков, внутренних аудитах, \r\nпостроении и поддержке систем управления качеством на основе ISO 9001, 22000,16949, 17025,15189 и 17024. \r\nУ нас работают профессионалы в  управлении качеством с солидным международным опытом, однако стоимость \r\nнаших услуг начинается всего лишь от 40 000 рублей! Полный перечень наших сервисов и преимуществ \r\nвы найдете на сайте https://qmconsult.ru \r\nИли напишите нам: customer@qmconsult.ru. \r\nС уважением, \"QM consult\".',1,'2019-12-13 10:08:21',0,0,NULL,''),(64,'максим','8(778) 112-0002','naumkin_maks@mail.ru','здравствуйте, интересует цена на сырный продукт и брест литовск сыры',1,'2019-12-13 10:09:07',0,0,NULL,''),(79,'Alexander Pindrikov','8(421) 917-1712','pindrikov@ewals.sk','Интересуют услуги автотранспорта по регулярным перевозкам из Голландии в Казахстан рефрижераторами и изотермами. При вашем интересе готов выслать подробную информацию',1,'2019-12-13 10:07:09',0,0,NULL,''),(82,'Сергей','8(081) 191-183_','sergei.l75@mail.ru','Интересует сёмга и форель',1,'2019-12-19 05:21:29',0,0,NULL,''),(156,'Александр','+74999999999','sales@seomozg.biz','Мы рады представить инструмент увеличения продаж, при помощи качественного SEO продвижения. \r\nМы оптимизируем код и контент Вашего сайта под SEO и начнем продвигать его в поиске Google и Яндекс по целевым ключевым словам. \r\n \r\nSEO продвижение является выгодным вложением денег в развитие бизнеса, увеличения продаж, и рекламы своих услуг в сети Интернет. Согласно данным множества исследований – 80% переходов на сайты поступает именно с поиска. \r\nПодавляющее большинство посетителей относятся к органическим результатам выдачи с большим доверием, чем к рекламе. Сайт занимаемый высокие позиции в Google и Яндекс, будет стабильно приносить клиентов на протяжении долгого времени. \r\n \r\nПродвижение в ТОП5 – от 15 000 до 25 000 руб. в месяц под ключ. Точный бюджет будет известен после обсуждения деталей. \r\n \r\nОбращайтесь – Почта: sales@seomozg.biz ,  Skype: alex-mdplus ,   Telegram: @alexvtop1 , Сайт: seomozg.biz',1,'2019-12-19 05:21:29',0,0,NULL,''),(161,'Ирина','8(707) 819-5508','irinazh78@mail.ru','Здравствуйте, не могу с вами связаться',1,'2019-12-19 05:21:29',0,0,NULL,''),(167,'Гульнара ','8(705) 705-7053','pavel90@mail.ru','Ваш менеджер Шалкар оставил прайс \r\nВчера ему звонила,скинул трубку и сам не перезвонил \r\nВопрос нахрена тогда бумагу переводите\r\nЕсли трубки не поднимаете',1,'2019-12-19 05:21:29',0,0,NULL,''),(179,'Мади','8(776) 525-8888','madi.tambiev@gmail.com','Хотел узнать по рыбе ',1,'2019-12-24 09:18:34',0,0,NULL,''),(180,'Акзияш ','8(776) 861-9201','k.akzi@mail.ru','Собираюсь открыть продуктовый магазин в Шиели. Интересно что вы можете предложить',1,'2019-12-24 09:18:41',0,0,NULL,''),(181,'Юлия','8(747) 404-2893','yulia_369@mail.ru','Добрый день, интересует сырная продукция, прайс, цены, и условия работы. СПАСИБО',1,'2019-12-26 12:31:42',2,0,NULL,'Сотрудничество'),(182,'Ахмад Сила','8(213) 123-1231','kavkaz.sila@ahmad.co','ahmad SILA',1,'2019-12-26 14:15:36',1,0,NULL,'Эта тема'),(183,'Зарина7','8(707) 877-9070','zarina.dosanova.89@mail.ru','Здраствуйте хотела бы заказывать у вас продукцию для ресторана могли бы вы мне скинуть свой прайс на вацаб',1,'2020-01-21 09:19:24',1,0,NULL,'заказ продукции'),(184,'Элизат','8(707) 608-0990','elizat.isaeva@mail.ru','Нужна куриная грудка , хочу узнать цену, хотела бы заказать себе домой одну коробку',1,'2020-02-03 04:16:32',1,0,NULL,'куриная грудка'),(185,'Зуля ','8(702) 220-1050','nazarbayeva@mail.ru','Прайс лист подучить',1,'2020-04-22 03:47:40',2,0,NULL,'Закуп продуктов'),(186,'Рустем','8(777) 889-4509','yuldabaev.rustem@mail.ru','На вакансию директор филиала',1,'2020-04-22 03:47:40',2,0,NULL,'Трудоустройство'),(187,'Рустем','8(777) 889-4509','yuldabaev.rustem@mail.ru','На вакансию директор филиала.Алматы',1,'2020-04-22 03:47:40',2,0,NULL,'Трудоустройство'),(188,'Николай, г. Чебоксары','8(937) 959-0715','mpts@inbox.ru','Добрый день! У вас объявлена вакансия через рекрутинговое агентство \"Руководитель проекта по развитию транспортно-логистического бизнеса\". Хочу уточнить у компетентного по данному вопросу лица о состоянии на данный момент ситуации по подбору кандидата на данную вакансию. В агентстве мне сообщили, что решение будет на следующей неделе. Я, как участник конкурса на данную вакансию заинтересован получить ответ в ближайшие 2 дня для дальнейшего планирования своих действий в поисках подходящего мне проекта. Со своей стороны сообщаю, что ваша вакансия мне интересна, имею релевантный опыт работы в данном направлении и готов реализовать его на ваших площадках. При необходимости отправлю резюме на указанный вами адрес. Буду признателен за ответ 04.02.2020',1,'2020-04-22 03:47:40',1,0,NULL,'Участие в конкурсе на вакансию \"Руководитель проекта по развитию транспортно-логистического бизнеса\"'),(189,'Николай, г. Чебоксары','8(937) 959-0715','mpts@inbox.ru','Добрый день! У вас объявлена вакансия через рекрутинговое агентство \"Руководитель проекта по развитию транспортно-логистического бизнеса\". Хочу уточнить у компетентного по данному вопросу лица о состоянии на данный момент ситуации по подбору кандидата на данную вакансию. В агентстве мне сообщили, что решение будет на следующей неделе. Я, как участник конкурса на данную вакансию заинтересован получить ответ в ближайшие 2 дня для дальнейшего планирования своих действий в поисках подходящего мне проекта. Со своей стороны сообщаю, что ваша вакансия мне интересна, имею релевантный опыт работы в данном направлении и готов реализовать его на ваших площадках. При необходимости отправлю резюме на указанный вами адрес. Буду признателен за ответ 04.02.2020',1,'2020-04-22 03:47:40',1,0,NULL,'Участие в конкурсе на вакансию \"Руководитель проекта по развитию транспортно-логистического бизнеса\"'),(190,'Алексей','8(798) 805-0459','a.voronin@germes-logistic.ru','груз из.г.Волжский Волгоградская область в г. Макинск Казахстан, 82 куба, тент',1,'2020-04-22 03:47:40',2,0,NULL,'атогрузоперевозки'),(191,'Гулназ','8(708) 088-0039','gulnaz_89_89@list.ru','Продукции',1,'2020-04-22 03:47:40',2,0,NULL,'Есть Ли в Алматы ваши продукции?'),(192,'Рустам ','8(701) 457-0077','rustam-ismailov@list.ru','По каким номерам я могу связаться в г. Актобе , по указанным контактам невозможно дозвониться.',1,'2020-04-22 03:47:40',5,0,NULL,'Не могу дозвониться по указанным номерам'),(193,'Анат','8(702) 578-1669','daulenulyanat@gmail.com','Добрый день, Уважаемые Господа. Не смог дозвониться до Вас по указанным контактам на сайте. По поводу сотрудничества хотелось бы переговорить. Благодарю э.',1,'2020-04-22 03:47:40',1,0,NULL,'Сотрудничество '),(194,'Арат','8(702) 578-1669','daulenulyanat@gmail.com','Добрый день, уважаемые Господа. Не дозвонился по указанным контактам на сайте. ',1,'2020-04-22 03:47:40',1,0,NULL,'Сотрудничество '),(195,'Анат','8(702) 578-1669','daulenulyanat@gmail.com','Добрый день, уважаемые Господа! не могу дозвониться по указанным контактным данным на сайте. Прошу связаться для обсуждения взаимовыгодного сотрудничества.',1,'2020-04-22 03:47:40',1,0,NULL,'Сотрудничество '),(196,'Бекали','8(701) 667-8989','konisovbekali@gmail.com','Я не робот ',1,'2020-04-22 03:47:40',6,0,NULL,'Нужен товар'),(197,'Алия','8(701) 836-9305','tulegenova_aliya@bk.ru','Кур.филе',1,'2020-04-22 03:47:40',5,0,NULL,'Кур.филе, фарш, ягоды'),(198,'Зарина','8(705) 717-4552','ls_astana2009@mail.ru','Интересует закуп продуктов в производственную столовую, на 200 человек',1,'2020-04-22 03:47:40',1,0,NULL,'Закупка продуктов'),(199,'Зарина','8(705) 717-4552','ls_astana2009@mail.ru','Закуп продктов',1,'2020-04-22 03:47:40',1,0,NULL,'Закупка продуктов'),(200,'Зарина','8(705) 717-4552','ls_astana2009@mail.ru','Закуп продуктов',1,'2020-04-22 03:47:40',1,0,NULL,'Закупка продуктов'),(201,'Айна','8(771) 599-8759','ainalg2014@gmail.com','Интересует цены на мясо без кости в коробке',1,'2020-04-22 03:47:40',5,0,NULL,'Уены на продукцию'),(202,'Айна','8(771) 599-8759','ainalg2014@gmail.com','Интересуют цены на мясо без кости в коробках',1,'2020-04-22 03:47:40',5,0,NULL,'Цены'),(203,'Нурбол','8(708) 111-9389','otdel_torgovli@list.ru','перезвоните пожалуйста ',1,'2020-04-22 03:47:40',1,0,NULL,'касательно работы ТЛЦ '),(204,'Хамзат','8(705) 628-0718','Zhanalinh@mail.ru','Просим Вас отправить прайст-лист на куринную проукцию',1,'2020-04-22 03:47:40',3,0,NULL,'Оптовые поставки куринные продукции'),(205,'Жандос','8(747) 113-1981','zh.zhienaliev@gmail.com','Хотел заказать ',1,'2020-04-22 03:47:40',5,0,NULL,'Хотел заказать '),(206,'Михаил','8(000) 000-0000','dverland@mail.ru','прошу выслать Ваш оптовый прайс по рыбе и рыбной продукции.',1,'2020-04-22 03:47:40',2,0,NULL,'оптовый прайс на рыбу'),(207,'Куаныш','8(701) 531-6639','sultanov.kuanysh@mail.ru','Открываем магазин. Нужен ваш прайс лист',1,'2020-11-27 05:19:24',5,0,NULL,'Открываем магазин'),(208,'Асель','8(777) 314-3471','asel656@mail.ru','Прайс на сыры',1,'2020-11-27 05:19:24',1,0,NULL,'Сыры'),(209,'Бакыт','8(777) 110-9757','b.bakit@mail.ru','д.д. надеюсь на долгое сотрудничество. прошу предоставить прайс вашей продукции. имеется рефрижератор 2х тонник. ',1,'2020-11-27 05:19:24',5,0,NULL,'Сотрудничество. закуп.'),(210,'Есет','8(777) 660-3334','eset_swa@mail.ru','Спасибо ',1,'2020-11-27 05:19:24',5,0,NULL,'На счёт куриного мяса'),(211,'Ева','8(747) 024-6945','amaru_13@mai.ru','очень важно',1,'2020-11-27 05:19:24',2,0,NULL,'калорийность продуктов,состав и бжу '),(212,'Марал','8(700) 000-7114','mbadyrova@mail.ru','Здравствуйте! Возможно ли купить 1 коробку горбушы как физ.лицо с доставкой в магазин рядом с домом?',1,'2020-11-27 05:19:24',3,0,NULL,'Купить рыбу '),(213,'Салтанат','8(701) 693-7271','saltuna_89@mail.ru','Здравствуйте.  Можете отправить контакты торг представителя г. Нур султан',1,'2020-11-27 05:19:24',1,0,NULL,'Торг представитель '),(214,'Салтанат','8(701) 693-7271','saltuna_89@mail.ru','Здравствуйте. Отправьте контакты торг представителя г. Нур султан ',1,'2020-11-27 05:19:24',1,0,NULL,'Торг представитель '),(215,'Жазира ','8(775) 337-9944','zhazi626@mail.ru','Дистрюбетер ',1,'2020-11-27 05:19:24',5,0,NULL,'Сотрудничество'),(216,'Павел','8(705) 730-9809','dpi-89@inbox.ru','Добрый день! Наша компания нуждается в долгосрочной аренде площади морозильного склада.  ',1,'2020-11-27 05:19:24',5,0,NULL,' аренда площади в морозильном складе'),(217,'Серик Досмухамбетов','8(777) 505-4905','biznes.trener@inbox.ru','Добрый день.\r\nЗаинтересовала вакансия Менеджер по субсидированию. На какой адрес можно отправить резюме и с кем можно уточнить по этой вакансии? \r\nСпасибо за оперативность! ',1,'2020-11-27 05:19:24',5,0,NULL,'Вакансия Менеджер по субсидированию'),(218,'Михаил','8(776) 755-2508','mati_m.v_90@mail.ru','Уточнить цены',1,'2020-11-27 05:19:24',5,0,NULL,'Цены на товар'),(219,'Михаил Мати','8(777) 675-5250','mati_m.v_90@mail.ru','хотелось бы увидеть праис на продукцию',1,'2020-11-27 05:19:24',5,0,NULL,'Праис на продукцию'),(220,'Дильшод ','8(777) 999-7131','po_hodudilik@mail.ru','Полуфабрикаты (фарш, тесто)',1,'2020-11-27 05:19:24',7,0,NULL,'Коммерческое предложения по замороженной  продукции (фарш, тесто) '),(221,'Улугбек','8(701) 518-5419','yuldashev_u@inbox.ru','прошу перезвонить срочно',1,'2020-11-27 05:19:24',7,0,NULL,'сотрудничество'),(222,'Улугбек','8(701) 518-5419','yuldashev_u@inbox.ru','прошу связаться срочно',1,'2020-11-27 05:19:24',7,0,NULL,'сотрудничество'),(223,'Эльмира','8(700) 690-7175','elmirabektemisova@gmail.com','Здравствуйте.  Купили рыбу(селёдку)в гипермаркете арзан г Алматы. Она оказалась вся в паразитах. Они сказали что не несут ответственность за это. И указали на Вашу компанию.  У меня вопрос к Вам. Почему Ваша компания продаёт зараженную рыбу? Вы ведь поставляете её по всему Казахстану. Получается кормите всех людей этим. Кто за это будет нести ответственность? Ваш менеджер в Алматы сказал что это нормальное явление и вы имеете право продавать рыбу с паразитами. Мне придётся обратиться в СЭС с жалобой. И опубликовать видео и фото съёмки в социальных сетях с названием Вашей компании. Жду ответа. ',1,'2020-11-27 05:19:24',2,0,NULL,'Паразиты в Вашей поставляемой рыбе'),(224,'Анастасия','8(705) 423-4867','privet_2101@mail.ru','Умение работать любой программой ппк',1,'2020-11-27 05:19:24',1,0,NULL,'Ищу работу торговым представителем имеется стаж более 4лет'),(225,'Нурсултан','8(702) 253-7720','nursikkz@mail.ru','Информация по продуктам ',1,'2020-11-27 05:19:24',5,0,NULL,'Общая информация по продуктам '),(226,'Елена','8(917) 527-6111','etarabukina@myasoff.ru','Добрый день !\r\nНаша компания  является производителем мясной, рыбной, молочной консервации.\r\nЦентральный офис и склад находятся в Москве. Заводы в Орловской и  Калининградской областях .\r\nПрошу рассмотреть сотрудничество . Ищем компании, готовые  стать постоянными партнерами .\r\n\r\nПредлагаемые консервы в трех товарных категориях –\r\nМясные консервы торговой марки «Товарищ Мясофф»\r\nМолочные консервы торговой марки «Молочный стандарт»\r\nРыбные консервы торговой марки «ТралФлот»\r\n \r\nНадеюсь на Ваш интерес к совместной работе. \r\nБуду ждать обратную связь.\r\nГотова ответить более подробно на вопросы , в случае их возникновения.\r\n\r\n\r\n\r\n\r\nС Уважением,\r\nМенеджер по работе с ключевыми клиентами\r\nТарабукина Елена .\r\n8-915-364-84-17\r\n',1,'2020-11-27 05:19:24',1,0,NULL,'сотрудничество'),(227,'Елена','8(917) 527-6111','etarabukina@myasoff.ru','Добрый день !\r\nНаша компания  является производителем мясной, рыбной, молочной консервации.\r\nЦентральный офис и склад находятся в Москве. Заводы в Орловской и  Калининградской областях .\r\nПрошу рассмотреть сотрудничество . Ищем компании, готовые  стать постоянными партнероми .\r\n\r\nПредлагаемые консервы в трех товарных категориях –\r\nМясные консервы торговой марки «Товарищ Мясофф»\r\nМолочные консервы торговой марки «Молочный стандарт»\r\nРыбные консервы торговой марки «ТралФлот»\r\n Надеюсь на Ваш интерес к совместной работе. \r\nБуду ждать обратную связь.\r\nГотова ответить более подробно на вопросы , в случае их возникновения.\r\n\r\n\r\n\r\n\r\nС Уважением,\r\nМенеджер по работе с ключевыми клиентами\r\nТарабукина Елена .\r\n8-915-364-84-17\r\n',1,'2020-11-27 05:19:24',1,0,NULL,'сотрудничество'),(228,'Илья','8(707) 883-2877','Kisfart@mail.ru','Аренда холодильного помещения ',1,'2020-11-27 05:19:24',5,0,NULL,'Аренда холодильника'),(229,'Алевтина','8(705) 488-2560','alevtinka8215@mail.ru','Добрый день',1,'2020-11-27 05:19:24',1,0,NULL,'Здравствуйте можно оптовый прайс?'),(230,'Денис','8(771) 144-9555','Kazakstan25today@gmail.com','   Добрый вечер! Прошу посодействовать в отправке трудовых документов. Работу прекратил в Марте. Жду 3 месяц.\r\n',1,'2020-11-27 05:19:24',1,0,NULL,'Расторжение Договора'),(231,'Степанова Катарина Владиславовна','8(908) 292-2112','ved@maslo53.ru','Компания «Масляный Король», основана в ноябре 1998 года. Компания специализируется на производстве растительных масел первого холодного отжима по собственной уникальной технологии, а также дистрибуции собственных торговых марок масел премиум-класса, муки и каш.\r\nБолее чем за 20 лет, компания расширила линейку до 20 сортов масла в различных потребительских фасовках, разработала и произвела экзотическую муку и каши на основе семени льна.\r\n\r\nВ копилки компании более 128 наград в области качества ( на международной выставке «Продэкспо-2020» компания получила 5ое Grand prix).\r\n\r\nПродукция компании присутствует как в локальных магазинах на территории России, так и в сетях федерального значения.\r\n\r\nКачество и безопасность продукции высоко оценили зарубежные партнеры, ежегодно компания наращивает обороты экспорта. На сегодняшний момент компания активно экспортирует в страны ЕС, Китай, Страны ЕАЭС и др.\r\n\r\nВся выпускаемая продукция сертифицирована и соответствует нормам безопасности пищевых продуктов HACCP.\r\n\r\nНаши преимущества:\r\n\r\n- Мы производим только натуральные продукты без различных удешевляющих добавок с 1998 г.;\r\n\r\n- Широкая ассортиментная линейка и яркий дизайн;\r\n\r\n- Известный брэнд в своём сегменте;\r\n\r\n- Долгосрочные отношения с поставщиками сырья;\r\n\r\n- Информационная поддержка товара (бесплатная горячая линия);\r\n\r\n- Участие в промо-акциях покупателя (скидки, каталоги, дегустации и т. д.);\r\n\r\n- Удобное логистическое расположение.\r\n\r\n \r\nС Дополнительной информацией о компании и продуктах можно ознакомиться на https://www.maslo53.ru/\r\n \r\n-- \r\n____________________\r\nС уважением,\r\nменеджер ВЭД\r\nСтепанова Катарина\r\nved@maslo53.ru\r\n+7908-292-21-12 (WeChat, Viber, WhatsApp, Telegram)\r\nhttps://www.maslo53.ru - Наш официальный сайт\r\nhttps://www.magazinmasla.ru - Наш интернет магазин\r\nhttps://vk.com/maslo53vn - Мы вконтакте\r\n',1,'2020-11-27 05:19:24',5,0,NULL,'стать поставщиком масла, муки и каши'),(232,'Дарья','8(707) 172-7847','oatglutenfree@gmail.com','Добрый день! Не смогли до вас дозвониться. Мы - Российский производитель овсяных хлопьев без глютена. Мы активно сотрудничаем с международными компаниями, и сейчас ищем партнеров в Казахстане. Если Вам интересно узнать о нашем продукте, перезвоните или отправьте письмо по e-mail. С уважением, Дарья',1,'2020-11-27 05:19:24',1,0,NULL,'Продукт от производителя'),(233,'Баян','8(707) 830-6959','bayan_dosj@mail.ru','Добрый вечер! Как связаться с вашей бухгалтерией, Мне необходимо получить акты сверок: 1,01,19-1,12,19 год и с 1,01,2020-31,05,2020 год',1,'2020-11-27 05:19:24',2,0,NULL,'ТОО Инсташоп'),(234,'Дархан Булатович','8(775) 828-3565','longfellow_dits@mail.ru','Здравствуйте, Ваш сайт не работает? Не получается отправить резюме, как это можно сделать?',1,'2020-11-27 05:19:24',5,0,NULL,'карьера'),(235,'Алина','8(702) 134-7072','aitinka0130@gmail.com','Добрый день! Прошу связаться со мной для получения прайса на продукты и дальнейшего сотрудничества.',1,'2020-11-27 05:19:24',1,0,NULL,'Закуп продуктов для общепита'),(236,'Алина','8(702) 134-7072','aitinka0130@gmail.com','Здравствуйте! Прошу связаться для дальнейшего сотрудничества',1,'2020-11-27 05:19:24',1,0,NULL,'Закуп продуктов для кафе'),(237,'Галина','8(777) 207-2781','korean.demon@mail.ru','Здравствуйте, не могу вам дозвониться',1,'2020-11-27 05:19:24',1,0,NULL,'Некорректная работа филиала'),(238,'Татьяна','8(017) 309-3730','t.krechko@butb.by','Добрый день. Вас беспокоит ОАО \"Белорусская универсальная товарная биржа\". Отправляла Вам на электронную почту письмо приглашение на участие в биржевых торгах мясом. Дайте, пожалуйста, обратную связь.',1,'2020-11-27 05:19:24',1,0,NULL,'мясо'),(239,'Марьям','8(727) 373-6349','mariyam-1965@mail.ru','Больше месяца торговые не ходят,телефоны отключены.Мы без товара.',1,'2020-11-27 05:19:24',2,0,NULL,'По заказу'),(240,'Марьям','8(727) 373-5349','mariyam-1965@mail.ru','По заказу.',1,'2020-11-27 05:19:24',2,0,NULL,'По заказу'),(241,'Даулет','8(705) 162-9000','Dauletbek@inbox.ru','Состав и энергетическая ценность сосисок мпк 2008. К сожалению на этикетке на сосисках нет никакой информации, кроме названия \"мпк 2008\"',1,'2020-11-27 05:19:24',1,0,NULL,'Сосиски МПК 2000'),(242,'Леонид','8(916) 156-6128','mla@viskotex.ru','МВ-Вискотекс – ПРОИЗВОДИТЕЛЬ и ПОСТАВЩИК: \r\n1.	ПАКЕТОВ  термоусадочных, вакуумных  и  пакетов под  МГС, \r\n2.	Пакетов для Запекания\r\n3.	Плёнки  Верх, Низ ( Прозрачные, черные, белые) , пленки для Флоу Пак,   Термоусадочный  Флоупак, \r\n4.	Жесткие пленки АРЕТ/РЕ\r\n5.	Пленки для  запекания\r\n6.	Пленки для запайки Лотков \r\n7.	Термоусадочные матовые пакеты\r\n8.	Целлюлозная Пленка ( листы и  намотка)\r\n9.	Оболочка из Целлюлозной пленки для ВАРЕНЫХ и ПОЛУКОПЧЕНЫХ колбас, для колбасного сыра.\r\n10.	Этикетки  рулонные самоклейка \r\nПродукция  выпускается   с  печатью  и  без  печати.\r\nС уважением,\r\nМилюшкин Леонид\r\nКоммерческий директор\r\nООО \"МВ-Вискотекс\"\r\nmla@mv-viskotex.ru mla@viskotex.ru\r\nТел. 8 (926)-331-33-07,\r\n        8-916-156-61-28\r\n        8 (495) 790-98-98 доб 333\r\nSkype:+79161566128\r\nwww.mv-viskotex.ru\r\n\r\n',1,'2020-11-27 05:19:24',1,0,NULL,'Сотрудничество'),(243,'Галина','8(916) 660-4862','mamatovagv@dubinino.com','Добрый день! Были Вашими поставщиками по фаршу ММО и куриной разделке пока не уволился наш менеджер. Ищем контакты, чтобы возобновить сотрудничество.',1,'2020-11-27 05:19:24',1,0,NULL,'Предложение по куриной разделке от АПК ДУБИНИНО'),(244,'Лейсан','8(999) 155-2131','zaripova@ncsd.ru','Предложение о сотрудничестве',1,'2020-11-27 05:19:24',1,0,NULL,'сотрудничество'),(245,'Мария','8(795) 250-0403','mariya.designer.kz@gmail.com','Разрабатываю упаковку и этикетку товара. Без предоплаты. \r\nМогу я показать вам свое портфолио?',1,'2020-11-27 05:19:24',1,0,NULL,'Современный дизайн упаковки и этикетки'),(246,'Турдакын Нурлан Кенесханович','8(747) 665-6741','pinta.mgr.aktobe@gmail.com','Жду обратной связи',1,'2020-11-27 05:19:24',5,0,NULL,'приобретать товар у вашей компании'),(247,'Мольдир','8(747) 607-7020','muldir.k@mail.ru','Жалоба на отсутствие мой.выплаты',1,'2020-11-27 05:19:24',5,0,NULL,'Бывший сотрудник'),(248,'Руслан','8(701) 171-7222','mushanov.ruslan@mail.ru','Перезвоните',1,'2020-11-27 05:19:24',1,0,NULL,'Ценник'),(249,'Еламан','8(777) 661-6969','sensationklass@mail.ru','Здравствуйте! Я хотел узнать насет семги',1,'2020-11-27 05:19:24',5,0,NULL,'Рыба'),(250,'Жулдызай','8(702) 936-3878','zhuldyzai.b@gmail.com','Добрый день! Вас беспокоят с компании Appazov Print, мы с вами ранее сотрудничали, просим рассмотреть дальнейшее сотрудничество!',1,'2020-11-27 05:19:24',1,0,NULL,'сотрудничество'),(251,'Александр','8(777) 869-4405','fen-1@mail.ru','Добрый день! Вышлите пожалуйста прайс лист на сыры.',1,'2020-11-27 05:19:24',1,0,NULL,'Прайс лист на сыры'),(252,'Александр','8(777) 869-4405','fen-1@mail.ru','Добрый день! Вышлите пожалуйста прайс лист на сыры.',1,'2020-11-27 05:19:24',1,0,NULL,'Прайс лист на сыры'),(253,'Марат','8(747) 114-2925','222zz04@mail.ru','В Актобе куры оптом хотел купить',1,'2020-11-27 05:19:24',5,0,NULL,'Куры оптом'),(254,'Алишер','8(777) 357-3355','hrystal_love@mail.ru','Хотел получить праис и переговорить по вапросам логистики',1,'2020-11-27 05:19:24',7,0,NULL,'Партнерство'),(255,'Кайрат ','8(707) 918-7412','kake85@mail.ru','Есть самовывоз?',1,'2020-11-27 05:19:24',1,0,NULL,'Лосось 12-14'),(256,'Жанар Енсегенова','8(712) 276-3614','ensegenova_2016@mail.ru','Добрый день. как можно с Вами связаться ?меня интересует Ваша продукция .',1,'2020-11-27 05:19:24',6,0,NULL,'сотрудничество'),(257,'Марат','8(777) 112-5533','himtrust.asia@himtrust.ru','Здравствуйте прошу со мной связаться',1,'2020-11-27 05:19:24',5,0,NULL,'Услуги по складу и ответ.хранению с услугами зав склада'),(258,'Елена','8(705) 444-7356','svidyuk_elena@mail.ru','сделать заявку на товар\r\n г.Костанай ул.Семина 48/4',1,'2020-11-27 05:19:24',4,0,NULL,'заявка товара'),(259,'Жанар Енсегенова','8(770) 196-2988','ensegenova_2016@mail.ru','прошу перезвонить ',1,'2020-11-27 05:19:24',6,0,NULL,'сотрудничество'),(260,'Даулет','8(777) 005-9022','tdk@turandot.kz','сотрудничество',1,'2020-11-27 05:19:24',2,0,NULL,'поставки ваших товаров в наши рестораны'),(261,'Жайнагуль ','8(771) 666-6118','dahu@mail.ru','Купить колбасы майонезы оптом',1,'2020-11-27 05:19:24',5,0,NULL,'Закуп'),(262,'Рафаиль','8(701) 939-0205','r.aisin@dastanhotels.kz','Предложение по аренде торговых мест в нашем ТК ',1,'2020-11-27 05:19:25',5,0,NULL,'Касательно сотрудничества'),(263,'Анаит ','8(705) 136-1032','anahitantonyan90@gmail.com','Добрый день, хотела ознакомиться с ценами в целях сотрудничества.',1,'2020-11-27 05:19:25',1,0,NULL,'Получить информацию '),(264,'Денис','8(775) 362-7200','Kazakstan25today@gmail.com','Здравствуйте работал в компании в 2019 году летом и в 2020 году в последний зимний месяц. Обещали расчет точный и нужны деньги на отправку документацию по продажам.',1,'2020-11-27 05:19:25',1,0,NULL,'Возврат служебной документации'),(265,'Сабыр','8(747) 946-6090','namazbayevsr@gmail.com','Колбасы',1,'2020-11-27 05:19:25',6,0,NULL,'Поставка товара в супермаркет'),(266,'Денис','8(775) 362-7200','Kazakstan25today@gmail.com','   Просьба перезвонить по поводу документации рабочей и остатков по зарплате. Спасибо!',1,'2020-11-27 05:19:25',1,0,NULL,'Задолженность'),(267,'Амангельди Шортанбаев ','8(701) 373-9022','aman72@inbox.ru','Индюки хайбрид конвектор.\r\nОт 8-13 кг тушка. Оптовая цена 1600 тг. Имеется 9 тон',1,'2020-11-27 05:19:25',5,0,NULL,'Предлагаем индюки хайбрид конвектор Канадский '),(268,'Ольга','8(770) 136-7038','olga.efanova.88@mail.ru','fdf',1,'2020-11-27 05:19:25',6,0,NULL,'Сотрудничество'),(269,'Акнур','8(702) 432-0550','aknura.nurgalieva@bk.ru','По заявкам не приходить ',1,'2020-11-27 05:19:25',7,0,NULL,'Не приходить по заявка '),(270,'Жаслан','8(702) 288-5522','altair.travel@bk.ru','Здравствуйте. У нас магазин, ищем поставщиков рыбы',1,'2020-11-27 05:19:25',1,0,NULL,'Хотим купить рыбу оптом'),(271,'Аяна ','8(775) 333-2767','ayana.khabibula@mail.ru','Позвоните мне ',1,'2020-11-27 05:19:25',1,0,NULL,'Заказ'),(272,'Виктория ','8(705) 868-5209','romik2030@mail.ru','ИП Бузина',1,'2020-11-27 05:19:25',7,0,NULL,'Сырный продукт'),(273,'Виктория ','8(705) 868-5200','romik2030@mail.ru','ИП Бузина',1,'2020-11-27 05:19:25',7,0,NULL,'Сырный продукт');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hall_of_fame`
--

DROP TABLE IF EXISTS `hall_of_fame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hall_of_fame` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) NOT NULL,
  `rank_en` varchar(255) DEFAULT NULL,
  `rank_kz` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `position` varchar(255) NOT NULL,
  `position_en` varchar(255) DEFAULT NULL,
  `position_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hall_of_fame`
--

LOCK TABLES `hall_of_fame` WRITE;
/*!40000 ALTER TABLE `hall_of_fame` DISABLE KEYS */;
/*!40000 ALTER TABLE `hall_of_fame` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,7,'1573542799_mpk__production-img-03.png'),(2,7,'1573542863_mpk__production-img-06.png'),(3,7,'1573542882_mpk__production-img-05.png');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_advantage`
--

DROP TABLE IF EXISTS `logistics_advantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_advantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_advantage`
--

LOCK TABLES `logistics_advantage` WRITE;
/*!40000 ALTER TABLE `logistics_advantage` DISABLE KEYS */;
INSERT INTO `logistics_advantage` VALUES (1,'1572854954_logicstics-01.png','44','','','единицы транспорта','',''),(2,'1572854976_logicstics-02.png','20','','','парковочных мест для грузового автотранспорта','',''),(3,'1572854995_logicstics-03.png','4,5 - 6 м','','','рабочая высота потолка','',''),(4,'1572855017_logicstics-04.png','от -18 °С до +5 °С','','','температура хранения','','');
/*!40000 ALTER TABLE `logistics_advantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_customer`
--

DROP TABLE IF EXISTS `logistics_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `statusActing` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_customer`
--

LOCK TABLES `logistics_customer` WRITE;
/*!40000 ALTER TABLE `logistics_customer` DISABLE KEYS */;
INSERT INTO `logistics_customer` VALUES (1,'Контрагента','1576821024_logicstics-clientbase_01.png',1,'24 тысячи','','','Общее количество контрагентов','',''),(2,'Дистрибьюция','1576821013_logicstics-clientbase_02-change.png',0,'14 тысяч','','','Оптово-розничных клиентов в дистрибьюции','','');
/*!40000 ALTER TABLE `logistics_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_customer_base`
--

DROP TABLE IF EXISTS `logistics_customer_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_customer_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `content` text,
  `content_en` text,
  `content_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_customer_base`
--

LOCK TABLES `logistics_customer_base` WRITE;
/*!40000 ALTER TABLE `logistics_customer_base` DISABLE KEYS */;
INSERT INTO `logistics_customer_base` VALUES (1,NULL,'Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических                     центров в Астане - 8000 м², в Актобе - 6500 м², а в Шымкенте - 2500 м².','','','<p>Технические параметры логистических центров &laquo;Рокос&raquo;: одноэтажное складское здание со встроенным АБК, бетонный пол с упрочняющим антипылевым покрытием с допустимой нагрузкой 6 т/м&sup2;, пандусы для работ с автотранспортом, перрон для приемки железнодорожных подвижных составов.</p>\r\n','','');
/*!40000 ALTER TABLE `logistics_customer_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_geography`
--

DROP TABLE IF EXISTS `logistics_geography`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_geography` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_geography`
--

LOCK TABLES `logistics_geography` WRITE;
/*!40000 ALTER TABLE `logistics_geography` DISABLE KEYS */;
INSERT INTO `logistics_geography` VALUES (1,'Казахстан'),(2,'Россия'),(3,'Беларусь'),(4,'Европа');
/*!40000 ALTER TABLE `logistics_geography` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_header`
--

DROP TABLE IF EXISTS `logistics_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `content` text,
  `content_en` text,
  `content_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_header`
--

LOCK TABLES `logistics_header` WRITE;
/*!40000 ALTER TABLE `logistics_header` DISABLE KEYS */;
INSERT INTO `logistics_header` VALUES (1,'Доверьте груз профессионалам!',NULL,NULL,'<p>Мы полностью отвечаем за Ваш груз с момента поступления его к нам, до его отгрузки со склада.</p>\r\n                <p>Рады предложить Вам услуги по перемещению груза автомобильным и железнодорожным транспортом, услуги \r\n                СВХ и таможенной очистки грузов, складирование, разгрузку и погрузку, документальное оформление и отчетность.\r\n                </p>\r\n                <h3>Сотрудничая с нами Вы получаете дополнительные преимущества:</h3>\r\n                <p>логистические комплексы, оборудованные парком весоизмерительных приборов и механизацией ручного труда, возможность аренды офисных помещений.</p>',NULL,NULL);
/*!40000 ALTER TABLE `logistics_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logistics_images`
--

DROP TABLE IF EXISTS `logistics_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logistics_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logistics_images`
--

LOCK TABLES `logistics_images` WRITE;
/*!40000 ALTER TABLE `logistics_images` DISABLE KEYS */;
INSERT INTO `logistics_images` VALUES (1,1,'1572855266_logicstics-clientbase_img-01.png'),(2,1,'1572855300_logicstics-clientbase_img-02.png'),(3,1,'1572855314_logicstics-clientbase_img-03.png');
/*!40000 ALTER TABLE `logistics_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logo`
--

DROP TABLE IF EXISTS `logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `copyright_en` varchar(255) DEFAULT NULL,
  `copyright_kz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logo`
--

LOCK TABLES `logo` WRITE;
/*!40000 ALTER TABLE `logo` DISABLE KEYS */;
INSERT INTO `logo` VALUES (1,'1573537458_internal-footer_logo.svg','2006 - 2019 © Rokos Group Global','2006 - 2019 © Rokos Group Global','2006 - 2019 © Rokos Group Global');
/*!40000 ALTER TABLE `logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `text_en` varchar(255) NOT NULL,
  `text_kz` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `metaName` varchar(255) NOT NULL,
  `metaDesc` text NOT NULL,
  `metaKey` text NOT NULL,
  `metaName_en` varchar(255) NOT NULL,
  `metaDesc_en` text NOT NULL,
  `metaKey_en` text NOT NULL,
  `metaName_kz` varchar(255) NOT NULL,
  `metaDesc_kz` text NOT NULL,
  `metaKey_kz` text NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','Басты',0,'/','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','«Rokos Group Global» - только лучшие продукты для вашего холодильника!','Главная','Главная','Главная',1),(2,'О RGG ','О RGG','О RGG',1,'/site/index#about_company','О RGG','О RGG','О RGG','О RGG','О RGG','О RGG','О RGG','О RGG','О RGG',2),(3,'Портфель','Портфель','Портфель',1,'/content/portfolio','«Rokos Group Global»','Мы наполняем Ваш холодильник лучшими продуктами, делая Вашу жизнь вкуснее и комфортнее!','Мы наполняем Ваш холодильник лучшими продуктами, делая Вашу жизнь вкуснее и комфортнее!','Портфель','Портфель','Портфель','Портфель','Портфель','Портфель',3),(4,'Карьера','Карьера','Карьера',1,'/content/career','Карьера','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','Карьера','Карьера','Карьера','Карьера','Карьера','Карьера',4),(5,'Новости','Новости','Новости',1,'/content/news','Новости','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','Новости','Новости','Новости','Новости','Новости','Новости',5),(6,'Контакты','Контакты','Контакты',1,'/site/contact','Контакты','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','«Rokos Group Global» - это динамично развивающаяся дистрибуционная компания, основанная в 1996 году, которая охватывает все регионы Республики Казахстан.','Контакты','Контакты','Контакты','Контакты','Контакты','Контакты',6),(7,'МПК ','МПК','МПК',1,'/content/mpk','МПК «Актобе» – один из лидеров мясоперерабатывающей промышленности Казахстана.','МПК «Актобе» – один из лидеров мясоперерабатывающей промышленности Казахстана.\r\n','МПК «Актобе» – один из лидеров мясоперерабатывающей промышленности Казахстана.\r\n','МПК','МПК\r\n','МПК\r\n','МПК','МПК\r\n','МПК\r\n',7),(8,'Логистика','Логистика','Логистика',1,'/content/logistics','Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических центров в Астане - 8000 м², в Актобе - 6500 м², а в Шымкенте - 2500 м².','Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических центров в Астане - 8000 м², в Актобе - 6500 м², а в Шымкенте - 2500 м².','Логистические центры «РОКОС» расположены в Астане, Актобе и Шымкенте. Общая площадь логистических центров в Астане - 8000 м², в Актобе - 6500 м², а в Шымкенте - 2500 м².','Логистика','Логистика\r\n','Логистика\r\n','Логистика','Логистика\r\n','Логистика\r\n',8);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  KEY `idx_message_language` (`language`),
  CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'en','Become a customer'),(1,'kz','Клиент болыңыз'),(1,'ru','Стать клиентом'),(2,'en',''),(2,'kz','Жабу'),(2,'ru','Закрыть'),(3,'en',''),(3,'kz',''),(3,'ru',''),(4,'en','Back call\r\n'),(4,'kz','Кері қоңырау шалыңыз\r\n'),(4,'ru','Обратный звонок'),(5,'en',NULL),(5,'kz',NULL),(5,'ru',NULL),(6,'en',NULL),(6,'kz',NULL),(6,'ru',NULL),(7,'en',NULL),(7,'kz',NULL),(7,'ru',NULL),(8,'en',NULL),(8,'kz',NULL),(8,'ru',NULL),(9,'en','More details\r\n'),(9,'kz','Толығырақ\r\n'),(9,'ru','Подробнее'),(10,'en',NULL),(10,'kz',NULL),(10,'ru',NULL),(11,'en',NULL),(11,'kz',NULL),(11,'ru',NULL),(12,'en',NULL),(12,'kz',NULL),(12,'ru',NULL),(13,'en',NULL),(13,'kz',NULL),(13,'ru',NULL),(14,'en',NULL),(14,'kz',NULL),(14,'ru',NULL),(15,'en',NULL),(15,'kz',NULL),(15,'ru',NULL),(16,'en',NULL),(16,'kz',NULL),(16,'ru',NULL),(17,'en',NULL),(17,'kz',NULL),(17,'ru',NULL),(18,'en',NULL),(18,'kz',NULL),(18,'ru',NULL),(19,'en',NULL),(19,'kz',NULL),(19,'ru',NULL),(20,'en',NULL),(20,'kz',NULL),(20,'ru',NULL),(21,'en',NULL),(21,'kz',NULL),(21,'ru',NULL),(22,'en',NULL),(22,'kz',NULL),(22,'ru',NULL),(23,'en',NULL),(23,'kz',NULL),(23,'ru',NULL),(24,'en',''),(24,'kz',''),(24,'ru','ФИО'),(25,'en',NULL),(25,'kz',NULL),(25,'ru',NULL),(26,'en',NULL),(26,'kz',NULL),(26,'ru',NULL),(27,'en',NULL),(27,'kz',NULL),(27,'ru',NULL),(28,'en',NULL),(28,'kz',NULL),(28,'ru',NULL),(29,'en',NULL),(29,'kz',NULL),(29,'ru',NULL),(30,'en',NULL),(30,'kz',NULL),(30,'ru',NULL),(31,'en',NULL),(31,'kz',NULL),(31,'ru',NULL),(32,'en',NULL),(32,'kz',NULL),(32,'ru',NULL),(33,'en',NULL),(33,'kz',NULL),(33,'ru',NULL),(34,'en',NULL),(34,'kz',NULL),(34,'ru',NULL),(35,'en',NULL),(35,'kz',NULL),(35,'ru',NULL),(36,'en',NULL),(36,'kz',NULL),(36,'ru',NULL),(37,'en',NULL),(37,'kz',NULL),(37,'ru',NULL),(38,'en',NULL),(38,'kz',NULL),(38,'ru',NULL),(39,'en',NULL),(39,'kz',NULL),(39,'ru',NULL),(40,'en',NULL),(40,'kz',NULL),(40,'ru',NULL),(41,'en',NULL),(41,'kz',NULL),(41,'ru',NULL),(42,'en',NULL),(42,'kz',NULL),(42,'ru',NULL),(43,'en',NULL),(43,'kz',NULL),(43,'ru',NULL),(44,'en',NULL),(44,'kz',NULL),(44,'ru',NULL),(45,'en',NULL),(45,'kz',NULL),(45,'ru',NULL),(46,'en',NULL),(46,'kz',NULL),(46,'ru',NULL),(47,'en',NULL),(47,'kz',NULL),(47,'ru',NULL),(48,'en',NULL),(48,'kz',NULL),(48,'ru',NULL),(49,'en',NULL),(49,'kz',NULL),(49,'ru',NULL),(50,'en',NULL),(50,'kz',NULL),(50,'ru',NULL),(51,'en',NULL),(51,'kz',NULL),(51,'ru',NULL),(52,'en',NULL),(52,'kz',NULL),(52,'ru',NULL),(53,'en',NULL),(53,'kz',NULL),(53,'ru',NULL),(54,'en',NULL),(54,'kz',NULL),(54,'ru',NULL),(55,'en',NULL),(55,'kz',NULL),(55,'ru',NULL),(56,'en',NULL),(56,'kz',NULL),(56,'ru',NULL),(57,'en',NULL),(57,'kz',NULL),(57,'ru',NULL),(58,'en',NULL),(58,'kz',NULL),(58,'ru',NULL),(59,'en',NULL),(59,'kz',NULL),(59,'ru',NULL),(60,'en',NULL),(60,'kz',NULL),(60,'ru',NULL),(61,'en',NULL),(61,'kz',NULL),(61,'ru',NULL),(62,'en',NULL),(62,'kz',NULL),(62,'ru',NULL),(63,'en',NULL),(63,'kz',NULL),(63,'ru',NULL),(64,'en',NULL),(64,'kz',NULL),(64,'ru',NULL),(65,'en',NULL),(65,'kz',NULL),(65,'ru',NULL),(66,'en',NULL),(66,'kz',NULL),(66,'ru',NULL),(67,'en',NULL),(67,'kz',NULL),(67,'ru',NULL),(68,'en',NULL),(68,'kz',NULL),(68,'ru',NULL),(69,'en',NULL),(69,'kz',NULL),(69,'ru',NULL),(70,'en',NULL),(70,'kz',NULL),(70,'ru',NULL),(71,'en',NULL),(71,'kz',NULL),(71,'ru',NULL);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1606368498),('m201104_113959_add_bin_column_to_user_table',1606368500),('m201105_115424_create_purchasing_guide_table',1606368500),('m201106_041006_create_purchases_table',1606368500),('m201106_041508_create_purchases_admins_table',1606368500),('m201106_041525_create_purchases_commissions_table',1606368500),('m201106_135217_create_stage_first_table',1606368500),('m201106_135252_create_stage_second_table',1606454909),('m201106_135459_create_stage_first_docs_table',1606368500),('m201106_135515_create_stage_second_docs_table',1606368500),('m201113_091417_create_purchase_requests_table',1606368500),('m201113_104138_create_purchase_request_files_table',1606368500);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpk`
--

DROP TABLE IF EXISTS `mpk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `mission_title` text NOT NULL,
  `mission_title_en` text,
  `mission_title_kz` text,
  `mission_content` text NOT NULL,
  `mission_content_en` text,
  `mission_content_kz` text,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpk`
--

LOCK TABLES `mpk` WRITE;
/*!40000 ALTER TABLE `mpk` DISABLE KEYS */;
INSERT INTO `mpk` VALUES (1,'МПК «Актобе» – один из лидеров мясоперерабатывающей промышленности Казахстана.','','','<p>Наше предприятие с уникальным, современным оборудованием и квалифицированным персоналом выпускает более 100 наименований колбасных изделий, деликатесов и полуфабрикатов под торговыми марками &laquo;D&aacute;st&uacute;r D&aacute;mі&raquo; и &laquo;МПК&raquo;. Благодаря профессиональной и слаженой работе коллектива, неустанному подбору специалистов, предприятие успешно обеспечивает высокое качество продукции.</p>\r\n','','','<p>Наша миссия &mdash; достигать лидерства, удовлетворяя ежедневные потребности населения Казахстана в качественной и безопасной продукции</p>\r\n','','','<p>Собственная система логистических решений и филиальная сеть складов позволяют гарантировать достаточный ассортимент во всех регионах Казахстана</p>\r\n','<p>Колбасы МПК &laquo;Актобе&raquo; &ndash; это прекрасное сочетание цены и качества. Комбинат постоянно участвует в национальных, городских выставках и других мероприятиях Казахстана, являясь их лауреатом и дипломантом. В 2017 году наша продукция была удостоена золотой медали &laquo;За качество и вкус выпускаемой продукции&raquo; на Международной выставке &laquo;InterFood Astana&raquo; по пищевой промышленности.</p>\r\n','','<p></p>\r\n','','');
/*!40000 ALTER TABLE `mpk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpk_advantage`
--

DROP TABLE IF EXISTS `mpk_advantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpk_advantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `text` text NOT NULL,
  `text_en` text,
  `text_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpk_advantage`
--

LOCK TABLES `mpk_advantage` WRITE;
/*!40000 ALTER TABLE `mpk_advantage` DISABLE KEYS */;
INSERT INTO `mpk_advantage` VALUES (1,'Мощность мясокомбината','Мощность мясокомбината','Мощность мясокомбината','1572943482_mpk__img-01.png','<h2>5000</h2><p>тонн в год</p>','<h2>5000</h2><p>тонн в год</p>','<h2>5000</h2><p>тонн в год</p>'),(2,'Численность работников','','','1572943546_mpk__img-02.png','<h2>400</h2><p>человек</p>','','');
/*!40000 ALTER TABLE `mpk_advantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpk_catalog`
--

DROP TABLE IF EXISTS `mpk_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpk_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpk_catalog`
--

LOCK TABLES `mpk_catalog` WRITE;
/*!40000 ALTER TABLE `mpk_catalog` DISABLE KEYS */;
INSERT INTO `mpk_catalog` VALUES (1,'1573551199_Новый текстовый документ.txt','https://drive.google.com/file/d/1vwj0ORyt4i4er_k97zYNNkfH2CcnUVGw/view',0);
/*!40000 ALTER TABLE `mpk_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpk_principles`
--

DROP TABLE IF EXISTS `mpk_principles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpk_principles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpk_principles`
--

LOCK TABLES `mpk_principles` WRITE;
/*!40000 ALTER TABLE `mpk_principles` DISABLE KEYS */;
INSERT INTO `mpk_principles` VALUES (1,'Стабильное качество                         продуктов, соответствие                         международным стандартам','','','1572943641_mpk__priciples-img_01.png'),(2,'Работа с сертифицированными                         поставщиками сырья и надежными                         партнерами','','','1572943661_mpk__priciples-img_02.png'),(3,'Использование                         современных технологий                         и традиций народов Казахстана','','','1572943675_mpk__priciples-img_03.png'),(4,'Качественный торговый сервис                         на стабильно высоком уровне','','','1572943690_mpk__priciples-img_04.png'),(5,'Контроль на всех этапах,                         от закупки до доставки','','','1572943704_mpk__priciples-img_05.png'),(6,'Грамотное построение                         системы сбыта продукции','','','1572943719_mpk__priciples-img_06.png');
/*!40000 ALTER TABLE `mpk_principles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name_en` varchar(255) NOT NULL,
  `content_en` text NOT NULL,
  `name_kz` varchar(255) NOT NULL,
  `content_kz` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (4,'Новый взгляд на овощи!','<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Компания RGG рада сообщить, что с середины лета 2019 года ввела в свой ассортимент замороженную продукцию торговой марки Bonduelle.&nbsp;</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Интересно, что Bonduelle одной из первых сделала овощную смесь. На тот момент это была смесь из горошка и моркови. Новый продукт произвел просто сумасшедший фурор. И сейчас Bonduelle является лидером и в заморозке и в консервации, предлагая для своих потребителей только лучшие продукты.</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">В нашей работе мы делаем ставку на качества продуктов, которые мы продаём, именно поэтому мы рады представлять торговую марку Bonduelle на рынке Казахстана.</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Компания Bonduelle занимает лидирующие позиции по производству овощей благодаря ее&nbsp;политике строжайшего контроля качества:</span></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Компания использует лишь фермерские угодья, отвечающие самым высоким стандартам.</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Компания запрашивает у всех своих поставщиков семян декларации, гарантирующие отсутствие в их продукции ГМО.</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Сбор урожая производится на полях, которые расположены в экологически чистых регионах мира и в непосредственной близости от завода.</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Близость завода к полям позволяет до минимума сократить время от сбора урожая до его переработки, и сохранить полезные витамины в овощах</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Перед заморозкой овощи проходят несколько стадий очистки от шелухи и подготовки, которая помогает сохранить максимум витаминов.</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Шоковая заморозка при -30С позволяет сохранить цвет, вкус и текстуру всех овощей.</span></p>\r\n	</li>\r\n</ul>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Всё эти стандарты работы Компании Bonduelle гарантируют качество продукции. Мы подобрали для своих покупателей не только стандартный ассортимент, но и решили удивить необычным подходом в употреблении замороженных овощей. Необычные овощные галеты порадуют натуральным составом и разнообразием вкусов: ореховые галеты с тыквой, сицилийские галеты с овощами гриль и зеленые со шпинатом и брокколи. Аккуратно порезанные овощи, приправленные пикантными специями готовятся всего за 7 &ndash; 10 минут и подойдут к столу в виде гарнира или самостоятельного блюда.&nbsp; Мы уверенны, что с Bonduelle каждый сможет найти любимый вкус.</span></p>\r\n\r\n<div>&nbsp;</div>\r\n','1572945629_Бондюэль.jpg','2019-11-05 09:20:29','Офис компании переехал по новому адресу','<p>В период с 17 по 20 апреля 2018г. ТОО &laquo;Компания&nbsp;ECOS&raquo; организован семинар для Заказчиков. На данном семинаре в качестве докладчиков выступили партнеры Компании, дилером/дистрибьютором которых является наша компания. Такие семинары теперь будут проходить периодически как минимум 1 раз в год. Кофе-брейки и обеды для всех участников предоставляются бесплатно.</p>\r\n\r\n<p>&nbsp;</p>\r\n','Офис компании переехал по новому адресу','<p>В период с 17 по 20 апреля 2018г. ТОО &laquo;Компания&nbsp;ECOS&raquo; организован семинар для Заказчиков. На данном семинаре в качестве докладчиков выступили партнеры Компании, дилером/дистрибьютором которых является наша компания. Такие семинары теперь будут проходить периодически как минимум 1 раз в год. Кофе-брейки и обеды для всех участников предоставляются бесплатно.</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(5,'Только натуральные ингредиенты!','<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Mr. RICCO&hellip; Такой притягательный, безукоризненный и восхитительный&hellip;&nbsp;&nbsp;</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Это настоящая находка для любителей кулинарных экспериментов.&nbsp;</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Мы рады сообщить, что с июля 2019 года мы представляем майонезы и кетчупы Mr. RICCO на рынке Казахстана!!!</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">Сегодня эти два вида соусов незаменимы в каждой семье, так как 90% населения употребляют его в пищу, каждый день. Поэтому важно, что бы майонез и кетчуп соответствовали всем стандартам качества. Торговая марка Mr. RICCO это новый уровень в производстве соусов. Соусы удачно подчеркивают вкус основных ингредиентов и способны сделать особенными даже привычные блюда.</span></p>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">&nbsp;Линейка ORGANIC - это сертифицированная на международном уровне органическая продукция:</span></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Все ингредиенты проходят тщательный внутренний контроль, прежде чем попасть в майонез.&nbsp;</span></p>\r\n	</li>\r\n	<li>\r\n	<p><span style=\"background-color:transparent; font-size:11pt\">Кетчупы готовятся по специальной технологии, которая позволяет отказаться от загустителей и стабилизаторов.</span></p>\r\n	</li>\r\n</ul>\r\n\r\n<p><span style=\"background-color:transparent; color:rgb(0, 0, 0); font-size:11pt\">&nbsp;Мы гордимся тем, что представляем соусы, которые были удостоен государственной премии РФ &quot;Знак качества&quot; а так же были победителями программы &quot;Контрольная закупка&quot;. Теперь мы можем доставить к вашему столу соусы, которые прекрасно дополнят ваши любимые блюда.</span></p>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>&nbsp;</p>\r\n','1572945639_Рикко.jpg','2019-11-05 09:20:39','Офис компании переехал по новому адресу','<p>В период с 17 по 20 апреля 2018г. ТОО &laquo;Компания&nbsp;ECOS&raquo; организован семинар для Заказчиков. На данном семинаре в качестве докладчиков выступили партнеры Компании, дилером/дистрибьютором которых является наша компания. Такие семинары теперь будут проходить периодически как минимум 1 раз в год. Кофе-брейки и обеды для всех участников предоставляются бесплатно.</p>\r\n\r\n<p>&nbsp;</p>\r\n','Офис компании переехал по новому адресу','<p>В период с 17 по 20 апреля 2018г. ТОО &laquo;Компания&nbsp;ECOS&raquo; организован семинар для Заказчиков. На данном семинаре в качестве докладчиков выступили партнеры Компании, дилером/дистрибьютором которых является наша компания. Такие семинары теперь будут проходить периодически как минимум 1 раз в год. Кофе-брейки и обеды для всех участников предоставляются бесплатно.</p>\r\n\r\n<p>&nbsp;</p>\r\n');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES (2,'Alfur',0,'1574416413_Альфур.png','https://petruha.by/'),(3,'',0,'1574406804_Мр Рикко (1).png','http://mrricco.ru/ '),(5,'Белорусские сыры',0,'1574416568_Белорусские сыры.png','1'),(8,'bonduelle',0,'1574416455_Бондюэль.png','https://bonduelle.ru/'),(10,'',0,'1574416579_Брест-литовск.png','https://brest-litovsk.by/'),(13,'komo',0,'1574416466_Комо.png','https://komo.ua/ru'),(14,'',0,'1574416589_Миладора.png','http://mrricco.ru/ '),(16,'',0,'1574416478_Шостка.png','https://shostkasyr.com/ru/'),(18,'Пуск',0,'1574416729_пуск.jpg','');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner_feedback`
--

DROP TABLE IF EXISTS `partner_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `reply` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner_feedback`
--

LOCK TABLES `partner_feedback` WRITE;
/*!40000 ALTER TABLE `partner_feedback` DISABLE KEYS */;
INSERT INTO `partner_feedback` VALUES (1,'1569843634_Sommerville-Software-Engineering-10ed.pdf','2019-09-30 11:40:34',0,NULL),(2,'1572345830_ESSEX Pianos.pdf','2019-10-29 10:43:50',0,NULL),(3,'1572346265_Новый текстовый документ.txt','2019-10-29 10:51:05',0,NULL),(4,'1572606561_Коммерческое предложение Уланов Иван.pdf','2019-11-01 11:09:21',0,NULL),(5,'1575891857_Коммерческое Рокос.pdf','2019-12-09 11:44:17',0,NULL),(6,'1576139590_Презентация по капсуле.pdf','2019-12-12 08:33:10',0,NULL),(7,'1579149898_Коммерческое предложение SAMAL CHAI (3) (2).docx','2020-01-16 04:44:58',0,NULL),(8,'1579149981_Коммерческое предложение SAMAL CHAI (3) (2).docx','2020-01-16 04:46:21',0,NULL),(9,'1580192471_Письмо1.doc','2020-01-28 06:21:11',0,NULL),(10,'1580200544_КП-Сохо с НДС.docx','2020-01-28 08:35:44',0,NULL),(11,'1580200819_КП-Соевый соус.docx','2020-01-28 08:40:19',0,NULL),(12,'1580202062_Ком.предложение для партнеров.docx','2020-01-28 09:01:02',0,NULL),(13,'1580202083_Ком.предложение для партнеров.docx','2020-01-28 09:01:23',0,NULL),(14,'1580706597_Предложение о сотрудничестве РК.pptx','2020-02-03 05:09:58',0,NULL),(15,'1580706662_Предложение о сотрудничестве РК.pptx','2020-02-03 05:11:03',0,NULL),(16,'1580706734_Предложение о сотрудничестве РК.pptx','2020-02-03 05:12:14',0,NULL),(17,'1580805602_Баварская сладкая горчица.pdf','2020-02-04 08:40:02',0,NULL),(18,'1580805631_Ponnath каталог.pdf','2020-02-04 08:40:32',0,NULL),(19,'1582706319_Коммерческое предложение ТОО ЛАРС.docx','2020-02-26 08:38:41',0,NULL),(20,'1582707568_Коммерческое предложение ТОО ЛАРС.docx','2020-02-26 08:59:30',0,NULL),(21,'1582707911_Коммерческое предложение ТОО ЛАРС.docx','2020-02-26 09:05:13',0,NULL),(22,'1583394023_КП с прайсoм тенге.pdf','2020-03-05 07:40:26',0,NULL),(23,'1583398432_КП с прайсoм тенге - копия_compressed (1).pdf','2020-03-05 08:53:53',0,NULL),(24,'1583821292_Знакомство с компанией TDS Media.pptx','2020-03-10 06:21:34',0,NULL),(25,'1583821294_Знакомство с компанией TDS Media.pptx','2020-03-10 06:21:36',0,NULL),(26,'1586263459_Каталог молочной продукции ТМ Хорошее дело (1).pdf','2020-04-07 12:44:24',0,NULL),(27,'1587453776_ООО Томак НК.pptx','2020-04-21 07:22:58',0,NULL),(28,'1587454275_Ком.предложение ООО Томак НК г.Новокузнецк.docx','2020-04-21 07:31:15',0,NULL),(29,'1587454509_Ком.предложение ООО Томак НК г.Новокузнецк.docx','2020-04-21 07:35:09',0,NULL),(30,'1587454869_Ком.предложение ООО Томак НК г.Новокузнецк.docx','2020-04-21 07:41:09',0,NULL),(31,'1588250483_ПРЕЗЕНТАЦИЯ.pdf','2020-04-30 12:41:28',0,NULL),(32,'1588250488_ПРЕЗЕНТАЦИЯ.pdf','2020-04-30 12:41:31',0,NULL),(33,'1588250492_прайс 2020.xlsx','2020-04-30 12:41:32',0,NULL),(34,'1589444842_КП Волшебные вкусы.doc','2020-05-14 08:27:22',0,NULL),(35,'1589533309_КП Волшебные вкусы.doc','2020-05-15 09:01:49',0,NULL),(36,'1591097326_Коммерческое предложение ООО Новатор.pdf','2020-06-02 11:28:46',0,NULL),(37,'1591097368_Коммерческое предложение ООО Новатор.pdf','2020-06-02 11:29:28',0,NULL),(38,'1591329960_Прайс-лист  в росс руб 25.05.2020.doc','2020-06-05 04:06:00',0,NULL),(39,'1592811531_presentation_MK_web_compressed (1).pdf','2020-06-22 07:38:53',0,NULL),(40,'1592811533_presentation_MK_web_compressed (1).pdf','2020-06-22 07:38:55',0,NULL),(41,'1593513828_WIELEC-FRUIT.pdf','2020-06-30 10:43:48',0,NULL),(42,'1594796470_Презентация Natur Bravo 2020.pptx','2020-07-15 07:01:12',0,NULL),(43,'1595322890_КП Агропром_дистрибьюторы.pdf','2020-07-21 09:14:50',0,NULL),(44,'1595322993_КП Агропром_дистрибьюторы.pdf','2020-07-21 09:16:33',0,NULL),(45,'1595914748_commercial offer №14 .pdf','2020-07-28 05:39:08',0,NULL),(46,'1595914826_commercial offer №14 .pdf','2020-07-28 05:40:26',0,NULL),(47,'1596447849_КП[1682].docx','2020-08-03 09:44:09',0,NULL),(48,'1596516917_Компания Green Food Company письмо с предложением (1).docx','2020-08-04 04:55:17',0,NULL),(49,'1596613092_КП NINBIONUT стаканы на август 2020 года.pdf','2020-08-05 07:38:12',0,NULL),(50,'1596709385_АБФ презентация (200710).pdf','2020-08-06 10:23:05',0,NULL),(51,'1597226054_DM FOOD COMPANY PROFILE PRESENTATION_2020_EXTERNAL.pdf','2020-08-12 09:54:16',0,NULL),(52,'1597226518_DM.docx','2020-08-12 10:01:58',0,NULL),(53,'1597809899_Презентация Банановые чипсы Kims с предложением.pptx','2020-08-19 04:05:01',0,NULL),(54,'1598509609_Коммерческое Антисептик Новые цены 25.08.2020.pdf','2020-08-27 06:26:49',0,NULL),(55,'1598509639_Коммерческое Антисептик Новые цены 25.08.2020.pdf','2020-08-27 06:27:19',0,NULL),(56,'1598509681_Коммерческое Антисептик Новые цены 25.08.2020.pdf','2020-08-27 06:28:01',0,NULL),(57,'1598510088_Автопарфюмерия VITA UDIN. КП.pdf','2020-08-27 06:34:48',0,NULL),(58,'1599625937_КП Шымкент.pptx','2020-09-09 04:32:18',0,NULL),(59,'1601533787_КДМ Новинки 2020.docx','2020-10-01 06:29:47',0,NULL),(60,'1601533797_КДМ Новинки 2020.docx','2020-10-01 06:29:57',0,NULL),(61,'1603285331_КП Иван-чай.pdf','2020-10-21 13:02:11',0,NULL),(62,'1604483822_ком пред для Казахстана NEW.pdf','2020-11-04 09:57:02',0,NULL),(63,'1604483836_ком пред для Казахстана NEW.pdf','2020-11-04 09:57:17',0,NULL);
/*!40000 ALTER TABLE `partner_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES (1,'Backend developer');
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_promo`
--

DROP TABLE IF EXISTS `product_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `category_en` varchar(255) DEFAULT NULL,
  `category_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_promo`
--

LOCK TABLES `product_promo` WRITE;
/*!40000 ALTER TABLE `product_promo` DISABLE KEYS */;
INSERT INTO `product_promo` VALUES (1,'Мусульманский с сыром','','','сервелат','','','1573543789_mpk__production-img-01.png'),(2,'Деликатесы Қазы','','','конина варёно-копчёная','','','1573543813_mpk__production-img-02.png'),(3,'Туркестан','','','сервелат','','','1573543835_mpk__production-img-04.png');
/*!40000 ALTER TABLE `product_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalog_id` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `fullname_en` varchar(255) DEFAULT NULL,
  `fullname_kz` varchar(255) DEFAULT NULL,
  `weight` varchar(255) NOT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `manufacturer_en` varchar(255) DEFAULT NULL,
  `manufacturer_kz` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,6,NULL,'Сливочный сыр полутвердый','','','Сливочный  сыр полутвердый','','','7 кг','','','',NULL,'<p>Жирность 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573294628_Белорусские сыры Сметанковый.jpg','2019-11-09 16:14:48'),(2,26,NULL,'Казы','','','Конина варено-копченая','','','450г . 950г','','','',NULL,'<p>Срок хранения: 30 суток</p>\r\n\r\n<p>Температура хранения: От 0 до +4С</p>\r\n','','','1573299065_казы.jpg','2019-11-09 17:16:40'),(3,26,NULL,'Шұжық','','','Шұжық конина варёно-копчёная  ','','','500г.','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573299043_Шужык.jpg','2019-11-09 17:24:06'),(4,26,NULL,'«Европейская»','','','«Европейская»говядина варёно-копчёная','','','350 г. / 950 г.','','','',NULL,'<p>Срок хранения: 20-30 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573299560_Говяжина Европейская.jpg','2019-11-09 17:39:20'),(5,26,NULL,'«Гурман»','','','«Гурман» свинина варёно-копчёная','','','350 г. / 950 г.','','','',NULL,'<p>Срок хранения:25&nbsp;-30 суток</p>\r\n\r\n<p>Температура хранения от 0 до+4 С</p>\r\n','','','1573299970_Гурман.jpg','2019-11-09 17:46:10'),(6,26,NULL,'Окорочка','','','Окорочка цыплят варёно-копчёные','','','600 г.','','','',NULL,'<p>Срок хранения: 20 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573300293_Окорочка.jpg','2019-11-09 17:51:33'),(7,26,NULL,'Грудка','','','Грудка куриная варёно-копчёная','','','350 г.','','','',NULL,'<p>Срок хранения: 30 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573300697_Грудка.jpg','2019-11-09 17:58:17'),(8,26,NULL,'Карпаччо','','','Карпаччо сыровяленное из свинины','','','350 г.','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573300954_Карпаччо свинины.jpg','2019-11-09 18:02:34'),(9,26,NULL,'Карпаччо','','','Карпаччо сыровяленное из мяса птицы','','','350 г.','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573301181_Карпаччо птицы.jpg','2019-11-09 18:06:21'),(10,26,NULL,'Рулет','','','Рулет куриный варёно-копчёный','','','700 г.','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4С</p>\r\n','','','1573301378_Рулет куриный.jpg','2019-11-09 18:09:38'),(11,26,NULL,'Рулет «Элитный»','','','Рулет «Элитный»куриный варёно-копчёный','','','1,4 кг','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +8 С</p>\r\n','','','1573301613_Рулет элитный.jpg','2019-11-09 18:13:33'),(12,26,NULL,'«Домашняя»','','','«Домашняя»ветчина варёно-копчёная','','','300 г. / 3,5 кг.','','','',NULL,'<p>Срок хранения:от 5 до 15 суток</p>\r\n\r\n<p>Температура хранения:от 0 до +6 С</p>\r\n','','','1573302110_Домашняя ветчина.jpg','2019-11-09 18:21:50'),(13,26,NULL,'«Семейная»','','','«Семейная» колбаса варёная','','','300 г / 3,5 кг','','','',NULL,'<p>Срок хранения: от 5 до 15 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573302420_Семейная ветчина.jpg','2019-11-09 18:27:00'),(14,26,NULL,'Копчёный','','','Копчёный шпик','','','350 г.','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Темпратура хранения: от 0 до +6 С</p>\r\n','','','1573302660_Шпик копченый.jpg','2019-11-09 18:31:00'),(15,26,NULL,'Солёный','','','Солёный шпик','','','350 г.','','','',NULL,'<p>Срок ханения : 90 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573302872_Шпик соленый.jpg','2019-11-09 18:34:32'),(16,26,NULL,'«Любительская»','','','«Любительская» ветчина варёно-копчёная','','','950 г.','','','',NULL,'<p>Срок хранения: 40 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573303138_Любительская ветчина.jpg','2019-11-09 18:38:58'),(17,26,NULL,'«По-домашнему»','','','«По-домашнему» шпик','','','250 г.','','','',NULL,'<p>Срок хранения : 90 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573303564_Шпик соленый.jpg','2019-11-09 18:46:04'),(19,6,NULL,'Сметанковый сыр полутвердый','','','Сметанковый сыр полутвердый','','','7 кг','Белорусь','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n\r\n<p>Жирность 50%</p>\r\n','','','1573377783_Белорусские сыры Сметанковый.jpg','2019-11-10 15:23:03'),(20,6,NULL,'Великокняжеский сыр полутвердый','','','Великокняжеский сыр полутвердый','','','7 кг','Белорусь','','',NULL,'<p>Жирность 45%&nbsp;</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573377998_Белорусские сыры Великокняжеский.jpg','2019-11-10 15:26:38'),(21,6,NULL,'Пошехонский сыр полутвердый','','','Пошехонский сыр полутвердый','','','7 кг','Белорусь','','',NULL,'<p>Жирность 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573463087_Белорусские сыры Пошехонский.jpg','2019-11-10 15:29:29'),(22,6,NULL,'Российский молодой сыр полутвердый','','','Российский молодой сыр полутвердый','','','7 кг','Белорусь','','',NULL,'<p>Жирность 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573378457_Белорусские сыры Российский молодой.jpg','2019-11-10 15:34:17'),(23,7,NULL,'Российский классический сыр полутвердый','','','Российский классический сыр полутвердый','','','1,65 кг','Украина','','',NULL,'<p>Жирность 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573378791_ШОСТКА СЫР Rossiysky 1,65 kg.jpg','2019-11-10 15:39:51'),(24,27,NULL,'Исламская колбаса полукопчёная','','','Исламская колбаса полукопчёная','','','350  г. / 700 г.','','','',NULL,'<p>Срок хранения: 30 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573383458_Исламская_прав.jpg','2019-11-10 16:57:38'),(25,27,NULL,'Мясная колбаса полукопчёная','','','Мясная колбаса полукопчёная','','','300 г /600 г.','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573383793_Мясная.jpg','2019-11-10 17:03:13'),(26,27,NULL,'Рубленная колбаса полукопчёная','','','Рубленная колбаса полукопчёная','','','300 г. / 600 г.','','','',NULL,'<p>Срок хранения: 30 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573383959_Рубленная.jpg','2019-11-10 17:05:59'),(27,27,NULL,'Мусульманский с сыром сервелат полукопчёный','','','Мусульманский с сыром сервелат полукопчёный','','','350 г. / 700 г.','','','',NULL,'<p>Срок хранения : 30 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573384157_Мусульм с сыром.jpg','2019-11-10 17:09:17'),(28,27,NULL,'Кремлёвская колбаса полукопчёная','','','Кремлёвская колбаса полукопчёная','','','300 г. / 600 г.','','','',NULL,'<p>Срок хранения : 25 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +4 С</p>\r\n','','','1573384333_Кремлевская.jpg','2019-11-10 17:12:13'),(29,27,NULL,'Имперская колбаса полукопчёная','','','Имперская колбаса полукопчёная','','','300 г. / 600 г.','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573384480_Имперская.jpg','2019-11-10 17:14:40'),(30,28,NULL,'Докторская колбаса варёная','','','Докторская колбаса варёная','','','950 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от0 до +4 С</p>\r\n','','','1573385119_Докторская.jpg','2019-11-10 17:25:19'),(31,28,NULL,'Русская колбаса варёная','','','Русская колбаса варёная','','','950 г.','','','',NULL,'<p>Срок хранения : 40 суток&nbsp;</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573385278_Русская.jpg','2019-11-10 17:27:58'),(32,29,NULL,'Докторские сосиски варёные','','','Докторские сосиски варёные','','','480 г. / 1 кг.','','','',NULL,'<p>Срок хранения : 15 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573385627_Докторские сосиски_прав.jpg','2019-11-10 17:33:47'),(33,33,NULL,'Астаналық  колбаса полукопчёная','','','Астаналық  колбаса полукопчёная','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573386438_Астаналык_прав.jpg','2019-11-10 17:47:18'),(34,33,NULL,'Нұр-шашу колбаса полукопчёная ','','','Нұр-шашу колбаса полукопчёная ','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573386622_Нур шашу_прав.jpg','2019-11-10 17:50:22'),(35,33,NULL,'Ақтөбе колбаса полукопчёная','','','Ақтөбе колбаса полукопчёная','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573386765_Актобе_прав.jpg','2019-11-10 17:52:45'),(36,33,NULL,'Восточный колбаса полукопчёная ','','','Восточный колбаса полукопчёная ','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>&nbsp;Температура хранения : от 0 до +6 С</p>\r\n','','','1573386882_Восточный_прав.jpg','2019-11-10 17:54:42'),(37,33,NULL,'Чесночная колбаса полукопчёная','','','Чесночная колбаса полукопчёная','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573387053_Чесночная_прав.jpg','2019-11-10 17:56:47'),(38,33,NULL,'Говяжья колбаса полукопчёная ','','','Говяжья колбаса полукопчёная ','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573387241_Говяжья коп_прав.jpg','2019-11-10 18:00:41'),(39,33,NULL,'Формовая колбаса полукопчёная','','','Формовая колбаса полукопчёная','','','800 г.','','','',NULL,'<p>Срок хранения: 20 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573387463_Формовая.jpg','2019-11-10 18:04:23'),(40,33,NULL,'Дачная колбаса полукопчёная','','','Дачная колбаса полукопчёная','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573387588_Дачная_прав.jpg','2019-11-10 18:06:28'),(41,33,NULL,'Краковская колбаса полукопчёная','','','Краковская колбаса полукопчёная','','','400 г.','','','',NULL,'<p>Срок хранения : от 7 до 30 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573387915_Краковская.jpg','2019-11-10 18:11:55'),(42,33,NULL,'Пикантная колбаса полукопчёная ','','','Пикантная колбаса полукопчёная ','','','350 г.','','','',NULL,'<p>Срок хранения : от&nbsp;7 до 20 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +4 С</p>\r\n','','','1573388102_Пикантная.jpg','2019-11-10 18:15:02'),(43,33,NULL,'Туркестан сервелат','','','Туркестан сервелат','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573388269_Туркестан_прав.jpg','2019-11-10 18:17:49'),(44,33,NULL,'Мусульманский сервелат ','','','Мусульманский сервелат ','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573388449_Мусульм_прав.jpg','2019-11-10 18:20:49'),(45,33,NULL,'Береке сервелат','','','Береке сервелат','','','650 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573388575_Береке_прав.jpg','2019-11-10 18:22:55'),(46,34,NULL,'Говяжья  колбаса варёная','','','Говяжья  колбаса варёная','','','950 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573388857_Говяжья вар.jpg','2019-11-10 18:27:37'),(47,34,NULL,'Наурыз колбаса варёная ','','','Наурыз колбаса варёная ','','','550 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения: от 0 до +6 С</p>\r\n','','','1573389068_Наурыз.jpg','2019-11-10 18:31:08'),(48,34,NULL,'Народная колбаса варёная','','','Народная колбаса варёная','','','950 г. / 1,5 кг.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения от 0 до +4 С</p>\r\n','','','1573389282_Народная.jpg','2019-11-10 18:34:42'),(49,34,NULL,'Чайная колбаса варёная ','','','Чайная колбаса варёная ','','','750 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573389436_Чайная.jpg','2019-11-10 18:37:16'),(50,34,NULL,'По-мусульмански  колбаса варёная','','','По-мусульмански  колбаса варёная','','','950 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573389613_Мусульм вареная.jpg','2019-11-10 18:40:13'),(51,34,NULL,'Останкинская колбаса варёная ','','','Останкинская колбаса варёная ','','','950 г. / 1,5 кг.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>&nbsp;Температура хранения : от 0&nbsp;до +6 С</p>\r\n','','','1573389767_Останкинская.jpg','2019-11-10 18:42:47'),(52,34,NULL,'Телячья колбаса варёная','','','Телячья колбаса варёная','','','750 г.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573389901_Телячья.jpg','2019-11-10 18:45:01'),(53,34,NULL,'Фирменная колбаса варёная','','','Фирменная колбаса варёная','','','950 г. / 1,5 кг.','','','',NULL,'<p>Срок хранения : 40 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573390027_Фирменная.jpg','2019-11-10 18:47:07'),(54,35,NULL,'Говяжьи сосиски','','','Говяжьи сосиски','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения: от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573390556_сардельки Говяжьи пачка.jpg','2019-11-10 18:55:56'),(55,35,NULL,'Телячьи сосиски','','','Телячьи сосиски','','','600 г. / 1 кг.','','','',NULL,'<p>Срок храненеия : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573390714_сосиски Телячьи пачка.jpg','2019-11-10 18:58:34'),(56,35,NULL,'Традиционные сосиски','','','Традиционные сосиски','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от0 до +6 С</p>\r\n','','','1573390898_сосиски Традиц пачка.jpg','2019-11-10 19:01:38'),(57,7,NULL,'Голландия сыр полутвёрдый','','','Голландия сыр полутвёрдый','','','1.5 кг / 3.5 кг','Украина','','',NULL,'<p>Жирность 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573391130_ШОСТКА СЫР Hollandskiy 3,35kg.jpg','2019-11-10 19:05:30'),(58,35,NULL,'По-мусульмански сосиски ','','','По-мусульмански сосиски ','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура храненеия : от 0 до +6 С</p>\r\n','','','1573391678_сосиски по мусульм пачка.jpg','2019-11-10 19:14:38'),(59,35,NULL,'Вкусные сосиски','','','Вкусные сосиски','','','1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573392148_сосиски Вкусные пачка.jpg','2019-11-10 19:22:28'),(60,35,NULL,'Вкусные сосиски замороженные','','','Вкусные сосиски замороженные','','','380 г. ','','','',NULL,'<p>Срок хранения :180 суток</p>\r\n\r\n<p>Темперетура хранения : -18 С</p>\r\n','','','1573392370_сосиски Вкусные.jpg','2019-11-10 19:26:10'),(61,35,NULL,'Аппетитные сосиски замороженные','','','Аппетитные сосиски замороженные','','','380 г. ','','','',NULL,'<p>Срок хранения :&nbsp;180 суток</p>\r\n\r\n<p>&nbsp;Температура хранения : -18 С</p>\r\n','','','1573392576_сосиски Аппетитные.jpg','2019-11-10 19:29:36'),(62,51,NULL,'Мойва соленая','','','Мойва соленая','','','2 кг','','','',NULL,'<p>Срок хранения: 30 суток</p>\r\n\r\n<p>Температура хранения: от 05 до 0 С</p>\r\n','','','1573392733_РЫба мойва.jpg','2019-11-10 19:32:13'),(63,35,NULL,'Студенческие сосиски замороженные','','','Студенческие сосиски замороженные','','','380 г.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573392734_сосиски Студенч.jpg','2019-11-10 19:32:14'),(64,35,NULL,'Говяжьи сардельки','','','Говяжьи сардельки','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573392928_сардельки Говяжьи пачка.jpg','2019-11-10 19:35:28'),(65,51,NULL,'Сельдь соленая','','','Сельдь соленая','','','5 / 10 кг','','','',NULL,'<p>Срок хранения: 120 суток</p>\r\n\r\n<p>Температура хранения: от -8&nbsp;до 0 С</p>\r\n','','','1573392936_Рыба сельдь.jpg','2019-11-10 19:35:36'),(66,35,NULL,'Телячьи сардельки ','','','Телячьи сардельки ','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393066_сардельки Телячьи пачка.jpg','2019-11-10 19:37:46'),(67,51,NULL,'Скумбрия соленая','','','Скумбрия соленая','','','3 / 5 кг','','','',NULL,'<p>Срок хранения: 25 суток</p>\r\n\r\n<p>Температура хранения: от -8 до -4 С</p>\r\n','','','1573393077_Рыба скумбрия.jpg','2019-11-10 19:37:57'),(68,35,NULL,'Куриные сардельки','','','Куриные сардельки','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393181_сардельки Куриные пачка.jpg','2019-11-10 19:39:41'),(69,51,NULL,'Вобла холодного копчения','','','Вобла холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576665754_vobla xk.jpg','2019-11-10 19:40:13'),(70,35,NULL,'Сочные сардельки ','','','Сочные сардельки ','','','600 г. / 1 кг.','','','',NULL,'<p>Срок хранения : от 25 до 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393306_сардельки Сочныепачка.jpg','2019-11-10 19:41:46'),(71,51,NULL,'Лещ холодного копчения','','','Лещ холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576665833_lesh\'xk.jpg','2019-11-10 19:41:50'),(72,51,NULL,'Мойва холодного копчения','','','Мойва холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576665887_moyva.jpg','2019-11-10 19:42:56'),(73,51,NULL,'Сельдь холодного копчения  ','','','Сельдь холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576665933_seld\'.jpg','2019-11-10 19:43:37'),(74,51,NULL,'Киперс леща холодного копчения','','','Киперс леща холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666064_kipers.jpg','2019-11-10 19:44:12'),(75,51,NULL,'Киперс жереха холодного копчения','','','Киперс жереха холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666116_kipers zh.jpg','2019-11-10 19:44:37'),(76,35,NULL,'Говяжьи сардельки','','','Говяжьи сардельки','','','1 кг.','','','',NULL,'<p>Срок хранения : 20 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393498_сардельки Говяжьи пачка.jpg','2019-11-10 19:44:58'),(77,51,NULL,'Скумбрия холодного копчения ','','','Скумбрия холодного копчения ','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666166_skum.jpg','2019-11-10 19:45:29'),(78,51,NULL,'Сом холодного копчения ','','','Сом холодного копчения ','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666198_som.jpg','2019-11-10 19:46:12'),(79,51,NULL,'Чехонь холодного копчения','','','Чехонь холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666350_chekhon\'.jpg','2019-11-10 19:46:42'),(80,51,NULL,'Соломка из красной рыбы холодного копчения','','','Соломка из красной рыбы холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576666421_solomka.jpg','2019-11-10 19:47:10'),(81,35,NULL,'Балапан сосиски','','','Балапан сосиски','','','1 кг.','','','',NULL,'<p>Срок хранения : 45 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393653_сосиски Балапан.jpg','2019-11-10 19:47:33'),(82,51,NULL,'Спинка жереха холодного копчения','','','Спинка жереха холодного копчения','','','10 кг','','','',NULL,'<p>Срок хранения: 90 суток</p>\r\n\r\n<p>Температура хранения: от -5 до 0 С</p>\r\n','','','1576665982_spinka.jpg','2019-11-10 19:47:46'),(83,35,NULL,'Дачные шпикачки','','','Дачные шпикачки','','','1 кг.','','','',NULL,'<p>Срок хранения : 20 суток</p>\r\n\r\n<p>Температура хранения : от 0 до +6 С</p>\r\n','','','1573393862_сардельки Дачные пачка.jpg','2019-11-10 19:51:02'),(84,36,NULL,'Аппетитный фарш','','','Аппетитный фарш','','','500 г. / 1 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температера хранения : -18</p>\r\n','','','1573394461_фарш Аппетитный_прав.jpg','2019-11-10 20:01:01'),(85,36,NULL,'Говяжий фарш ','','','Говяжий фарш ','','','1 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573394611_фарш Говяжий_прав.jpg','2019-11-10 20:03:31'),(86,36,NULL,'Куриный фарш','','','Куриный фарш','','','500 г. / 1 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения :-18 С</p>\r\n','','','1573394757_фарш Куриный_прав.jpg','2019-11-10 20:05:57'),(87,36,NULL,'Нежный фарш ','','','Нежный фарш ','','','500 г. / 1 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573394919_фарш Нежный_прав.jpg','2019-11-10 20:08:39'),(88,36,NULL,'Южный фарш','','','Южный фарш','','','500г.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : -18</p>\r\n\r\n<p>&nbsp;</p>\r\n','','',NULL,'2019-11-10 20:12:07'),(89,36,NULL,'Эконом фарш ','','','Эконом фарш ','','','1 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573395276_фарш Эконом_прав.jpg','2019-11-10 20:14:36'),(90,37,NULL,'Говяжьи купаты','','','Говяжьи купаты','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>&nbsp;Температура хранения : -18 С</p>\r\n','','','1573395442_купаты Говяжьи_прав.jpg','2019-11-10 20:17:22'),(91,37,NULL,'Куриные купаты ','','','Куриные купаты ','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573395561_Купаты Куриные_прав.jpg','2019-11-10 20:19:21'),(92,37,NULL,'Конские купаты','','','Конские купаты','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573395674_купаты Конские_прав.jpg','2019-11-10 20:21:14'),(93,37,NULL,'Свиные купаты','','','Свиные купаты','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>&nbsp;Температура хранения : - 18 С</p>\r\n','','','1573395789_купаты Свиные_прав.jpg','2019-11-10 20:23:09'),(94,7,NULL,'Рыцарь сыр полутвёрдый','','','Рыцарь сыр полутвёрдый','','','7 кг / 9 кг','Украина','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573395806_ШОСТКА СЫР Rytsar 3,35kg.jpg','2019-11-10 20:23:26'),(95,37,NULL,'Куриный шашлык гриль','','','Куриный шашлык гриль','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С&nbsp;</p>\r\n','','','1573396004_шашлык Куриный.jpg','2019-11-10 20:26:44'),(96,37,NULL,'Свиной шашлык гриль','','','Свиной шашлык гриль','','','1,2 кг.','','','',NULL,'<p>Срок хранения : 180 суток</p>\r\n\r\n<p>Температура хранения : - 18 С</p>\r\n','','','1573396112_шашлык Свиной.jpg','2019-11-10 20:28:32'),(97,7,NULL,'Шостка сыр полутвёрдый','','','Шостка сыр полутвёрдый','','','7 кг / 9 кг','Украина','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573396988_ШОСТКА СЫР Shostka 7-9 kg.jpg','2019-11-10 20:43:08'),(98,7,NULL,'Гауда сыр полутвёрдый','','','Гауда сыр полутвёрдый','','','1,5 кг / 3,5 кг','Украина','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573397114_ШОСТКА СЫР Gouda 3,35kg.jpg','2019-11-10 20:45:14'),(99,10,NULL,'Кантри сыр твёрдый ','','','Кантри сыр твёрдый ','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573397485_Комо Кантри брус.jpg','2019-11-10 20:51:25'),(100,10,NULL,'Ореховый сыр твёрдый','','','Ореховый сыр твёрдый','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398103_Комо Гороховый брус.jpg','2019-11-10 20:58:15'),(101,18,NULL,'Фермерское масло сливочное','','','Фермерское масло сливочное','','','170 г.','','','',NULL,'<p>Жирность :73 %</p>\r\n\r\n<p>Количество : 50 шт.</p>\r\n\r\n<p>Срок хранения : 300 суток</p>\r\n','','','1573398137_Достойный выбор Фермерское 170г.jpg','2019-11-10 21:02:17'),(102,10,NULL,'Сметанковый сыр твёрдый','','','Сметанковый сыр твёрдый','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398227_Комо Сметанковый брус.jpg','2019-11-10 21:03:47'),(103,10,NULL,'Голландский сыр твёрдый классический','','','Голландский сыр твёрдый классический','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398350_Комо Голландский брус.jpg','2019-11-10 21:05:50'),(104,18,NULL,'Фермерское масло сливочное','','','Фермерское масло сливочное','','','450 г.','','','',NULL,'<p>Жирность : 73 %</p>\r\n\r\n<p>&nbsp;Количество : 40 штук</p>\r\n\r\n<p>Срок хранения : 300 суток</p>\r\n','','','1573398377_Достойный выбор Фермерское 450г.jpg','2019-11-10 21:06:17'),(105,10,NULL,'Российский сыр твёрдый классический','','','Российский сыр твёрдый классический','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398453_Комо Российский брус.jpg','2019-11-10 21:07:33'),(106,10,NULL,'Сливочный сыр твёрдый','','','Сливочный сыр твёрдый','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398600_Комо Сливочный брус.jpg','2019-11-10 21:10:00'),(107,10,NULL,'Пошехонский сыр твёрдый','','','Пошехонский сыр твёрдый','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398681_Комо Пошехонский брус.jpg','2019-11-10 21:11:21'),(108,10,NULL,'Гауда сыр твёрдый','','','Гауда сыр твёрдый','','','3-3,5 кг','Дубномолоко','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573398756_Комо Гауда брус.jpg','2019-11-10 21:12:36'),(109,10,NULL,'Голландия сыр твёрдый классический','','','Голландия сыр твёрдый классический','','','150 г','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401047_COMO 150 Holland.jpg','2019-11-10 21:26:20'),(110,10,NULL,'Российский сыр твёрдый классический','','','Российский сыр твёрдый классический','','','150г','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401094_COMO 150 Российский.jpg','2019-11-10 21:47:00'),(111,10,NULL,'Старый Голландец сыр твёрдый','','','Старый Голландец сыр твёрдый','','','150г','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401422_COMO 150 Олд Holland.jpg','2019-11-10 21:57:02'),(112,10,NULL,'Кантри сыр твёрдый со вкусом топлёного молока','','','Кантри сыр твёрдый со вкусом топлёного молока','','','150г','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401550_COMO 150 Country.jpg','2019-11-10 21:59:10'),(113,10,NULL,'Тенеро сыр твёрдый','','','Тенеро сыр твёрдый','','','170г','Дубномолоко','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401641_COMO 150 Тенеро.jpg','2019-11-10 22:00:41'),(114,10,NULL,'Голландия сыр твёрдый','','','Голландия сыр твёрдый','','','220г','Дубномолоко','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401785_COMO 220 Holland.jpg','2019-11-10 22:03:05'),(115,10,NULL,'Кантри сыр твёрдый со вкусом топлёного молока','','','Кантри сыр твёрдый со вкусом топлёного молока','','','220г','Дубномолоко','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573401902_COMO 220 Country.jpg','2019-11-10 22:05:02'),(116,11,NULL,'Король сыров сыр твёрдый со вкусом и ароматом топлёного молока','','','Король сыров сыр твёрдый со вкусом и ароматом топлёного молока','','','2,25-2,45 кг','Украина','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573402726_Pyriatyn_KorolSyrov_Brus-2kg.jpg','2019-11-10 22:18:46'),(117,11,NULL,'Витязь сыр твёрдый классический','','','Витязь сыр твёрдый классический','','','2,25-2,45 кг','Украина','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573402922_Pyriatyn_Vitiaz_Brus-2kg.jpg','2019-11-10 22:22:02'),(118,11,NULL,'Гауда сыр твёрдый','','','Гауда сыр твёрдый','','','2,25-2,45 кг','Украина','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573403107_Pyriatyn_Gauda_Brus-2kg.jpg','2019-11-10 22:25:07'),(119,11,NULL,'С семенами тмина сыр плавленный','','','С семенами тмина сыр плавленный','','','150г','Украина','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 60 суток<o:p></o:p></span></p>\r\n','','','1573403219_Pyriatyn_Plavsyr-Tmin_Kolbaska-150g.jpg','2019-11-10 22:26:59'),(120,11,NULL,'С ветчиной сыр плавленный','','','С ветчиной сыр плавленный','','','150г','Украина','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 60 суток<o:p></o:p></span></p>\r\n','','','1573403289_Pyriatyn_Plavsyr-Vetchina_Kolbaska-150g.jpg','2019-11-10 22:28:09'),(121,11,NULL,'С баварской горчицей сыр плавленный','','','С баварской горчицей сыр плавленный','','','150г','Украина','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 60 суток<o:p></o:p></span></p>\r\n','','','1573403359_Pyriatyn_Plavsyr-Gorchitsa_Kolbaska-150g.jpg','2019-11-10 22:29:19'),(122,12,NULL,'SVALIA сыр твёрдый','','','SVALIA сыр твёрдый','','','200 г / 350 г / 3 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576726375_svalyaa.jpg','2019-11-10 22:37:33'),(123,12,NULL,'DVARO сыр твёрдый','','','DVARO сыр твёрдый','','','200 г / 350 г / 3 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 50%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576667003_dvaro.jpg','2019-11-10 22:38:32'),(124,12,NULL,'BALTIJA сыр твёрдый','','','BALTIJA сыр твёрдый','','','200 г / 350 г / 3 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576666940_Baltiya.jpg','2019-11-10 22:39:35'),(125,12,NULL,'GILDIJA сыр твёрдый','','','GILDIJA сыр твёрдый','','','200 г / 350 г / 3 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576666888_gil\'diya.jpg','2019-11-10 22:40:25'),(126,12,NULL,'GOUDA сыр твёрдый','','','GOUDA сыр твёрдый','','','200 г / 3 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 50%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576666812_gauda.jpg','2019-11-10 22:41:11'),(127,12,NULL,'ЕDAM сыр твёрдый','','','ЕDAM сыр твёрдый','','','200г','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576666599_edam.jpg','2019-11-10 22:41:57'),(128,12,NULL,'Гурманам сыр плавленный копчёный','','','Гурманам сыр плавленный копчёный','','','180г','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1576666732_gurmanam.jpg','2019-11-10 22:42:44'),(129,12,NULL,'MAASDAM сыр твёрдый','','','MAASDAM сыр твёрдый','','','150г','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 45%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 120 суток<o:p></o:p></span></p>\r\n','','','1576666667_maasdam.jpg','2019-11-10 22:43:31'),(130,12,NULL,'Parnidzio сыр твёрдый','','','Parnidzio сыр твёрдый','','','3-3,5 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 38%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1576667141_Parnidzio.jpg','2019-11-10 22:44:45'),(131,12,NULL,'SVALYA SNACKS сыр плавленый копчёный','','','SVALYA SNACKS сыр плавленый копчёный','','','3-3,5 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 50%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1576667225_SVALYA SNACKS.jpg','2019-11-10 22:45:32'),(132,12,NULL,'SVALYA SNACKS с чесноком','','','SVALYA SNACKS с чесноком','','','3-3,5 кг','Сваля','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Жирность: 50%<o:p></o:p></span></p>\r\n\r\n<p style=\"margin-top:0cm;background:white;box-sizing: border-box;margin-bottom:\r\n1rem;font-variant-ligatures: normal;font-variant-caps: normal;orphans: 2;\r\nwidows: 2;-webkit-text-stroke-width: 0px;text-decoration-style: initial;\r\ntext-decoration-color: initial;word-spacing:0px\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1576667263_SVALYA S.jpg','2019-11-10 22:46:23'),(133,13,NULL,'Голландский сыр твёрдый','','','Голландский сыр твёрдый','','','150г','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406536_Брест Голладский 150г.jpg','2019-11-10 23:22:17'),(134,13,NULL,'Финский сыр твёрдый','','','Финский сыр твёрдый','','','150г','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406628_Брест Финский 150г.jpg','2019-11-10 23:23:48'),(135,13,NULL,'Гауда сыр твёрдый','','','Гауда сыр твёрдый','','','150г','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406693_Брест Гауда 150г.jpg','2019-11-10 23:24:53'),(136,13,NULL,'Тильзитер сыр твёрдый','','','Тильзитер сыр твёрдый','','','150г','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406759_Брест Тильзитер 150г.jpg','2019-11-10 23:25:59'),(137,13,NULL,'Российский сыр твёрдый','','','Российский сыр твёрдый','','','3,5 кг','Беларусь','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406843_Брест Российский 3,5кг.jpg','2019-11-10 23:27:23'),(138,13,NULL,'Голландский сыр твёрдый','','','Голландский сыр твёрдый','','','3,5 кг','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573406933_Брест Голладский 3,5кг.jpg','2019-11-10 23:28:53'),(139,13,NULL,'Финский сыр твёрдый','','','Финский сыр твёрдый','','','3,5 кг','Беларусь','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573407035_Брест Финский 3,5кг.jpg','2019-11-10 23:30:35'),(140,13,NULL,'Российский сыр твёрдый','','','Российский сыр твёрдый','','','150г','Беларусь','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573407115_Брест Российский 150г.jpg','2019-11-10 23:31:55'),(141,14,NULL,'Пуск сыр плавленный','','','Пуск сыр плавленный','','','140 г / 200 г / 500 г','Арла Фудс','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573407585_Puck-Cream-Cheese-500g.jpg','2019-11-10 23:39:29'),(142,14,NULL,'Пуск сыр плавленный','','','Пуск сыр плавленный','','','150г','Арла Фудс','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667055_pusk.jpg','2019-11-10 23:41:01'),(143,14,NULL,'Natura сыр моцарелла','','','Natura сыр моцарелла','','','2,5 кг','Арла Фудс','','',NULL,'<p>Жирность: 40%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423705_Пуск моцарелла натура.jpg','2019-11-10 23:42:11'),(144,14,NULL,'Pizza Topping сыр моцарелла','','','Pizza Topping сыр моцарелла','','','2,5 кг','Арла Фудс','','',NULL,'<p>Жирность: 40%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423667_Пуск моцарелла пиццатоппинг.jpg','2019-11-10 23:43:18'),(145,14,NULL,'Ваниль творожная масса','','','Ваниль творожная масса','','','700 г / 3 кг','Арла Фудс','','',NULL,'<p>Жирность: 23%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667378_vanilla.jpg','2019-11-10 23:44:58'),(146,14,NULL,'Курага творожная масса','','','Курага творожная масса','','','700 г / 3 кг','Арла Фудс','','',NULL,'<p>Жирность: 23%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667420_kuraga.jpg','2019-11-10 23:45:58'),(147,14,NULL,'Голландия Продукт белково-жировой','','','Голландия Продукт белково-жировой','','','2-2,5 кг','Арла Фудс','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667490_golland.jpg','2019-11-10 23:52:17'),(148,14,NULL,'Русский Продукт белково-жировой','','','Русский Продукт белково-жировой','','','2-2,5 кг','Арла Фудс','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667531_Rus.jpg','2019-11-10 23:53:02'),(149,69,NULL,'Для гриля и шашлыка кетчуп','','','Для гриля и шашлыка кетчуп','','','350г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573408726_Кетчуп_Mr_Ricco_для_гриля_и_шашлыка.jpg','2019-11-10 23:58:46'),(150,72,NULL,'Томатный кетчуп','','','Томатный кетчуп','','','350г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573409443_Кетчуп Mr.Ricco томатный дой-пак 350г.jpg','2019-11-11 00:01:42'),(151,69,NULL,'Острый кетчуп','','','Острый кетчуп','','','350г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573408995_Кетчуп Mr.Ricco острый дой-пак 350г.jpg','2019-11-11 00:03:15'),(152,70,NULL,'Болоньезе соус','','','Болоньезе соус','','','350г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1576667616_Bol.jpg','2019-11-11 00:07:56'),(153,68,NULL,'Провансаль майонез','','','Провансаль майонез','','','220 мл / 400 мл / 800 мл','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573409736_МЗ Mr.Ricco Провансаль 67% дп 800мл.jpg','2019-11-11 00:15:36'),(154,68,NULL,'Оливковый майонез','','','Оливковый майонез','','','220 мл / 400 мл / 800 мл','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573409851_МЗ Mr.Ricco дп 400 оливковый.jpg','2019-11-11 00:17:31'),(155,68,NULL,'На перепелином яйце майонез','','','На перепелином яйце майонез','','','150г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573410061_МЗ Mr.Ricco дп 220 пер яйцо.jpg','2019-11-11 00:21:01'),(156,70,NULL,'Болоньезе соус','','','Болоньезе соус','','','150г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667707_Bol.jpg','2019-11-11 00:24:26'),(157,54,NULL,'Провансаль майонез','','','Провансаль майонез','','','375 г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p style=\"margin-top:0cm;background:white\"><span style=\"font-family:&quot;Open Sans&quot;,serif;\r\ncolor:#A5A5A5\">Срок хранения: 180 суток<o:p></o:p></span></p>\r\n','','','1573410936_1.jpg','2019-11-11 00:31:24'),(158,68,NULL,'Organic на перепелином яйце майонез','','','Organic на перепелином яйце майонез','','','3-3,5 кг','НЭФИС-БИОПРОДУКТ','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423982_МЗ Mr.Ricco перепело 760 слив.jpg','2019-11-11 00:33:56'),(159,68,NULL,'Extra Virgin Organic Оливковый майонез','','','Extra Virgin Organic Оливковый майонез','','','3-3,5 кг','НЭФИС-БИОПРОДУКТ','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573424089_МЗ Mr.Ricco дп 220 оливковый.jpg','2019-11-11 00:40:53'),(160,54,NULL,'Organic Провансаль майонез','','','Organic Провансаль майонез','','','150 г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','',NULL,'2019-11-11 00:43:08'),(162,71,NULL,'Краснодарский соус томатный','','','Краснодарский соус томатный','','','150 г','НЭФИС-БИОПРОДУКТ','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667786_milo.jpg','2019-11-11 00:46:52'),(163,18,NULL,'Фермерское масло сливочное','','','Фермерское масло сливочное','','','170 г','Аяла','','',NULL,'<p>Жирность: 73%</p>\r\n\r\n<p>Срок хранения: 300 суток</p>\r\n','','','1573412033_Достойный выбор Фермерское 170г.jpg','2019-11-11 00:53:53'),(164,18,NULL,'Фермерское масло сливочное','','','Фермерское масло сливочное','','','450 г','Аяла','','',NULL,'<p>Жирность: 73%</p>\r\n\r\n<p>Срок хранения: 300 суток</p>\r\n','','','1573412369_Достойный выбор Фермерское 450г.jpg','2019-11-11 00:59:29'),(165,18,NULL,'Селянка масло сливочное','','','Селянка масло сливочное','','','3-3,5 кг','Аяла','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667676_sel\'yanka.jpg','2019-11-11 01:07:23'),(166,22,NULL,'Белорусское масло растительно-сливочное','','','Белорусское масло растительно-сливочное','','','3-3,5 кг','Россия','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','',NULL,'2019-11-11 01:12:32'),(167,22,NULL,'Башкирское спред растительно- жировой','','','Башкирское спред растительно- жировой','','','3-3,5 кг','Россия','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667897_bashk.jpg','2019-11-11 01:16:12'),(168,22,NULL,'Коровки спред растительно- жировой','','','Коровки спред растительно- жировой','','','500 г','Россия','','',NULL,'<p>Жирность: 72,5%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573413509_Коровки спред.jpg','2019-11-11 01:18:29'),(169,22,NULL,'Крестьянское спред растительно- сливочный','','','Крестьянское спред растительно- сливочный','','','500 г','Россия','','',NULL,'<p>Жирность: 72,5%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573413613_Крестьянское воронеж.jpg','2019-11-11 01:20:13'),(170,20,NULL,'Верхний Услон масло сливочное','','','Верхний Услон масло сливочное','','','5 кг / 20 кг','Верхний Услон','','',NULL,'<p>Жирность: 72,5 % / 82,5 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573413848_Верхний услон.jpg','2019-11-11 01:24:08'),(171,24,NULL,'Русагро спред растительно- жировой','','','Русагро спред растительно- жировой','','','180 г / 3,6 кг','Россия','','',NULL,'<p>Жирность: 72,5 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573413980_Русагро.jpg','2019-11-11 01:26:20'),(172,24,NULL,'Масло Гост масло сладкосливочное','','','Масло Гост масло сладкосливочное','','','20 кг','Россия','','',NULL,'<p>Жирность: 73 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573414120_Верхний услон.jpg','2019-11-11 01:28:40'),(173,60,NULL,'Ореховая тыква с мускатом овощные галеты','','','Ореховая тыква с мускатом овощные галеты','','','300 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414456_Бондюэль ореховая тыква.jpg','2019-11-11 01:34:16'),(174,60,NULL,'Звёздное трио овощные галеты','','','Звёздное трио овощные галеты','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414586_Бондюэль звездное трио.jpg','2019-11-11 01:36:26'),(175,60,NULL,'Цветная капуста','','','Цветная капуста','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414723_Bonduelle Цветная капуста 400г.jpg','2019-11-11 01:38:43'),(176,60,NULL,'Капуста брокколи','','','Капуста брокколи','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414815_Bonduelle брокколи 400г.jpg','2019-11-11 01:40:15'),(177,60,NULL,'Зелёный букет овощные галеты','','','Зелёный букет овощные галеты','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414923_Bonduelle галеты Зеленый букет 3D.jpg','2019-11-11 01:42:03'),(178,60,NULL,'Кантри овощные галеты','','','Кантри овощные галеты','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573414989_Bonduelle галеты кантри 3D.jpg','2019-11-11 01:43:09'),(179,60,NULL,'Королевские овощные галеты','','','Королевские овощные галеты','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573415075_Bonduelle галеты Королевские 3D.jpg','2019-11-11 01:44:35'),(180,60,NULL,'Сицилийские овощные галеты','','','Сицилийские овощные галеты','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573415138_Bonduelle галеты сицилийские 3D.jpg','2019-11-11 01:45:38'),(181,60,NULL,'Зелёная фасоль резаная','','','Зелёная фасоль резаная','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573415287_Bonduelle зеленая фасоль резаная 400г.jpg','2019-11-11 01:48:07'),(182,60,NULL,'Зелёная фасоль целая','','','Зелёная фасоль целая','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573415386_Bonduelle зеленая фасоль целая 400г.jpg','2019-11-11 01:49:46'),(183,60,NULL,'Кубань фасоль тонкая резаная','','','Кубань фасоль тонкая резаная','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573415533_Bonduelle_Кубань_фасоль_тонкая_резаная.jpg','2019-11-11 01:52:13'),(184,60,NULL,'Кубань фасоль экстра-тонкая резаная','','','Кубань фасоль экстра-тонкая резаная','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416302_Bonduelle_Кубань_фасоль_экстра_тонкая.jpg','2019-11-11 02:05:02'),(185,60,NULL,'Мексиканская овощная смесь для жарки','','','Мексиканская овощная смесь для жарки','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416433_Bonduelle_овощная_смесь_Мексиканская.jpg','2019-11-11 02:07:13'),(186,60,NULL,'Гавайская овощная смесь','','','Гавайская овощная смесь','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416504_Bonduelle овощная смесь Гавайская 400г.jpg','2019-11-11 02:08:24'),(187,60,NULL,'Кубань зелёный горошек','','','Кубань зелёный горошек','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416565_Bonduelle_Кубань_горошек_зеленый.jpg','2019-11-11 02:09:25'),(188,60,NULL,'Царская овощная смесь','','','Царская овощная смесь','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416630_Bonduelle овощная смесь Царская 400г.jpg','2019-11-11 02:10:30'),(189,60,NULL,'Баклажаны гриль','','','Баклажаны гриль','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416721_Bonduelle Баклажаны гриль 400г.jpg','2019-11-11 02:12:01'),(190,60,NULL,'Перцы гриль','','','Перцы гриль','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416805_Bonduelle Перцы гриль 400г.jpg','2019-11-11 02:13:25'),(191,60,NULL,'Трио гриль','','','Трио гриль','','','3-3,5 кг','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416915_Bonduelle трио гриль 400г.jpg','2019-11-11 02:15:15'),(192,60,NULL,'Цукини гриль','','','Цукини гриль','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573416988_Bonduelle Цуккини гриль 400г.jpg','2019-11-11 02:16:28'),(193,60,NULL,'Парижская смесь для жарки','','','Парижская смесь для жарки','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573417062_Bonduelle Парижская 700г.jpg','2019-11-11 02:17:42'),(194,60,NULL,'Парижская с копчёным беконом смесь для жарки','','','Парижская с копчёным беконом смесь для жарки','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573417165_Bonduelle_Парижская_с_копченым_беконом.jpg','2019-11-11 02:19:25'),(195,60,NULL,'Средиземноморская смесь для жарки','','','Средиземноморская смесь для жарки','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573417257_Bonduelle Средиземноморская 700г.jpg','2019-11-11 02:20:57'),(196,60,NULL,'Средиземноморская с овощами смесь для жарки','','','Средиземноморская с овощами смесь для жарки','','','150 г','Бондюэль','','',NULL,'<p>Срок хранения: 180 суток</p>\r\n','','','1573417314_Bonduelle_Средиземноморская_с_овощами.jpg','2019-11-11 02:21:54'),(197,39,NULL,'Большое филе цыплёнок-бройлер','','','Большое филе цыплёнок-бройлер','','','0,8-0,9 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573417912_Петруха большое филе.jpg','2019-11-11 02:31:52'),(198,39,NULL,'Голень цыплёнок-бройлер','','','Голень цыплёнок-бройлер','','','0,8-0,9 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418144_Петруха голень.jpg','2019-11-11 02:35:44'),(199,39,NULL,'Микс цыплёнок-бройлер: бедро, крыло, голень','','','Микс цыплёнок-бройлер: бедро, крыло, голень','','','13,6 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418277_Петруха микс 2.jpg','2019-11-11 02:37:57'),(200,39,NULL,'Микс цыплёнок-бройлер: филе, крыло, голень','','','Микс цыплёнок-бройлер: филе, крыло, голень','','','13,6 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418355_Петруха микс 1.jpg','2019-11-11 02:39:15'),(201,39,NULL,'Плечевая часть крыла цыплёнок-бройлер','','','Плечевая часть крыла цыплёнок-бройлер','','','0,8-0,9 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418531_Петруха плеч.jpg','2019-11-11 02:42:11'),(202,39,NULL,'Тушка цыплёнок-бройлер','','','Тушка цыплёнок-бройлер','','','1,2-2,0 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418711_Петруха тушка.jpg','2019-11-11 02:45:11'),(203,39,NULL,'Печень цыплёнок-бройлер','','','Печень цыплёнок-бройлер','','','0,8-0,9 кг','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573418864_Петруха печень.jpg','2019-11-11 02:47:44'),(204,39,NULL,'Фарш цыплёнок-бройлер','','','Фарш цыплёнок-бройлер','','','500 г','Серволюкс/Смолевичи','','',NULL,'<p>Срок хранения: 12 месяцев</p>\r\n','','','1573419037_Петруха фарш.jpg','2019-11-11 02:50:38'),(205,45,NULL,'Говядина бескостная замороженная','','','Говядина бескостная замороженная','','','3-3,5 кг','','','',NULL,'<p>Жирность: 50 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573419284_Говядина.jpg','2019-11-11 02:54:44'),(206,47,NULL,'Печень говяжья замороженная','','','Печень говяжья замороженная','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573419502_Печень.jpg','2019-11-11 02:58:22'),(207,47,NULL,'Сердце говяжье','','','Сердце говяжье','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573419599_Сердце говядина.jpg','2019-11-11 02:59:59'),(208,47,NULL,'Язык говяжий замороженный','','','Язык говяжий замороженный','','','150 г','','','',NULL,'<p>Жирность: 50 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573419738_Язык говяжий.jpg','2019-11-11 03:02:18'),(209,50,NULL,'Эсколар рыба свежемороженая','','','Эсколар рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573420050_Рыба эсколар.jpg','2019-11-11 03:07:30'),(210,50,NULL,'Сёмга рыба свежемороженая','','','Сёмга рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 50 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573420877_Рыба семга.jpg','2019-11-11 03:21:17'),(211,50,NULL,'Скумбрия рыба свежемороженая','','','Скумбрия рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573420997_Рыба скумбрия.jpg','2019-11-11 03:23:17'),(212,50,NULL,'Форель рыба свежемороженая','','','Форель рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421104_Рыба форель.jpg','2019-11-11 03:25:04'),(213,50,NULL,'Нототения рыба свежемороженая','','','Нототения рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45 %</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421256_РЫба нототения.jpg','2019-11-11 03:27:36'),(214,50,NULL,'Сельдь','','','Сельдь','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421364_Рыба сельдь.jpg','2019-11-11 03:29:24'),(215,50,NULL,'Камбала рыба свежемороженая','','','Камбала рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421496_Рыба камбала.jpg','2019-11-11 03:31:36'),(216,50,NULL,'Лемонема рыба свежемороженая','','','Лемонема рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421576_Рыба лемонема.jpg','2019-11-11 03:32:56'),(217,50,NULL,'Тилапия рыба свежемороженая','','','Тилапия рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421682_Рыба тилапия.jpg','2019-11-11 03:34:42'),(218,50,NULL,'Пангасиус рыба свежемороженая','','','Пангасиус рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421767_Рыба пангасиус.jpg','2019-11-11 03:36:07'),(219,50,NULL,'Путассу рыба свежемороженая','','','Путассу рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421924_Рыба путассу.jpg','2019-11-11 03:38:44'),(220,50,NULL,'Хек рыба свежемороженая','','','Хек рыба свежемороженая','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573421987_Рыба хек.jpg','2019-11-11 03:39:47'),(221,50,NULL,'Мойва рыба свежемороженая','','','Мойва рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422166_РЫба мойва.jpg','2019-11-11 03:42:46'),(222,50,NULL,'Минтай рыба свежемороженая','','','Минтай рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422295_Рыба минтай.jpg','2019-11-11 03:44:55'),(223,50,NULL,'Горбуша рыба свежемороженая','','','Горбуша рыба свежемороженая','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422422_Рыба Горбуша.jpg','2019-11-11 03:47:02'),(224,50,NULL,'Кета рыба свежемороженая','','','Кета рыба свежемороженая','','','150 г','Россия','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422533_Рыба кета.jpg','2019-11-11 03:48:53'),(225,50,NULL,'Крабовые палочки','','','Крабовые палочки','','','150 г','Россия','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1576667953_krab.jpg','2019-11-11 03:51:06'),(226,50,NULL,'Килька','','','Килька','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422768_Рыба килька.jpg','2019-11-11 03:52:48'),(227,50,NULL,'Угорь жареный','','','Угорь жареный','','','150 г','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422836_Рыба угорь.jpg','2019-11-11 03:53:56'),(228,50,NULL,'Кальмар','','','Кальмар','','','150 г','Россия','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573422923_Рыба кальмар.jpg','2019-11-11 03:55:23'),(229,50,NULL,'Креветки','','','Креветки','','','3-3,5 кг','','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423080_Рыба креветки.jpg','2019-11-11 03:58:00'),(230,50,NULL,'Икра','','','Икра','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423219_Рыба икра.jpg','2019-11-11 04:00:19'),(231,50,NULL,'Морской коктейль','','','Морской коктейль','','','3-3,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423317_Рыба Морской коктейль.jpg','2019-11-11 04:01:57'),(232,50,NULL,'Мидии','','','Мидии','','','150 г','Россия','','',NULL,'<p>Жирность: 50%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573423418_Рыба мидии.jpg','2019-11-11 04:03:38'),(233,62,NULL,'French Frites straight картофель фри','','','French Frites straight картофель фри','','','2,5 кг / 12,5 кг','','','',NULL,'<p>Жирность: 45%</p>\r\n\r\n<p>Срок хранения: 180 суток</p>\r\n','','','1573424388_12556.jpg','2019-11-11 04:19:27');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_request_files`
--

DROP TABLE IF EXISTS `purchase_request_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_request_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `purchase_request_id` int(11) DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `content` text,
  `model` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_request_files`
--

LOCK TABLES `purchase_request_files` WRITE;
/*!40000 ALTER TABLE `purchase_request_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_request_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_requests`
--

DROP TABLE IF EXISTS `purchase_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `delivery_days` int(11) DEFAULT NULL,
  `delivery_condition` varchar(255) DEFAULT NULL,
  `payment_condition` varchar(255) DEFAULT NULL,
  `description` text,
  `delivery_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_requests`
--

LOCK TABLES `purchase_requests` WRITE;
/*!40000 ALTER TABLE `purchase_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `procurement_method` int(11) DEFAULT NULL,
  `procurement_type` int(11) DEFAULT NULL,
  `purchase_stage` int(11) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `delivery_time` date DEFAULT NULL,
  `delivery_condition` varchar(255) DEFAULT NULL,
  `payment_condition` varchar(255) DEFAULT NULL,
  `description` text,
  `docs` varchar(255) DEFAULT NULL,
  `tech_task` varchar(255) DEFAULT NULL,
  `is_active` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
INSERT INTO `purchases` VALUES (1,1,'test',3,5,14,7,9,4,'2020-12-04','test','test','<p>test</p>\r\n','uploads/files/test/A3xZpO1olvxaP_a4wIqS.docx','',1),(2,1,'test',3,5,14,7,9,4,'2020-12-04','test','test','<p>test</p>\r\n','','',0),(3,1,'test',3,5,15,6,9,12,'2020-12-22','test','test','test','','',1);
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_admins`
--

DROP TABLE IF EXISTS `purchases_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_admins`
--

LOCK TABLES `purchases_admins` WRITE;
/*!40000 ALTER TABLE `purchases_admins` DISABLE KEYS */;
INSERT INTO `purchases_admins` VALUES (1,1,5),(2,2,5),(3,3,8);
/*!40000 ALTER TABLE `purchases_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_commissions`
--

DROP TABLE IF EXISTS `purchases_commissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_commissions`
--

LOCK TABLES `purchases_commissions` WRITE;
/*!40000 ALTER TABLE `purchases_commissions` DISABLE KEYS */;
INSERT INTO `purchases_commissions` VALUES (1,1,7),(2,2,7),(3,3,7);
/*!40000 ALTER TABLE `purchases_commissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchasing_guide`
--

DROP TABLE IF EXISTS `purchasing_guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchasing_guide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchasing_guide`
--

LOCK TABLES `purchasing_guide` WRITE;
/*!40000 ALTER TABLE `purchasing_guide` DISABLE KEYS */;
INSERT INTO `purchasing_guide` VALUES (1,'ROKOS',0),(2,'тендер',1),(3,'ценовое предложение',1),(4,'ТМЦ',2),(5,'СМР',2),(6,'KZT',3),(7,'USD',3),(8,'комплект',4),(9,'шт',4),(10,'не активно',5),(11,'активно',5),(12,'не активно',6),(13,'активно',6),(14,'Активно',7),(15,'Тестовый',7);
/*!40000 ALTER TABLE `purchasing_guide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_network`
--

DROP TABLE IF EXISTS `social_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instagram` text,
  `youtube` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_network`
--

LOCK TABLES `social_network` WRITE;
/*!40000 ALTER TABLE `social_network` DISABLE KEYS */;
INSERT INTO `social_network` VALUES (1,'https://instagram.com/rokos_group_global?igshid=1s0zgwxr9wazb','https://www.youtube.com/channel/UCL9xKCgFcuHj_mKKMWNo0hg');
/*!40000 ALTER TABLE `social_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_source_message_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_message`
--

LOCK TABLES `source_message` WRITE;
/*!40000 ALTER TABLE `source_message` DISABLE KEYS */;
INSERT INTO `source_message` VALUES (1,'main','Стать клиентом'),(2,'main','Закрыть'),(3,'main','Отправить'),(4,'main','Обратный звонок'),(5,'main','Доска почета'),(6,'main','Продукция компании'),(7,'main','<span>Посмотреть</span> полный каталог'),(8,'main','Перейти'),(9,'main','Подробнее'),(10,'main','Отправить резюме'),(11,'main','Подать заявку'),(12,'main','Результаты поиска по запросу {text} .'),(13,'main','По запросу ничего {text} не найдено'),(14,'main','Заказать'),(15,'main','Разработано в'),(16,'main','Портфель'),(17,'main','Новости'),(18,'main','Контакты'),(19,'main','МПК'),(20,'main','Логистика'),(21,'form','Имя'),(22,'form','Телефон'),(23,'form','Если вы хотите сотрудничать с нами, заполните следущие поля'),(24,'form','Контактное лицо'),(25,'form','@@Выберите файл@@'),(26,'form','Город'),(27,'form','Адрес'),(28,'form','Наименование торговой точки'),(29,'form','ФИО'),(30,'form','Авдеев Сергей Юрьевич'),(31,'form','Электронная почта'),(32,'form','Комментарий'),(33,'form','Оставить комментарий'),(34,'form','Резюме'),(35,'form','Ссылка на резюме со сторонних сайтов'),(36,'form','Файл резюме'),(37,'main-portfolio','Тип товара'),(38,'main-portfolio','Поиск по названию'),(39,'main-portfolio','Бренды'),(40,'main-portfolio','Категории'),(41,'main-portfolio','Вес'),(42,'main-portfolio','Категория'),(43,'main-logistics','Наша транспортная база'),(44,'main-logistics','География наших перевозок'),(45,'main-logistics','Наша клиентская база'),(46,'main-logistics','С нами сотрудничают'),(47,'main-mpk','Продукция МПК'),(48,'main-mpk','Наши принципы'),(49,'main-salesman','Обязанности'),(50,'main-salesman','Требования'),(51,'main-salesman','Условия'),(52,'main-salesman','Резюме принимаются на электронный адрес'),(53,'main-salesman','Справки по телефону'),(54,'main-index','О компании'),(55,'main-index','Наши партнеры'),(56,'main-contact','Наши филиалы'),(57,'main-career','Развивайтесь вместе с нами!'),(58,'main-index','КЛИЕНТАМ'),(59,'main-index','Команда торговых представителей всегда готова\r\n                    обеспечить вас продукцией наших партнёров и\r\n                    собственного мясоперерабатывающего комбината.'),(60,'main-index','ПАРТНЕРАМ'),(61,'main-index','Если Вы производитель, отправьте нам коммерческое предложение.\r\n                    После рассмотрения Мы с Вами\r\n                    свяжемся и ответим на все вопросы.'),(62,'main-error','Вышеуказанная ошибка произошла, когда веб-сервер обрабатывал ваш запрос.'),(63,'main-error','Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.'),(64,'main','Если вы не нашли подходящую вакансию, вы можете прислать нам свое резюме.'),(65,'main-career','Группа компаний RGG уделяет большое значение развитию своих сотрудников и считает\r\n                                своим долгом поддерживать амбициозных людей в их стремлении добиваться успехов\r\n                                вместе с нашей командой.'),(66,'main-career','Мы понимаем, что в своем стремлении к успеху наша главная\r\n                                ценность – это наши сотрудники. Именно поэтому приглашаем к сотрудничеству\r\n                                талантливых людей готовых стать частью нашей команды.'),(67,'form','Вакансия'),(68,'form','Загрузка резюме'),(69,'form','Если Вы производитель, отправьте нам коммерческое предложение или обратитесь\r\n                                по контактам:'),(70,'form','Руководитель отдела Закупа - Махмутова Малика Шервалиевна:\r\n                                    <a href=\"tel:+7-701-871-1202\">+7-701-871-1202</a>'),(71,'form','Загрузить коммерческое предложение');
/*!40000 ALTER TABLE `source_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage_first`
--

DROP TABLE IF EXISTS `stage_first`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage_first` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `purchase_status` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remaining` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage_first`
--

LOCK TABLES `stage_first` WRITE;
/*!40000 ALTER TABLE `stage_first` DISABLE KEYS */;
INSERT INTO `stage_first` VALUES (1,1,11,'2004-12-20','2031-12-20',851990400,NULL),(2,2,11,'2004-12-20','2031-12-20',851990400,NULL),(3,3,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `stage_first` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage_first_docs`
--

DROP TABLE IF EXISTS `stage_first_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage_first_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_required` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage_first_docs`
--

LOCK TABLES `stage_first_docs` WRITE;
/*!40000 ALTER TABLE `stage_first_docs` DISABLE KEYS */;
INSERT INTO `stage_first_docs` VALUES (2,2,0,'test',0);
/*!40000 ALTER TABLE `stage_first_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage_second`
--

DROP TABLE IF EXISTS `stage_second`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage_second` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `purchase_status` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remaining` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage_second`
--

LOCK TABLES `stage_second` WRITE;
/*!40000 ALTER TABLE `stage_second` DISABLE KEYS */;
INSERT INTO `stage_second` VALUES (1,2,12,'2002-12-20','2030-12-20',883612800,NULL),(2,1,13,'2020-12-22','2020-12-31',777600,NULL),(3,3,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `stage_second` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage_second_docs`
--

DROP TABLE IF EXISTS `stage_second_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage_second_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_required` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage_second_docs`
--

LOCK TABLES `stage_second_docs` WRITE;
/*!40000 ALTER TABLE `stage_second_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `stage_second_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation`
--

DROP TABLE IF EXISTS `translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `name_en` text NOT NULL,
  `name_kz` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation`
--

LOCK TABLES `translation` WRITE;
/*!40000 ALTER TABLE `translation` DISABLE KEYS */;
INSERT INTO `translation` VALUES (3,'Поиск','Поиск','Поиск'),(4,'Заказать звонок','Feedback','Кері байланыс'),(5,'Ваше имя','Ваше имя','Ваше имя'),(6,'Ваше телефон:','Ваш телефон:','Телефон нөмірі'),(7,'Ваше email','Ваш email','Ваш email'),(8,'Ваше сообщение','Your message',' Сіздің хабарламаңыз'),(9,'Отправить','Submit',' Жіберу'),(10,'Ошибка','Error','Қате'),(11,'Заполните поле ФИО',' Fill in the name field','АЖТ өрісті толтырыңыз'),(12,'Телефон должен состоять из 10 цифр',' Phone must be 10 digits','Телефон 10 саннан тұруы керек'),(13,'Заполните поле E-mail',' Fill in the e-mal field','E-mail өрісті толтырыңыз'),(14,'Заполните поле сообщение','Fill in the message field',' Хабарлама өрісін толтырыңыз'),(15,'Заказ успешно отправлен','Order sent successfully','Тапсырыс сәтті жіберілді'),(16,'В ближайшее время мы свяжемся с вами',' In the near future we will contact you','Жақын арада сізбен хабарласамыз'),(17,'Вышеуказанная ошибка произошла, когда веб-сервер обрабатывал ваш запрос.',' The above error occurred when the web server was processing your request.',' Жоғарыда көрсетілген қате веб-сервер сіздің сұрауыңызды өңдеген кезде пайда болды'),(18,'Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.',' Please contact us if you think this is a server error. Thanks.','Егер сіз бұл сервер қатесі деп ойласаңыз, бізге хабарласыңыз. Рахмет.'),(19,'Подробнее','More details','Толығырақ'),(22,'Результаты поиска по запросу','Search Results','Іздеу нәтижелері'),(23,'По запросу',' Upon request',' Сұраныс бойынша'),(24,'Ничего не найдено','Nothing found','Еш нәрсе табылмады'),(26,'Заполните поле телефон','Заполните поле телефон','Заполните поле телефон'),(27,'Закрыть','Закрыть','Закрыть'),(28,'ТОО «Компания ECOS» предоставляет своим Заказчикам широкий спектр услуг своим заказчикам','ТОО «Компания ECOS» предоставляет своим Заказчикам широкий спектр услуг своим заказчикам','ТОО «Компания ECOS» предоставляет своим Заказчикам широкий спектр услуг своим заказчикам'),(29,'Производитель','Производитель','Производитель'),(30,'Все','Все','Все'),(31,'Категории товара','Категории товара','Категории товара'),(32,'Подкатегории товара','Подкатегории товара','Подкатегории товара'),(33,'Продукты не найдено','Продукты не найдено','Продукты не найдено'),(34,'Нажмите чтобы увеличить','Нажмите чтобы увеличить','Нажмите чтобы увеличить'),(35,'Применение','Применение','Применение'),(36,'Ключевые слова','Ключевые слова','Ключевые слова'),(37,'Наши партнеры','Наши партнеры','Наши партнеры'),(38,'© 2017','© 2017','© 2017'),(39,'ТОО «Компания ECOS»','ТОО «Компания ECOS»','ТОО «Компания ECOS»'),(40,'Все права защищены','Все права защищены','Все права защищены'),(41,'Тел','Тел','Тел'),(42,'Факс','Факс','Факс'),(43,'Email','Email','Email');
/*!40000 ALTER TABLE `translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'demo@site.com','Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v','$2y$13$0njEXBHZ6TEmpNozNcu7hOjNl1zpNi7UYYCGqfMV4cVxaZTv/fQee',NULL,'demo@site.com',16,1533018523,1608612241,'test','13123123213','teest','test','test','test test','test','1231212312'),(2,'manager','MpbLFhoMz6XLHJkJOP9sQDd0-5BYiSyN','$2y$13$r5axb.q.KQ1mD0HyVAOQw.RZzsteGIdlnqPfvBwP0wHZukVZXX6.G',NULL,'manager@rokos.kz',8,1580127698,1580127698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'operator','5gVMx5gm6xuk8MlOtfsKAXPAzxavPXCI','$2y$13$dACeRYGLWXgvtCOV2d4kc.UDmwL9P4sQOn4Tm3dbtOaBLkCF2o6Zq',NULL,'operator@rokos.kz',4,1580127708,1580127708,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'portfolio_manager','DEydZD48SwwBHr-cYoCEp5YPpGqKAR1G','$2y$13$X9i84RQqL1fupFmfF/Yk3uohuO3jVet6IFCAjx816OAu/CkYqpjaO',NULL,'portfolio@rokos.kz',6,1580703352,1580703352,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'purchase','k77hC6WMV89WXgfAlIwv-QTUclzk9TnF','$2y$13$0njEXBHZ6TEmpNozNcu7hOjNl1zpNi7UYYCGqfMV4cVxaZTv/fQee',NULL,'purchase@gmail.com',128,1606455761,1606455761,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'comission','dN3fDEF7iiw0r9_zjCZ3jz0vrTb7NHcl','$2y$13$0oM7Ue3Z8YdUUXgbd6lsre1NLGl9URWGvpwLtjsSWxl54usKcxbse',NULL,'comission@gmail.com',64,1606455809,1606455809,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'comission2','cXWLNHH-1PIe0siUC6CNI_pQYqaQgAcx','$2y$13$XyXfBE7bcHYlWiSWByDiC.0cUPR588B6gu4imVmNQh0iP9syXE846',NULL,'comission2@gmail.com',64,1606455852,1606455852,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'admin','kNuFCDtygPfegU50txYNFKJSWbW7vfOu','$2y$13$vRSA08hJmDZL3uvR9YQAfO5rTvB.j4Qhyw28TOrWscOBUIuFsPjj2',NULL,'admin@admin.com',256,1607313343,1607313343,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'test@test.com','u8g-VncPHp1IhgoYYGf3I_5MfJoAAZEW','$2y$13$sVnILErdfFjz1xbGiZ1egeOpRw3Gh8GiPculldq37Lr1.4ZLP1nH.',NULL,'test@test.com',128,1607521089,1607521089,'test','123123213','test','test','test','test','test','test'),(10,'testtest@test.com','qzaz3B-hyW3DM1Dw7cxbOpHxp_olXmk8','$2y$13$SIPsH/3oF0QXiqH.b/q9XeTDLeIYKvTLrvXz0d3toahka6d5ETizi',NULL,'testtest@test.com',128,1607521219,1607521219,'test','123123123','test','test','test','test','test','123123123');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy`
--

DROP TABLE IF EXISTS `vacancy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text,
  `price` varchar(255) DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `content_en` text,
  `name_kz` varchar(255) DEFAULT NULL,
  `content_kz` text,
  `schedule` varchar(255) DEFAULT NULL,
  `schedule_en` varchar(255) DEFAULT NULL,
  `schedule_kz` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_en` varchar(255) DEFAULT NULL,
  `address_kz` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `by_phone` varchar(255) DEFAULT NULL,
  `by_phone_en` varchar(255) DEFAULT NULL,
  `by_phone_kz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy`
--

LOCK TABLES `vacancy` WRITE;
/*!40000 ALTER TABLE `vacancy` DISABLE KEYS */;
INSERT INTO `vacancy` VALUES (2,1,1,'Торговый представитель','<p>В ТОО &quot;RGG&quot; в связи с расширением пакета требуются Торговые представители следующих категорий:</p>\r\n','200000','Торговый представитель','<p>В ТОО &quot;RGG&quot; в связи с расширением пакета требуются Торговые представители следующих категорий:</p>\r\n','Торговый представитель','<p>В ТОО &quot;RGG&quot; в связи с расширением пакета требуются Торговые представители следующих категорий:</p>\r\n','','','','','','','info@rokos.kz','+7 (727) 260 28 35','',''),(5,5,5,'Директор филиала г.Актобе','<p>ТОО RGG&nbsp;</p>\r\n','','','','','','','','','','','','a.kravchenko@rokos.kz','','','');
/*!40000 ALTER TABLE `vacancy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy_conditions`
--

DROP TABLE IF EXISTS `vacancy_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `condition` text NOT NULL,
  `condition_en` text,
  `condition_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy_conditions`
--

LOCK TABLES `vacancy_conditions` WRITE;
/*!40000 ALTER TABLE `vacancy_conditions` DISABLE KEYS */;
INSERT INTO `vacancy_conditions` VALUES (19,2,'Работа в успешной, стабильной Компании',NULL,'null'),(20,2,'Оформление в соответствии с законодательством РК;',NULL,'null'),(21,2,'Работа с собственными и международными брендами;',NULL,'null'),(22,2,'Своевременная выплата заработной платы;',NULL,'null'),(23,2,'Пятидневный график работы;',NULL,'null'),(24,2,'Обучение.',NULL,'null'),(28,5,'Конкурентная заработная плата;',NULL,'null'),(29,5,'Премия за выполнение KPI и задач (ежемесячная, полугодовая);',NULL,'null'),(30,5,'Возможность карьерного роста и развития;',NULL,'null');
/*!40000 ALTER TABLE `vacancy_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy_description`
--

DROP TABLE IF EXISTS `vacancy_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy_description`
--

LOCK TABLES `vacancy_description` WRITE;
/*!40000 ALTER TABLE `vacancy_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacancy_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy_requirements`
--

DROP TABLE IF EXISTS `vacancy_requirements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `requirement` text NOT NULL,
  `requirement_en` text,
  `requirement_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy_requirements`
--

LOCK TABLES `vacancy_requirements` WRITE;
/*!40000 ALTER TABLE `vacancy_requirements` DISABLE KEYS */;
INSERT INTO `vacancy_requirements` VALUES (11,2,'Желателен опыт работы в дистрибуции не менее 1 года;',NULL,'null'),(12,2,'Умение договариваться и вести результативные переговоры с Клиентами;',NULL,'null'),(13,2,'Целеустремленность;',NULL,'null'),(19,5,'Опыт работы на руководящих позициях не менее 3х лет;',NULL,'null'),(20,5,'Опыт работы в области дистрибуции продуктов питания является обязательным условием;',NULL,'null'),(21,5,'Хорошие навыки ведения переговоров, постановки задач и контроля их исполнения;',NULL,'null'),(22,5,'Организаторские способности, навыки планирования;',NULL,'null'),(23,5,'Умение эффективно работать самостоятельно и в команде, нацеленность на результат, высокий уровень ответственности и самоорганизации.',NULL,'null');
/*!40000 ALTER TABLE `vacancy_requirements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy_responsibilities`
--

DROP TABLE IF EXISTS `vacancy_responsibilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy_responsibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(11) NOT NULL,
  `responsibility` text NOT NULL,
  `responsibility_en` text,
  `responsibility_kz` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy_responsibilities`
--

LOCK TABLES `vacancy_responsibilities` WRITE;
/*!40000 ALTER TABLE `vacancy_responsibilities` DISABLE KEYS */;
INSERT INTO `vacancy_responsibilities` VALUES (66,2,'Выполнение плана продаж продукции на закреплённой территории;',NULL,'null'),(67,2,'Расширение ассортимента продукции Компании в торговой точке;',NULL,'null'),(68,2,'Ежедневный, еженедельный, ежемесячный анализ продаж и постановка целей по выполнению планов;',NULL,'null'),(69,2,'Проверка товарных запасов Клиента;',NULL,'null'),(70,2,'Формирование эффективных заявок по количеству и ассортименту продукции на основе анализа продаж Клиента;',NULL,'null'),(71,2,'Презентация продукции Клиентам;',NULL,'null'),(72,2,'Работа по осуществлению мерчендайзинга в торговых точках;',NULL,'null'),(73,2,'Работа по расширению, открытию и регистрации новых торговых точек с целью максимального охвата всех Клиентов на территории своего района;',NULL,'null'),(74,2,'Осуществление контроля за своевременной доставкой продукции согласно заявки;',NULL,'null'),(82,5,'Организация эффективной работы филиала;',NULL,'null'),(83,5,'Обеспечение выполнения плана продаж;',NULL,'null'),(84,5,'Управление бюджетом и обеспечение выполнения плана по валовой и чистой прибыли;',NULL,'null'),(85,5,'Формирование эффективных заявок на поставку продукции и управление складскими запасами; ',NULL,'null'),(86,5,'Управление транспортной логистикой (собственный автопарк)',NULL,'null'),(87,5,'Обеспечение взаимодействия между подразделениями филиала;',NULL,'null'),(88,5,'Решение административно хозяйственных вопросов и взаимодействие с гос. органами;',NULL,'null');
/*!40000 ALTER TABLE `vacancy_responsibilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy_resume`
--

DROP TABLE IF EXISTS `vacancy_resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `href` varchar(255) DEFAULT NULL,
  `file` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy_resume`
--

LOCK TABLES `vacancy_resume` WRITE;
/*!40000 ALTER TABLE `vacancy_resume` DISABLE KEYS */;
INSERT INTO `vacancy_resume` VALUES (1,'qwdasasd',1,'asdasd','8(231) 231-2312','asasdasd',NULL,'');
/*!40000 ALTER TABLE `vacancy_resume` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-23 11:20:37
