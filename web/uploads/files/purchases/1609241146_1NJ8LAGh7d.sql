-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: rokos
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'demo@site.com','Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v','$2y$13$0njEXBHZ6TEmpNozNcu7hOjNl1zpNi7UYYCGqfMV4cVxaZTv/fQee',NULL,'demo@site.com',16,1533018523,1564726553,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'manager','MpbLFhoMz6XLHJkJOP9sQDd0-5BYiSyN','$2y$13$r5axb.q.KQ1mD0HyVAOQw.RZzsteGIdlnqPfvBwP0wHZukVZXX6.G',NULL,'manager@rokos.kz',8,1580127698,1580127698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'operator','5gVMx5gm6xuk8MlOtfsKAXPAzxavPXCI','$2y$13$dACeRYGLWXgvtCOV2d4kc.UDmwL9P4sQOn4Tm3dbtOaBLkCF2o6Zq',NULL,'operator@rokos.kz',4,1580127708,1580127708,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'portfolio_manager','DEydZD48SwwBHr-cYoCEp5YPpGqKAR1G','$2y$13$X9i84RQqL1fupFmfF/Yk3uohuO3jVet6IFCAjx816OAu/CkYqpjaO',NULL,'portfolio@rokos.kz',6,1580703352,1580703352,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'purchase','k77hC6WMV89WXgfAlIwv-QTUclzk9TnF','$2y$13$0njEXBHZ6TEmpNozNcu7hOjNl1zpNi7UYYCGqfMV4cVxaZTv/fQee',NULL,'purchase@gmail.com',128,1606455761,1606455761,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'comission','dN3fDEF7iiw0r9_zjCZ3jz0vrTb7NHcl','$2y$13$0oM7Ue3Z8YdUUXgbd6lsre1NLGl9URWGvpwLtjsSWxl54usKcxbse',NULL,'comission@gmail.com',64,1606455809,1606455809,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'comission2','cXWLNHH-1PIe0siUC6CNI_pQYqaQgAcx','$2y$13$XyXfBE7bcHYlWiSWByDiC.0cUPR588B6gu4imVmNQh0iP9syXE846',NULL,'comission2@gmail.com',64,1606455852,1606455852,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-03 10:19:42
