<?php

namespace app\modules\admin\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;

class AdminProfileController extends BackendController
{
    public function actionIndex()
    {
        $admin = User::find()->where(['id' => \Yii::$app->user->identity->id])->one();

        return $this->render('index',compact('admin'));
    }

    public function actionSetProfile($username, $password){
        $admin = User::findOne(['id' => \Yii::$app->user->identity->id]);
        $admin->username = $username;
        $admin->setPassword($password);
        if($admin->save(false)){
            Yii::$app->session->setFlash('profile_success','Профиль успешно обновлен!');
            return $this->redirect('/admin/');
        }else{
            Yii::$app->session->setFlash('profile_error','Что-то пошло не так!');
            return $this->redirect('/admin/');
        }
    }

}