<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelTypeEmail
{
    public static function typeList()
    {
        return [
            4 => 'Обратная связь',
            3 => 'Партнеры',
            2 => 'Клиенты',
            1 => 'Вакансии',
            0 => 'Все',
        ];
    }

    public static function typeLabel($type)
    {
        switch ($type) {
            case 0:
                $class = 'label label-success';
                break;
            case 1:
                $class = 'label label-default';
                break;
            case 2:
                $class = 'label label-default';
                break;
            case 3:
                $class = 'label label-default';
                break;
            case 4:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::typeList(), $type), [
            'class' => $class,
        ]);
    }


}