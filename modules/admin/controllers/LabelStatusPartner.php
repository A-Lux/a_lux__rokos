<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusPartner
{
    public static function statusPartnerList()
    {
        return [
            1 => 'Сотрудничество',
            0 => 'Партнеры',
        ];
    }

    public static function statusPartnerLabel($statusPartner)
    {
        switch ($statusPartner) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusPartnerList(), $statusPartner), [
            'class' => $class,
        ]);
    }


}