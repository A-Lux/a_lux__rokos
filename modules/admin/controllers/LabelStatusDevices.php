<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusDevices
{
    public static function statusDevicesList()
    {
        return [
            1 => 'Для мобильной',
            0 => 'Для десктопной',
        ];
    }

    public static function statusDevicesLabel($statusDevices)
    {
        switch ($statusDevices) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusDevicesList(), $statusDevices), [
            'class' => $class,
        ]);
    }


}