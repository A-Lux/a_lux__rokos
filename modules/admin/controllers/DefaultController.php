<?php

namespace app\modules\admin\controllers;

use app\models\User;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if(\Yii::$app->user->identity->role == User::ROLE_MANAGER){
            return $this->redirect('/admin/products/index');
        }elseif (\Yii::$app->user->identity->role == User::ROLE_OPERATOR){
            return $this->redirect('/admin/feedback/index');
        }elseif (\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            return $this->redirect("/admin/menu/index");
        }else{
            \Yii::$app->user->logout();
            return $this->redirect('/auth/login');
        }
    }
}
