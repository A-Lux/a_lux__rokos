<?php

namespace app\modules\admin\controllers;

use app\models\VacancyConditions;
use app\models\VacancyDescription;
use app\models\VacancyRequirements;
use app\models\VacancyResponsibilities;
use Yii;
use app\models\Vacancy;
use app\models\search\VacancySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class VacancyController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vacancy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VacancySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vacancy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vacancy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vacancy();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $responses      = $this->arraySort(Yii::$app->request->post()['responses_ru'], Yii::$app->request->post()['responses_kz'], Yii::$app->request->post()['responses_en']);
            $conditions     = $this->arraySort(Yii::$app->request->post()['conditions_ru'], Yii::$app->request->post()['conditions_kz'], Yii::$app->request->post()['conditions_en']);
            $requirements   = $this->arraySort(Yii::$app->request->post()['requirements_ru'], Yii::$app->request->post()['requirements_kz'], Yii::$app->request->post()['requirements_en']);
            $descriptions   = $this->arraySort(Yii::$app->request->post()['descriptions_ru'], Yii::$app->request->post()['descriptions_kz'], Yii::$app->request->post()['descriptions_en']);

            $this->saveResponses($responses, $model);
            $this->saveConditions($conditions, $model);
            $this->saveRequirements($requirements, $model);
            $this->saveDescriptions($descriptions, $model);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vacancy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $responses      = $this->arraySort(Yii::$app->request->post()['responses_ru'], Yii::$app->request->post()['responses_kz'], Yii::$app->request->post()['responses_en']);
            $conditions     = $this->arraySort(Yii::$app->request->post()['conditions_ru'], Yii::$app->request->post()['conditions_kz'], Yii::$app->request->post()['conditions_en']);
            $requirements   = $this->arraySort(Yii::$app->request->post()['requirements_ru'], Yii::$app->request->post()['requirements_kz'], Yii::$app->request->post()['requirements_en']);
            $descriptions   = $this->arraySort(Yii::$app->request->post()['descriptions_ru'], Yii::$app->request->post()['descriptions_kz'], Yii::$app->request->post()['descriptions_en']);

            $this->saveResponses($responses, $model);
            $this->saveConditions($conditions, $model);
            $this->saveRequirements($requirements, $model);
            $this->saveDescriptions($descriptions, $model);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vacancy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vacancy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vacancy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vacancy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function arraySort($ru, $kz, $en)
    {
        $array['ru']    = $ru;
        $array['kz']    = $kz;
        $array['en']    = $en;

        $result = [];
        $count_result = count($array['ru']);

        for($i = 0; $i < $count_result; $i++){
            $result[] = array_column($array, $i);
        }

        return $result;
    }

    protected function saveResponses($responses, $model)
    {
        VacancyResponsibilities::deleteAll(['vacancy_id' => $model->id]);

        foreach ($responses as $response) {
            $responsibility                     = new VacancyResponsibilities();
            $responsibility->vacancy_id         = $model->id;
            $responsibility->responsibility     = $response[0];
            $responsibility->responsibility_kz  = !empty($response[1]) ? $response[1] : null;
            $responsibility->responsibility_en  = !empty($response[2]) ? $response[2] : null;
            $responsibility->save();
        }
    }

    protected function saveConditions($conditions, $model)
    {
        VacancyConditions::deleteAll(['vacancy_id' => $model->id]);

        foreach ($conditions as $condition) {
            $_condition                   = new VacancyConditions();
            $_condition->vacancy_id       = $model->id;
            $_condition->condition        = $condition[0];
            $_condition->condition_kz     = !empty($condition[1]) ? $condition[1] : null;
            $_condition->condition_en     = !empty($condition[2]) ? $condition[2] : null;
            $_condition->save();
        }
    }

    protected function saveRequirements($requirements, $model)
    {
        VacancyRequirements::deleteAll(['vacancy_id' => $model->id]);

        foreach ($requirements as $requirement) {
            $_requirement                   = new VacancyRequirements();
            $_requirement->vacancy_id       = $model->id;
            $_requirement->requirement      = $requirement[0];
            $_requirement->requirement_kz   = !empty($requirement[1]) ? $requirement[1] : null;
            $_requirement->requirement_en   = !empty($requirement[2]) ? $requirement[2] : null;
            $_requirement->save();
        }
    }

    protected function saveDescriptions($descriptions, $model)
    {
        VacancyDescription::deleteAll(['vacancy_id' => $model->id]);

        foreach ($descriptions as $description) {
            $_description                   = new VacancyDescription();
            $_description->vacancy_id       = $model->id;
            $_description->description      = $description[0];
            $_description->description_kz   = !empty($description[1]) ? $description[1] : null;
            $_description->description_en   = !empty($description[2]) ? $description[2] : null;
            $_description->save();
        }
    }
}
