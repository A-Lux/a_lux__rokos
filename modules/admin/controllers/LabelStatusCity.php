<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusCity
{
    public static function statusCityList()
    {
        return [
            1 => 'Город партнер',
            0 => 'Город филиал',
        ];
    }

    public static function statusCityLabel($statusCity)
    {
        switch ($statusCity) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusCityList(), $statusCity), [
            'class' => $class,
        ]);
    }


}