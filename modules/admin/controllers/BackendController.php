<?php

namespace app\modules\admin\controllers;

use app\models\Feedback;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class BackendController extends Controller
{
    public function init()
    {

        $feedback = Feedback::findAll(['isRead'=>0]);
        Yii::$app->view->params['feedback'] = $feedback;
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function beforeAction($action)
    {
//        if(\Yii::$app->user->identity->role == User::ROLE_ADMIN && $action->actionMethod == 'actionLogin'){
//            return $this->redirect('/admin/menu/index');
//        }elseif(Yii::$app->user->identity->role == User::ROLE_MANAGER && $action->actionMethod == 'actionLogin'){
//            return $this->redirect('/admin/products/index');
//        }elseif(Yii::$app->user->identity->role == User::ROLE_OPERATOR && $action->actionMethod == 'actionLogin'){
//            return $this->redirect('/admin/feedback/index');
//        }else{
//            \Yii::$app->user->logout();
//            return $this->redirect('/auth/login');
//        }


        if(Yii::$app->user->isGuest){
            \Yii::$app->user->logout();
            return $this->redirect('/auth/login');
        }

        return parent::beforeAction($action);

    }

    protected function createImage($model)
    {
        $image = UploadedFile::getInstance($model, 'image');
        if($image != null) {
            $time = time();
            $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
        }
    }

    protected function updateImage($model, $oldImage)
    {
        $image = UploadedFile::getInstance($model, 'image');

        if($image == null){
            $model->image = $oldImage;
        }else{
            $time = time();
            $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            if(!($oldImage == null)){
                if(file_exists($model->path . $oldImage)) {
                    unlink($model->path . $oldImage);
                }
            }
        }
    }

    protected function deleteImage($model)
    {
        $oldImage = $model->image;

        if(file_exists($model->path . $oldImage)){
            unlink($model->path . $oldImage);
        }

        $model->image = null;
    }

}