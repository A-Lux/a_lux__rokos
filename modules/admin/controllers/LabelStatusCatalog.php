<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusCatalog
{
    public static function statusCatalogList()
    {
        return [
            1 => 'Файл',
            0 => 'Ссылка',
        ];
    }

    public static function statusCatalogLabel($statusCatalog)
    {
        switch ($statusCatalog) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusCatalogList(), $statusCatalog), [
            'class' => $class,
        ]);
    }


}