<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Advantage;
use app\models\search\AdvantageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdvantageController implements the CRUD actions for Advantage model.
 */
class AdvantageController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Advantage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvantageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advantage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advantage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Advantage();

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            $image_en = UploadedFile::getInstance($model, 'image_en');
            $image_kz = UploadedFile::getInstance($model, 'image_kz');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_ru_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_ru_' . $image->baseName . '.' . $image->extension;
            }
            
            if($image_en != null) {
                $time = time();
                $image_en->saveAs($model->path . $time . '_en_' . $image_en->baseName . '.' . $image_en->extension);
                $model->image_en = $time . '_en_' . $image_en->baseName . '.' . $image_en->extension;
            }

            if($image_kz != null) {
                $time = time();
                $image_kz->saveAs($model->path . $time . '_kz_' . $image_kz->baseName . '.' . $image_kz->extension);
                $model->image_kz = $time . '_kz_' . $image_kz->baseName . '.' . $image_kz->extension;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Advantage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldImage = $model->image;
        $oldImageEN = $model->image_en;
        $oldImageKZ = $model->image_kz;

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            $image_en = UploadedFile::getInstance($model, 'image_en');
            $image_kz = UploadedFile::getInstance($model, 'image_kz');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_ru_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_ru_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)) {
                    if (file_exists($model->path . $oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }
            

            if($image_kz == null){
                $model->image_kz = $oldImageKZ;
            }else{
                $time = time();
                $image_kz->saveAs($model->path . $time . '_kz_' . $image_kz->baseName . '.' . $image_kz->extension);
                $model->image_kz = $time . '_kz_' . $image_kz->baseName . '.' . $image_kz->extension;
                if(!($oldImageKZ == null)){
                    if (file_exists($model->path . $oldImageKZ)) {
                        unlink($model->path . $oldImageKZ);
                    }
                }
            }


            if($image_en == null){
                $model->image_en = $oldImageEN;
            }else{
                $time = time();
                $image_en->saveAs($model->path . $time . '_en_' . $image_en->baseName . '.' . $image_en->extension);
                $model->image_en = $time . '_en_' . $image_en->baseName . '.' . $image_en->extension;
                if(!($oldImageEN == null)){
                    if (file_exists($model->path . $oldImageEN)) {
                        unlink($model->path . $oldImageEN);
                    }
                }
            }


            if($model->save()){
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Advantage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Advantage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advantage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advantage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
