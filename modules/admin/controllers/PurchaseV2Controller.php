<?php

namespace app\modules\admin\controllers;

use app\models\PurchasesAdmins;
use app\models\PurchasesCommissions;
use app\models\StageFirst;
use app\models\StageFirstDocs;
use app\models\StageSecond;
use app\models\StageSecondDocs;
use app\models\User;
use Yii;
use app\models\Purchases;
use app\models\search\PurchaseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PurchaseV2Controller implements the CRUD actions for Purchases model.
 */
class PurchaseV2Controller extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Purchases models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Purchases model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Purchases model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Purchases();
        $fakeAdmin = new PurchasesAdmins();
        $commission = new PurchasesCommissions();

        $stageFirst = new StageFirst();
        $stageSecond = new StageSecond();


        if ($model->load(Yii::$app->request->post())) {
            $model->admin = (int)$model->admin;
            $model->commission = (int)$model->commission;

            $this->createFileDocs($model);
            $this->createFileTechTask($model);
            $this->createFileProtocol($model);
            $this->createFileProtocol2($model);

//            $model->file = UploadedFile::getInstance($model, 'docs');
//            if ($model->file) {
//                $model->docs = $model->uploadFile();
//                $model->file = null;
//            }
//            $model->file = UploadedFile::getInstance($model, 'tech_task');
//            if ($model->file) {
//                $model->tech_task = $model->uploadFile();
//            }
            $model->delivery_time = date('Y-m-d', strtotime($model->delivery_time));

//            print_r("<pre>");
//            print_r(Yii::$app->request->post()); die;

            if ($model->save()) {
                $fakeAdmin->user_id = $model->admin;
                $fakeAdmin->purchase_id = $model->id;
                $commission->user_id = $model->commission;
                $commission->purchase_id = $model->id;
                $fakeAdmin->save();
                $commission->save();

                $stageFirst->load(Yii::$app->request->post());
                $stageFirst->purchase_id = $model->id;

                $stageSecond->load(Yii::$app->request->post());
                $stageSecond->purchase_id = $model->id;

                $this->statusStages($model);


                if ($stageFirst->save() && !empty($stageFirst->files)) {
                    StageFirstDocs::deleteAll(['stage_id' => $stageFirst->id]);
                    foreach ($stageFirst->files as $stageFirstFile) {
                        $stageFirstDoc = new StageFirstDocs();
                        $stageFirstDoc->type = (int)$stageFirstFile['type'];
                        $stageFirstDoc->title = $stageFirstFile['title'];
                        $stageFirstDoc->is_required = (int)$stageFirstFile['is_required'];
                        $stageFirstDoc->stage_id = $stageFirst->id;
                        $stageFirstDoc->save();
                    }
                }

                if ($stageSecond->save() && !empty($stageSecond->files)) {
                    StageSecondDocs::deleteAll(['stage_id' => $stageFirst->id]);
                    foreach ($stageSecond->files as $stageSecondFile) {
                        $stageSecondDoc = new StageSecondDocs();
                        $stageSecondDoc->type = (int)$stageSecondFile['type'];
                        $stageSecondDoc->title = $stageSecondFile['title'];
                        $stageSecondDoc->is_required = (int)$stageSecondFile['is_required'];
                        $stageSecondDoc->stage_id = $stageSecond->id;
                        $stageSecondDoc->save();
                    }
                }


                Yii::$app->session->setFlash('success', 'Успех');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->delivery_time = date('Y-m-d', strtotime($model->delivery_time));
            }
        }

        return $this->render('create', [
            'model' => $model,
            'fakeAdmin' => $fakeAdmin,
            'commission' => $commission,
            'stageFirst' => $stageFirst,
            'stageSecond' => $stageSecond,
        ]);
    }

    /**
     * Updates an existing Purchases model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $stageFirst = $model->stageFirst;
        $stageSecond = $model->stageSecond;
        if ($stageFirst) {
            $stageFirst->files = $model->stageFirstDocs;
        } else {
            $stageFirst = new StageFirst();
        }
        if ($stageSecond) {
            $stageSecond->files = $model->stageSecondDocs;
        } else {
            $stageSecond = new StageSecond();
        }
        $model->delivery_time = date('d.m.Y', strtotime($model->delivery_time));
        $model->admin = $model->adminUser->user_id;
        $model->commission = $model->commissionUser->user_id;
        $doc = $model->docs;
        $task = $model->tech_task;
        $protocol = $model->protocol;
        $protocol2 = $model->protocol2;
        if ($model->load(Yii::$app->request->post())) {
            $this->updateFileDocs($model, $doc);
            $this->updateFileTechTask($model, $task);
            $this->updateFileProtocol($model, $protocol);
            $this->updateFileProtocol2($model, $protocol2);

            $model->delivery_time = date('Y-m-d', strtotime($model->delivery_time));


            $stageFirst->load(Yii::$app->request->post());
            $stageSecond->load(Yii::$app->request->post());

            $this->statusStages($model);

            if (!$stageFirst->purchase_id)
                $stageFirst->purchase_id = $model->id;
            if (!$stageSecond->purchase_id)
                $stageSecond->purchase_id = $model->id;

            if ($stageFirst->save() && !empty($stageFirst->files)) {
                StageFirstDocs::deleteAll(['stage_id' => $stageFirst->id]);
                foreach ($stageFirst->files as $stageFirstFile) {
                    $stageFirstDoc = new StageFirstDocs();
                    $stageFirstDoc->type = (int)$stageFirstFile['type'];
                    $stageFirstDoc->title = $stageFirstFile['title'];
                    $stageFirstDoc->is_required = (int)$stageFirstFile['is_required'];
                    $stageFirstDoc->stage_id = $stageFirst->id;
                    $stageFirstDoc->save();
                }
            }

            if ($stageSecond->save() && !empty($stageSecond->files)) {
                StageSecondDocs::deleteAll(['stage_id' => $stageFirst->id]);
                foreach ($stageSecond->files as $stageSecondFile) {
                    $stageSecondDoc = new StageSecondDocs();
                    $stageSecondDoc->type = (int)$stageSecondFile['type'];
                    $stageSecondDoc->title = $stageSecondFile['title'];
                    $stageSecondDoc->is_required = (int)$stageSecondFile['is_required'];
                    $stageSecondDoc->stage_id = $stageSecond->id;
                    $stageSecondDoc->save();
                }
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->delivery_time = date('d.m.Y', strtotime($model->delivery_time));
            }
        }

        return $this->render('update', [
            'model' => $model,
            'stageFirst' => $stageFirst,
            'stageSecond' => $stageSecond,
        ]);
    }

    /**
     * Deletes an existing Purchases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Purchases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Purchases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Purchases::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function createFileDocs($model)
    {
        $files = UploadedFile::getInstances($model, 'docs');
        $path = $model->pathFile();
        $jsonFiles = [];
        $time = time();

        foreach ($files as $key => $file) {
            $hash = \Yii::$app->security->generateRandomString(10);
            $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
            $jsonFiles[] = [
                'name' => $file->baseName,
                'file' => $time . '_' . $hash . '.' . $file->extension,
                'extension' => $file->extension,
            ];
        }

        $model->docs = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);

        return $model->docs;
    }

    protected function createFileProtocol($model)
    {
        $files = UploadedFile::getInstances($model, 'protocol');
        $path = $model->pathFile();
        $jsonFiles = [];
        $time = time();

        foreach ($files as $key => $file) {
            $hash = \Yii::$app->security->generateRandomString(10);
            $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
            $jsonFiles[] = [
                'name' => $file->baseName,
                'file' => $time . '_' . $hash . '.' . $file->extension,
                'extension' => $file->extension,
            ];
        }

        $model->protocol = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);

        return $model->protocol;
    }

    protected function createFileProtocol2($model)
    {
        $files = UploadedFile::getInstances($model, 'protocol2');
        $path = $model->pathFile();
        $jsonFiles = [];
        $time = time();

        foreach ($files as $key => $file) {
            $hash = \Yii::$app->security->generateRandomString(10);
            $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
            $jsonFiles[] = [
                'name' => $file->baseName,
                'file' => $time . '_' . $hash . '.' . $file->extension,
                'extension' => $file->extension,
            ];
        }

        $model->protocol2 = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);

        return $model->protocol2;
    }


    protected function createFileTechTask($model)
    {
        $files = UploadedFile::getInstances($model, 'tech_task');
        $path = $model->pathFile();
        $jsonFiles = [];
        $time = time();

        foreach ($files as $key => $file) {
            $hash = \Yii::$app->security->generateRandomString(10);
            $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
            $jsonFiles[] = [
                'name' => $file->baseName,
                'file' => $time . '_' . $hash . '.' . $file->extension,
                'extension' => $file->extension,
            ];
        }

        $model->tech_task = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);

        return $model->tech_task;
    }

    protected function updateFileDocs($model, $oldFile)
    {
        $files = UploadedFile::getInstances($model, 'docs');

        if (empty($files)) {
            $model->docs = $oldFile;
        } else {
            $path = $model->pathFile();
            $jsonFiles = [];
            $time = time();
            foreach ($files as $key => $file) {
                $hash = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[] = [
                    'name' => $file->baseName,
                    'file' => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles = json_decode($oldFile, true);

            foreach ($oldFiles as $old) {
                if (!empty($old['file'])) {
                    if (file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }
            $model->docs = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }
        return $model->docs;
    }

    protected function updateFileProtocol($model, $oldFile)
    {
        $files = UploadedFile::getInstances($model, 'protocol');

        if (empty($files)) {
            $model->protocol = $oldFile;
        } else {
            $path = $model->pathFile();
            $jsonFiles = [];
            $time = time();
            foreach ($files as $key => $file) {
                $hash = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[] = [
                    'name' => $file->baseName,
                    'file' => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles = json_decode($oldFile, true);

            foreach ($oldFiles as $old) {
                if (!empty($old['file'])) {
                    if (file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }
            $model->protocol = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }
        return $model->protocol;
    }

    protected function updateFileProtocol2($model, $oldFile)
    {
        $files = UploadedFile::getInstances($model, 'protocol2');

        if (empty($files)) {
            $model->protocol2 = $oldFile;
        } else {
            $path = $model->pathFile();
            $jsonFiles = [];
            $time = time();
            foreach ($files as $key => $file) {
                $hash = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[] = [
                    'name' => $file->baseName,
                    'file' => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles = json_decode($oldFile, true);

            foreach ($oldFiles as $old) {
                if (!empty($old['file'])) {
                    if (file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }
            $model->protocol2 = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }
        return $model->protocol2;
    }


    protected function updateFileTechTask($model, $oldFile)
    {
        $files = UploadedFile::getInstances($model, 'tech_task');

        if (empty($files)) {
            $model->tech_task = $oldFile;
        } else {
            $path = $model->pathFile();
            $jsonFiles = [];
            $time = time();
            foreach ($files as $key => $file) {
                $hash = \Yii::$app->security->generateRandomString(10);
                $file->saveAs($path . $time . '_' . $hash . '.' . $file->extension);
                $jsonFiles[] = [
                    'name' => $file->baseName,
                    'file' => $time . '_' . $hash . '.' . $file->extension,
                    'extension' => $file->extension,
                ];
            }

            $oldFiles = json_decode($oldFile, true);

            foreach ($oldFiles as $old) {
                if (!empty($old['file'])) {
                    if (file_exists($path . $old['file'])) {
                        unlink($path . $old['file']);
                    }
                }
            }
            $model->tech_task = json_encode($jsonFiles, JSON_UNESCAPED_UNICODE);
        }

        return $model->tech_task;

    }

    protected function statusStages($model)
    {
        if ($model->stage_second == 1) {
            $model->stage_first = 0;
        } else if ($model->stage_first == 1) {
            $model->stage_second = 0;
        } else {
            $model->stage_first = 0;
            $model->stage_second = 0;
        }

        return $model;
    }
}
