<?php

namespace app\modules\admin\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusFeedback
{
    public static function statusList()
    {
        return [
            1 => 'Обработан',
            0 => 'Не обработан',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}