<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PurchasingGuide */

$this->title = 'Добавить Справочник';
$this->params['breadcrumbs'][] = ['label' => 'Справчоник', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchasing-guide-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
