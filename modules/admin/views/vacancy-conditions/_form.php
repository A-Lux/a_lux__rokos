<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyConditions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-conditions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vacancy_id')->textInput() ?>

    <?= $form->field($model, 'condition')->textInput() ?>
    <div id="addInput" class="addInput" data-input-name="responses">
        <span></span>
        <span></span>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>



    <?php ActiveForm::end(); ?>



    <script>
        var removeDiv = function(e) {
            console.log(document.querySelector('div#input'+e.target.getAttribute('input_id')+''));
            // element.parentElement.parentElement.innerHTML = "";
            document.querySelector('div#input'+e.target.getAttribute('input_id')+'').remove()
        }
        var plus = function (element) {
            console.log(element)
            element.addEventListener("click", removeDiv);
        }

        var DrawInput = function () {
            
            var deleteInput = document.querySelectorAll(".deleteInputField");
          
            addInput.insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group "><input style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' + '' + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ')
                deleteInput = document.querySelectorAll(".deleteInputField");
                deleteInput.forEach(plus)

        }

        var VacancyArray = ['1','2','3','4'];
        
        for (let i  = 0;  i < VacancyArray.length + 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
          
          addInput.insertAdjacentHTML("beforebegin",
              '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group "><input value="'+ VacancyArray[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' + '' + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ')
              deleteInput = document.querySelectorAll(".deleteInputField");
            
        }

        var drawInput = document.getElementsByClassName("form-group");
        var addInput = document.getElementById("addInput");


        addInput.addEventListener("click", DrawInput)

        

          
    </script>


    <style>
        .addInput {
            height: 50px;
            position: relative;
            transition: all .5s;
        }

        .addInput span {
            display: inline-block;
            width: 30px;
            height: 3px;
            background: #00a65a;
            position: absolute;
            border-radius: 14px;
        }

        .addInput span:first-child {
            transform: rotate(90deg);
            top: 20%;
        }

        .addInput span:last-child {
            top: 20%;
        }

        .deleteInput {
            position: relative;
        }

        .form-control {
            width: 95%;
        }


        .deleteInput span {
            display: inline-block;
            width: 20px;
            height: 3px;
            background: darkred;
            position: absolute;
            top: 30%;
            margin-left: .5rem;
            border-radius: 14px;
            cursor: pointer;
        }
    </style>
</div>