<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\MpkCatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpk-catalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'file')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'file/*'],
    ]);
    ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\app\modules\admin\controllers\LabelStatusCatalog::statusCatalogList(),
        ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
