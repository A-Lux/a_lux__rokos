<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MpkCatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'МПК Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpk-catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'link',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusCatalog::statusCatalogList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusCatalog::statusCatalogLabel(($model->status));
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
