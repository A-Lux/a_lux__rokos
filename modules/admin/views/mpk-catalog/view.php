<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MpkCatalog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'МПК Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mpk-catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php if($files != null) $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else $file =  ['attribute'=>'file','value' => '']?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            $file,
            'link',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusCatalog::statusCatalogList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusCatalog::statusCatalogLabel(($model->status));
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
