<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticsCustomerBase */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Наша клиентская база', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="logistics-customer-base-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'title_kz',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
        ],
    ]) ?>

</div>
