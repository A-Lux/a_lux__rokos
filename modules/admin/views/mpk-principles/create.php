<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MpkPrinciples */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Наши принципы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpk-principles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
