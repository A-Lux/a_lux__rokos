<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LogisticsCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Наши клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistics-customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'title',
            //'title_en',
            //'title_kz',
            [
                'attribute' => 'statusActing',
                'filter' => \app\modules\admin\controllers\LabelActing::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelActing::statusLabel($model->statusActing);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            //'subtitle_en',
            //'subtitle_kz',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
