<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\HallOfFameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hall-of-fame-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'rank') ?>

    <?= $form->field($model, 'rank_en') ?>

    <?= $form->field($model, 'rank_kz') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'name_en') ?>

    <?php // echo $form->field($model, 'name_kz') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'position_en') ?>

    <?php // echo $form->field($model, 'position_kz') ?>

    <?php // echo $form->field($model, 'image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
