<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyRequirements */

$this->title = 'Create Vacancy Requirements';
$this->params['breadcrumbs'][] = ['label' => 'Vacancy Requirements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-requirements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
