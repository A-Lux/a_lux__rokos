<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PartnerFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Партнеры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="partner-feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusFeedback::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusFeedback::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'file',
                'value' => function ($model) {
                    return
                        Html::a($model->file, $model->getfile(),['target'=>'_blank']);
                },
                'format' => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
