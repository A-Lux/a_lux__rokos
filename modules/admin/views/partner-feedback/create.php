<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PartnerFeedback */

$this->title = 'Вручное создание';
$this->params['breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
