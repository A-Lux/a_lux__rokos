<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyResume */

$this->title = 'Update Vacancy Resume: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vacancy Resumes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacancy-resume-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
