<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyResume */

$this->title = 'Create Vacancy Resume';
$this->params['breadcrumbs'][] = ['label' => 'Vacancy Resumes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-resume-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
