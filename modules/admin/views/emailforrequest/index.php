<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\search\EmailforrequestSearch */

$this->title = 'Эл. почта для связи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emailforrequest-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить почту', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'email:email',
            [
                'attribute' => 'type',
                'filter' => \app\modules\admin\controllers\LabelTypeEmail::typeList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelTypeEmail::typeLabel($model->type);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
        ],
    ]); ?>
</div>
