<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyResponsibilities */

$this->title = 'Create Vacancy Responsibilities';
$this->params['breadcrumbs'][] = ['label' => 'Vacancy Responsibilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-responsibilities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
