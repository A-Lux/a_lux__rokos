<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

    <?php foreach ($model->messages as $language => $message) : ?>
        <div class="four wide column">
            <?= $form->field($model->messages[$language], '[' . $language . ']translation')->label($language) ?>
        </div>
    <?php endforeach; ?>

    <hr />

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
