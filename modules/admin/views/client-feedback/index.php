<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClientFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'city_id',
                'filter'=>\app\models\City::getList(),
                'value'=>function($model){
                    return $model->city->name;
                },
            ],
//            'address',
            'name',
            'contact_person',
            'email:email',
            'telephone',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusFeedback::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusFeedback::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}'],
        ],
    ]); ?>
</div>
