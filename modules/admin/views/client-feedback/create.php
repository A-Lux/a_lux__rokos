<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientFeedback */

$this->title = 'Вручное создание';
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
