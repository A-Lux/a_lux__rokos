<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientFeedback */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => ' Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusFeedback::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusFeedback::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'reply',
            [
                'attribute'=>'city_id',
                'filter'=>\app\models\City::getList(),
                'value'=>function($model){
                    return $model->city->name;
                },
            ],
            'address',
            'name',
            'contact_person',
            'email:email',
            'telephone',
            'created_at',
        ],
    ]) ?>

</div>
