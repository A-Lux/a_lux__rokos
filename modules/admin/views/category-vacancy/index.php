<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoryVacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категория вакансии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-vacancy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'name_en',
            'name_kz',
            //'metaName',
            //'metaName_en',
            //'metaName_kz',
            //'metaDesc:ntext',
            //'metaDesc_en:ntext',
            //'metaDesc_kz:ntext',
            //'metaKey:ntext',
            //'metaKey_en:ntext',
            //'metaKey_kz:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
