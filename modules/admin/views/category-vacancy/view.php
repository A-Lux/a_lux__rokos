<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryVacancy */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категория вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-vacancy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'name_en',
            'name_kz',
            'metaName',
            'metaName_en',
            'metaName_kz',
            'metaDesc:ntext',
            'metaDesc_en:ntext',
            'metaDesc_kz:ntext',
            'metaKey:ntext',
            'metaKey_en:ntext',
            'metaKey_kz:ntext',
        ],
    ]) ?>

</div>
