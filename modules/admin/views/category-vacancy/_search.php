<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CategoryVacancySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-vacancy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'name_en') ?>

    <?= $form->field($model, 'name_kz') ?>

    <?= $form->field($model, 'metaName') ?>

    <?php // echo $form->field($model, 'metaName_en') ?>

    <?php // echo $form->field($model, 'metaName_kz') ?>

    <?php // echo $form->field($model, 'metaDesc') ?>

    <?php // echo $form->field($model, 'metaDesc_en') ?>

    <?php // echo $form->field($model, 'metaDesc_kz') ?>

    <?php // echo $form->field($model, 'metaKey') ?>

    <?php // echo $form->field($model, 'metaKey_en') ?>

    <?php // echo $form->field($model, 'metaKey_kz') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
