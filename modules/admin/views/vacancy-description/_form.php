<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\VacancyDescription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-description-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vacancy_id')->textInput() ?>

    <?= $form->field($model, 'description')->widget(MultipleInput::className(), [
            'max'               => 10,
            'enableGuessTitle'  => true,
            'addButtonPosition' => MultipleInput::POS_FOOTER,
    ])->label(false)  ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
