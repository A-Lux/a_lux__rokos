<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PurchaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customer') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'procurement_method') ?>

    <?= $form->field($model, 'procurement_type') ?>

    <?php // echo $form->field($model, 'purchase stage') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'unit') ?>

    <?php // echo $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'delivery_time') ?>

    <?php // echo $form->field($model, 'delivery_condition') ?>

    <?php // echo $form->field($model, 'payment_condition') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'docs') ?>

    <?php // echo $form->field($model, 'tech_task') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
