<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Purchases */

$this->title = 'Создать закупку';
$this->params['breadcrumbs'][] = ['label' => 'Закупки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fakeAdmin' => $fakeAdmin,
        'commission' => $commission,
        'stageFirst' => $stageFirst,
        'stageSecond' => $stageSecond,
    ]) ?>

</div>
