<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Purchases;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Закупки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchases-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать закупку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'customer',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->customerName;
                },
                'filter' => \app\models\PurchasingGuide::getCustomersDropDown()
            ],
            'name',
            [
                'attribute' => 'procurement_method',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementMethodName;
                },
                'filter' => \app\models\PurchasingGuide::getProcurementMethodsDropDown()
            ],

            [
                'attribute' => 'procurement_type',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementTypeName;
                },
                'filter' => \app\models\PurchasingGuide::getProcurementTypesDropDown()
            ],
            [
                'attribute' => 'purchase_stage',
                'format' => 'raw',
                'value' => function ($data) {
                    return ArrayHelper::getValue(Purchases::purchaseStageDescription(),
                        $data->purchase_stage);
                },
                'filter' => Purchases::purchaseStageDescription()
            ],
            [
                'attribute' => 'is_active',
                'filter' => Purchases::activeDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Purchases::activeDescription(), $model->is_active);
                },
                'format' => 'raw',
            ],
            //'purchase stage',
            //'currency',
            //'unit',
            //'quantity',
            //'delivery_time',
            //'delivery_condition',
            //'payment_condition',
            //'description:ntext',
            //'docs',
            //'tech_task',
            //'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
