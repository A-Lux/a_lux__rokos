<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Purchases;

/* @var $this yii\web\View */
/* @var $model app\models\Purchases */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Закупки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="purchases-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'customer',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->customerName;
                },
                'filter' => \app\models\PurchasingGuide::getCustomersDropDown()
            ],
            'name',
            [
                'attribute' => 'procurement_method',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementMethodName;
                },
                'filter' => \app\models\PurchasingGuide::getProcurementMethodsDropDown()
            ],
            [
                'attribute' => 'procurement_type',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementTypeName;
                },
                'filter' => \app\models\PurchasingGuide::getProcurementTypesDropDown()
            ],
            [
                'attribute' => 'purchase_stage',
                'format' => 'raw',
                'value' => function ($data) {
                    return ArrayHelper::getValue(Purchases::purchaseStageDescription(),
                        $data->purchase_stage);
                },
                'filter' => Purchases::purchaseStageDescription()
            ],
            [
                'attribute' => 'protocol',
                'value' => function ($model) {
                    return '<a href="/uploads/files/purchases/'.json_decode($model->protocol)[0]->file.'">'.json_decode($model->protocol)[0]->name.'</a>';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'protocol2',
                'value' => function ($model) {
                    return '<a href="/uploads/files/purchases/'.json_decode($model->protocol2)[0]->file.'">'.json_decode($model->protocol2)[0]->name.'</a>';
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'currency',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->currencyName;
                },
                'filter' => \app\models\PurchasingGuide::getCurrencyDropDown()
            ],
            [
                'attribute' => 'unit',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->unitName;
                },
                'filter' => \app\models\PurchasingGuide::getUnitDropDown()
            ],
            'quantity',
            'delivery_time',
            'delivery_condition',
            'payment_condition',
            'description:ntext',
            [
                'attribute' => 'tech_task',
                'value' => function ($model) {
                    return '<a href="/uploads/files/purchases/'.json_decode($model->tech_task)[0]->file.'">'.json_decode($model->tech_task)[0]->name.'</a>';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'docs',
                'value' => function ($model) {
                    return '<a href="/uploads/files/purchases/'.json_decode($model->docs)[0]->file.'">'.json_decode($model->docs)[0]->name.'</a>';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Purchases::activeDescription(), $model->is_active);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
