<?php

use app\models\PurchasingGuide;
use app\models\User;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Purchases;

/* @var $this yii\web\View */
/* @var $model app\models\Purchases */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="purchases-form">

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#main_tab">Основное</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#form_tab">Формы этапов</a>
        </li>
    </ul>


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="tab-content">
        <div class="tab-pane fade in  active" id="main_tab" role="tabpanel">

            <?= $form->field($model, 'customer')->widget(Select2::classname(), [
                'data' => PurchasingGuide::getCustomersDropDown(),
                'options' => ['placeholder' => 'Выберите заказчика ...'],
            ]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'procurement_method')->widget(Select2::classname(), [
                'data' => PurchasingGuide::getProcurementMethodsDropDown(),
                'options' => ['placeholder' => 'Выберите метод закупки ...'],
            ]) ?>

            <?= $form->field($model, 'procurement_type')->widget(Select2::classname(), [
                'data' => PurchasingGuide::getProcurementTypesDropDown(),
                'options' => ['placeholder' => 'Выберите вид закупки ...'],
            ]) ?>

            <?= $form->field($model, 'purchase_stage')->dropDownList(
                    Purchases::purchaseStageDescription()
            ); ?>

            <?= $form->field($model, 'currency')->widget(Select2::classname(), [
                'data' => PurchasingGuide::getCurrencyDropDown(),
                'options' => ['placeholder' => 'Выберите валюту ...'],
            ]) ?>

            <?= $form->field($model, 'unit')->widget(Select2::classname(), [
                'data' => PurchasingGuide::getUnitDropDown(),
                'options' => ['placeholder' => 'Выберите единицу измерения ...'],
            ]) ?>

            <?= $form->field($model, 'quantity')->textInput() ?>

            <?= $form->field($model, 'delivery_time')->widget(DatePicker::className()) ?>

            <?= $form->field($model, 'delivery_condition')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'payment_condition')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea([
                    'maxlength' => 1000,
                    'rows'      => 10,
                    'cols'      => 50,

                ]) ?>

            <?= $form->field($model, 'docs')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload'            => false ,
//                        'initialCaption'        => $model->isNewRecord ? '': $model->file,
                ] ,'options' => ['multiple' => 'true', 'accept' => '*'],
            ]); ?>


            <?= $form->field($model, 'tech_task')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload'            => false ,
//                        'initialCaption'        => $model->isNewRecord ? '': $model->file,
                ] ,'options' => ['multiple' => 'true', 'accept' => '*'],
            ]); ?>

            <?= $form->field($model, 'protocol')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload'            => false ,
//                        'initialCaption'        => $model->isNewRecord ? '': $model->file,
                ] ,'options' => ['multiple' => 'true', 'accept' => '*'],
            ]); ?>
            <?= $form->field($model, 'protocol2')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showUpload'            => false ,
//                        'initialCaption'        => $model->isNewRecord ? '': $model->file,
                ] ,'options' => ['multiple' => 'true', 'accept' => '*'],
            ]); ?>




            <!--    --><? //= $form->field($model, 'admin')->dropDownList(User::getFakeAdminsList(), ['prompt' => 'Выберите']) ?>
            <?= $form->field($model, 'admin')->widget(Select2::classname(), [
                'data' => User::getFakeAdminsList(),
                'options' => ['placeholder' => 'Выберите администраторов ...', 'multiple' => false],
                'pluginOptions' => ['tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10],]) ?>

            <?= $form->field($model, 'commission')->widget(Select2::classname(), ['data' => User::getCommissionsList(),
                'options' => ['placeholder' => 'Выберите членов комиссии ...', 'multiple' => false],
                'pluginOptions' => [//            'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10],]) ?>


            <?= $form->field($model, 'is_active')->dropDownList(['0' => 'Нет', '1' => 'Да']) ?>


        </div>

        <div class="tab-pane" id="form_tab" role="tabpanel">
            <h3>Этап 1</h3>
            <?= $form->field($model, 'stage_first')
                ->dropDownList(Purchases::stageFirstDescription()) ?>

            <?= $form->field($stageFirst, 'start_date')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Start Date'],
                'pluginOptions' => [
                    'autoclose' =>true,
                    'format'    => 'yyyy-mm-dd'
                ],
            ]) ?>
            <?= $form->field($stageFirst, 'end_date')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'end_date'],
                'pluginOptions' => [
                    'autoclose' =>true,
                    'format'    => 'yyyy-mm-dd'
                ],
            ]) ?>
            <?= $form->field($stageFirst, 'files')->widget(MultipleInput::className(), [
                'min' => 1,
                'cloneButton' => true,
                'columns' => [
                    [
                        'name' => 'type',
                        'type' => 'dropDownList',
                        'title' => 'Тип поля',
                        'defaultValue' => 0,
                        'items' => \app\models\StageFirstDocs::getTypesList(),
                    ],
                    [
                        'name' => 'title',
                        'title' => 'Заголовок',
                    ],
                    [
                        'name' => 'is_required',
                        'title' => 'Обязательно?',
                        'type' => 'checkbox',
                        'options' => [
                            'class' => 'form-check-input'
                        ]
                    ]
                ]
            ])->label(false);
            ?>


            <h3>Этап 2</h3>
            <?= $form->field($model, 'stage_second')
                ->dropDownList(Purchases::stageSecondDescription()) ?>

            <?= $form->field($stageSecond, 'start_date')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Start Date'],
                'pluginOptions' => [
                    'autoclose' =>true,
                    'format'    => 'yyyy-mm-dd'
                ],
            ]) ?>
            <?= $form->field($stageSecond, 'end_date')->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Start Date'],
                'pluginOptions' => [
                    'autoclose' =>true,
                    'format'    => 'yyyy-mm-dd'
                ],
            ]) ?>
            <?= $form->field($stageSecond, 'files')->widget(MultipleInput::className(), [
                'min' => 1,
                'cloneButton' => true,
                'columns' => [
                    [
                        'name' => 'type',
                        'type' => 'dropDownList',
                        'title' => 'Тип поля',
                        'defaultValue' => 0,
                        'items' => \app\models\StageFirstDocs::getTypesList(),
                    ],
                    [
                        'name' => 'title',
                        'title' => 'Заголовок',
                    ],
                    [
                        'name' => 'is_required',
                        'title' => 'Обязательно?',
                        'type' => 'checkbox',
                        'options' => [
                            'class' => 'form-check-input'
                        ]
                    ]
                ]
            ])->label(false);
            ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
