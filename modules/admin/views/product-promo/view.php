<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductPromo */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продвигаемые продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-promo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'name_en',
            'name_kz',
            'category',
            'category_en',
            'category_kz',
            'image',
            [
                'format'    => 'html',
                'attribute' => 'image',
                'value'     => function($data){
                    return Html::img($data->getImage(), ['width' => 200]);
                }
            ],
        ],
    ]) ?>

</div>
