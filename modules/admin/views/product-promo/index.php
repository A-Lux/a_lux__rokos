<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductPromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продвигаемые продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-promo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'name_en',
            //'name_kz',
            'category',
            //'category_en',
            //'category_kz',
            //'image',
            [
                'format'    => 'html',
                'attribute' => 'image',
                'value'     => function($data){
                    return Html::img($data->getImage(), ['width' => 100]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
