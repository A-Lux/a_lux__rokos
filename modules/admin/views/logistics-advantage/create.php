<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticsAdvantage */

$this->title = 'Создание преимущества логистики';
$this->params['breadcrumbs'][] = ['label' => 'Преимущества Логистики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistics-advantage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
