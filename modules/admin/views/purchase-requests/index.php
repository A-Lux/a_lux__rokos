<?php

use app\models\PurchasingGuide;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\PurchaseRequests;
use app\models\Purchases;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PurchaseRequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на участие';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-requests-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Purchasing Guide', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'attribute' => 'purchase_id',
                'filter'    => Purchases::getListName(),
                'value' => function ($model) {
                    return
                        Html::a($model->purchase->name, ['/admin/purchase-v2/view', 'id' => $model->purchase->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'purchase_customer',
                'filter'    => PurchasingGuide::getCustomersDropDown(),
                'value' => function ($model) {
                    return
                        Html::a($model->purchase->customerName,
                            ['/admin/purchase-v2/view', 'id' => $model->purchase->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'procurement_method',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementMethodName;
                },
                'filter' => PurchasingGuide::getProcurementMethodsDropDown()
            ],
            [
                'attribute' => 'purchase_stage',
                'format' => 'raw',
                'value' => function ($data) {
                    return ArrayHelper::getValue(Purchases::purchaseStageDescription(),
                        $data->purchase_stage);
                },
                'filter' => Purchases::purchaseStageDescription()
            ],
            [
                'attribute' => 'user_id',
                'filter'    => \app\models\User::getListCompanyName(),
                'value' => function ($model) {
                    return
                        Html::a($model->user->company_name, ['/admin/user/view', 'id' => $model->user->id]);
                },
                'format' => 'raw',
            ],
//            'name_product',
//            'price',
            [
                'attribute' => 'status_approved',
                'filter' => PurchaseRequests::approvedDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(PurchaseRequests::approvedDescription(), $model->status_approved);
                },
                'format' => 'raw',
            ],
            'created_at',
//            'payment_condition',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
