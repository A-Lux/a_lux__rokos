<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PurchaseRequests;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-requests-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_approved')->dropDownList(PurchaseRequests::approvedDescription()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
