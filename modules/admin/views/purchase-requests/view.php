<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\PurchaseRequests;
use yii\helpers\ArrayHelper;
use app\models\PurchasingGuide;
use app\models\Purchases;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseRequests */
/* @var $requestFiles app\models\PurchaseRequestFiles */

$this->title = $model->name_product;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на участие', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="purchase-requests-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return
                        Html::a($model->user->company_name,
                            ['/admin/user/view', 'id' => $model->user->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'purchase_customer',
                'value' => function ($model) {
                    return
                        Html::a($model->purchase->customerName,
                            ['/admin/purchase-v2/view', 'id' => $model->purchase->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'purchase_id',
                'value' => function ($model) {
                    return
                        Html::a($model->purchase->name, ['/admin/purchase-v2/view', 'id' => $model->purchase->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'procurement_method',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->procurementMethodName;
                },
            ],
            [
                'attribute' => 'purchase_stage',
                'format' => 'raw',
                'value' => function ($data) {
                    return ArrayHelper::getValue(Purchases::purchaseStageDescription(),
                        $data->purchase_stage);
                },
            ],
            'name_product',
            'price',
            'vat',
            [
                'attribute' => 'status_approved',
                'value' => function ($model) {
                    return ArrayHelper::getValue(PurchaseRequests::approvedDescription(), $model->status_approved);
                },
                'format' => 'raw',
            ],
            'unit',
            'quantity',
            'currency',
            'delivery_condition',
            'payment_condition',
            'description',
            'delivery_time',
            'created_at',

        ],
    ]) ?>

</div>

<div class="box" id = 'result'>
    <div class="box-header">
        <h3 class="box-title">Файлы заявки:</h3>
    </div>
    <div class="box-body">
        <? if(!empty($files)): ?>
            <? foreach ($files as $file): ?>
                <a href="<?= '/uploads/files/request-files/' . $file['file'] ?>" target="_blank">
                    <?= $file['name']; ?>
                </a>
            <br><br>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</div>
