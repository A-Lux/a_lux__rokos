<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PurchaseRequestsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-requests-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name_product') ?>

    <?= $form->field($model, 'user_id') ?>
    <?= $form->field($model, 'purchase_id') ?>
    <?= $form->field($model, 'vat') ?>
    <?= $form->field($model, 'unit') ?>
    <?= $form->field($model, 'quantity') ?>
    <?= $form->field($model, 'currency') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'delivery_days') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
