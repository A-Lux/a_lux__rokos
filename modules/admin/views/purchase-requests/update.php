<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseRequests */

$this->title = 'Обновить заявку: ' . $model->name_product;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на участие', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_product, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="purchase-requests-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
