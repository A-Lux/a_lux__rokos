<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Вакансии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'city_id',
                'value' => function ($model) {
                    return
                        Html::a($model->city->name);
                },
                'format' => 'raw',
            ],
            'name',
//            'content:ntext',
//            'price',
//            'name_en',
            //'content_en:ntext',
            //'name_kz',
            //'content_kz:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
