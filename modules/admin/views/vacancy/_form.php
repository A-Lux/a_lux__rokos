<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-form" style="padding-bottom: 1500px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <li class="nav-item">
                <a id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru" aria-selected="false" class="nav-link">RU</a>
            </li>
            <li class="nav-item">
                <a id="add-tab" data-toggle="tab" href="#add" role="tab" aria-controls="add" aria-selected="false" class="nav-link">Дополнительное</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">EN</a>
            </li>
            <li class="nav-item">
                <a id="add-en-tab" data-toggle="tab" href="#add-en" role="tab" aria-controls="add-en" aria-selected="false" class="nav-link">Дополнительное EN</a>
            </li>
            <li class="nav-item">
                <a id="kz-tab" data-toggle="tab" href="#kz" role="tab" aria-controls="kz" aria-selected="false" class="nav-link">KZ</a>
            </li>
            <li class="nav-item">
                <a id="add-kz-tab" data-toggle="tab" href="#add-kz" role="tab" aria-controls="add-kz" aria-selected="false" class="nav-link">Дополнительное KZ</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">

            <div id="ru" role="tabpanel" aria-labelledby="ru-tab" class="tab-pane fade inputTarget ">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'schedule')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'by_phone')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <div>
                    <h2>Описание содержании</h2>
                    <div id="addInputDescriptions" class="addInput" data-input-name="description_ru">
                        <span></span>
                        <span></span>
                    </div>
                </div>

            </div>

            <div id="add" role="tabpanel" aria-labelledby="add-tab" class="tab-pane fade inputTarget ">

                <div>
                    <h2>Обязанности:</h2>
                    <div id="addInputResponses" class="addInput" data-input-name="responses_ru">
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div>
                    <h2>Требования:</h2>
                    <div id="addInputRequirements" class="addInput" data-input-name="requirements_ru">
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div>
                    <h2>Условия:</h2>
                    <div id="addInputConditions" class="addInput" data-input-name="conditions_ru">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade inputTarget">

                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'schedule_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'by_phone_en')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_en')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>
                <div>
                        <h2>Описание содержании</h2>
                        <div id="addInputDescriptions" class="addInput" data-input-name="description_en">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
            </div>
            <div id="add-en" role="tabpanel" aria-labelledby="add-tab" class="tab-pane fade inputTarget ">

                    <div>
                        <h2>Обязанности:</h2>
                        <div id="addInputResponses" class="addInput" data-input-name="responses_en">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
    
                    <div>
                        <h2>Требования:</h2>
                        <div id="addInputRequirements" class="addInput" data-input-name="requirements_en">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
    
                    <div>
                        <h2>Условия:</h2>
                        <div id="addInputConditions" class="addInput" data-input-name="conditions_en">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            <div id="kz" role="tabpanel" aria-labelledby="kz-tab" class="tab-pane fade inputTarget">


                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'schedule_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'by_phone_kz')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>
                <div>
                        <h2>Описание содержании</h2>
                        <div id="addInputDescriptions" class="addInput" data-input-name="description_kz">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
            </div>
            <div id="add-kz" role="tabpanel" aria-labelledby="add-tab" class="tab-pane fade inputTarget ">

                    <div>
                        <h2>Обязанности:</h2>
                        <div id="addInputResponses" class="addInput" data-input-name="responses_kz">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
    
                    <div>
                        <h2>Требования:</h2>
                        <div id="addInputRequirements" class="addInput" data-input-name="requirements_kz">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
    
                    <div>
                        <h2>Условия:</h2>
                        <div id="addInputConditions" class="addInput" data-input-name="conditions_kz">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'category_id')->dropDownList(\app\models\CategoryVacancy::getList(),
                    ['prompt' => '']) ?>

                <?= $form->field($model, 'city_id')->dropDownList(\app\models\City::getList(),
                    ['prompt' => '']) ?>

                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            </div>

            <div id="add-en" role="tabpanel" aria-labelledby="add-en-tab" class="tab-pane fade inputTarget ">
                
            </div>

            <div id="add-kz" role="tabpanel" aria-labelledby="add-kz-tab" class="tab-pane fade inputTarget ">
                
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <script>
        var drawInput = document.getElementsByClassName("form-group");
        var addInput = document.querySelectorAll("#addInputResponses");
        var addInputRequirements = document.querySelectorAll("#addInputRequirements");
        var addInputConditions = document.querySelectorAll("#addInputConditions");
        var addInputDescriptions = document.querySelectorAll("#addInputDescriptions");

        <?php if($model->isNewRecord) { ?>
            var VacancyArrayRU          = [];
            var VacancyRequirementsRU   = [];
            var VacancyConditionsRU     = [];
            var VacancyDescriptionsRU   = [];

            var VacancyArrayEN          = [];
            var VacancyRequirementsEN   = [];
            var VacancyConditionsEN     = [];
            var VacancyDescriptionsEN   = [];

            var VacancyArrayKZ          = [];
            var VacancyRequirementsKZ   = [];
            var VacancyConditionsKZ     = [];
            var VacancyDescriptionsKZ   = [];

        <?php } else { ?>
            <?php
            $responses = $model->responses;
            $responses = array_map(function($v) { return $v->responsibility; }, $responses);
            $responses = json_encode($responses);

            $responses_en = $model->responses;
            $responses_en = array_map(function($v) { return $v->responsibility_en; }, $responses_en);
            $responses_en = json_encode($responses_en);

            $responses_kz = $model->responses;
            $responses_kz = array_map(function($v) { return $v->responsibility_kz; }, $responses_kz);
            $responses_kz = json_encode($responses_kz);

            $requirements = $model->requirements;
            $requirements = array_map(function($v) { return $v->requirement; }, $requirements);
            $requirements = json_encode($requirements);

            $requirements_en = $model->requirements;
            $requirements_en = array_map(function($v) { return $v->requirement_en; }, $requirements_en);
            $requirements_en = json_encode($requirements_en);

            $requirements_kz = $model->requirements;
            $requirements_kz = array_map(function($v) { return $v->requirement_kz; }, $requirements_kz);
            $requirements_kz = json_encode($requirements_kz);

            $conditions = $model->conditions;
            $conditions = array_map(function($v) { return $v->condition; }, $conditions);
            $conditions = json_encode($conditions);

            $conditions_en = $model->conditions;
            $conditions_en = array_map(function($v) { return $v->condition_en; }, $conditions_en);
            $conditions_en = json_encode($conditions_en);

            $conditions_kz = $model->conditions;
            $conditions_kz = array_map(function($v) { return $v->condition_kz; }, $conditions_kz);
            $conditions_kz = json_encode($conditions_kz);

            $description = $model->description;
            $description = array_map(function($v) { return $v->description; }, $description);
            $description = json_encode($description);

            $description_en = $model->description;
            $description_en = array_map(function($v) { return $v->description_en; }, $description_en);
            $description_en = json_encode($description_en);

            $description_kz = $model->description;
            $description_kz = array_map(function($v) { return $v->description_kz; }, $description_kz);
            $description_kz = json_encode($description_kz);

            ?>
            var VacancyArrayRU          = <?=$responses?>;
            var VacancyRequirementsRU   = <?=$requirements?>;
            var VacancyConditionsRU     = <?=$conditions?>;
            var VacancyDescriptionsRU   = <?=$description?>;

            var VacancyArrayEN          = <?=$responses_en?>;
            var VacancyRequirementsEN   = <?=$requirements_en?>;
            var VacancyConditionsEN     = <?=$conditions_en?>;
            var VacancyDescriptionsEN   = <?=$description_en?>;

            var VacancyArrayKZ          = <?=$responses_kz?>;
            var VacancyRequirementsKZ   = <?=$requirements_kz?>;
            var VacancyConditionsKZ     = <?=$conditions_kz?>;
            var VacancyDescriptionsKZ   = <?=$description_kz?>;

        <?php } ?>

        var removeDiv = function(e) {
            console.log(document.querySelector('div#input'+e.target.getAttribute('input_id')+''));
            // element.parentElement.parentElement.innerHTML = "";
            document.querySelector('div#input'+e.target.getAttribute('input_id')+'').remove()
        };
        var plus = function (element) {
            console.log(element);
            element.addEventListener("click", removeDiv);
        };

        

         /////RU Input 
        
        for (let i  = 0;  i <= VacancyArrayRU.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            
            addInput[0].insertAdjacentHTML("beforebegin",
              '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyArrayRU[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'responses_ru[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

              deleteInput = document.querySelectorAll(".deleteInputField");
                   deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyRequirementsRU.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputRequirements[0].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyRequirementsRU[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'requirements_ru[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyConditionsRU.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputConditions[0].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyConditionsRU[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'conditions_ru[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyDescriptionsRU.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputDescriptions[0].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyDescriptionsRU[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'description_ru[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }

        ///RU input End


        /////EN Input 
        
        for (let i  = 0;  i <= VacancyArrayEN.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            
            addInput[1].insertAdjacentHTML("beforebegin",
              '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyArrayEN[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'responses_en[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

              deleteInput = document.querySelectorAll(".deleteInputField");
                   deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyRequirementsEN.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputRequirements[1].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyRequirementsEN[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'requirements_en[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyConditionsEN.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputConditions[1].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyConditionsEN[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'conditions_en[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyDescriptionsEN.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputDescriptions[1].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyDescriptionsEN[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'description_en[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }

        ///EN input End

          /////KZ Input 
        
          for (let i  = 0;  i <= VacancyArrayKZ.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            var langVersion;
            

          if (this.getAttribute("data-input-name") === "description_ru") {
            langVersion = 'description_ru[]'
          } else if (this.getAttribute("data-input-name") === "description_en"){
            langVersion = 'description_en[]'
          }
            else if(this.getAttribute("data-input-name") === "description_kz") {
                langVersion = 'description_kz[]'
            }
            
            addInput[2].insertAdjacentHTML("beforebegin",
              '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyArrayKZ[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'responses_kz[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

              deleteInput = document.querySelectorAll(".deleteInputField");
                   deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyRequirementsKZ.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            var langVersion;
            

          if (this.getAttribute("data-input-name") === "description_ru") {
            langVersion = 'description_ru[]'
          } else if (this.getAttribute("data-input-name") === "description_en"){
            langVersion = 'description_en[]'
          }
            else if(this.getAttribute("data-input-name") === "description_kz") {
                langVersion = 'description_kz[]'
            }

            addInputRequirements[2].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyRequirementsKZ[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'requirements_kz[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyConditionsKZ.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            

            addInputConditions[2].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyConditionsKZ[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'conditions_kz[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }
        for (let i  = 0;  i <= VacancyDescriptionsKZ.length - 1 ; i++) {
            var deleteInput = document.querySelectorAll(".deleteInputField");

            addInputDescriptions[2].insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input value="'+ VacancyDescriptionsKZ[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" ' +
                'name="' + 'description_kz[]' + '" aria-required="true"> <div class="deleteInput"> ' +
                '<span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');

            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        }

        ///KZ input End


        // for (let i  = 0;  i <= VacancyArrayEN.length - 1  ; i++) {
        //     var deleteInput = document.querySelectorAll(".deleteInputField");
        //
        //     addInput[1].insertAdjacentHTML("beforebegin",
        //       '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
        //         '<input value="'+ VacancyArrayEN[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" ' +
        //         'class="form-control" name="' + 'responses_ru[]' + '" aria-required="true"> ' +
        //         '<div class="deleteInput"> <span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '">' +
        //         '</span> </div></div> ');
        //       deleteInput = document.querySelectorAll(".deleteInputField");
        //            deleteInput.forEach(plus)
        // }
        // for (let i  = 0;  i < VacancyArrayKZ.length  ; i++) {
        //     var deleteInput = document.querySelectorAll(".deleteInputField");
        //
        //     addInput[2].insertAdjacentHTML("beforebegin",
        //       '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
        //         '<input value="'+ VacancyArrayKZ[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" ' +
        //         'class="form-control" name="' + 'responses_ru[]' + '" aria-required="true"> ' +
        //         '<div class="deleteInput"> <span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '">' +
        //         '</span> </div></div> ');
        //
        //       deleteInput = document.querySelectorAll(".deleteInputField");
        //            deleteInput.forEach(plus)
        //   addInput.insertAdjacentHTML("beforebegin",
        //       '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group "><input value="'+ VacancyArrayRU[i] +'" style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' + '' + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ')
        //       deleteInput = document.querySelectorAll(".deleteInputField");
        //
        //}

       
        var DrawInput = function () {

               var deleteInput = document.querySelectorAll(".deleteInputField");
               var langVersion;

               console.log(this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name"))
               if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "responses_ru") {
              langVersion = 'responses_ru[]'
            } else if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "responses_en"){
              langVersion = 'responses_en[]'
            }
              else if(this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "responses_kz") {
                  langVersion = 'responses_kz[]'
              }
               this.insertAdjacentHTML("beforebegin",
                   '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                   '<input style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' +
                   langVersion + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" ' +
                   'input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');
                   deleteInput = document.querySelectorAll(".deleteInputField");
                   deleteInput.forEach(plus)
           };

        var DrawInputRequirements = function () {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            var langVersion;
            
            console.log(langVersion)

            if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "requirements_ru") {
              langVersion = 'requirements_ru[]'
            } else if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "requirements_en"){
              langVersion = 'requirements_en[]'
            }
              else if(this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "requirements_kz") {
                  langVersion = 'requirements_kz[]'
              }
            

            this.insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' +
                langVersion + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" ' +
                'input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');
            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        };

        var DrawInputConditions = function (e) {

            var deleteInput = document.querySelectorAll(".deleteInputField");
            var langVersion;
            
            console.log(langVersion)
            if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "conditions_ru") {
              langVersion = 'conditions_ru[]'
            } else if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "conditions_en"){
              langVersion = 'conditions_en[]'
            }
              else if(this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "conditions_kz") {
                  langVersion = 'conditions_kz[]'
              }

            this.insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' +
                langVersion + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" ' +
                'input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');
            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        };

        var DrawInputDescriptions = function () {
            var deleteInput = document.querySelectorAll(".deleteInputField");
            var langVersion;
            
            if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "description_ru") {
            langVersion = 'description_ru[]'
          } else if (this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "description_en"){
            langVersion = 'description_en[]'
          }
            else if(this.parentNode.getElementsByClassName("addInput")[0].getAttribute("data-input-name") === "description_kz") {
                langVersion = 'description_kz[]'
            }
            this.insertAdjacentHTML("beforebegin",
                '<div id="input'+ (deleteInput.length+1) +'" style="display:flex" class="form-group ">' +
                '<input style="margin-bottom: 10px;" type="text" id="' + '' + '" class="form-control" name="' +
                langVersion + '" aria-required="true"> <div class="deleteInput"> <span class="deleteInputField" ' +
                'input_id="'+ (deleteInput.length+1) + '"></span> </div></div> ');
            deleteInput = document.querySelectorAll(".deleteInputField");
            deleteInput.forEach(plus)
        };

        addInput.forEach(function(element) {
            element.addEventListener("click", DrawInput)   
        });

        addInputRequirements.forEach(function(element) {
            element.addEventListener("click", DrawInputRequirements)
        });

        addInputConditions.forEach(function(element) {
            element.addEventListener("click", DrawInputConditions)
        });

        addInputDescriptions.forEach(function(element) {
            element.addEventListener("click", DrawInputDescriptions)
        });


    </script>


    <style>
        .addInput {
            height: 50px;
            position: relative;
            transition: all .5s;
        }

        .addInput span {
            display: inline-block;
            width: 30px;
            height: 3px;
            background: #00a65a;
            position: absolute;
            border-radius: 14px;
        }

        .addInput span:first-child {
            transform: rotate(90deg);
            top: 20%;
        }

        .addInput span:last-child {
            top: 20%;
        }

        .deleteInput {
            position: relative;
        }

        .form-control {
            width: 95%;
        }


        .deleteInput span {
            display: inline-block;
            width: 20px;
            height: 3px;
            background: darkred;
            position: absolute;
            top: 30%;
            margin-left: .5rem;
            border-radius: 14px;
            cursor: pointer;
        }
    </style>
</div>
