<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticsHeader */

$this->title = 'Создать Logistics Header';
$this->params['breadcrumbs'][] = ['label' => 'Logistics Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistics-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
