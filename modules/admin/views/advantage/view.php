<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Advantage */

$this->title = $model->subtitle;
$this->params['breadcrumbs'][] = ['label' => 'Advantages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="advantage-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'subtitle',
            [
                'format' => 'html',
                'attribute' => 'image',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage(""), ['width'=>100]);
                }
            ],
            'title_en',
            'subtitle_en',
            [
                'format' => 'html',
                'attribute' => 'image_en',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage("_en"), ['width'=>100]);
                }
            ],
            'title_kz',
            'subtitle_kz',
            [
                'format' => 'html',
                'attribute' => 'image_kz',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage("_kz"), ['width'=>100]);
                }
            ],
        ],
    ]) ?>

</div>
