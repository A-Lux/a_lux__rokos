<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address:ntext',
            'telephone',
            'email:email',
            'name_en',
            'address_en:ntext',
            'name_kz',
            'address_kz:ntext',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'statusCity',
                'filter' => \app\modules\admin\controllers\LabelStatusCity::statusCityList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusCity::statusCityLabel(($model->statusCity));
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
