<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'address:ntext',
            'telephone',
            'email:email',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'statusCity',
                'filter' => \app\modules\admin\controllers\LabelStatusCity::statusCityList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusCity::statusCityLabel(($model->statusCity));
                },
                'format' => 'raw',
            ],
            //'name_en',
            //'address_en:ntext',
            //'name_kz',
            //'address_kz:ntext',
            //'status',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}' ],
        ],
    ]); ?>
</div>
