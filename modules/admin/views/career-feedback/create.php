<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CareerFeedback */

$this->title = 'Вручное создание';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="career-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
