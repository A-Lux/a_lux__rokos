<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CareerFeedback */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на вакансии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="career-feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusFeedback::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusFeedback::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'reply',
            'contact_person',
            [
                'attribute'=>'city_id',
                'filter'=>\app\models\City::getList(),
                'value'=>function($model){
                    return $model->city->name;
                },
            ],

            [
                'attribute'=>'position_id',
                'filter'=>\app\models\Position::getList(),
                'value'=>function($model){
                    return $model->position->name;
                },
            ],
            'email:email',
            'telephone',
            [
                'attribute' => 'file',
                'value' => function ($model) {
                    return
                        Html::a($model->file, $model->getfile(),['target'=>'_blank']);
                },
                'format' => 'raw',
            ],
            'comment',
            'href',
            'created_at',
        ],
    ]) ?>

</div>
