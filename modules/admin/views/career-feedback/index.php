<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CareerFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на вакансии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="career-feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'contact_person',
            'email:email',
            [
                'attribute'=>'city_id',
                'filter'=>\app\models\City::getList(),
                'value'=>function($model){
                    return $model->city->name;
                },
            ],
            'telephone',
            [
                'attribute'=>'position_id',
                'filter'=>\app\models\Vacancy::getList(),
                'value'=>function($model){
                    return $model->position->name;
                },
            ],
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelStatusFeedback::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelStatusFeedback::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            //'telephone',
            //'file',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}'],
        ],
    ]); ?>
</div>
