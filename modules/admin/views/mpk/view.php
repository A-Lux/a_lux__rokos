<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mpk */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'МПК', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mpk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'title_kz',
            'description:ntext',
            'description_en:ntext',
            'description_kz:ntext',
            'mission_title:ntext',
            'mission_title_en:ntext',
            'mission_title_kz:ntext',
            'mission_content:ntext',
            'mission_content_en:ntext',
            'mission_content_kz:ntext',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
        ],
    ]) ?>

</div>
