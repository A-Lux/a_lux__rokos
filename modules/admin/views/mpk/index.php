<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MpkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'МПК';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Mpk', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'title_en',
            //'title_kz',
            'description:ntext',
            //'description_en:ntext',
            //'description_kz:ntext',
            //'mission_title:ntext',
            //'mission_title_en:ntext',
            //'mission_title_kz:ntext',
            //'mission_content:ntext',
            //'mission_content_en:ntext',
            //'mission_content_kz:ntext',
            //'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
