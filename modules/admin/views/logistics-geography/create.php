<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticsGeography */

$this->title = 'Создать Logistics Geography';
$this->params['breadcrumbs'][] = ['label' => 'Logistics Geographies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistics-geography-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
