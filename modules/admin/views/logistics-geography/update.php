<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticsGeography */

$this->title = 'Редактировать Logistics Geography: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logistics Geographies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="logistics-geography-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
