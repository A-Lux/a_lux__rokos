<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logo */

$this->title = 'Создать логотип';
$this->params['breadcrumbs'][] = ['label' => 'Логотип и Копирайт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
