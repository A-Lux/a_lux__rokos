<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fullname_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'manufacturer_en')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_en')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fullname_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'manufacturer_kz')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'price')->textInput() ?>

                <div class="form-group field-products-catalog_id required">
                    <label class="control-label" for="products-catalog_id">Категория</label>

                    <select name="Products[catalog_id]" class="form-control">
                        <option></option>
                        <?
                        $catalog = \app\models\Catalog::find()
                            ->where("level = 1")->all();
                        $array = [];
                        foreach ($catalog as $v){
                            echo '<option value="'.$v->id.'" disabled="disabled">'.$v->name.'</option>';
                            $array[$v->id] = $v->name;
                            if(isset($v->childs)){
                                foreach($v->childs as $child1) {
                                    if(!empty($child1->childs))
                                        echo '<option value="'.$child1->id.'" disabled="disabled">--'.$child1->name.'</option>';
                                    else{
                                        $selected = '';
                                        if($child1->id == $model->catalog_id)
                                            $selected = 'selected="selected"';
                                        echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                                    }
                                    $array[$child1->id] = '--'.$child1->name;
                                    if(isset($child1->childs)){
                                        foreach($child1->childs as $child2){
                                            if(!empty($child2->childs))
                                                echo '<option value="'.$child2->id.'">----'.$child2->name.'</option>';
                                            else {
                                                $selected = '';
                                                if ($child2->id == $model->catalog_id)
                                                    $selected = 'selected="selected"';
                                                echo '<option ' . $selected . ' value="' . $child2->id . '">----' . $child2->name . '</option>';
                                                $array[$child2->id] = '----' . $child2->name;
                                            }
                                            if(isset($child2->childs)){
                                                foreach($child2->childs as $child3) {
                                                    if (!empty($child3->childs))
                                                        echo '<option value="' . $child3->id . '">--------' . $child3->name . '</option>';
                                                    else {
                                                        $selected = '';
                                                        if ($child3->id == $model->catalog_id)
                                                            $selected = 'selected="selected"';
                                                        echo '<option ' . $selected . ' value="' . $child3->id . '">--------' . $child3->name . '</option>';
                                                        $array[$child3->id] = '----' . $child3->name;
                                                    }
                                                    if(isset($child3->childs)){
                                                        foreach($child3->childs as $child4) {
                                                            if (!empty($child4->childs))
                                                                echo '<option value="' . $child4->id . '">--------' . $child4->name . '</option>';
                                                            else {
                                                                $selected = '';
                                                                if ($child4->id == $model->catalog_id)
                                                                    $selected = 'selected="selected"';
                                                                echo '<option ' . $selected . ' value="' . $child4->id . '">--------' . $child4->name . '</option>';
                                                                $array[$child4->id] = '----' . $child4->name;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>

                    <div class="row">
                        <div class="col-md-4 col-xs-3" style="text-align: center">
                            <div style="margin-top:20px;">
                                <img src="/<?=$model->path?><?=$model->image?>" alt="" style="width:200px;">
                            </div>
                        </div>
                    </div>

                    <?php
                    echo $form->field($model, 'image')->widget(FileInput::classname(), [
                        'pluginOptions' => [
                            'showUpload' => false ,
                        ] ,
                        'options' => ['accept' => 'image/*'],
                    ]);
                    ?>

                    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
                            'options' => ['rows' => 6],
                            'allowedContent' => true,
                            'preset' => 'full',
                            'inline' => false
                        ]),
                    ]) ?>

                </div>

            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
