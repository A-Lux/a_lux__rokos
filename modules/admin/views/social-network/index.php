<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SocialNetworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Социальные сети';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-network-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'instagram:ntext',
            'youtube:ntext',

            ['class' => 'yii\grid\ActionColumn','template' => '{update} {view}'],
        ],
    ]); ?>
</div>
