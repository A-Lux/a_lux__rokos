<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'О компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="about-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'text',
                'value' => function($data){
                    return $data->text;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'text_en',
                'value' => function($data){
                    return $data->text_en;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'text_kz',
                'value' => function($data){
                    return $data->text_kz;
                }
            ],
        ],
    ]) ?>

</div>
