<?php

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "/public/dist/css/glide.core.css",
        "/public/dist/css/glide.theme.css",
        "/public/dist/css/normalize.css",
        "/public/dist/css/custom.css",
        "/public/dist/css/hc-offcanvas-nav.css",
        "/public/dist/css/main.css",
        "/public/dist/slick/slick.css",
        "/public/dist/slick/slick-theme.css",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css",
        "https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
    ];
    public $js = [
        "/public/dist/js/bundle.js",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js",
        "/public/dist/js/catalog.js",
        "/public/dist/js/career.js",
    ];
    public $depends = [

    ];
}