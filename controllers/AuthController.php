<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\RegistrationForm;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\IdentityInterface;

class AuthController extends FrontendController
{
    public $layout = "auth";

    public function actionRegister()
    {
//        if (!Yii::$app->user->isGuest) {
//            if (\Yii::$app->user->identity->role == User::ROLE_MANAGER) {
//                return $this->redirect('/admin/products/index');
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_OPERATOR) {
//                return $this->redirect('/admin/feedback/index');
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_CAREER) {
//                return $this->redirect("/admin/career/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_CONTACT) {
//                return $this->redirect("/admin/contact/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_LOGISTICS) {
//                return $this->redirect("/admin/logistics-advantage/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_MAIN) {
//                return $this->redirect("/admin/menu/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_MPK) {
//                return $this->redirect("/admin/mpk/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_NEWS) {
//                return $this->redirect("/admin/news/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_PORTFOLIO) {
//                return $this->redirect("/admin/products/index");
//            } elseif (\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
//                return $this->redirect("/admin/menu/index");
//            } else {
//                \Yii::$app->user->logout();
//                return $this->redirect('/auth/login');
//            }
//        }

        $model = new RegistrationForm();
        $newUser = new User();
        $loginForm = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $newUser->email = $model->email;
                $newUser->company_name = $model->company_name;
                $newUser->bin = $model->bin;
                $newUser->country = $model->country;
                $newUser->address = $model->address;
                $newUser->activity_type = $model->activity_type;
                $newUser->name = $model->name;
                $newUser->position = $model->position;
                $newUser->phone = $model->phone;
                $newUser->setPassword($model->password);
                $newUser->username = $model->email;
                $newUser->role = User::ROLE_DEFAULT;
                $newUser->save();
//                $loginForm->username = $newUser->username;
//                $loginForm->password = $model->password;
//                $loginForm->login();
                return $this->redirect("/profile");
            }
        }

        return Json::encode($model->errors);


//        return $this->renderAjax('registration');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            if (\Yii::$app->user->identity->role == User::ROLE_MANAGER) {
                return $this->redirect('/admin/products/index');
            } elseif (\Yii::$app->user->identity->role == User::ROLE_OPERATOR) {
                return $this->redirect('/admin/feedback/index');
            } elseif (\Yii::$app->user->identity->role == User::ROLE_CAREER) {
                return $this->redirect("/admin/career/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_CONTACT) {
                return $this->redirect("/admin/contact/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_LOGISTICS) {
                return $this->redirect("/admin/logistics-advantage/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_MAIN) {
                return $this->redirect("/admin/menu/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_MPK) {
                return $this->redirect("/admin/mpk/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_NEWS) {
                return $this->redirect("/admin/news/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_PORTFOLIO) {
                return $this->redirect("/admin/products/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                return $this->redirect("/admin/menu/index");
            } else {
                \Yii::$app->user->logout();
                return $this->redirect('/auth/login');
            }
        }


        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (\Yii::$app->user->identity->role == User::ROLE_MANAGER) {
                return $this->redirect('/admin/products/index');
            } elseif (\Yii::$app->user->identity->role == User::ROLE_OPERATOR) {
                return $this->redirect('/admin/feedback/index');
            } elseif (\Yii::$app->user->identity->role == User::ROLE_CAREER) {
                return $this->redirect("/admin/career/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_CONTACT) {
                return $this->redirect("/admin/contact/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_LOGISTICS) {
                return $this->redirect("/admin/logistics-advantage/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_MAIN) {
                return $this->redirect("/admin/menu/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_MPK) {
                return $this->redirect("/admin/mpk/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_NEWS) {
                return $this->redirect("/admin/news/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_PORTFOLIO) {
                return $this->redirect("/admin/products/index");
            } elseif (\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                return $this->redirect("/admin/menu/index");
            } else {
                \Yii::$app->user->logout();
                return $this->redirect('/auth/login');
            }
        }

//        return Json::encode($model->errors);
//        return $this->render('login2', [
//            'model' => $model,
//        ]);
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/auth/login');
    }


    public function actionUpdatePassword()
    {
        $admin = User::findOne(['id' => Yii::$app->user->identity->id]);
        $code = $admin->randomPassword;
        if (isset($_SESSION['confirmCode'])) {
            return $this->render('confirm-code');
        } else {
            $_SESSION['confirmCode'] = $code;
            if ($this->sendNewPassword($admin->username, $code)) {
                return $this->render('confirm-code');
            }
        }


    }

    public function actionConfirmCode($code)
    {
        if ($_SESSION['confirmCode'] == $code) {
            return $this->render('update-password', compact('code'));
        } else {
            Yii::$app->session->setFlash('code_error', 'неверный код!');
            return $this->render('confirm-code');
        }
    }

    public function actionNewPassword($code, $password, $confirmPassword)
    {
        if ($_SESSION['confirmCode'] == $code) {
            if ($password == $confirmPassword) {
                $admin = User::find()->where('role=1')->one();
                $admin->setPassword($password);
                if ($admin->save(false)) {
                    unset($_SESSION['confirmCode']);
                    Yii::$app->session->setFlash('set_password_success', 'Пароль успешно обновлен!');
                    $model = new LoginForm();
                    return $this->render('login', compact('model'));
                } else {
                    Yii::$app->session->setFlash('set_password_error', 'Упс, что-то пошло не так!');
                    return $this->redirect('/auth/confirm-code?code=' . $code);
                }

            } else {
                Yii::$app->session->setFlash('set_password_error', 'Пароли не совпадает!');
                return $this->redirect('/auth/confirm-code?code=' . $code);
            }
        } else {
            return $this->render('confirm-code');
        }
    }


}