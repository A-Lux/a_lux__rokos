<?php

namespace app\controllers;

use app\models\ProfileForm;
use app\models\PurchaseRequests;
use app\models\SecureForm;
use app\models\PurchaseRequestFiles;
use app\models\Purchases;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

class ProfileController extends FrontendController
{

    public function actionIndex()
    {
        if(\Yii::$app->user->isGuest){
            \Yii::$app->session->setFlash('Вы либо не зарегистрировались либо Ваш аккаунт не подтвержден!');
            return $this->redirect('/');
        }

        $userPurchases  = PurchaseRequests::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->orderBy(['id'=>SORT_DESC])
            ->all();

        return $this->render('index', compact('userPurchases'));
    }

    public function actionPurchase($purchase_id, $request_id)
    {
        $purchase = Purchases::find()->with('stageFirst.allFiles', 'stageSecond.allFiles')
            ->where(['id' => $purchase_id])
            ->orderBy(['id' => SORT_DESC])
            ->one();
        $purchaseRequest = new PurchaseRequests();
        $userPurchase   = PurchaseRequests::findOne([
            'id'            => $request_id,
            'purchase_id'   => $purchase_id,
            'user_id'       => Yii::$app->user->identity->id
        ]);

        $requestFiles       = PurchaseRequestFiles::findOne([
            'purchase_request_id' => $userPurchase->id
        ]);

        if(!is_null($requestFiles)){
            $files          = json_decode($requestFiles->content, true);
        }else{
            $files          = [];
        }

        return $this->render('purchase',
            compact('purchase', 'purchaseRequest',
                'userPurchase', 'files'));
    }

    public function actionUpdatePassword()
    {

        return $this->render('update-password');
    }

    public function actionUpdate()
    {
        $model  = new ProfileForm();

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->update()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message', 'Вы успешно обновили личные данные!')
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }


        return Json::encode($model->errors);
    }

    /**
     * Смена пароля
     * @return mixed
     */
    public function actionSecure()
    {
        $model = new SecureForm();

//        print_r("<pre>");
//        print_r($model->load(\Yii::$app->request->post()));die;

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->updatePassword()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message', 'Пароль изменён!')
                ];
                return json_encode($response);

            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }

}
