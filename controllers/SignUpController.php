<?php

namespace app\controllers;

use app\models\RegistrationForm;
use yii\db\Exception;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;

/**
 * SignUp controller
 */
class SignUpController extends FrontendController
{

    public function actionIndex()
    {
        $model  = new RegistrationForm();

//        print_r("<pre>");
//        print_r(json_encode(['message' => 1123213]));

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->signUp()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message', 'Благодарим Вас за предложение!')
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }


        return Json::encode($model->errors);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (\InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (\Yii::$app->user->login($user)) {
                \Yii::$app->session->setFlash('success', 'Ваш аккаунт был подтвержден!');
                return $this->goHome();
            }
        }

        \Yii::$app->session->setFlash('error', 'К сожалению, мы не можем подтвердить ваш аккаунт с помощью предоставленного токена.');
        return $this->goHome();
    }
}