<?php

namespace app\controllers;


use app\models\LoginFormFront;
use yii\helpers\Json;

/**
 * Login controller
 */
class LoginController extends FrontendController
{
    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionIndex()
    {
        $model  = new LoginFormFront();

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->login()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message', 'Вы успешно вошли в свой аккаунт')
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }

        return Json::encode($model->errors);
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }
}