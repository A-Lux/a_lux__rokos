<?php

namespace app\controllers;

use app\models\PurchaseRequestFiles;
use app\models\PurchaseRequests;
use app\models\Purchases;
use app\models\User;
use app\models\search\PurchaseSearch;
use frontend\models\RequestForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use PHPExcel;
use PHPExcel_Writer_Excel5;
use PHPExcel_Cell_DataType;
use PHPExcel_Writer_Excel2007;
use PHPExcel_Style_Alignment;

class PurchasesController extends FrontendController
{
    public function actionIndex()
    {
        $searchModel = new PurchaseSearch();
        $dataProvider = $searchModel->searchFront(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $purchase = Purchases::findOne(['id' => $id]);

        $userPurchase   = PurchaseRequests::findOne([
                                                        'purchase_id' => $id,
                                                        'user_id'       => Yii::$app->user->identity->id
                                                    ]);

        return $this->render('view', compact('purchase', 'userPurchase'));
    }

    public function actionDocumentations($id)
    {
        $purchase   = Purchases::findOne(['id' => $id]);

        return $this->render('documentations', [
            'purchase'      => $purchase,
        ]);
    }

    public function actionTechnicalTasks($id)
    {
        $purchase   = Purchases::findOne(['id' => $id]);

        return $this->render('technical-tasks', [
            'purchase'      => $purchase,
        ]);
    }

    public function actionAdmission($id)
    {
        $purchase = Purchases::find()->with('stageFirst.allFiles', 'stageSecond.allFiles')
                             ->where(['id' => $id])
                             ->orderBy(['id' => SORT_DESC])
                             ->one();
        $purchaseRequest = new PurchaseRequests();
        $userPurchase   = PurchaseRequests::findOne([
                                                        'purchase_id' => $id,
                                                        'user_id'       => Yii::$app->user->identity->id
                                                    ]);

        $requestFiles       = PurchaseRequestFiles::findOne([
                                                                'purchase_request_id' => $userPurchase->id
                                                            ]);

        if(!is_null($requestFiles)){
            $files          = json_decode($requestFiles->content, true);
        }else{
            $files          = [];
        }

        return $this->render('admission',
                             compact('purchase', 'purchaseRequest',
                                     'userPurchase', 'files'));
    }

    public function actionRequest()
    {
        $model  = new RequestForm();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->request()) {
                $response   = ['status' => 1,
                               'message' => \Yii::t('main-message', 'Благодарим Вас за предложение!')];
                return json_encode($response);
            } else {
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }

    public function actionExcel($id) {
		$purchase = Purchases
				::find()
				->with('requests')
				->where(['id' => $id])
				->one();

		$xls = new PHPExcel();

		$xls->setActiveSheetIndex(0);
		$sheet = $xls->getActiveSheet();
		$sheet->setTitle('Test');

		$sheet->mergeCells("A1:Q1");
		$sheet->mergeCells("A2:E2");
		$sheet->mergeCells("F2:Q2");
		$sheet->mergeCells("A3:E3");
		$sheet->mergeCells("F3:Q3");
		$sheet->mergeCells("A4:E4");
		$sheet->mergeCells("F4:Q4");
		$sheet->mergeCells("A5:E5");
		$sheet->mergeCells("F5:Q5");
		$sheet->mergeCells("A6:E6");
		$sheet->mergeCells("F6:Q6");

		$sheet->getRowDimension("1")->setRowHeight(60);
		$sheet->getStyle("A1")->getFont()->setSize(18);
		$sheet->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


        $sheet->getRowDimension("2")->setRowHeight(40);
        $sheet->getRowDimension("3")->setRowHeight(40);
        $sheet->getRowDimension("4")->setRowHeight(40);
        $sheet->getRowDimension("5")->setRowHeight(40);
        $sheet->getRowDimension("6")->setRowHeight(40);

        $sheet->getColumnDimension("A")->setWidth(15);
        $sheet->getColumnDimension("B")->setWidth(50);
        $sheet->getColumnDimension("C")->setWidth(15);
        $sheet->getColumnDimension("D")->setWidth(20);
        $sheet->getColumnDimension("E")->setWidth(20);
        $sheet->getColumnDimension("F")->setWidth(10);
        $sheet->getColumnDimension("G")->setWidth(15);
        $sheet->getColumnDimension("H")->setWidth(20);
        $sheet->getColumnDimension("I")->setWidth(40);
        $sheet->getColumnDimension("J")->setWidth(60);
        $sheet->getColumnDimension("K")->setWidth(50);
        $sheet->getColumnDimension("L")->setWidth(40);
        $sheet->getColumnDimension("M")->setWidth(20);
        $sheet->getColumnDimension("N")->setWidth(30);
        $sheet->getColumnDimension("O")->setWidth(40);
        $sheet->getColumnDimension("P")->setWidth(20);
        $sheet->getColumnDimension("Q")->setWidth(30);

		$sheet->setCellValueExplicit("A1", $purchase->name);
		$sheet->setCellValueExplicit("A2", 'Метод закупки: ' . $purchase->procurementMethodName);
		$sheet->setCellValueExplicit("F2", 'Наименование закупки: ' . $purchase->name);
		$sheet->setCellValueExplicit("A3", 'Этап закупки: '. "\n" . $purchase->purchaseStageName . "\n" . $purchase->stagesPurchaseDate);
		$sheet->setCellValueExplicit("F3", 'Ед. измерения: ' . $purchase->unitName);
        $sheet->setCellValueExplicit("A4", 'Заказчик: ' . $purchase->customerName);
		$sheet->setCellValueExplicit("F4", 'Условия поставки: ' . $purchase->delivery_condition);
        $sheet->setCellValueExplicit("A5", 'Валюта: ' . $purchase->currencyName);
		$sheet->setCellValueExplicit("F5", 'Количество: ' . $purchase->quantity);
        $sheet->setCellValueExplicit("A6", 'Срок поставки: ' . $purchase->delivery_time);
		$sheet->setCellValueExplicit("F6", 'Условие оплаты: ' . $purchase->payment_condition);

        $sheet->setCellValueExplicit('A8', 'Участник закупки');
        $sheet->setCellValueExplicit('B8', 'Наименование товара');
        $sheet->setCellValueExplicit('C8', 'НДС');
        $sheet->setCellValueExplicit('D8', 'Ед. измерения');
        $sheet->setCellValueExplicit('E8', 'Количество');
        $sheet->setCellValueExplicit('F8', 'Валюта');
        $sheet->setCellValueExplicit('G8', 'Стоимость с НДС');
        $sheet->setCellValueExplicit('H8', 'День поставки');
        $sheet->setCellValueExplicit('I8', 'Условия поставки');
        $sheet->setCellValueExplicit('J8', 'Условия оплаты');
        $sheet->setCellValueExplicit('K8', 'Описание');
        $sheet->setCellValueExplicit('L8', 'Срок поставки');
        $sheet->setCellValueExplicit('M8', 'Отправлена');
        $sheet->setCellValueExplicit('N8', 'Статус заявки');
        $sheet->setCellValueExplicit('O8', 'Метод закупки');
        $sheet->setCellValueExplicit('P8', 'Этап закупки');
        $sheet->setCellValueExplicit('Q8', 'Дата отправки');

        $i = 9;
        foreach($purchase->requests as $request) {
            $sheet->setCellValueExplicit('A' . $i, User::find()->where(['id' => $request->user_id])->one()->name);
            $sheet->setCellValueExplicit('B' . $i, $request->name_product);
            $sheet->setCellValueExplicit('C' . $i, $request->vat);
            $sheet->setCellValueExplicit('D' . $i, $request->getUnitName());
            $sheet->setCellValueExplicit('E' . $i, $request->quantity);
            $sheet->setCellValueExplicit('F' . $i, $request->currency);
            $sheet->setCellValueExplicit('G' . $i, $request->price);
            $sheet->setCellValueExplicit('H' . $i, $request->delivery_days);
            $sheet->setCellValueExplicit('I' . $i, $request->delivery_condition);
            $sheet->setCellValueExplicit('J' . $i, $request->payment_condition);
            $sheet->setCellValueExplicit('K' . $i, $request->description);
            $sheet->setCellValueExplicit('L' . $i, $request->delivery_time);
            $sheet->setCellValueExplicit('M' . $i, $request->sentDescription()[$request->status_sent]);
            $sheet->setCellValueExplicit('N' . $i, $request->approvedDescription()[$request->status_approved]);
            $sheet->setCellValueExplicit('O' . $i, $request->getProcurementMethodName());
            $sheet->setCellValueExplicit('P' . $i, $request->purchase_stage);
            $sheet->setCellValueExplicit('Q' . $i, $request->created_at);
            $i = $i + 1;
        }

		header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=file.xls");

		$objWriter = new PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
		exit;
    }
}
