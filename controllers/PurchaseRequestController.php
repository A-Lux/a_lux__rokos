<?php

namespace app\controllers;

use app\models\PurchaseRequestForm;
use yii\helpers\Json;
use yii\web\UploadedFile;
use Yii;

/**
 * Purchase request controller
 */
class PurchaseRequestController extends FrontendController
{
    /**
     * Sent purchase.
     * @return mixed
     */
    public function actionSentPurchase()
    {
      //   if (Yii::$app->request->get('request_id')) {
      //       $model  = PurchaseRequestForm::findOne([
      //          'id' => Yii::$app->request->get('request_id')
      //       ]);
      //   }
      //   else{
            $model  = new PurchaseRequestForm();
      //   }

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->sentPurchase()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message',
                        'Ваша заявка была отправлена. Для внесения изменений и отправки измененной заявки перейдите в личный кабинет в категорию'
                    )
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }

    /**
     * Save purchase.
     * @return mixed
     */
    public function actionSavePurchase()
    {
         if (Yii::$app->request->get('request_id')) {
             $model  = PurchaseRequestForm::findOne([
                'id' => Yii::$app->request->get('request_id')
            ]);
         }
         else {
            $model = new PurchaseRequestForm();
         }

        if ($model->load(\Yii::$app->request->post(), '')) {

            if ($request = $model->savePurchase()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message',
                        'Ваша заявка была сохранена. Для доработки и отправки заявки перейдите в личный кабинет в категорию')
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }

}
