<?php

namespace app\controllers;


use app\models\Banner;
use app\models\BannerMobile;
use app\models\Contact;
use app\models\Emailforrequest;
use app\models\Feedback;
use app\models\LoginForm;
use app\models\Logo;
use app\models\Menu;
use app\models\Partner;
use app\models\Products;
use app\models\RegistrationForm;
use app\models\SocialNetwork;
use app\models\Translation;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {
        $loginForm = new LoginForm();
        $registrationForm = new RegistrationForm();
        $this->setLang();
        $menu = Menu::find()->where('status=1')->orderBy('sort asc')->limit(7)->all();
        $banner = Banner::find()->orderBy('id ASC')->all();
        $partners = Partner::find()->all();
        $logo = Logo::find()->orderBy('id ASC')->one();
        $bannerMobile = BannerMobile::find()->orderBy('sort ASC')->all();
        $social = SocialNetwork::find()->orderBy('id ASC')->one();
        $contacts = Contact::findOne(['status' => 1]);
        $feedback = new Feedback();


        Yii::$app->view->params['headerMenu'] = $menu;
        Yii::$app->view->params['banner'] = $banner;
        Yii::$app->view->params['partners'] = $partners;
        Yii::$app->view->params['logo'] = $logo;
        Yii::$app->view->params['bannerMobile'] = $bannerMobile;
        Yii::$app->view->params['social'] = $social;
        Yii::$app->view->params['contacts'] = $contacts;
        Yii::$app->view->params['feedback'] = $feedback;
        Yii::$app->view->params['loginForm'] = $loginForm;
        Yii::$app->view->params['registrationForm'] = $registrationForm;

        parent::init();

    }


    protected function setMeta($title = null, $keywords = null, $description = null)
    {
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }

    public function get_category($catalogs)
    {
        $arr_cat = array();
        if (count($catalogs)) {
            //В цикле формируем массив
            foreach ($catalogs as $v) {
                //Формируем массив, где ключами являются адишники на родительские категории
                if (empty($arr_cat[$v['parent_id']])) {
                    $arr_cat[$v['parent_id']] = [];
                }
                $arr_cat[$v['parent_id']][] = $v;
            }
            //возвращаем массив
            return $arr_cat;
        }
    }

    public function get_level($result, $id)
    {
        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    if (!empty($result[$val2->id]))
                        foreach ($result[$val2->id] as $val3) {
                            if (!empty($result[$val3->id]))
                                foreach ($result[$val3->id] as $val4) {
                                    if (!empty($result[$val4->id]))
                                        foreach ($result[$val4->id] as $val5) {
                                            $arr[] = $val5->id;
                                        }
                                    $arr[] = $val4->id;
                                }
                            $arr[] = $val3->id;
                        }
                    $arr[] = $val2->id;
                }
            }
        }

        sort($arr);

        if ($arr)
            $arr = Products::find()->where('catalog_id in (' . implode(',', $arr) . ')')->orderBy('catalog_id ASC')->all();

        return $arr;
    }

    private function setLang()
    {
        if (Yii::$app->session['lang'] == '')
            Yii::$app->language = 'ru';
        elseif (Yii::$app->session['lang'] == '_kz')
            Yii::$app->language = 'kz';
        elseif (Yii::$app->session['lang'] == '_en')
            Yii::$app->language = 'en';
    }

    protected function getFilterCareer($city_id, $category_id)
    {
        $sql = '';

        if (!empty($category_id) && !empty($city_id)) {
            if (is_numeric($category_id) == false)
                $sql .= 'city_id = ' . $city_id;
            elseif (is_numeric($city_id) == false)
                $sql .= 'category_id = ' . $category_id;
            else
                $sql .= 'category_id = ' . $category_id . ' AND city_id = ' . $city_id;

            return $sql;
        } elseif (!empty($city_id) && empty($category_id)) {
            $sql .= 'city_id = ' . $city_id;

            return $sql;
        } elseif (empty($city_id) && !empty($category_id)) {
            $sql .= 'category_id = ' . $category_id;

            return $sql;
        }
    }

    protected function sendClientAll($contact, $email, $phone, $name, $address, $city)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 0])->all();
        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового клиента')
                    ->setHtmlBody(
                        "<p>Контактное лицо: $contact</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон клиента: $phone</p></br>
						 <p>Наименование торговой точки: $name</p></br>
						 <p>Адрес клиента: $address</p></br>
						 <p>Город: $city</p></br>");

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendClient($contact, $email, $phone, $name, $address, $city)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 2])->all();

        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового клиента')
                    ->setHtmlBody(
                        "<p>Контактное лицо: $contact</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон клиента: $phone</p></br>
						 <p>Наименование торговой точки: $name</p></br>
						 <p>Адрес клиента: $address</p></br>
						 <p>Город: $city</p></br>");

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendPartnerAll($file, $fileName)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 0])->all();
        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового партнера')
                    ->setHtmlBody(
                        "<p>Заявка о партнерстве</p></br>");

                $emailSend->attach($file, ['fileName' => $fileName]);

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendPartner($file, $fileName)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 3])->all();

        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового партнера')
                    ->setHtmlBody(
                        "<p>Заявка о партнерстве</p></br>");
                $emailSend->attach($file, ['fileName' => $fileName]);

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendFeedbackAll($topic, $fio, $email, $phone, $content, $city)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 0])->all();
        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового клиента')
                    ->setHtmlBody(
                        "<p>Тема обращения: $topic</p></br>
						 <p>ФИО: $fio</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон: $phone</p></br>
						 <p>Сообщение: $content</p></br>
						 <p>Город: $city</p></br>");

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendFeedback($topic, $fio, $email, $phone, $content, $city)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 4])->all();

        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Поступила заявка от нового клиента')
                    ->setHtmlBody(
                        "<p>Тема обращения: $topic</p></br>
						 <p>ФИО: $fio</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон: $phone</p></br>
						 <p>Сообщение: $content</p></br>
						 <p>Город: $city</p></br>");

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendVacancyAll($contact_person, $position, $email, $phone, $city, $comment, $href, $file, $fileName)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 0])->all();
        $comment = !empty($comment) ? "<p>Сообщение: $comment</p></br>" : '';
        $href = !empty($comment) ? "<p>Ссылка на резюме: $href</p></br>" : '';

        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Отклик на вакансию')
                    ->setHtmlBody(
                        "<p>ФИО: $contact_person</p></br>
						 <p>Должность: $position</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон: $phone</p></br>
						 $comment
						 $href
						 <p>Город: $city</p></br>");

                $emailSend->attach($file, ['fileName' => $fileName]);

                $emailSend->send();
            }
            return 1;
        }

    }

    protected function sendVacancy($contact_person, $position, $email, $phone, $city, $comment, $href, $file, $fileName)
    {
        $adminEmail = Emailforrequest::find()->where(['type' => 2])->all();

        $comment = !empty($comment) ? "<p>Сообщение: $comment</p></br>" : '';
        $href = !empty($comment) ? "<p>Ссылка на резюме: $href</p></br>" : '';

        if (!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => '«Rokos Group Global»'])
                    ->setTo($item->email)
                    ->setSubject('Отклик на вакансию')
                    ->setHtmlBody(
                        "<p>ФИО: $contact_person</p></br>
						 <p>Должность: $position</p></br>
						 <p>E-mail: $email</p></br>
						 <p>Телефон: $phone</p></br>
						 $comment
						 $href
						 <p>Город: $city</p></br>");

                $emailSend->attach($file, ['fileName' => $fileName]);

                $emailSend->send();
            }
            return 1;
        }
    }

}