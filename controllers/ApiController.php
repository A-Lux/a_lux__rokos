<?php

namespace app\controllers;

use app\models\BannerMobile;
use Yii;
use yii\rest\Controller;

class ApiController extends Controller
{
    public function actionBanner()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model  = BannerMobile::find()->orderBy('sort ASC')->asArray()->all();
        $text   = "text" . Yii::$app->session["lang"];

        foreach ($model as $key => $item){

            if(!empty($model[$key]['image'])) {
                $model[$key]['image'] = $model[$key]['image'];
                $model[$key]['text'] = $model[$key][$text];
            }

            unset($model[$key]['text_en']);
            unset($model[$key]['text_kz']);
            unset($model[$key]['sort']);

        }

        $banner = json_encode($model);

        return $banner;
    }
}