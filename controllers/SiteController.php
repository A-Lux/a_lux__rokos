<?php

namespace app\controllers;


use app\models\About;
use app\models\Advantage;
use app\models\Banner;
use app\models\CareerFeedback;
use app\models\Catalog;
use app\models\City;
use app\models\ClientFeedback;
use app\models\Contact;
use app\models\ContactType;
use app\models\Emailforrequest;
use app\models\Faq;
use app\models\Feedback;
use app\models\Menu;
use app\models\News;
use app\models\Partner;
use app\models\PartnerFeedback;
use app\models\Service;

use app\models\Vacancy;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $model      = Menu::find()->where('url = "/"')->one();
        $this->setMeta($model->getMetaName(), $model->getMetaKey(), $model->getMetaDesc());

        $advantage  = Advantage::find()->all();
        $about      = About::find()->all();
        $partners   = Partner::find()->where(['status' => 0])->all();

        $clientFeedback     = new ClientFeedback();
        $partnerFeedback    = new PartnerFeedback();

        return $this->render('index',
            compact('advantage',
                            'about', 'partners', 'clientFeedback', 'partnerFeedback'));
    }

    public function actionContact()
    {
        $model = Menu::find()->where('url = "/site/contact"')->one();
        $this->setMeta($model->getMetaName(), $model->getMetaKey(), $model->getMetaDesc());

        $contact    = Contact::find()->all();
        $branches   = Contact::find()->where(['statusCity' => 0])->all();

        return $this->render('contact', compact('model', 'contact', 'branches'));
    }



    public function actionClientFeedback()
    {
        $model = new ClientFeedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendClientAll($model->contact_person, $model->email, $model->telephone, $model->name, $model->address, $model->city->name);
            $this->sendClient($model->contact_person, $model->email, $model->telephone, $model->name, $model->address, $model->city->name);
            \Yii::$app->session->setFlash('successFeedback', 'Заявка принята');
            return $this->redirect(['index']);
        }
    }

    public function actionPartnerFeedback()
    {
        $model = new PartnerFeedback();


        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');

            if($file != null) {
                $time = time();
                $file->saveAs($model->path . $time . '_' . $file->baseName . '.' . $file->extension);
                $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
            }

            $filePath = $model->path . $time . '_' . $file->baseName . '.' . $file->extension;
            $fileName = $file->baseName . '.' . $file->extension;

            if ($model->save()) {
                $this->sendPartnerAll($filePath, $fileName);
                $this->sendPartner($filePath, $fileName);
                \Yii::$app->session->setFlash('successFeedback', 'Заявка принята');
                return $this->redirect(['index']);
            }
        }
    }

    public function actionCareerFeedback()
    {
        $model = new CareerFeedback();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');

            if($file != null) {
                $time = time();
                $file->saveAs($model->path . $time . '_' . $file->baseName . '.' . $file->extension);
                $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
            }

            $filePath = $model->path . $time . '_' . $file->baseName . '.' . $file->extension;
            $fileName = $file->baseName . '.' . $file->extension;

            if ($model->save()) {

                $this->sendVacancyAll($model->contact_person, $model->position->name, $model->email, $model->telephone,
                    $model->city->name, $model->comment, $model->href, $filePath, $fileName);
                $this->sendVacancy($model->contact_person, $model->position->name, $model->email, $model->telephone,
                    $model->city->name, $model->comment, $model->href, $filePath, $fileName);
                \Yii::$app->session->setFlash('successFeedback', 'Заявка принята');
                return $this->redirect(['index']);
            }
        }
    }

    public function actionFooterFeedback()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->post()['reCaptcha'] == null){
                \Yii::$app->session->setFlash('errorFeedback', 'Пожалуйста, подтвердите, что вы не бот');
            }
            $model->isRead = 0;
            if ($model->save()) {
                $this->sendFeedbackAll($model->topic, $model->fio, $model->email, $model->telephone, $model->content, $model->city->name);
                $this->sendFeedback($model->topic, $model->fio, $model->email, $model->telephone, $model->content, $model->city->name);
                \Yii::$app->session->setFlash('successFeedback', 'Заявка принята');
                return $this->redirect(['index']);
            }
        }
    }

    public function actionTest()
    {
        $feedback = Feedback::findOne(['id' => 11]);

        $ret = $feedback->city->name;


        var_dump($ret);die;
    }


}
