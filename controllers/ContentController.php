<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 15:26
 */

namespace app\controllers;


use app\models\Brands;
use app\models\CareerFeedback;
use app\models\Catalog;
use app\models\CategoryVacancy;
use app\models\City;
use app\models\HallOfFame;
use app\models\Images;
use app\models\LogisticsAdvantage;
use app\models\LogisticsCustomer;
use app\models\LogisticsCustomerBase;
use app\models\LogisticsGeography;
use app\models\LogisticsHeader;
use app\models\LogisticsImages;
use app\models\Menu;
use app\models\Mpk;
use app\models\MpkAdvantage;
use app\models\MpkCatalog;
use app\models\MpkPrinciples;
use app\models\News;
use app\models\Partner;
use app\models\ProductPromo;
use app\models\Vacancy;
use app\models\VacancyConditions;
use app\models\VacancyDescription;
use app\models\VacancyRequirements;
use app\models\VacancyResponsibilities;
use app\models\VacancyResume;
use app\models\Products;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ContentController extends FrontendController
{
    public $layout = "main";

    public static function cutStr($str, $length=50, $postfix=' ...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }

    public function actionPortfolio()
    {
        $model = Menu::find()->where('url = "/content/portfolio"')->one();
        $this->setMeta($model->getMetaName(), $model->getMetaKey(), $model->getMetaDesc());

        $catalog    = Catalog::find()->where(['level' => 1])->orderBy('id DESC')->all();
        $category   = Catalog::find()->where(['parent_id' => 2])->all();

        $products   = $this->getProductsPortfolio();

        return $this->render('portfolio2', compact('catalog', 'products', 'category'));
    }

    public function actionCareer()
    {
        $model = Menu::find()->where('url = "/content/career"')->one();
        $this->setMeta($model->getMetaName(), $model->getMetaKey(), $model->getMetaDesc());

        $careerFeedback     = new CareerFeedback();
        $category           = CategoryVacancy::find()->all();
        $vacancy            = Vacancy::find()->orderBy('id DESC')->all();
        $hallOfFame         = HallOfFame::find()->all();
        $city               = City::find()->all();

        return $this->render('career',
            compact('model', 'careerFeedback', 'vacancy', 'hallOfFame', 'category', 'city'));
    }

    public function actionVacancy()
    {
        $city_id        = $_GET['city_id'];
        $category_id    = $_GET['category_id'];

        $sql = $this->getFilterCareer($city_id, $category_id);

        $vacancy    = Vacancy::find()->where($sql)->orderBy('id DESC')->all();

        return $this->renderAjax('vacancy',compact('vacancy'));
    }

    public function actionSalesman($id)
    {
        $vacancy = Vacancy::findOne(['id' => $id]);
        $vacancyDescriptions        = VacancyDescription::find()->where(['vacancy_id' => $vacancy->id])->all();
        $vacancyResponsibilities    = VacancyResponsibilities::find()->where(['vacancy_id' => $vacancy->id])->all();
        $vacancyRequirements        = VacancyRequirements::find()->where(['vacancy_id' => $vacancy->id])->all();
        $vacancyConditions          = VacancyConditions::find()->where(['vacancy_id' => $vacancy->id])->all();

        $vacancyPosition    = $vacancy->id;


        return $this->render('salesman',
            compact('vacancy', 'vacancyDescriptions', 'vacancyResponsibilities',
                            'vacancyRequirements', 'vacancyConditions', 'vacancyPosition'));
    }

    public function actionNewsCard($id)
    {
        $news = News::findOne(['id' => $id]);

        return $this->render('news-card', compact('news'));
    }

    public function actionNews()
    {
        $model = Menu::find()->where('url = "/content/news"')->one();
        $this->setMeta($model->getMetaName(), $model->getMetaKey(), $model->getMetaDesc());

        $news   = News::find()->orderBy('created_at DESC')->all();

        return $this->render('news', compact('model', 'news'));
    }

    public function actionLogistics()
    {
        $menu = Menu::find()->where('url = "/content/logistics"')->one();
        $this->setMeta($menu->getMetaName(), $menu->getMetaKey(), $menu->getMetaDesc());

        $header         = LogisticsHeader::find()->orderBy('id ASC')->one();
        $advantage      = LogisticsAdvantage::find()->all();
        $geography      = LogisticsGeography::find()->all();
        $customer       = LogisticsCustomer::find()->orderBy('id ASC')->all();
        $customerBase   = LogisticsCustomerBase::find()->orderBy('id ASC')->one();
        $images         = LogisticsImages::find()->all();
        $partners       = Partner::find()->where(['status' => 1])->all();

        return $this->render('logistics', compact('menu', 'partners',
                            'header', 'advantage', 'geography', 'customer', 'customerBase', 'images'));
    }

    public function actionMpk()
    {
        $menu       = Menu::find()->where('url = "/content/mpk"')->one();
        $this->setMeta($menu->getMetaName(), $menu->getMetaKey(), $menu->getMetaDesc());

        $mpk        = Mpk::find()->orderBy('id ASC')->one();
        $advantage  = MpkAdvantage::find()->all();
        $principles = MpkPrinciples::find()->all();
        $images     = Images::find()->where(['menu_id' => $menu->id])->all();
        $promo      = ProductPromo::find()->all();
        $catalog    = MpkCatalog::find()->orderBy('id ASC')->one();

        return $this->render('mpk', compact('menu',
                    'mpk', 'advantage', 'principles', 'images', 'promo', 'catalog'));
    }

    public function actionVacancyResume()
    {
        $model = new CareerFeedback();

        if ($model->load(\Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');

            if($file != null) {
                $time = time();
                $file->saveAs($model->path . $time . '_' . $file->baseName . '.' . $file->extension);
                $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
            }

            $filePath = $model->path . $time . '_' . $file->baseName . '.' . $file->extension;
            $fileName = $file->baseName . '.' . $file->extension;

            if ($model->save()) {
                $this->sendVacancyAll($model->contact_person, $model->position->name, $model->email, $model->telephone,
                    $model->city->name, $model->comment, $model->href, $filePath, $fileName);
                $this->sendVacancy($model->contact_person, $model->position->name, $model->email, $model->telephone,
                    $model->city->name, $model->comment, $model->href, $filePath, $fileName);

                \Yii::$app->session->setFlash('successFeedback', 'Заявка принята');
                return $this->redirect(['career']);
            }
        }
    }

    public function getProductsPortfolio()
    {
        $catalog = Catalog::find()->all();

        $result = $this->get_category($catalog);

        $products = $this->get_level($result, 2);

        return $products;
    }

    public function actionProducts()
    {
        $id = $_GET['id'];

        $catalog = Catalog::find()->all();

        if(!$catalog)
            throw new NotFoundHttpException();

        $result =$this->get_category($catalog);

        $products = $this->get_level($result, $id);

        return $this->renderAjax('products',compact('products', 'category_id'));
    }

    public function actionCategories()
    {
        $id = $_GET['id'];

        $category = Catalog::find()->where(['parent_id' => $id])->all();

        return $this->renderAjax('category',compact('category'));
    }

    public function actionSearch()
    {
        $text = $_GET['search'];

        $search = $text;
        if($text){
            $products = Products::find()->where("name LIKE '%$text%'")->all();
            $count = count($products);
        }else{
            $count = 0;
            return $this->renderAjax('search', compact('search', 'count', 'text'));
        }

        return $this->renderAjax('search', compact('products','count','search'));
    }

    protected function lastCatalog($catalog) {
        if(!empty($catalog->parent)) {
            return $this->lastCatalog($catalog->parent);
        } else {
            return $catalog->name;
        }
    }
}