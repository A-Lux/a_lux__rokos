<?php

namespace app\controllers;


use app\models\About;
use app\models\Advantage;
use app\models\Catalog;
use app\models\Contact;
use app\models\Faq;
use app\models\LogisticsAdvantage;
use app\models\LogisticsCustomerBase;
use app\models\LogisticsHeader;
use app\models\Menu;
use app\models\Mpk;
use app\models\MpkAdvantage;
use app\models\MpkPrinciples;
use app\models\News;
use app\models\Partner;
use app\models\Product;
use app\models\Products;
use app\models\Service;
use app\models\Vacancy;
use Yii;
use yii\web\Controller;

class SearchController extends FrontendController
{

    public function actionIndex($text)
    {
        $this->setMeta('Поиск', "Поиск", "Поиск");
        $search = $text;
		if($text){
			$about      = About::find()->where("text LIKE '%$text%'")->all();
			$advantage  = Advantage::find()->where("title LIKE '%$text%' OR subtitle LIKE '%$text%'")->all();
			$products   = Products::find()->where(
			                "name LIKE '%$text%' OR fullname LIKE '%$text%'
			                        OR manufacturer LIKE '%$text%' OR content LIKE '%$text%'
			                        ")->all();
			$news       = News::find()->where("name LIKE '%$text%' OR content LIKE '%$text%'")->all();
			$contact    = Contact::find()->where("
			                        name LIKE '%$text%' OR address LIKE '%$text%' 
			                        OR telephone LIKE '%$text%' OR email LIKE '%$text%'
			                        ")->all();
			$career     = Vacancy::find()->where("name LIKE '%$text%' OR content LIKE '%$text%'")->all();
			$mpk        = Mpk::find()->where("title LIKE '%$text%' OR description LIKE '%$text%'")->one();
			$mission    = Mpk::find()->where("mission_title LIKE '%$text%' OR 
			                                            mission_content LIKE '%$text%'")->one();
			$principles = MpkPrinciples::find()->where("name LIKE '%$text%'")->all();

			$logistics_header    = LogisticsHeader::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->one();
			$logistics_advantage = LogisticsAdvantage::find()->where("title LIKE '%$text%' OR subtitle LIKE '%$text%'")->all();
			$logistics_customer  = LogisticsCustomerBase::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();

			$count      = count($about) + count($about) + count($products)  + count($news)  + count($contact)
                        + count($career) + count($mpk) + count($mission) + count($principles)
                        + count($logistics_header) + count($logistics_advantage) + count($logistics_customer);
        }else{
		    $count = 0;
			return $this->render('index', compact('search', 'count', 'text'));
		}

        return $this->render('index', compact('about','advantage','news','products','contact',
                                            'career','mpk','mission','principles','count','search',
                                            'logistics_header', 'logistics_advantage', 'logistics_customer'));
    }

    public static function cutStr($str, $length=50, $postfix='...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }

    public function actionTest()
    {
        $career = Vacancy::find()->where(['id' => 2])->with('requirements. conditions')->all();

        var_dump($career);
    }
}