<?php

namespace app\controllers;

use app\models\ResetPasswordForm;
use app\models\ResetPasswordUpdateForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class ResetPasswordController extends FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionForgotPassword()
    {
        $model  = new ResetPasswordForm();

        if ($model->load(\Yii::$app->request->post(), '')) {
            if ($model->resetPassword()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message',
                        'На вашу почту было отправлено сообщение с ссылкой на изменение вашего пароля')
                ];
                return json_encode($response);
            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }


        return Json::encode($model->errors);
    }

    public function actionUpdatePassword($token)
    {
        $user   = User::findOne(['password_reset_token' => $token]);

        return $this->render('update-password', [
            'user'      => $user,
            'token'     => $token
        ]);
    }

    /**
     * Смена пароля
     * @return mixed
     */
    public function actionSecure()
    {
        $model = new ResetPasswordUpdateForm();

        if ($model->load(\Yii::$app->request->post(), '')) {

            if ($user = $model->updatePassword()) {
                $response   = [
                    'status'    => 1,
                    'message'   => \Yii::t('main-message', 'Пароль изменён!')
                ];
                return json_encode($response);

            } else {
                $message = '';
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = [
                    'status' => 0,
                    'message' => $message
                ];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }

        return Json::encode($model->errors);
    }

}
